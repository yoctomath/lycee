% \usepackage[suite]{tdsfrmath}
% \usepackage{customcommands}
\chapter{Suites géométriques}

\section{Définitions}
\subsection{Principe}
Une suite géométrique est une suite infinie de nombres avec un \textbf{facteur} constant entre chacun d'eux.
\begin{center}
  \begin{tikzpicture}[fleche/.style={-latex',black!40},comment/.style={midway,above,colorSec,font=\scriptsize}]
    \pgfmathsetlengthmacro{\TermSep}{25}
    \node (u0) at (0,0) {$u_0$};
    \foreach \i [remember=\i as \lasti (initially 0)] in {1,...,4}{%
        \node[right=\TermSep of u\lasti] (u\i) {$u_\i$};
        \draw [fleche] (u\lasti) to[out=30,in=150] node[comment] {$\times q$} (u\i);
      }
    \node[right=6*\TermSep of u4] (un) {$u_n$};
    \node[right=\TermSep of un] (unp1) {$u_{n+1}$};
    \node[right=\TermSep of u4] (u5) {\phantom{$u_5$}};
    \node[left=\TermSep of un] (unm1) {\phantom{$u_5$}};
    \node[right=\TermSep of unp1] (unp2) {$\ldots$};
    \draw [fleche] (u4) to[out=30,in=150] node[comment] {$\times q$} (u5);
    \draw [fleche] (unm1) to[out=30,in=150] node[comment] {$\times q$} (un);
    \draw [fleche] (un) to[out=30,in=150] node[comment] {$\times q$} (unp1);
    \draw [fleche] (unp1) to[out=30,in=150] node[comment] {$\times q$} (unp2);
    \draw [fleche,looseness=0.2] (u0) to[out=-30,in=210] node[comment,below] {$\times q^n$} (un);
    \draw [dotted,very thick] (u5) -- (unm1);
  \end{tikzpicture}
\end{center}


\begin{exemple}
  Dans un système de compte bancaire d'épargne rémunéré à $3\,\%$, les intérêts sont cumulés d'une année sur l'autre.
  Le capital déposé augmente donc de $3\,\%$ chaque année.

  Si le montant du dépôt initial est de $M_0$ et qu'il n'y ait plus d'apport alors,
  l'année suivante, le capital sera $M_0\times 1,03$, l'année d'après $M_0\times1,03\times1,03$, etc jusqu'à $n$ années après le dépôt initial
  où le capital sera $M_0\times 1,03^n$.
  \medskip

  Le capital sur ce compte bancaire suit donc une suite géométrique car il est multiplié chaque année par $1,03$.
\end{exemple}

\subsection{Définition par récurrence}
En utilisisant le principe précédent, on peut définir les termes de la suite géométrique en fonction du terme précédent.
\begin{definition}[Définition par récurrence d'une suite géométrique]\index{Suites!Géométriques!Définition par récurrence}
  Une suite \suite est dite \textbf{géométrique} s'il existe un réel $q$ tel que, pour tout entier naturel $n$, on a \[u_{n+1}=u_n \times q\]
  Le nombre $q$ est appelé la \textbf{raison} de la suite et $u_0$ le \textbf{premier terme} (ou terme \textbf{initial}).
\end{definition}

\begin{exemple}
  La suite définie ci-dessous est une suite géométrique de premier terme $2$ et de raison $7$.
  \[\begin{cases}
      u_0 = 2 \\
      u_{n+1} = u_n \times 7
    \end{cases}\]
\end{exemple}

\subsection{Définition explicite}

\begin{propriete}[Définition explicite d'une suite géométrique]\label{TSTL:cours:suitesgeometriques:prop:explicite}\index{Suites!Géométriques!Définition explicite}
  Soit \suite une suite géométrique de raison $q$ et de premier terme $u_0$. Pour tout entier naturel $n$, on a \[u_n=u_0 \times q^n\]
  De même, si $u_1$ est le premier terme de la suite, alors on a \[u_n=u_1 \times q^{n-1}\]
\end{propriete}

\begin{exemple}
  Soit \suitar[w]{3}{5}. Pour tout $n \in \N$, on a $w_n=5+3n$.
\end{exemple}

\begin{remarque}
  Grâce à la définition explicite d'une suite géométrique, on a pas besoin de connaître les termes précédent $u_n$ pour le calculer.
\end{remarque}

\begin{methodetwocols}[Calculer un terme d'une suite géométrique]

  \textbf{Énoncé}\quad Soit \suitgeo{2}{5}. Calculer $u_{8}$.\bigskip

  \textbf{Solution}\quad D'après la définition explicite \ref{TSTL:cours:suitesgeometriques:prop:explicite} d'une suite géométrique, on peut dire que
  \columnbreak
  \begin{align*}
    u_{8} & = u_0 \times q^8 \\
    u_{8} & = 5 \times 2^8   \\
    u_{8} & = 5 \times 256   \\
    u_{8} & = \num{1280}     \\
  \end{align*}
\end{methodetwocols}


\begin{methode}[Déterminer si les termes d'une suite sont ceux d'une suite géométrique]\label{TSTL::cours:suitesgeometriques:meth:sommepremierstermes}
  \textbf{Énoncé}\quad Les nombres  des séries suivantes peuvent-ils être des termes consécutifs d'une suite géométrique~?

  \begin{quote}
    \begin{description}
      \item[\color{colorTer}Serie A]\qquad
            \begin{tblr}{width=0.5\linewidth,colspec={*{4}{X[l,1]}},rows={mode=math}}
              5 & 8 & 12,8 & 20,48 \\
            \end{tblr}
      \item[\color{colorTer}Serie B]\qquad
            \begin{tblr}{width=0.5\linewidth,colspec={*{4}{X[l,1]}},rows={mode=math}}
              3 & 6 & 7,2 & 10,8
            \end{tblr}
    \end{description}
  \end{quote}
  \bigskip

  \textbf{Solution}
  \begin{description}
    \item[\color{colorTer}Série A] Posons $u_0=5$, $u_1=8$, $u_2=12,8$ et $u_3=20,48$.
          \begin{align*}
            \frac{u_1}{u_0} & = \frac{8}{5} & \frac{u_2}{u_1} & = \frac{12,8}{8} & \frac{u_3}{u_2} & = \frac{20,48}{12,8} \\
                            & = 1,6         &                 & = 1,6            &                 & = 1,6                \\
          \end{align*}
          On remarque que nous passons d'un terme à l'autre en multipliant par le même facteur $1,6$ donc ces nombres
          peuvent être les termes consécutifs d'une suite géométrique de raison $q=1,6$ et de terme initial $5$.
    \item[\color{colorTer}Série B] Posons $v_0=5$, $v_1=6$, $v_2=7,2$ et $v_3=10,8$.
          \begin{align*}
            \frac{v_1}{v_0} & = \frac{6}{5} & \frac{v_2}{v_1} & = \frac{7,2}{6} & \frac{v_3}{v_2} & = \frac{10,8}{7,2} \\
                            & = 1,2         &                 & = 1,2           &                 & = 1,5              \\
          \end{align*}
          On remarque que nous passons d'un terme à l'autre en multipliant par le même facteur $1,2$
          sauf pour passer de $v_2$ à $v_3$ où il faut multiplier par $1,5$.

          Donc ces nombres ne peuvent pas être les termes d'une suite géométrique.
  \end{description}

\end{methode}

\section{Variations}

\begin{propriete}
  Soit $(u_n)_{n\in\N}$ une suite géométrique de raison $q$ et de terme initial $u_0$.


  \begin{multicols}{2}
    Cas où $u_0>0$.
    \begin{itemize}
      \item Si $q>1$, alors la suite est \textbf{croissante}.
      \item Si $0<q<1$, alors la suite est \textbf{décroissante}.
      \item Si $q=1$, alors la suite est \textbf{constante}.
    \end{itemize}

    Cas où $u_0<0$.
    \begin{itemize}
      \item Si $q>1$, alors la suite est \textbf{décroissante}.
      \item Si $0<q<1$, alors la suite est \textbf{croissante}.
      \item Si $q=1$, alors la suite est \textbf{constante}.
    \end{itemize}
  \end{multicols}
\end{propriete}

\begin{exemple}
  Soit les suites \suitgeo[v]{0,95}{\num{1200}} et \suitgeo[w]{1,13}{\num{900}}.
  \begin{itemize}
    \item Pour la suite \suite[v], la raison est $0,95<1$ et le premier terme est $\num{1200}>0$.

          \suite[v] est donc \textit{décroissante}.
    \item Pour la suite \suite[w], la raison est $1,13>1$ et le premier terme est $\num{900}>0$.

          \suite[w] est \textit{croissante}.
  \end{itemize}
\end{exemple}

\section{Moyenne géométrique}

\begin{definition}\index{Suites!Géométriques!Moyenne géométrique}\index{Moyenne!Géométrique}
  Soit deux nombres réels positifs $a$ et $b$.

  On appelle \textbf{moyenne géométrique} de $a$ et $b$ le nombre \[\sqrt{ab}\]
\end{definition}

\begin{propriete}\label{TSTL:cours:suitesgeometriques:prop:caracterisationmoyenne}
  Trois nombres positifs $a$, $b$ et $c$ sont des termes consécutifs d'une suite géométrique
  lorsque la moyenne géométrique du plus grand et du plus petit est égale au troisième nombre,
  autrement dit : \[\sqrt{ac}=b\]
\end{propriete}

\begin{exemple}
  Les nombres $2.5$ ; $3$ et $3,6$ sont tels que :
  \begin{align*}
    \sqrt{2,5\times3,6} &= \sqrt{9}\\
    &=3
  \end{align*}
  D'après la propriété \ref{TSTL:cours:suitesgeometriques:prop:caracterisationmoyenne} précédente,
  on peut affirmer que ces trois nombres sont des termes consécutifs d'une suite géométrique.
\end{exemple}

\section{Somme des premiers termes}

\begin{propriete}[Somme des premiers termes]\label{TSTL:cours:suitesgeometriques:prop:somme}\index{Suites!Géométriques!Somme premiers termes}
  Soit \suite une suite géométrique de raison $q$ ($q\neq 1$). Pour tout $n \in \N$, on a\footnotemark :
  \[\sum_{k=0}^n u_k=u_0\times\frac{1-q^{n+1}}{1-q}\]
\end{propriete}
\footnotetext{$\sum\limits_{k=0}^n u_k$ est une notation pour $u_0+u_1+\ldots+u_n$.}

\begin{remarque}
  On peut retenir cette formule en remarquant que cette somme des premiers termes d'une suite géométrique peut se décrire en français comme~:
  \[\text{premier terme}\times\frac{1-\text{raison}^{\text{nombre de termes}}}{1 - \text{raison}}\]
\end{remarque}

\paragraph*{\faCogs\quad Preuve} Soit $n \in \N$. Appelons $S$ la somme des termes d'une suite \suite jusqu'au rang $n$.

D'après la définition explicite \ref{TSTL:cours:suitesgeometriques:prop:explicite} d'une suite géométrique, on peut affirmer que $u_n=u_0\times q^n$.

On a donc :
\begin{align*}
  S          & = u_0 + u_1 + u_2 + u_3 + \ldots + u_n                                                                           &  &                                                           \\
  S          & = u_0 \times q^0 + u_0 \times q^1 + u_0 \times q^2 + \ldots + u_0 \times q^n                                     &  & \longleftarrow \text{on applique la définition explicite} \\
  S \times q & = \left(u_0 \times q^0 + u_0 \times q^1 + u_0 \times q^2 + \ldots + u_0 \times q^n\right)\times q                &  & \longleftarrow \text{on multiplie chaque membre par }q    \\
  S \times q & = u_0 \times q^0 \times q + u_0 \times q^1 \times q + u_0 \times q^2 \times q + \ldots + u_0 \times q^n \times q &  & \longleftarrow \text{on développe}                        \\
  S \times q & = u_0 \times q^1 + u_0 \times q^2 + u_0 \times q^3 + \ldots + u_0 \times q^{n+1}                                 &  & \longleftarrow \text{on réduit}                           \\
\end{align*}

On va à présent soustraire cette expressions à la première :
{\footnotesize\begin{align*}
  S - S \times q & =  u_0 \times q^0 + u_0 \times q^1 + u_0 \times q^2 + \ldots + u_0 \times q^n - \left(u_0 \times q^1 + u_0 \times q^2 + u_0 \times q^3 + \ldots + u_0 \times q^{n+1}\right)                  \\
  S - S \times q & = u_0 \times q^0 + u_0 \times q^1 + u_0 \times q^2 + \ldots + u_0 \times q^n -u_0 \times q^1 - u_0 \times q^2 - u_0 \times q^3 - \ldots - u_0 \times q^{n+1}                                 \\
  S - S \times q & = u_0 \times q^0 + \left(u_0 \times q^1 -u_0 \times q^1\right) + \left(u_0 \times q^2  - u_0 \times q^2+\right) + \ldots + \left(u_0 \times q^n- u_0 \times q^n \right) - u_0 \times q^{n+1} \\
  S - S \times q & = u_0 \times q^0 - u_0 \times q^{n+1}                                                                                                                                                        \\
  S\times(1 - q) & = u_0 \times (q^0 - q^{n+1})                                                                                                                                                                 \\
  S              & = u_0\times\frac{1-q^{n+1}}{1-q}
\end{align*}}

\begin{methode}[Calculer la somme des premiers termes d'une suite géométrique]\label{TSTL:cours:suitesgeometriques:meth:sommepremierstermes}
  \textbf{Énoncé}\quad On donne \suitgeo[v]{1,05}{\num{1200}}.

  Calculer la somme des $12$ premiers termes\bigskip

  \textbf{Solution}\quad On a $v_0=\num{1200}$ et $q=1,05$.
  Soit $\mathcal{S}$ la somme des $12$ premiers termes de cette suite.

  D'après la propriété \ref{TSTL:cours:suitesgeometriques:prop:somme},
  on peut dire que~:
  \begin{align*}
    \mathcal{S} & = \num{1200}\times\frac{1-1,05^{12}}{1-1,05}     \\
    \mathcal{S} & \approx \num{1200}\times\frac{1-1,79585}{1-1,05} \\
    \mathcal{S} & \approx \num{1200}\times 15,9171                 \\
    \mathcal{S} & \approx \num{19100,55}                           \\
  \end{align*}

  La somme des 12 premiers termes de la suite \suite[v] vaut donc $\num{19100,55}$ environ.
\end{methode}

\section{Utilisation des outils informatiques}
Dans cette partie, nous reprenons l'exemple de la suite \suite[v] utilisé dans la méthode \ref{TSTL:cours:suitesgeometriques:meth:sommepremierstermes}.

Nous cherchons à faire la somme des $12$ premiers termes de cette suite à l'aide de l'outil informatique.

\subsection{Feuille de calcul}
Dans la solution proposée ci-dessous, on calcule les termes de la suite de proche en proche à l'aide de la définition par récurrence
dans la colonne \texttt{B}. Ensuite, on a positionné
dans la cellule \texttt{D2} la raison de la suite et dans la cellule \texttt{B2} la valeur du premier terme.
Enfin, les sommes successives sont calculées dans la colonne \texttt{C}.

\begin{center}
  \begin{tikzpicture}
    \tableur[4]{A,B,C,D}
    \celtxt*[align=center,font=\bfseries]{A}{1}{$n$}
    \celtxt*[align=center]{A}{2}{0}
    \celtxt*[align=center]{A}{3}{1}
    \celtxt*[align=center]{A}{4}{2}
    \celtxt*[align=center,font=\bfseries]{B}{1}{$v_n$}
    \celtxt*[align=center]{B}{2}{1200}
    \celtxt[align=center]{B}{3}{=$B2*$D$2}
    \celtxt*[align=center,font=\bfseries]{C}{1}{$s$}
    \celtxt[align=center]{C}{2}{=B2}
    \celtxt[align=center]{C}{3}{=$C2+$B3}
    \celtxt*[align=center,font=\bfseries]{D}{1}{$q$}
    \celtxt*[align=center]{D}{2}{$1,05$}
  \end{tikzpicture}
\end{center}


\subsection{Algorithmes}

\subsubsection*{En pseudocode}

\begin{pseudocode}[Somme des termes d'une suite géométrique]
  \State $n \gets 0$ \Comment {Rang (réel) : initialisé à 0}
  \State $v \gets \num{1200}$ \Comment {Terme (réel) : initialisé à \num{1200}}
  \State $s \gets v$ \Comment {Somme (réel) : initialisée au terme initial}
  \While{$n < 12$}
  \State $v \gets v \times 1,05$ \Comment {Calcul du terme}
  \State $s \gets s + v $ \Comment {Actualisation de la somme}
  \State $n \gets n + 1$ \Comment {Actualisation du rang}
  \EndWhile
  \State Afficher $s$ \Comment {Affichage de la somme}
\end{pseudocode}


\subsubsection*{En Python}
L'algorithme ci-dessus peut se traduire en Python de la manière suivante~:
\begin{pythoncode}[Somme des termes d'une suite géométrique]
  # Initialisations des variables
  n = 0                 # rang (initialisation à 0)
  v = 1200              # terme (initialisation à 1200)
  somme = v             # somme (initialisation au terme initial)

  # Boucle
  while n < 12 :
  v = v * 1.05        # calcul du terme courant
  somme = somme + v   # actualisation de la somme
  n = n + 1           # actualisation du rang

  # Affichage du résultat
  print("S =", somme)
\end{pythoncode}


