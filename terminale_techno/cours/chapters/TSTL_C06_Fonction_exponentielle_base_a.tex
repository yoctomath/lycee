\chapter{Fonction exponentielle base $a$}


\section{Notion d'exponentielle}
\subsection{Rappels puissances d'exposants entiers}
Soit $a$ un nombre réel non nul et soit $n$ un entier naturel.
\begin{align*}
  a^n & = \underbrace{a\times a \times a \times\ldots\times a \times a}_{n\text{ facteurs égaux à }a}
      & a^{-n}                                                                                        & = \frac{1}{a^n}
      & a^1                                                                                           & = a
      & a^0                                                                                           & = 1
\end{align*}
L'idée dans ce chapitre est d'étendre cette définition à un exposant $x$ réel (donc pas forcément entier).
\begin{definition}\index{Fonction!Exponentielle base $a$}
  Soit $a$ un nombre réel \textit{strictement positif.}

  La fonction $f$ définie sur $\R$ par \[f : x \mapsto a^x\] étend à tout exposant $x\in \R$ les propriétés des puissances entières.

  \medskip

  Cette fonction $f$ est appelée \textbf{fonction exponentielle} de base $a$.
\end{definition}

\begin{exemple}\label{termtechno:cours:expa:exemple:1}
  La \og{}cote Argus\fg{} d'un Renault Scénic acheté à \Eu{18000} au 01/01/2015 a diminué d'environ $15,5\,\%$ par an.

  La fonction $f:x\longmapsto \num{18000}\times 0,845^x$ permet de déterminer la cote pour un nombre non entier d'années.

  Par exemple, la cote au bout de $5$ ans et $9$ mois est : $f(5,75)=\num{18000}\times 0,845^5,75$
  ($9$ mois représente $\frac{3}{4}$ d'une année) soit environ \Eu{6834}.
\end{exemple}

\begin{propriete}

  Soit $a$ un nombre réel strictement positif. Pour tout réel $x$, on a : $a^x > 0$.

  C'est-à-dire qu'une exponentielle est toujours strictement positive.
\end{propriete}

\subsection{Propriétés algébriques}

\begin{propriete}
  Soit un nombre $a>0$. Pour tous $x, y \in \R$ et pour tout $n \in \Z$ :
  \begin{align*}
    a^{x+y} & = a^x \times a^y   & a^{-x}             & = \dfrac{1}{a^x} \\
    a^{x-y} & = \dfrac{a^x}{a^y} & \left(a^x\right)^n & = a^{nx}
  \end{align*}
\end{propriete}

\section{Variations et représentation graphique}

\subsection{Variations}
\begin{propriete}
  Soit $a$ un nombre réel strictement positif. Le sens de variation de la fonction $x \mapsto a^x$ est le même que celui de la suite géométrique $(a^n)$ :
  \begin{item}
        \item Si $0<a<1$, alors la fonction $x \mapsto a^x$ est décroissante sur $\R$.

        \item Si $a=1$, alors la fonction $x \mapsto a^x$ est constante sur $\R$.

        \item Si $a>1$, alors la fonction $x \mapsto a^x$ est croissante sur $\R$.
  \end{item}
\end{propriete}

\begin{remarque}
  IL s'agit du même sens de variation que pour les suites géométriques.
\end{remarque}

\subsection{Représentation graphique}
\begin{tblr}{width=\linewidth,colspec={X[c,1]X[c,1]X[c,1]},row{1}={mode=math,font={\large}}}
  0<a<1 & a=1 & a>1 \\

  \begin{tikzpicture}[declare function={f1(\x) = 0.8^\x;},scale=0.7]%
    \begin{axis}[%
        axis y line=center,axis x line=middle,        % centrer les axes
        x=0.4cm,y=0.4cm,                                  % unités sur les axes
        xmin=-8.4,xmax=8.4,ymin=-0.7,ymax=8.5,        % bornes du repère
        xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
        % minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
        grid=both,                                    % indiquer quelle geilles afficher
        tick label style={font=\tiny,black!50}, % description des labels sur les axes
      ]
      \addplot[mark=none,colorPrim,very thick,domain=-8.4:8.4,samples=150] {f1(x)};
      % \addplot[mark=x,only marks,samples at={-2.5},colorSec,thick] {f1(x)} coordinate[label=left:$\mathcal{C}_f$] (A);
    \end{axis}
  \end{tikzpicture}
        &
  \begin{tikzpicture}[declare function={f1(\x) = 1^\x;},scale=0.7]%
    \begin{axis}[%
        axis y line=center,axis x line=middle,        % centrer les axes
        x=0.6cm,y=0.6cm,                                  % unités sur les axes
        xmin=-5.4,xmax=5.4,ymin=-0.7,ymax=5.5,        % bornes du repère
        xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
        % minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
        grid=both,                                    % indiquer quelle geilles afficher
        tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
      ]
      \addplot[mark=none,colorPrim,very thick,domain=-5.4:5.4,samples=150] {f1(x)};
      % \addplot[mark=x,only marks,samples at={-2.5},colorSec,thick] {f1(x)} coordinate[label=left:$\mathcal{C}_f$] (A);
    \end{axis}
  \end{tikzpicture}
        &
  \begin{tikzpicture}[declare function={f1(\x) = 1.5^\x;},scale=0.7]%
    \begin{axis}[%
        axis y line=center,axis x line=middle,        % centrer les axes
        x=0.4cm,y=0.4cm,                                  % unités sur les axes
        xmin=-8.4,xmax=8.4,ymin=-0.7,ymax=8.5,        % bornes du repère
        xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
        % minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
        grid=both,                                    % indiquer quelle geilles afficher
        tick label style={font=\tiny,black!50}, % description des labels sur les axes
      ]
      \addplot[mark=none,colorPrim,very thick,domain=-8.4:8.4,samples=150] {f1(x)};
      % \addplot[mark=x,only marks,samples at={-2.5},colorSec,thick] {f1(x)} coordinate[label=left:$\mathcal{C}_f$] (A);
    \end{axis}
  \end{tikzpicture}
\end{tblr}

\section{Applications}

\subsection{Phénomène d'évolution exponentielle}

\begin{propriete}
  Soit $a$ un nombre strictement positif et soit $k$ un nombre non nul.

  \begin{itemize}
    \item Si $k$ est \textbf{positif}, alors la fonction $x \mapsto k \times a^x$
          a le \textbf{même} sens de variation que la fonction $x \mapsto a^x$.
    \item Si $k$ est \textbf{négatif}, alors la fonction $x \mapsto k \times a^x$
          a le sens de variation \textbf{contraire} à celui de la fonction $x \mapsto a^x$.
  \end{itemize}
\end{propriete}

\begin{exemple}
  Reprenons la fonction donnant la cote d'une Renault Scénic de l'exemple \ref{termtechno:cours:expa:exemple:1}.

  La fonction est $f:x\longmapsto \num{18000}\times 0,845^x$.

  On a $\num{18000}>0$ donc $f$ a les mêmes variations que $x\longmapsto 0,845^x$.

  Or $0,845<1$ donc $x\longmapsto 0,845^x$ est décroissante.

  On en déduit que la fonction $f$ est décroissante sur $\R$.
\end{exemple}

\subsection{Application au calcul d'un taux d'évolution moyen}

\begin{propriete}\index{Taux d'évolution!Global}\index{Taux d'évolution!Moyen}
  Soit $t_G$ le taux d'évolution \textbf{global} correspondant à $n$ évolutions successives.

  Le taux d'évolution \textbf{moyen} est l'unique taux $t_m$ qui, répété $n$ fois, conduit au taux global $t_G$. Ainsi :
  \[ 1+t_G = (1+t_m)^n\]

  On aura aussi

  \[ 1+t_m = (1+t_G)^{\frac{1}{n}} \]

\end{propriete}

\begin{remarque}
  \begin{enumerate}\index{Coefficient multiplicateur}
    \item Dans ce qui précède, on entend par \textit{taux} un rapport, non exprimé en pourcentage, qui peut être positif ou négatif
          suivant que l'évolution est une augmentation ou une diminution.
    \item Si on nomme $CM$ le coefficient multiplicateur global correspondant à $n$ évolutions successives, alors :
          \[ CM = (1+t_m)^n \]
          et donc :
          \[ t_m = CM^{\frac{1}{n}} - 1 \]
  \end{enumerate}
\end{remarque}

\begin{methode}[Déterminer un taux moyen d'évolution]
  \textbf{Énoncé}\quad Un produit financier a subi une baisse de $3\,\%$ par an pendant $5$ ans
  et une hausse globale de $6\,\%$ sur $4$ ans.

  Déterminer le taux moyen annuel d'évolution de ce produit financier sur cette période de $9$ ans.\bigskip

  \textbf{Solution}\quad On peut représenter la situation par le schéma suivant :
  \begin{center}
    \begin{tikzpicture}[every node/.append style={font=\scriptsize}]
      \node[text width=3cm,align=center,draw] (p1) {Début};
      \node[right=3cm of p1,text width=3cm,align=center,draw] (p2){Fin de la baisse};
      \node[right=3cm of p2,text width=3cm,align=center,draw] (p3) {Fin de la hausse};
      \draw[->] (p1) -- node[above] {$-3\,\%$ par an sur $5$ ans} node[below] {$\times(1-\frac{3}{100})^5$} (p2);
      \draw[->] (p2) -- node[above] {$+6\,\%$ sur 4 ans} node[below] {$\times(1+\frac{6}{100})$} (p3);
    \end{tikzpicture}
  \end{center}
  Donc le coefficient multiplicateur global est $0,97^5\times1,06$.

  Si $t_m$ est le taux d'évolution moyen annuel sur cette période de $9$ ans, alors :
  \begin{align*}
    t_m & = \left(0,97^5\times1,06\right)^{\frac{1}{9}} - 1 \\
        & \approx -0,0104
  \end{align*}
  Donc le produit financier a donc subit en moyenne une baisse de $1,04\,\%$ par an sur cette période.
\end{methode}