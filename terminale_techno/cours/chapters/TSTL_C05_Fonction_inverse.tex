\chapter{Fonction inverse}

\section{Généralités}

\subsection{Définitions}

\begin{definition}\index{Fonction!Inverse}
  On appelle \textbf{fonction inverse} la fonction $f$ définie sur $\R[*]$ telle que pour tout $x$ de $\R[*]$ on ait :
  \[
    f(x) = \dfrac{1}{x}
  \]
\end{definition}

\begin{propriete}
  La fonction inverse est une fonction \textbf{impaire}.
\end{propriete}

\paragraph{Preuve\quad\faCogs} Soit $f(x)=\frac{1}{x}$ pour tout $x\in\R[*]$. On a $f(-x)=\frac{1}{-x}$ donc $f(-x)=f(x)$.

\subsection{Représentation graphique}
\begin{definition}
  La courbe représentative de la fonction inverse est une \textbf{hyperbole}\index{Hyperbole}.

  Elle admet l'origine du repère comme centre de symétrie.
  \begin{center}
    \begin{tikzpicture}
      \begin{axis}[%
          axis y line=center,axis x line=middle,        % centrer les axes
          x=1cm,y=1cm,                                  % unités sur les axes
          xmin=-3.4,xmax=3.4,ymin=-3.4,ymax=3.4,        % bornes du repère
          xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
          minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
          grid=both,                                    % indiquer quelle geilles afficher
          tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
        ]
        % on trace les fonctions
        %  domain : intervalle, 
        %  samples : nombres de points
        \addplot[mark=none,colorSec,very thick,domain=0.2:3.4,samples=150] {1/x};
        \addplot[mark=none,colorSec,very thick,domain=-3.4:-0.2,samples=150] {1/x};

        \node[above right,colorSec] at (1.2,1/1.2) {$\mathcal{C}_f$};
        \coordinate[label=below left:$O$] (O) at (0, 0);
        \coordinate[label=above right:$M$] (M) at (0.5, 2);
        \coordinate[label=below left:$M'$] (Mp) at (-0.5, -2);
        \draw[densely dashed, thin] (M) -- (O) node[midway,sloped,font=\scriptsize] {/\!/}
        -- (Mp)node[midway,sloped,font=\scriptsize] {/\!/};
        % \foreach \pt in {M,Mp} \node[cross=3pt,thick] at (\pt) {};
      \end{axis}
    \end{tikzpicture}
  \end{center}
\end{definition}
\subsection{Variations}

\begin{propriete}
  La fonction inverse est dérivable sur \R[*]. Sa dérivée est :
  \[f'(x) = -\frac{1}{x^2}\qquad \text{pour tout }x\in\R[*]\]
\end{propriete}

\paragraph{Preuve\quad\faCogs} Posons $f(x)=\dfrac{1}{x}$ pour tout $x\in\R[*]$.


  {\setlength{\columnsep}{1cm}
    \setlength{\columnseprule}{0.25pt}
    \begin{multicols}{2}
      Examinons le taux de variations de $f$ entre $a$ et $a+h$ (avec $a \in \R[*], h \in \R$) :
      \begin{align*}
        \frac{f(a+h) - (a)}{h} & = \dfrac{\dfrac{1}{a+h}-\dfrac{1}{a}}{h} \\
                               & = \dfrac{\dfrac{a-(a+h)}{a(a+h)}}{h}     \\
                               & = \dfrac{\dfrac{-h}{a(a+h)}}{h}          \\
                               & = \frac{-h}{a(a+h)}\times\frac{1}{h}     \\
                               & = -\dfrac{1}{a(a+h)}
      \end{align*}

      Calculons le nombre dérivée de $f$ en $a$ :
      \begin{align*}
        f'(a) & = \frac{f(a+h) - (a)}{h}                  \\
              & =\lim\limits_{h \to 0} -\dfrac{1}{a(a+h)} \\
              & = -\dfrac{1}{a^2}
      \end{align*}

      En conséquence, pour tout $x\in\R[*]$, la dérivée de $f$ est :
      \[f'(x) = -\frac{1}{x^2}\]
    \end{multicols}}


\begin{propriete}
  La fonction inverse est \textbf{strictement décroissante} sur $\interoo{-\infty{} 0}$ et aussi sur $\interoo{0 +\infty}$.
\end{propriete}

\paragraph{Preuve\quad\faCogs}  Pour tout $x\in\R[*]$, on a $x^2>0$ donc on peut affirmer que $-\frac{1}{x^2}<0$.

Donc pour tout $x\in\R[*]$, $f'(x)<0$.

On en déduit que la fonction inverse est strictement décroissante sur $\interoo{-\infty{} 0}$ et aussi sur $\interoo{0}{+\infty}$.

\begin{methode}[Étudier les variations d'une fonction inverse]
  \textbf{Énoncé}\quad Soit la fonction $g$ définie sur \interoo{0 +\infty} par $g(x)=-\frac{7}{x}$.

  Déterminer les variations de $f$ sur son domaine de définition.
  \bigskip

  
  {
    \setlength{\columnsep}{0.5cm}
    \setlength{\columnseprule}{0.25pt}
    \begin{multicols}{2}
    \textbf{Solution}\quad Calculons la dérivée de $g$ :
    \begin{align*}
      g'(x) & = -7 \times \left(-\frac{1}{x^2}\right) \\
            & = \frac{7}{x^2}
    \end{align*}
  
    On observe que pour tout $x\in\R[*]$, $x^2>0$ donc $g'(x)>0$. 
    
    Donc, la fonction $g$ est strictement croissante sur \interoo{0 +\infty}.
  \end{multicols}}

\end{methode}

\section{Notion de limite}
\begin{propriete}[Limites de la fonction inverse]\index{Fonction!Inverse!Limites}
  Soit $x$ un nombre réel de $\R[*]$.
  \begin{itemize}[label=\raisebox{0.2ex}{\textcolor{colorSec}{\tiny \faCircle}}]\doublespacing
    \item Lorsque $x$ tend vers $+\infty$ alors $f(x)$ tend vers $0$ et on note : $\lim\limits_{x \to +\infty}\dfrac{1}{x}=0$
    \item Lorsque $x$ tend vers $-\infty$ alors $f(x)$ tend vers $0$ et on note : $\lim\limits_{x \to -\infty}\dfrac{1}{x}=0$
    \item Lorsque $x$ tend vers $0$ en restant positif alors $f(x)$ tend vers $+\infty$ et on note : $\lim\limits_{\substack{x \to 0\\ x > 0}}\dfrac{1}{x}=+\infty$
    \item Lorsque $x$ tend vers $0$ en restant négatif alors $f(x)$ tend vers $-\infty$ et on note : $\lim\limits_{\substack{x \to 0\\ x < 0}}\dfrac{1}{x}=-\infty$
  \end{itemize}
\end{propriete}

\begin{exemple}
  On dira par exemple que la limite de la fonction $x\longmapsto\dfrac{1}{x}$ lorsque $x$ tend vers $+\infty$ est $0$ par valeur supérieure.
\end{exemple}

\begin{definition}[Asymptotes]\index{Asymptotes}\index{Fonction!Inverse!Asymptotes}
  Soit $f$ la fonction inverse définie sur $\R[*]$ par $f(x)=\dfrac{1}{x}$. On nomme $\mathcal{C}_f$ sa courbe représentative
  dans un repère orthonormé.
  \begin{itemize}[label=\raisebox{0.2ex}{\textcolor{colorPrim}{\tiny \faCircle}}]
    \item La droite d''équation $y=0$ (c'est-à-dire l'axe des abscisses) est une \textbf{asymptote horizontale} à $\mathcal{C}_f$
          en $+\infty$ et $-\infty$.
    \item La droite d'équation $x=0$ (c'est-à-dire l'axe des ordonnées) est une \textbf{asymptote verticale} à $\mathcal{C}_f$
          au voisinage de $0$.
  \end{itemize}

\end{definition}

\begin{remarque}
  Lorsqu'une courbe admet une \textit{asymptote}, elle s'en rapproche sans jamais la toucher.
\end{remarque}

\begin{center}
  \begin{tblr}{width=\linewidth,colspec={X[c,1]X[c,1]},hline{1,3}={},vlines,stretch=2}
    $-\infty \longleftarrow x$ & $x \longrightarrow +\infty$ \\
    \begin{tikzpicture}
      \begin{axis}[%
        clip mode=individual,
        axis y line=center,axis x line=middle,        % centrer les axes
        x=1cm,y=1cm,                                  % unités sur les axes
        xmin=-6.4,xmax=0.7,ymin=-1.3,ymax=0.7,        % bornes du repère
        xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
        minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
        grid=both,                                    % indiquer quelle geilles afficher
        x tick label style={font=\scriptsize,black!50,above}, % description des labels sur les axes
        y tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
      ]
      \addplot[mark=none,colorSec,very thick,domain=-6.4:-0.8,samples=150] {1/x};
      \coordinate (A) at (-6.4, 1/-6.4);
      \node[below right,font={\scriptsize},text=black!40,fill=white,inner sep=1pt] (comment) at ([xshift=5mm,yshift=-3mm]A) {$f(x)$ se rapproche de $0$};
      \draw[black!40,thin,latex'-] (A) -- (comment.north west);
      \draw[opacity=0.3,line width=3pt,colorTer] (-6.4,0) -- (0.7,0);
      \coordinate[pin={[black!40,font=\scriptsize,pin anchor=south west,pin edge pin anchor=south west,inner sep=1pt,pin distance=1cm]60:asymptote horizontale}] (C) at (-5.7,0);
    \end{axis}
    \end{tikzpicture}
    &
    \begin{tikzpicture}
      \begin{axis}[%
        clip mode=individual,
        axis y line=center,axis x line=middle,        % centrer les axes
        x=1cm,y=1cm,                                  % unités sur les axes
        xmin=-0.7,xmax=6.4,ymin=-0.7,ymax=1.3,        % bornes du repère
        xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
        minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
        grid=both,                                    % indiquer quelle geilles afficher
        tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
      ]
      \addplot[mark=none,colorSec,very thick,domain=0.8:6.4,samples=150] {1/x};

      \coordinate (A) at (6.4, 1/6.4);
      \node[above left,font={\scriptsize},text=black!40,fill=white,inner sep=1pt] (comment) at ([xshift=-5mm,yshift=3mm]A) {$f(x)$ se rapproche de $0$};
      \draw[black!40,thin,latex'-] (A) -- (comment.south east);
      \draw[opacity=0.3,line width=3pt,colorTer] (6.4,0) -- (-0.7,0);
      \coordinate[pin={[black!40,font=\scriptsize,pin anchor=north east,pin edge pin anchor=north east,inner sep=1pt,pin distance=1cm]240:asymptote horizontale}] (C) at (5.7,0);
    \end{axis}
    \end{tikzpicture}\\
  \end{tblr}
\end{center}

\begin{center}
  \begin{tblr}{width=\linewidth,colspec={X[c,1]X[c,1]},hline{1,3}={},vlines,stretch=2}
    $x \longrightarrow 0^-$ & $0^+ \longleftarrow x$ \\
    \begin{tikzpicture}
      \begin{axis}[%
        clip mode=individual,
        axis y line=center,axis x line=middle,        % centrer les axes
        x=1cm,y=1cm,                                  % unités sur les axes
        xmin=-1.8,xmax=0.7,ymin=-5.4,ymax=0.7,        % bornes du repère
        xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
        minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
        grid=both,                                    % indiquer quelle geilles afficher
        x tick label style={font=\scriptsize,black!50,above}, % description des labels sur les axes
        y tick label style={font=\scriptsize,black!50,right}, % description des labels sur les axes
      ]
      \addplot[mark=none,colorSec,very thick,domain=-1.8:-1/5.4,samples=150] {1/x};
      \coordinate (A) at (-1/5.4,-5.4);
      \node[above left,font={\scriptsize},fill=white,text=black!40,inner sep=1pt,text width=2.8cm,align=right] (comment) at ([xshift=-10mm,yshift=5mm]A) {$f(x)$ reste négatif et sa partie numérique devient de plus en plus grande.};
      \draw[black!40,thin,latex'-] (A) -- (comment.east);
      \draw[opacity=0.3,line width=3pt,colorTer] (0,-5.3) -- (0,0.7);
      \coordinate[pin={[black!40,,font=\scriptsize,pin anchor=south west,pin edge pin anchor=south west,inner sep=1pt,pin distance=1cm]30:asymptote verticale}] (C) at (0,-4.5);
    \end{axis}
    \end{tikzpicture}
    &
    \begin{tikzpicture}
      \begin{axis}[%
        clip mode=individual,
        axis y line=center,axis x line=middle,        % centrer les axes
        x=1cm,y=1cm,                                  % unités sur les axes
        xmin=-0.7,xmax=1.8,ymin=-0.7,ymax=5.4,        % bornes du repère
        xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
        minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
        grid=both,                                    % indiquer quelle geilles afficher
        tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
      ]
      \addplot[mark=none,colorSec,very thick,domain=1/5.4:1.8,samples=150] {1/x};
      \coordinate (A) at (1/5.4,5.4);
      \node[below right,font={\scriptsize},text=black!40,fill=white,inner sep=1pt,text width=2.8cm,align=left] (comment) at ([xshift=10mm,yshift=-5mm]A) {$f(x)$ devient de plus en plus grand.};
      \draw[black!40,thin,latex'-] (A) -- (comment.west);
      \draw[opacity=0.3,line width=3pt,colorTer] (0,5.4) -- (0,-0.7);
      \coordinate[pin={[black!40,font=\scriptsize,pin anchor=north east,pin edge pin anchor=north east,inner sep=1pt,pin distance=1cm]210:asymptote verticale}] (C) at (0,4.5);
    \end{axis}
    \end{tikzpicture}\\
  \end{tblr}
\end{center}