\chapter{Probabilités conditionnelles}

\section{Rappels}

\subsection{Notations et vocabulaire}

\begin{definition}
  On appelle {\bfseries expérience aléatoire}\index{Expérience!Aléatoire} une expérimentation ou un phénomène conduisant à plusieurs
  résultats bien définis et pour lequel on ne peut pas savoir \textit{a priori} quel résultat se produira.\\
  Ces différents résultats sont appelés \textbf{issues}\footnotemark{}\index{Issue}.\\
  L'ensemble de toute les issues d'une expérience aléatoire est appelé \textbf{univers}. Il est souvent noté $\Omega$.
\end{definition}%\medskip
\footnotetext{Ou \textbf{épreuve} (ou encore \textbf{cas}) suivant les différentes littératures ou les différents auteurs.}

\begin{exemple}
  Donnons nous deux exemples d'expériences aléatoires que nous utiliserons plusieurs fois dans le chapitre.
  \begin{description}
    \item[Expérience 1]\label{proba:experience:un}\og\textit{On jette un dé et on observe la face supérieure.}\fg{}\\
          Les issues de cette expérience aléatoire sont les nombres~: 1~; 2~; 3~; 4~; 5~; 6.

          L'univers est $\Omega=\anuplet{1 2 3 4 5 6}$.

    \item[Expérience 2]\label{proba:experience:quatre} \og\textit{On lance une pièce de monnaie et on regarde la face supérieure}\fg{}

          L'univers sera alors $\Omega=\anuplet{pile face}$.
  \end{description}
\end{exemple}

\begin{definition}
  Soit une expérience aléatoire d'univers $\Omega$.

  On appelle \textbf{événement} une partie de l'ensemble $\Omega$ des issues d'une expérience aléatoire.
  Il est définit par une phrase et désigné par une lettre majuscule.
\end{definition}

\begin{remarque}
  Quelques remarques sur la notion d'événement~:
  \begin{itemize}
    \item Un événement est toujours défini en rapport avec une expérience aléatoire. Cela n'a pas de sens de parler
          d'une événement tout seul.
    \item Pour désigner un événement, on doit énoncer une phrase précise afin qu'il n'existe
          pas de doute sur les issues à prendre en compte. Quelquefois,
          on peut donner directement les issues correspondant à l'événement.

          Dans les deux cas, on peut nommer l'événement d'une lettre majuscule ("\textit{soit $A$ l'événement...}")
  \end{itemize}
\end{remarque}

\begin{definition}
  Soit $A$ un événement d'une expérience aléatoire.

  Un issue est dite \textbf{favorable}\index{Issue!Favorable} à l'événement $A$ si elle est un élément de cet  événement et
  \textbf{défavorable} s'il n'en fait pas partie.
\end{definition}

\begin{exemple}
  Dans l'expérience 1, on pourrait considérer l'événement $A$~:
  \og\textit{le nombre désigné par la face supérieure du dé est pair}\fg{}.

  $2$, $4$ et $6$ sont toutes les issues \textit{favorables} à l'événement $A$ car se sont les
  nombres divisibles par $2$ dans l'univers $\Omega$.

  On peut dire alors que $A=\anuplet{2 4 6}$ qui est une partie (sous-ensemble) de l'univers
  (c'est-à-dire toutes les issues possibles de l'expérience).
\end{exemple}

\begin{definition}[Cas particulier]
  L'événement est dit \textbf{élémentaire}\index{Evénement!Élémentaire} s'il ne correspond qu'à une seule et unique issue.\\
  Un événement correspondant à un ensemble vide est dit \textbf{impossible}\index{Evénement!Impossible}. \\
  Un événement correspondant à tout l'univers est dit \textbf{certain}\index{Evénement!Certain}.
\end{definition}
\begin{exemple} Voici quelques cas particulier d'événements~:
  \begin{itemize}
    \item Dans l'expérience 1, on pourrait considérer l'événement~: $B=\{\text{"pile"}\}$.
          Ici, l'événement est définit directement par un ensemble d'issue.

          On remarque que $B$ est un événement \textit{élémentaire}.
    \item Dans l'expérience 1, soit $C$ l'événement~: "Le nombre lu est supérieur à $10$".

          L'univers de l'expérience étant $\Omega=\anuplet{1 2 3 4 5 6}$, on peut dire qu'il
          n'existe pas d'issue répondant aux condition de $C$ donc $C=\varnothing$ (ensemble vide).

          On remarque que $C$ est un événement \textit{impossible}.
    \item Dans l'expérience 1, soit $D$ l'événement ~: "le nombre lu est strictement plus petit que $7$".
          On a $D=\anuplet{1 2 3 4 5 6}$, soit $D=\Omega$.

          On remarque que $D$ est un événement \textit{certain}.
  \end{itemize}
\end{exemple}

\begin{definition}
  Soit un événement $A$. L'événement \textbf{contraire}\index{Evénement!Contraire} à $A$, noté $\overline{A}$, est constitué
  de toutes les issues possibles qui ne sont pas favorables à $A$. Il est décrit par la négation de
  l'événement $A$ ($\overline{A}$ se lit \textit{non $A$}).

  Si l'univers des possibles est noté $\Omega$ alors $\overline{A}=\Omega \backslash A$.
\end{definition}

\begin{exemple}
  Dans l'expérience 1, soit $E$ l'événement~: \og{}\textit{le nombre lu est premier}\fg{}.
  \begin{multicols}{2}
    On a $E=\anuplet{2 3 5}$ et $\Omega=\anuplet{1 2 3 4 5 6}$.
    \medskip

    Donc $\overline{E}$ est l'événement~: \og{}\textit{le nombre lu n'est pas premier}\fg{}  et on a
    $\overline{E}=\anuplet{1 4 6}$.
    \vspace*{\fill}{handler}{list macro}\columnbreak

    \begin{center}
      \begin{center}
        \scriptsize
        \begin{tikzpicture}[scale=0.5,pin distance=10mm]
          \foreach \nb/\x/\y in {1/0/0,2/0.5/-2,4/2/0.3,3/1.8/-1.8,6/4/-0.2,5/4.2/-2}
            {
              \draw ($(\x,\y)+(0.08,0.08)$) -- ($(\x,\y)+(-0.08,-0.08)$);
              \draw ($(\x,\y)+(0.08,-0.08)$) -- ($(\x,\y)+(-0.08,0.08)$);
              \node[left] (e\nb) at (\x,\y) {\nb};
            }
          \node[inner sep=5pt,draw=colorSec,rounded corners,very thick,fit=(e1)(e4)(e6),pin={[colorSec,pin edge={colorSec}]right:$\overline{E}$}] (nonE) {};
          \node[inner sep=5pt,draw=colorTer,rounded corners,very thick,fit=(e2)(e3)(e5),pin={[colorTer,pin edge={colorTer}]right:$E$}] (E) {};
          \node[inner sep=5pt,draw={black},rounded corners,very thick,fit=(E) (nonE),pin={[pin distance=5mm]left:$\Omega$}] (Omega) {};
        \end{tikzpicture}
      \end{center}
    \end{center}
  \end{multicols}
\end{exemple}

\begin{definition}
  Deux événements sont dits \textbf{incompatibles}\index{Evénement!Incompatibles} s'ils ne peuvent pas se produire en même temps.
\end{definition}

\begin{exemple}
  Reprenons l'expérience 1 de l'exemple \ref{proba:experience:un}.

  Dans cette expérience, les événements \og{}\textit{la face supérieure du dé est $1$}\fg{}
  et \og{}\textit{la face supérieure du dé est $2$}\fg{} sont \textit{incompatibles}. En effet, un dé
  immobilisé ne peut montrer les faces $1$ et $2$ en même temps.
\end{exemple}

\subsubsection{Notations ensemblistes}

\begin{definition}
  Soient $A$ et $B$ deux ensembles. On définit les deux ensembles suivants~:
  \begin{itemize}
    \item[\textbullet] L'\textbf{union} de $A$ et de $B$, notée $A \cup B$,  qui regroupe tous les éléments de $A$ et de $B$~;
    \item[\textbullet] L'\textbf{intersection} de $A$ et de $B$, notée $A \cap B$, qui regroupe tous les éléments commun à $A$ et à $B$, c'est-à-dire qui sont dans les deux ensembles en même temps.
  \end{itemize}
  \begin{center}
    \begin{tikzpicture}[every label/.append style={font=\Large}]
      % Definition of circles
      \def\firstcircle{(0,0) circle (1.5cm)}
      \def\secondcircle{(0:2cm) circle (1.5cm)}

      \colorlet{circle edge}{colorPrim!50}
      \colorlet{circle area}{colorPrim!20}
      \tikzset{filled/.style={fill=circle area, draw=circle edge, thick},
        outline/.style={draw=circle edge, thick}}
      % Set A and B
      \node[label=below:$A \cap B$] (AandB) {
        \begin{tikzpicture}
          \begin{scope}
            \clip \firstcircle;
            \fill[filled] \secondcircle;
          \end{scope}
          \draw[outline] \firstcircle node {$A$};
          \draw[outline] \secondcircle node {$B$};
        \end{tikzpicture}
      };
      % Set A or B
      \node[right=of AandB,label=below:$A \cup B$] (AorB){
        \begin{tikzpicture}
          \begin{scope}
            \draw[filled] \firstcircle node[anchor=center] {$A$}
            \secondcircle node[anchor=center] {$B$};
          \end{scope}
        \end{tikzpicture}
      };

    \end{tikzpicture}
  \end{center}
\end{definition}



\begin{remarque}
  On peut remarquer que si $A$ et $B$ sont deux événements~:
  \begin{itemize}
    \item[\textbullet] $A\cup B$ signifie $A$ \textcolor{colorSec}{\bfseries ou} $B$ c'est-à-dire les issues
          qui réalisent soit l'événement $A$, soit l'événement $B$ (soit $A$ et $B$ en même temps -- le \textit{ou} n'est pas \textit{exclusif})~;
    \item[\textbullet] $A\cap B$ signifie $A$ \textcolor{colorSec}{\bfseries et} $B$ c'est-à-dire les issues
          qui réalisent les événements $A$ et $B$ en même temps.
    \item[\textbullet] Si $A$ et $B$ sont incompatibles\index{Evénement!Incompatibles} alors $A\cap B=\varnothing$.
  \end{itemize}
\end{remarque}

\subsection{Formules}

\begin{propriete}\label{tstl:cours:probas:prop:formules}
  Soient $A$ et $B$ deux événements.
  \begin{itemize}
    \item Dans une situation d'\textbf{équiprobabilité} : \qquad
          $P(A) = \dfrac{\mbox{nombre d'issues favorables à $A$}}{\mbox{nombre d'issues total}}$

    \item $P(\overline A) = 1 - P(A)$

    \item $P(A \cup B) = P(A) + P(B) - P(A \cap B)$
  \end{itemize}
\end{propriete}

\begin{methode}[Utiliser la formule $P(A \cup B) = P(A) + P(B) - P(A \cap B)$]
  \textbf{Énoncé}\quad--\quad Dans un lot de calculatrices graphiques, on a $2\%$ de chance qu'une machine ait un écran défectueux,
  $1,5\%$ de chance qu'un clavier soit défectueux et $0,8\%$ de chance que la calculatrice ait les deux défaut réunis.

  Si je prends une calculatrice au hasard dans ce lot, quelle est la probabilité qu'elle soit défectueuse~?
  \bigskip

  \textbf{Solution}\quad Nommons $A$ l'événement \og{}\textit{la calculatrice a un écran défectueux}\fg{}
  et $B$ l'événement
  \og{}\textit{la calculatrice a un clavier défectueux}\fg{}.

  On peut reformuler l'événement $C$~:~\og{}\textit{la calculatrice est défectueuse}\fg{}
  en \og{}\textit{la calculatrice a au moins un défaut}\fg{} ou encore en \og{}\textit{la calculatrice à un défaut d'écran ou un défaut de clavier}\fg{}.

  D'après la propriété \ref{tstl:cours:probas:prop:formules}, on en déduit que~:
  \begin{align*}
    P(C) & = P(A\cup B)                                    \\
    P(C) & = P(A)+P(B)-P(A\cap B)                          \\
    P(C) & = \frac{2}{100}+\frac{1,5}{100}-\frac{0,8}{100} \\
    P(C) & = \frac{2,7}{100}
  \end{align*}
  Il y a donc $2,7\%$ de chance que la calculatrice tirée au hasard dans le lot soit défectueuse.
\end{methode}


\section{Probabilités conditionnelles}

\subsection{Formules}

\begin{definition}\label{tstl:cours:probas:def:conditionnelle}\index{Probabilités!Formule des probabilités conditionnelles}
  Soient $A$ et $B$ deux événements tels que $P(A) \neq 0$.

  La probabilité de l'événement $B$ \textbf{sachant que} l'événement $A$ est réalisé, notée $P_A (B)$, est définie par :
  \[P_A(B) = \frac{P(A\cap B)}{P(A)}\]
\end{definition}

\begin{methode}[Utiliser la formule des probabilités conditionnelles]
  \textbf{Énoncé}\quad--\quad Dans un pays où sévit une maladie contagieuse, 35 \% de la population a été atteinte l'hiver dernier. 65~\%~des habitants se sont fait vacciner, mais le vaccin n'est pas parfaitement efficace : 1 \% des habitants ont été vaccinés \textsc{et} sont tombés malades.

  On choisit un habitant au hasard dans la population. On définit les événements :

  \begin{itemize}
    \item $M$~: \og{}\textit{L'individu choisi est tombé malade.}\fg{}
    \item $V$~: \og{}\textit{L'individu choisi a été vacciné.}\fg{}
  \end{itemize}
  \begin{enumerate}
    \item Traduire les données de l'énoncé sous forme de probabilités.
    \item Pour un habitant, quelle est la probabilité de tomber malade sachant qu'il a été vacciné ?
  \end{enumerate}
  \bigskip

  \textbf{Solution}

  \begin{multicols}{2}
    \begin{enumerate}
      \item On a
              {\footnotesize\begin{align*}
                  P(M)        & = 0,35 \\
                  P(V)        & = 0,65 \\
                  P(M \cap V) & = 0,01 \\
                \end{align*}}
            \columnbreak
      \item Calcul de la probabilité de $M$ sachant $V$ :
            {\footnotesize\begin{align*}
              P_V(M) & = \frac{P(M\cap V)}{P(V)} \\
                     & = \frac{0,01}{0,65}       \\
                     & \approx 0,0154
            \end{align*}}
    \end{enumerate}
  \end{multicols}
  La probabilité de tomber malade sachant qu'on a été vacciné est de $1,54 \%$.
\end{methode}

\begin{propriete}
  Soient $A$ et $B$ deux événements tels que $P(A) \neq 0$ et $P(B) \neq 0$. Alors :
  \[P(A \cap B) = P_A(B) \times P(A) \qquad \text{ et de m\^eme } \qquad P(A \cap B) = P_B(A) \times P(B)\]
\end{propriete}

\begin{remarque}
  Cette propriété est une conséquence directe de la définition \ref{tstl:cours:probas:def:conditionnelle} précédente.
\end{remarque}

\begin{methode}
  \textbf{Énoncé}\quad--\quad Une entreprise emploie $60\% $d'hommes. La proportion de syndiqués \textbf{parmi les hommes} y est de $45\%$.

  On choisit un employé au hasard. On définit les événements :
  \begin{itemize}
    \item $S$~: \og{}\textit{L'individu choisi est syndiqué.}\fg{}
    \item $H$~: \og{}\textit{L'individu choisi est un homme.}\fg{}
  \end{itemize}
  Quelle est la probabilité que l'individu choisi soit un homme et qu'il soit syndiqué ?
  \bigskip

  \textbf{Solution}\quad--\quad D'après l'énoncé, on a : \quad $P(H) = 0,6$ \quad et \quad $P_H(S) = 0,45$.

  Alors, la probabilité que l'individu choisi soit un homme et qu'il soit syndiqué est :
  \begin{align*}
    P(H\cap S) & = P_H(S) \times P(H) \\
               & = 0,45 \times 0,6    \\
               & = 0,27
  \end{align*}
\end{methode}


\subsection{Arbres pondérés}

On utilise volontiers un arbre pour visualiser une expérience dans son ensemble.
On parlera d'arbre \textbf{pondéré} dans le cas où les probabilités de chaque événement ont été mentionnés sur chaque branche.
\clearpage

\begin{definition}[Vocabulaire -- Fonctionnement de l'arbre]\index{Probabilités!Arbres pondérés}
  Voici un arbre pondéré.
  \begin{center}
    \begin{tikzpicture}[%
        grow'=right,%
        level 1/.style={sibling distance=40mm,level distance=30mm},%
        level 2/.style={sibling distance=20mm,level distance=30mm},%
        proba/.style={midway,font=\scriptsize,fill=colorPrim!5,inner sep=1pt},%
        noeud/.style={font=\small},%
        scale=0.8,
        pin distance=10mm
      ]
      \node[inner sep=0pt]{}
      child {node[noeud] (A) {$A$}
          child {node[noeud] (AB) {$B$} edge from parent node[proba] {$P_A(B)$}}
          child {node[noeud] (ABb) {$\overline{B}$} edge from parent node[proba] {$P_A\left(\overline{B}\right)$}}
          edge from parent node[proba] {$P(A)$} }
      child {node[noeud] (Ab){$\overline{A}$}
          child {node[noeud] (AbB) {$B$} edge from parent node[proba] {$P_{\overline{A}}(B)$}}
          child {node[noeud] (AbBb) {$\overline{B}$} edge from parent node[proba] {$P_{\overline{A}}\left(\overline{B}\right)$}}
          edge from parent node[proba] (pAb) {$P(\overline{A})$}};
      \node[right=of AB] (eAB) {$A \cap B$};
      \node[right=of ABb] (eABb) {$A \cap \overline{B}$};
      \node[right=of AbB] (eAbB) {$\overline{A} \cap B$};
      \node[right=of AbBb] (eAbBb) {$\overline{A} \cap \overline{B}$};
      \node[right=of eAB] (pAB) {$P(A \cap B)=P(A) \times P_A(B)$};
      \node[right=of eABb] (pABb) {$P(A \cap \overline{B})=P(A) \times P_A\left(\overline{B}\right)$};
      \node[right=of eAbB] (pAbB) {$P(\overline{A} \cap B)=P(\overline{A}) \times P_{\overline{A}}\left(B\right)$};
      \node[right=of eAbBb] (pAbBb) {$P(\overline{A} \cap \overline{B})=P(\overline{A}) \times P_{\overline{A}}\left(\overline{B}\right)$};
      \node[right=of pAB,font=\lightfont\scriptsize,colorSec,text width=2cm] (expAB) {produit des probabilités sur chaque branche};
      \node[inner sep=1pt,draw=colorSec,thin,rounded corners,fit=(Ab)(AbB)(AbBb),pin={[colorSec,font=\lightfont\scriptsize,text width=4cm,anchor=north west,pin edge={colorSec,thin}]below:{la somme des probabilités sur toutes les branches partant d'un même nœud vaut 1}}] {};
      \node[above left=of A,font=\lightfont\scriptsize,colorSec] (n) {nœud};
      \draw[colorSec,thin] (pAB) -- (expAB);
      \draw[colorSec,thin] (A) -- (n);
      \draw[line width=4mm,colorSec,opacity=0.2,line cap=round,shorten >=-3mm,shorten <=-3mm] (A) -- (AB) coordinate[pos=0.75] (branche);
      \node[above=of branche,font=\lightfont\scriptsize,colorSec] (brancheexp) {branche};
      \draw[colorSec,thin] ([yshift=2.5mm]branche) -- (brancheexp);
      \node[below=of pAb,font=\lightfont\scriptsize,colorSec,text width=2cm] (pAbexp) {probabilité de l'événement en bout de branche};
      \draw[colorSec,thin] (pAb) -- (pAbexp);
      \node[above=5mm of eAB,font=\bfseries] {Événements};
      \node[above=5mm of pAB,font=\bfseries] {Probabilités};
    \end{tikzpicture}
  \end{center}
\end{definition}

\section{Calculs de probabilités}


\subsection{Formule des probabilités totales}

\vspace{-0.3cm}

\begin{definition}\index{Probabilités!Partition}
  On appelle \textbf{partition de l'univers} un ensemble d'événements \textbf{incompatibles} entre eux et dont la \textbf{réunion} forme l'univers.
\end{definition}


\begin{propriete}[Formules des probabilités totales]\index{Probabilités!Formule des probabilités totales}
  Soient $A_1, A_2, \ldots, A_n$ une partition de l'univers $\Omega$, et soit $B$ un événement. On a alors~:
  \[P(B) = P(A_1 \cap B) + P(A_2 \cap B) + \ldots + P(A_n \cap B)\]
\end{propriete}

\subsection{Synthèse des formules utiles}
Dans un problème, on va souvent utiliser les mêmes formules.

\begin{tblr}{width=\linewidth,colspec={X[l,2]X[l,1]},hlines,vlines,stretch=2,row{1}={bg=colorPrim!5,font={\bfseries}},rows={valign={m}}}
  Conditions & Formules                      \\
  La probabilité d'un chemin est le \textbf{produit} des probabilités de ses branches
             &
  $P(A \cap B) = P(A) \times P_A(B)$         \\
  Pour les branches issues d'un même n{\oe}ud, la somme des probabilités vaut $1$
             &
  $P_A(B) + P_A(\overline B) = 1$            \\
  La probabilité d'un événement est la somme des probabilités de tous les chemins qui y aboutissent
  (probabilités totales)
             &
  $P(B) = P(A\cap B) + P(\overline A\cap B)$ \\
\end{tblr}


\begin{methode}[Utilisation des probabilités conditionnelles et de l'arbre]
  \textbf{Énoncé}\quad -- \quad La puissance électrique consommée en France en hiver dépend en partie
  des conditions climatiques. Si la demande est trop forte, la France doit importer une partie de son énergie électrique.

  On considère un hiver en France, au hasard. On définit les événements suivants :

  \begin{quote}
    $E$ : \textit{\og{}la France doit importer une partie de son énergie électrique\fg{}}

    $R$ : \textit{\og{}l'hiver est rigoureux\fg{}}
    \qquad
    $\overline R$ : \textit{\og{}l'hiver n'est pas rigoureux\fg{}}
  \end{quote}

  On considère que les hivers sont rigoureux dans $10\,\%$ des cas.
  Lorsque l'hiver est rigoureux, la probabilité que la France doive importer
  une partie de son énergie électrique est de $80\,\%$ ; sinon, cette probabilité tombe à $60\,\%$.
  \medskip

  Quelle est la probabilités que la France importe une partie de son énergie électrique l'hiver~?
  \bigskip

  \textbf{Solution}\quad -- \quad Modélisons la situation par un arbre pondéré.

  On doit déterminer toutes les probabilités nécessaires.
  \begin{itemize}
    \item À l'aide du texte, on peut dire que ~:
          \begin{itemize}
            \item les hivers sont rigoureux dans $10\,\%$ des cas~: $P(R)=0,1$~;
            \item si l'hiver est rigoureux, la probabilité d'importer est de $80\,\%$~: $P_R(E)=0,8$~;
            \item sinon, cette probabilité tombe à $60\,\%$~: $P_{\overline{R}}(E)=0,6$.
          \end{itemize}
    \item En utilisant les formules, on en déduit les probabilités manquante sur les branches.
          \begin{align*}
            P(\overline{R}) & = 1- P(R) & P_R(\overline{E}) & = 1 - P_R(E) & P_{\overline{R}}(\overline{E}) & = 1 -  P_{\overline{R}}(E) \\
            P(\overline{R}) & = 1- 0,1  & P_R(\overline{E}) & = 1 - 0,8    & P_{\overline{R}}(\overline{E}) & = 1 -  0,6                 \\
            P(\overline{R}) & = 0,9     & P_R(\overline{E}) & = 0,2        & P_{\overline{R}}(\overline{E}) & = 0,4                      \\
          \end{align*}
  \end{itemize}
  On peut alors compléter l'arbre qui nous servira à visualiser les calculs à faire par la suite.
  \begin{center}
    \begin{tikzpicture}[%
        grow'=right,%
        level 1/.style={sibling distance=30mm},%
        level 2/.style={sibling distance=15mm,level distance=30mm},%
        proba/.style={midway,font=\scriptsize,fill=colorTer!5,inner sep=1pt}
      ]
      \node[inner sep=0pt]{}
      child {node{$R$}
          child {node {$E$} edge from parent node[proba] {$0,8$}}
          child {node {$\overline{E}$} edge from parent node[proba] {$0,2$}}
          edge from parent node[proba] {$0,1$} }
      child {node{$\overline{R}$}
          child {node {$E$} edge from parent node[proba] {$0,6$}}
          child {node {$\overline{E}$} edge from parent node[proba] {$0,4$}}
          edge from parent node[proba] {$0,9$}};
    \end{tikzpicture}
  \end{center}
  On souhaite calculer $P(E)$. D'après la formule des probabilités totales, on a~:
  \[P(E) = P(R\cap E) + P(\overline R\cap E)\]
  L'événement $R\cap E$ est au bout de la branche
  \tikz[baseline=(R.base)] \draw (0,0) node[anchor=base,left] (R) {$R$} -- ++(2,0) node[anchor=base,right]{$E$};
  donc \[P(R\cap E)=P(R)\times P_R(E)\]

  L'événement $\overline{R} \cap E$ est au bout de la branche
  \tikz[baseline=(R.base)] \draw (0,0) node[left] (R) {$\overline{R}$} -- ++(2,0) node[right] {$E$};
  donc \[P(\overline{R}\cap E)=P(\overline{R})\times P_{\overline{R}}(E)\]
  Il vient donc~:
  \begin{align*}
    P(E) & = 0,1\times0,8 + 0,9\times0,6 \\
    P(E) & = 0,08 + 0,54                 \\
    P(E) & = 0,62                        \\
  \end{align*}
  La probabilités que la France importe une partie de son énergie électrique l'hiver est de $62\,\%$.
\end{methode}


\section{Événements indépendants}

\begin{definition}[Événements indépendants]\index{Probabilités!Événements indépendants}
  Soient $A$ et $B$ deux événements de probabilité non nulle. $A$ et $B$ sont dits \textbf{indépendants} lorsque :
  \[ P_B(A) = P(A) \qquad \text{ ou } \qquad P_A(B) = P(B)\]
  Cela revient à dire que la réalisation de l'un des deux événements n'influence pas la probabilité de la réalisation de l'autre.
\end{definition}

\begin{methode}[Montrer que des événements sont indépendants (1)]\label{tstl:cours:probas:meth:independants}
  \textbf{Énoncé}\quad -- \quad Une usine fabrique des cafetières.
  Ces cafetières présentent deux types de défaut : défaut \textit{mécanique} ou défaut \textit{électrique}.

  On sait que $3\,\%$ des cafetières produites présentent le défaut mécanique
  et que $5\,\%$ présentent le défaut électrique.
  Enfin, $0,15\,\%$ des cafetières présentent les deux défauts.

  On prélève au hasard une cafetière à la sortie de la chaîne de production
  (chaque cafetière a la même probabilité d'être choisie). On définit les événements suivants :

  \begin{quote}

    $M$ : \textit{\og{}La cafetière présente le défaut mécanique\fg{}} \qquad
    et \qquad $E$ : \textit{\og{}La cafetière présente le défaut électrique\fg{}}

  \end{quote}
  \begin{enumerate}
    \item Traduire les données de l'énoncé sous forme de probabilités.
    \item Les événements $M$ et $E$ sont-ils indépendants ? Justifier.
  \end{enumerate}
  \bigskip

  \textbf{Solution}\quad -- \quad
  \begin{enumerate}
    \item Avec le texte, on a~:
          $P(M) = 0,03 \qquad P(E) = 0,05 \qquad P(M \cap E) = 0,15 \% = 0,0015$.
    \item On a :
          \begin{align*}
            P_E(M) & = \frac{P(E\cap M)}{P(E)} & P(M) & = 0,03 \\
            P_E(M) & = \frac{0,0015}{0,05}                     \\
            P_E(M) & = 0,3                                     \\
          \end{align*}
          On constate que $P(M)=P_E(M)$. On en déduit que les événements $M$ et $E$ sont indépendants.
  \end{enumerate}
\end{methode}

\begin{propriete}
  Soient $A$ et $B$ des événements de probabilité non nulle. Alors $A$ et $B$ sont \textbf{indépendants}
  si et seulement si :
  \[ P(A \cap B) = P(A) \times P(B)\]
\end{propriete}

\begin{remarque}
  Lorsque les événements $A$ et $B$ sont indépendants on a aussi~:
  \begin{multicols}{3}
    $A$ et $\overline{B}$ indépendants.

    $\overline{A}$ et $B$ indépendants.

    $\overline{A}$ et $\overline{B}$ indépendants.
  \end{multicols}
  \faExclamationTriangle\quad Attention de ne pas confondre \textit{indépendants} avec \textit{incompatibles}.
\end{remarque}

\begin{methode}[Montrer que des événements sont indépendants (2)]
  On reprend la situation exposée dans la méthode \ref{tstl:cours:probas:meth:independants}. Montrons d'une autre façon que les événements $M$ et $E$ sont indépendants.
  \medskip

  On sait que :
  \begin{align*}
    P(M) \times P(E) & = 0,03 \times 0,05 & P(M \cap E) & = 0,0015 \\
                     & =0,0015                                     \\
  \end{align*}

  Donc : $P(M) \times P(E) = P(M \cap E)$. Les événements $M$ et $E$ sont donc indépendants.
\end{methode}

