\chapter{Logarithme décimal}

\section{Définition}

\begin{definition}

  Pour tout nombre $b >0$, l'équation $10^{x} = b$ possède une unique solution $x$ dans $\R$.

  Cette solution est appelée \textbf{logarithme décimal} de $b$, noté $\log(b)$.
\end{definition}

\begin{exemple}
  \begin{itemize}
    \item $10^x = \num{100000} \iff x = 5$ \qquad ainsi \quad $\log(\num{100000}) = 5$
    \item $10^x = 23 \iff  x = \log(23)$. À l'aide de la calculatrice, on trouve $x \approx 1,362$.
  \end{itemize}
\end{exemple}

\begin{propriete}\label{TSTL:cours:logdecimal:prop:immediate}
  \begin{enumerate}
    \item Pour tout $x\in\R$ et $y\in\R[+*]$, on a \[\log(y)=x \Longleftrightarrow y=10^x\]
    \item Pour tout $x\in\R$ , on a \[\log\left(10^x\right)=x\]
    \item Pour tout $x\in\R[+*]$, on a \[10^{\log(x)}=x\]
    \item On a les égalités suivantes :
          \begin{align*}
            \log(1) & = 0 & \log(10) & = 1 & \log(10^2) & = 2 &  & \text{etc.} \\
          \end{align*}
  \end{enumerate}
\end{propriete}

\begin{exemple}
  \begin{align*}
    \log(\num{100000}) & = 5 & \log\left(10^{-3}\right) & = -3
  \end{align*}
\end{exemple}

\section{Étude de la fonction $\log$}
\subsection{Définition de $x\mapsto\log(x)$}

\begin{definition}
  La fonction \textbf{logarithme décimal}\index{Logarithme Décimal} est la fonction notée $\log$
  qui, à tout réel $b$ strictement positif associe l'unique solution de l'équation $10^x=b$.
  \begin{align*}
    \log : \interoo{0 +\infty} & \longrightarrow \R                                            \\
    b                          & \longmapsto \log(b) &  & \leftarrow \text{solution de }10^x=b
  \end{align*}
\end{definition}

\subsection{Variations}

\begin{propriete}
  La fonction logarithme décimal est une fonction \textbf{strictement croissante} sur \interoo{0 +\infty}.
\end{propriete}

\begin{propriete}\label{TSTL:cours:logdecimal:prop:transfo}
  Pour tous les nombres $x$ et $y$ strictement positifs, on a :
  \begin{align*}
    \log(x)=\log(y) & \Longleftrightarrow x = y & \log(x)<\log(y) & \Longleftrightarrow x < y
  \end{align*}
\end{propriete}

\begin{methode}[Résoudre une équation incluant la fonction $\log$]
  \textbf{Énoncé}\quad Résoudre l'équation $\log(2x-1) = 2$.\bigskip

  \textbf{Solution}\quad Pour résoudre des équations contenant des logarithmes décimaux, on doit :
  \begin{enumerate}[label = \textcolor{colorTer}{\textbf{(\alph*)} }]
    \item Se poser la question du domaine de résolution (la fonction $\log$ n'étant définie que sur \interoo{0 +\infty})
    \item Utiliser la propriété \ref{TSTL:cours:logdecimal:prop:transfo} précédente et de la propriété \ref{TSTL:cours:logdecimal:prop:immediate} pour se débarrasser du $\log$.
  \end{enumerate}

  Ici, on peut dire :
  \begin{itemize}[label=\raisebox{0.2ex}{\textcolor{colorTer}{\tiny \faCircle}}]
    \item La fonction $x\longmapsto\log(2x-1)$ est définie sur \interoo{0 +\infty}
          donc il faut $2x-1>0$, c'est-à-dire $x>\frac{1}{2}$.

          On peut donc résoudre cette équation sur l'intervalle \interoo{\frac{1}{2} +\infty}.
    \item Résolvons l'équation :
          {\footnotesize\begin{align*}
            \log(2x-1) = 2 & \iff  \log(2x-1) = \log\left(10^2\right) \\
                           & \iff  2x-1 = 10^2                        \\
                           & \iff 2x = 10^2 + 1                       \\
                           & \iff x = \frac{10^2 + 1}{2}              \\
                           & \iff x = \frac{101}{2}                   \\
          \end{align*}}
  \end{itemize}
  On a bien $\frac{101}{2} > \frac{1}{2}$ donc l'équation admet une solution : $\frac{101}{2}$.
\end{methode}

\subsection{Signe}

La fonction logarithme décimal est définie sur \interoo{0 +\infty}. Elle change de signe pour $x=1$.

\begin{center}
  \begin{tikzpicture}
    \tkzTabInit[lgt=1.5,espcl=2.5]{%
      $x$		/0.8,%
      {\footnotesize Signe de $\log(x)$}    /1.5%
    }%
    {$0$, $1$ , $+\infty$}%
    \tkzTabLine{ d,-,z,+, }
  \end{tikzpicture}
\end{center}

\subsection{Représentation graphique}

\begin{center}
  \begin{tikzpicture}
    \begin{axis}[%
        axis y line=center,axis x line=middle,        % centrer les axes
        x=1cm,y=1cm,                                  % unités sur les axes
        xmin=-0.4,xmax=15.4,ymin=-2.7,ymax=2.7,        % bornes du repère
        xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
        minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
        grid=both,                                    % indiquer quelle geilles afficher
        tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
      ]
      \addplot[mark=none,colorSec,very thick,domain=0.001:15.4,samples=150] {log10(x)};
    \end{axis}
  \end{tikzpicture}
\end{center}

\section{Propriétés algébriques}

\begin{propriete}[Propriété fondamentale des logarithmes]\index{Logarithme Décimal!Propriété fondamentale}
  Pour tous les réels $a$ et $b$ strictement positifs, on a \[\log(ab) = \log(a) + \log(b)\]
\end{propriete}

\begin{remarque}
  Le logarithme décimal transforme les produits en somme.
\end{remarque}

\begin{exemple}
  Utilisons la propriété fondamentale des logarithmes pour simplifier $A=\log(800)$.
  \begin{align*}
    A & = \log(8\times100)                \\
    A & = \log(8)+\log(100)               \\
    A & = \log\left(2^3\right)+\log(10^2) \\
    A & = 3\log(2)+2\log(10)              \\
    A & = 3\log(2)+2\times1               \\
    A & = 3\log(2)+2
  \end{align*}
\end{exemple}

\begin{propriete}
  Soient $a$ et $b$ deux réels strictement positifs, $n$ un entier naturel et $x$ un réel.

  \begin{align*}
    \log\left(a^n\right)         & = n\log(a) & \log\left(a^x\right)         & = x\log(a)          \\
    \log\left(\frac{1}{b}\right) & = -\log(b) & \log\left(\frac{a}{b}\right) & = \log(a) - \log(b)
  \end{align*}
\end{propriete}

\begin{methode}[Résoudre une équation où l'inconnue est une puissance]
  \textbf{Énoncé}\quad Résoudre dans\interoo{0 +\infty} l'équation $x^5 = 3$. \bigskip

  \textbf{Solution}\quad Étant donné que $x$ est strictement positif, on peut appliquer
  le logarithme décimal à chaque côté de l'équation (propriété \ref{TSTL:cours:logdecimal:prop:transfo}) :
  \[ \log\left(x^5\right) = \log(3)\]
  En utilisant la propriété des logarithmes qui dit que $\log(a^x) = x\log(a)$, on obtient :
  \[5\log(x) = \log(3)\]
  On divise les deux côtés par $5$ pour isoler $\log(x)$ :
  \begin{align*}
    \log(x) & = \frac{1}{5}\log(3)               \\
    \log(x) & = \log\left(3^{\frac{1}{5}}\right) \\
  \end{align*}
  Enfin, en utilisant la propriété \ref{TSTL:cours:logdecimal:prop:transfo}, on obtient :
  \[x = 3^{\frac{1}{5}}\]
\end{methode}

\begin{methode}[Résoudre une équation où l'inconnue est un exposant]
  \textbf{Énoncé}\quad Résoudre dans \R l'équation $6^x = 3$.\bigskip

  \textbf{Solution}\quad On applique le logarithme décimal à chaque côté de l'équation (propriété \ref{TSTL:cours:logdecimal:prop:transfo}) :
  \[\log\left(6^x\right) = \log(3)\]
  En utilisant la propriété des logarithmes qui dit que $\log\left(a^x\right) = x\log(a)$, on obtient :
  \[x\log(6) = \log(3)\]
  On divise les deux côtés par $\log(6)$ pour isoler $x$ :
  \[x = \frac{\log(3)}{\log(6)}\]
\end{methode}