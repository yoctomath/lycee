% \usepackage[suite]{tdsfrmath}
% \usepackage{customcommands}
\chapter{Suites arithmétiques}

\section{Définitions}
\subsection{Principe}
Une suite arithmétique est une suite infinie de nombres avec un écart constant entre chacun d'eux.
\begin{center}
  \begin{tikzpicture}[fleche/.style={-latex',black!40},comment/.style={midway,above,colorSec,font=\scriptsize}]
    \pgfmathsetlengthmacro{\TermSep}{25}
    \node (u0) at (0,0) {$u_0$};
    \foreach \i [remember=\i as \lasti (initially 0)] in {1,...,4}{%
      \node[right=\TermSep of u\lasti] (u\i) {$u_\i$};
      \draw [fleche] (u\lasti) to[out=30,in=150] node[comment] {$+r$} (u\i);
    }
    \node[right=6*\TermSep of u4] (un) {$u_n$};
    \node[right=\TermSep of un] (unp1) {$u_{n+1}$};
    \node[right=\TermSep of u4] (u5) {\phantom{$u_5$}};
    \node[left=\TermSep of un] (unm1) {\phantom{$u_5$}};
    \node[right=\TermSep of unp1] (unp2) {$\ldots$};
    \draw [fleche] (u4) to[out=30,in=150] node[comment] {$+r$} (u5);
    \draw [fleche] (unm1) to[out=30,in=150] node[comment] {$+r$} (un);
    \draw [fleche] (un) to[out=30,in=150] node[comment] {$+r$} (unp1);
    \draw [fleche] (unp1) to[out=30,in=150] node[comment] {$+r$} (unp2);
    \draw [fleche,looseness=0.2] (u0) to[out=-30,in=210] node[comment,below] {$+ n \times r$} (un);
    \draw [dotted,very thick] (u5) -- (unm1);
  \end{tikzpicture}
\end{center}

\subsection{Définition par récurrence}
Des considérations précédentes, il vient la définition suivante où les termes de la suite arithmétique sont définis en fonction du terme précédent.
\begin{definition}[Définition par récurrence d'une suite arithmétique]\index{Suites!Arithmétiques!Définition par récurrence}
  Une suite \suite est dite \textbf{arithmétique} s'il existe un réel $r$ tel que, pour tout entier naturel $n$, on a \[u_{n+1}=u_n+r\]
  Le nombre $r$ est appelé la \textbf{raison} de la suite et $u_0$ le \textbf{premier terme}.
\end{definition}

\begin{exemple}
  La suite définie ci-dessous est une suite arithmétique de premier terme $5$ et de raison $3$.
  \[\begin{cases}
    u_0 = 5\\
    u_{n+1} = u_n + 3
  \end{cases}\]
\end{exemple}

\subsection{Définition explicite}

\begin{propriete}[Définition explicite d'une suite arithmétique]\label{TSTL:cours:suitesarithmetiques:prop:explicite}\index{Suites!Arithmétiques!Définition explicite}
  Soit \suite une suite arithmétique de raison $r$ et de premier terme $u_0$. Pour tout entier naturel $n$, on a \[u_n=u_0+rn\]
  De même, si $u_1$ est le premier terme de la suite, alors on a \[u_n=u_1+r(n-1)\]
\end{propriete}

\begin{exemple}
  Soit \suitar[w]{3}{5}. Pour tout $n \in \N$, on a $w_n=5+3n$.
\end{exemple}

\begin{remarque}
  L'avantage de la définition explicite d'une suite arithmétique réside dans le fait qu'on a pas besoin de connaître les termes précédent $u_n$ pour le calculer.
\end{remarque}

\begin{methodetwocols}[Calculer un terme d'une suite arithmétique]

    \textbf{Énoncé}\quad Soit \suitar{9}{-7}. Calculer $u_{33}$.\bigskip
    
    \textbf{Solution}\quad D'après la définition explicite \ref{TSTL:cours:suitesarithmetiques:prop:explicite} d'une suite arithmétique, on peut dire que
    \columnbreak
    \begin{align*}
      u_{33} &= u_0 + r \times 33\\
      u_{33} &= -7 + 9 \times 33\\
      u_{33} &= -7 + 297\\
      u_{33} &= 290\\
    \end{align*}
\end{methodetwocols}

\section{Variations}

\begin{propriete}\label{TSTL:cours:suitesarithmetiques:prop:variations}\index{Suites!Arithmétiques!Variations}
  Soit \suite une suite arithmétique de raison $r$.
  \begin{itemize}
    \item Si $r>0$ alors \suite est \textbf{croissante}.
    \item Si $r<0$ alors \suite est \textbf{décroissante}.
    \item Si $r=0$ alors \suite est \textbf{constante}.
  \end{itemize}
\end{propriete}

\paragraph*{\faCogs\quad Preuve} Pour connaître le sens de variation, étudions le signe de $u_{n+1} - u_n$ :
\begin{align*}
  u_{n+1} - u_n &=  u_n + r - u_n\\
  &= r
\end{align*}

Ainsi : $r>0 \Longleftrightarrow  u_{n+1} - u_n > 0 \Longleftrightarrow u_{n+1} > u_n$, c'est-à-dire que $(u_n)$ est croissante.

De même pour les deux autres cas de figure.

\begin{exemple}
  Soit \suitar[v]{-2}{12}. 
  
  On a une raison négative égale à $-2$ donc, d'après la propriété \ref{TSTL:cours:suitesarithmetiques:prop:variations} \suite[v] est décroissante.
\end{exemple}

\begin{remarque}
  Lorsqu'une système peut être modélisé en utilisant une suite arithmétique, on dira qu'il connaît une \textbf{croissance linéaire}.
\end{remarque}
\section {Représentations graphiques}
\begin{multicols}{3}\small
  Représentation graphique de \suitar{3}{-2} (croissante).
  \begin{center}
    \begin{tikzpicture}[scale=0.5]
      \begin{axis}[axis y line=center,%
          axis x line=middle,x=1cm,y=0.25cm,%
          xmin=-0.8,xmax=8.8,ymin=-3.7,ymax=20,%
          xtick distance=1,ytick distance =1,grid=both,%
          xticklabels=\empy, yticklabels=\empty]
        \addplot+[domain=0:7,only marks,colorPrim,mark=+,mark size=3pt,ultra thick,samples=8] {-2+3*x} node [pin=0:{$\suite$}]{};
        \coordinate[label=below:$1$] (I) at (1, 0);
        \coordinate[label=left:$1$] (J) at (0, 1);
      \end{axis}
    \end{tikzpicture}
  \end{center}

  Représentation graphique de \suitar[v]{-2}{12} (décroissante).
  \begin{center}
    \begin{tikzpicture}[scale=0.5]
      \begin{axis}[axis y line=center,%
          axis x line=middle,x=1cm,y=0.3cm,%
          xmin=-0.8,xmax=8.8,ymin=-3.7,ymax=13,%
          xtick distance=1,ytick distance =1,grid=both,%
          xticklabels=\empy, yticklabels=\empty]
        \addplot+[domain=0:7,only marks,colorSec,mark=+,mark size=3pt,ultra thick,samples=8] {12-2*x} node [pin=0:{$\suite[v]$}]{};
        \coordinate[label=below:$1$] (I) at (1, 0);
        \coordinate[label=left:$1$] (J) at (0, 1);
      \end{axis}
    \end{tikzpicture}
  \end{center}

  Représentation graphique de \suitar[v]{0}{7} (constate).
  \begin{center}
    \begin{tikzpicture}[every node/.append style={font=\scriptsize},scale=0.5]
      \begin{axis}[axis y line=center,%
          axis x line=middle,x=1cm,y=0.3cm,%
          xmin=-0.8,xmax=8.8,ymin=-3.7,ymax=13,%
          xtick distance=1,ytick distance =1,grid=both,%
          xticklabels=\empy, yticklabels=\empty]
        \addplot+[domain=0:7,only marks,colorTer,mark=+,mark size=3pt,ultra thick,samples=8] {7} node [pin=0:{$\suite[w]$}]{};
        \coordinate[label=below:$1$] (I) at (1, 0);
        \coordinate[label=left:$1$] (J) at (0, 1);
      \end{axis}
    \end{tikzpicture}
  \end{center}
\end{multicols}

\section{Moyenne arithmétique}
\begin{definition}\index{Suites!Arithmétiques!Moyenne arithmétique}\index{Moyenne!Arithmétique}
  \begin{itemize}
    \item Soit deux nombres réels $a$ et $b$. La \textbf{moyenne arithmétique} $M_a$ de $a$ et $b$ est donnée par \[\dfrac{a+b}{2}\]
    \item Soit $n$ nombres réels $x_1 , x_2, \ldots , x_n$. La \textbf{moyenne arithmétique} $M_a$ de ces $n$ nombres est donnée par \[\dfrac{x_1+x_2+\ldots+x_n}{n}\]
  \end{itemize}
\end{definition}

\begin{remarque}
  La moyenne arithmétique correspond à l'idée qu'on a de la moyenne traditionnellement.
\end{remarque}

\section{Somme des premiers termes}

\begin{propriete}[Somme des premiers termes]\label{TSTL:cours:suitesarithmetiques:prop:somme}\index{Suites!Arithmétiques!Somme premiers termes}
  Soit \suite une suite arithmétique. Pour tout $n \in \N$, on a\footnotemark :
  \[\sum_{k=0}^n u_k=(n+1)\frac{u_0+u_n}{2}\]
\end{propriete}
\footnotetext{$\sum\limits_{k=0}^n u_k$ est une notation pour $u_0+u_1+\ldots+u_n$.}

\begin{remarque}
  On peut retenir cette formule en remarquant que cette somme des premiers termes d'une suite arithmétique peut se décrire en français comme~:
  \begin{itemize}
    \item $\text{nombre de termes}\times\dfrac{\text{premier terme}+\text{dernier terme}}{2}$
    \item $\text{nombre de termes}\times\text{moyenne des extrêmes}$
  \end{itemize}
\end{remarque}

\paragraph*{\faCogs\quad Preuve} Soit $n \in \N$. Appelons $S$ la somme des termes d'une suite \suite jusqu'au rang $n$.

D'après la définition explicite \ref{TSTL:cours:suitesarithmetiques:prop:explicite} d'une suite arithmétique, on peut affirmer que $u_n=u_0+rn$.

On peut donc écrire~:
\begin{align*}
  S &= u_0 + u_1 + u_2 + u_3 + \ldots + u_n  & &\\
  S &= u_0 + u_0 + r \times 1 + u_0 + r \times 2 + u_0 + r \times 3 + \ldots + u_0 + r \times n  & &\longleftarrow \text{on applique la définition explicite}\\
  S &= (n+1) \times u_0 + r \times 1  + r \times 2  + r \times 3 + \ldots  + r \times n   & &\longleftarrow \text{on regroupe les termes }u_0\\
  S &= (n+1) \times u_0 + r \times (1  + 2  + 3 + \ldots  + n)  & &\longleftarrow \text{on factorise par }r\\
\end{align*}
\begin{align*}
  S &= (n+1) \times u_0 + r \times \frac{n\times(n+1)}{2}  & &\longleftarrow \text{formule de la somme des premiers entiers}\\
  S &= (n+1) \times \left(u_0 + r \times \frac{n}{2}\right)  & &\longleftarrow \text{on factorise par }n+1\\
  S &= (n+1) \times \left(\frac{u_0 \times 2 + r \times n}{2}\right)  & &\\
  S &= (n+1) \times \left(\frac{u_0 + \Mhl{u_0 + r \times n}}{2}\right)  & &\\
  S &= (n+1) \times \left(\frac{u_0 + u_n}{2}\right)  & &\longleftarrow u_n=u_0 + r \times n\\
\end{align*}

\begin{methode}[Calculer la somme des premiers termes d'une suite arithmétique]
  \textbf{Énoncé}\quad On donne \suitar{2}{1}.
  
  Calculer la somme des $100$ premiers termes\bigskip
  
  \textbf{Solution}\quad On a $u_0=1$. On a besoin de calculer le dernier terme de la somme 
  soit la valeur de $u_{99}$ (si on commence à compter à $0$ alors pour avoir $100$ termes, il faut s'arrêter à $99$).
   D'après la définition explicite \ref{TSTL:cours:suitesarithmetiques:prop:explicite} d'une suite arithmétique, on peut dire que
   \begin{align*}
     u_{99} &= 1 + 2 \times 99\\
     u_{99} &= 1 + 198\\
     u_{99} &= 199\\
   \end{align*}
   Soit $\mathcal{S}_{100}$ la somme des $100$ premiers termes de cette suite. D'après la propriété \ref{TSTL:cours:suitesarithmetiques:prop:somme},
   on peut dire que~:
   \begin{align*}
    \mathcal{S}_{100} &= 100 \times \frac{1+199}{2}\\
    \mathcal{S}_{100} &= 100 \times 100\\
    \mathcal{S}_{100} &= \num{10000}\\
   \end{align*}
\end{methode}