\chapter{Fonction exponentielle base \E}

\section{Mise en place}

\subsection{Une définition sous contrainte}
\begin{propriete}
  Il existe une unique fonction exponentielle $x \mapsto a^x$ dont la courbe représentative
  admet une tangente en 0 de pente égale à 1.

  La base de cette fonction exponentielle est notée $\E$.

  On a : $\E \approx \num{2,71828}\ldots$ (ce nombre est irrationnel).
\end{propriete}

\begin{definition}\index{Fonction!Exponentielle base $\E$}
  La fonction exponentielle de base $\E$ est appelée la \og{}\textit{fonction exponentielle}\fg.

  On la note $\exp : x \mapsto \E^x$.
\end{definition}

\subsection{Représentation graphique}


\begin{multicols}{2}
  \begin{tikzpicture}[declare function={f(\x) = exp(x);}]
    \begin{axis}[%
        axis y line=center,axis x line=middle,        % centrer les axes
        x=0.6cm,y=0.6cm,                                  % unités sur les axes
        xmin=-5.4,xmax=5.4,ymin=-0.7,ymax=5.4,        % bornes du repère
        xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
        minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
        grid=both,                                    % indiquer quelle geilles afficher
        tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
      ]
      \addplot[mark=none,colorSec,very thick,domain=-5.4:5.4,samples=150] {f(x)};
      \addplot[mark=none,colorPrim,very thick,domain=-5.4:5.4,samples=150] {x+1};
      \draw[densely dashed,thick,colorSec] (0,e) coordinate(E) -- (1,e) -- (1,0);
      \node[left=1cm of E,colorSec] (Evalue) {$\E\approx \num{2,71828}$};
      \draw[colorSec,-latex'] (Evalue) -- (E);
      \node[below right,colorPrim] at (3,4) {$T$};
      % \addplot[mark=none,only marks,samples at={-2.5},colorSec,nodes near coords=$T$,nodes near coords style={anchor={east},font={\large}}] {x+1};
    \end{axis}
  \end{tikzpicture}
  \columnbreak

  \vspace*{\fill}
  Le coefficient directeur de la tangente $T$ en $0$ est égal à $1$, c'est-à-dire $\exp'(0) = 1$.
  \vspace*{\fill}
\end{multicols}

\subsection{Conséquences algébriques et fonctionnelles}

La fonction $x\mapsto\E^x$ est une fonction exponentielle donc à ce titre elle hérite des propriétés des fonctions $x\mapsto a^x$ (avec $a>0$).

\begin{propriete}
  \begin{itemize}
    \item La fonction exponentielle est \textbf{strictement croissante} sur \R.
    \item Pour tout $x\in\R$, on a $\E^x>0$.
    \item On a des valeurs remarquables :
          \begin{align*}
            \E^0 & = 1 & \E^1 & = \E & \E^{-1} & = \frac{1}{\E}
          \end{align*}
    \item Pour tout $x,y\in\R$ et pour tout $n\in\Z$, on a :
          \begin{align*}
            \E^{x+y} & = \E^x \times \E^y & \E^{-x} & = \frac{1}{\E^x} & \E^{x-y} & = \frac{\E^x}{\E^y} & \left(\E^x\right)^n & = \E^{n \times x}
          \end{align*}
  \end{itemize}

\end{propriete}

\subsection{Variations et limites}

\begin{propriete}
  Pour tous les nombres $a,b\in\R$
  \begin{itemize}
    \item $\E^a=\E^b$ si et seulement si $a=b$.
    \item $\E^a<\E^b$ si et seulement si $a<b$.
    \item $\E^a>\E^b$ si et seulement si $a>b$.
  \end{itemize}
\end{propriete}

\begin{propriete}\index{Limites!Fonction exponentielle}
  On a les limites suivantes (pour tout entier $n\pgq 1$) :
  \begin{align*}
    \lim_{x\to+\infty}\E^x &= +\infty & \lim_{x\to-\infty}\E^x &= 0 \\
    \lim_{x\to+\infty}\frac{\E^x}{x^n} &= +\infty & \lim_{x\to+\infty}x^n\E^{-x} &= 0
  \end{align*}
\end{propriete}


La fonction exponentielle admet le tableau de variation suivant :

\begin{center}
  \begin{tikzpicture}
    \tkzTabInit[lgt=2.5,espcl=5]%
    {$x$		/0.8,
      {\footnotesize Signe de $\exp'(x)$}		/0.8,
      {\footnotesize Variations de $\exp$}    /1.5}%
    {$-\infty$, $+\infty$}%
    \tkzTabLine{ ,+, }
    \tkzTabVar{		-/$0$ ,
      +/$+\infty$ 	}
    \tkzTabVal[draw]{1}{2}{0.33}{0}{1}
    \tkzTabVal[draw]{1}{2}{0.67}{1}{$\E$}
  \end{tikzpicture}
\end{center}

\begin{exemple}
  On a $\lim\limits_{x\to+\infty}x^5\E^{-x}=0$.
\end{exemple}

\section{Dérivées de $x\mapsto\E^x$ et $x\mapsto\E^{u(x)}$}

\begin{propriete}\index{Dérivée!Exponentielle base $\E$}
  La fonction exponentielle est dérivable sur $\R$ et elle est égale à sa fonction dérivée :
  \[\exp'(x) = \exp(x)\]
\end{propriete}

\begin{exemple} Voici deux exemples d'utilisation :
  \begin{itemize}
    \item Soit $f : x \mapsto 2\E^x - 4x + 7$ définie et dérivable sur $\R$. 
    
    Pour $x \in \R$, on a $f'(x)=2\E^x-4$.
    \item Soit $g : x \mapsto x\E^x$ définie et dérivable sur \R.
    
    Pour tout $x \in \R$, on a :
    \begin{align*}
      g'(x) &= \E^x + x\E^x\\
      g'(x) &= (x+1)\E^x
    \end{align*}
  \end{itemize}
\end{exemple}

\begin{propriete}
  Soit $u$ une fonction dérivable sur l'intervalle $\mathtt{I}$. 
  \medskip

  La fonction $x\mapsto \E^{u(x)}$ est alors dérivable sur $\mathtt{I}$ et on a :
  \[\left(\E^{u(x)}\right)'=u'(x)\times \E^{u(x)}\]
\end{propriete}

\begin{exemple}
  Soit la fonction $f : x \mapsto \E^{2x^3+1}$ définie et dérivable sur \R. En posant $u(x)=2x^3+1$, on a $f(x)=\E^{u(x)}$.
\begin{align*}
  u'(x) &= 2 \times 3x^2 & f'(x) &= u'(x) \times \E^{u(x)}\\
  u'(x) &= 6x^2 & f'(x) &= 6x^2\times \E^{2x^3+1}
\end{align*}
\end{exemple}

