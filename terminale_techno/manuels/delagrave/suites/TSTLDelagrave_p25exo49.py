u1 = 4    # premier terme
r = 0.5   # raison
n = 1     # compteur
u = u1    #terme courant
# on demande à l'utilisateur le rang du terme à calculer
fin = int(input("Rang du terme à calculer ? "))
while n <= fin:
  u = u + r
  n = n + 1
print("Le terme u{0} est {1}".format(fin,u))
