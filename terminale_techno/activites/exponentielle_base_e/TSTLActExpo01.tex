Le but de cette activité est de construire la courbe représentative d'une fonction $f$ vérifiant les propriétés suivantes :
\begin{itemize}
  \item $f(0) = 1$ ;
  \item Pour tout nombre réel $a$, on a $f'(a)=f(a)$.
\end{itemize}

\section{Méthode d'Euler}
La \textbf{méthode d'Euler} utilise une approximation par une fonction affine d'une fonction en un point
et donc permet de construire la courbe $\mathcal{C}_f$ représentative de $f$ point par point.

\subsection{Obtention d'un premier point}
\faHandPointRight[regular]\quad On a déjà $A\nuplet{0 1}\in \mathcal{C}_f$ qui est un point de la courbe car $f(0)=1$.
\medskip


\begin{multicols}{2}
  Appelons $B$ le point de la courbe $\mathcal{C}_f$ d'abscisse $0,1$.

  Comme $B$ est proche du point $A$, la droite $(AB)$ est proche de la tangente $\mathcal{T}_A$
  à la courbe $\mathcal{C}_f$ en $A$ (représentée en pointillés sur le graphique ci-contre).
  \medskip



  \begin{enumerate}
    \item La pente de la tangente $\mathcal{T}_A$ égale à $f'(0)$, la dérivée de $f$ en~$0$.

          Quelle est cette valeur ? \pointilles
          \medskip

    \item Quel est l'expression du coefficient directeur de la droite $(AB)$ ?

          \pointilles
  \end{enumerate}

  \begin{center}
    \begin{tikzpicture}[spy using outlines={circle, magnification=4, size=3cm, connect spies}]
      \begin{axis}[%
          axis y line=center,axis x line=middle,        % centrer les axes
          x=20cm,y=20cm,                                  % unités sur les axes
          xmin=-0.12,xmax=0.15,ymin=0.87,ymax=1.2,        % bornes du repère
          xtick distance=0.1, ytick distance=0.1,           % distance entre les tirets sur les axes
          minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
          grid=both,                                    % indiquer quelle geilles afficher
          tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
        ]
        \addplot[mark=none,colorSec,very thick,domain=-0.12:0.15,samples=150] {exp(x)};
        \addplot[mark=none,colorPrim,dashed,thick,domain=-0.12:0.15,samples=150] {x+1};
        \addplot[mark=+,only marks,samples at={0},colorSec,nodes near coords=$A$,nodes near coords style={anchor={north west}}] {exp(x)};
        \addplot[mark=+,only marks,samples at={0.1},colorSec,nodes near coords=$B$,nodes near coords style={anchor={south}}] {exp(x)};
        \addplot[mark=+,only marks,samples at={0.14},colorSec,nodes near coords=$\mathcal{C}_f$,nodes near coords style={anchor={south east}}] {exp(x)};
        \addplot[mark=none,only marks,samples at={-0.1},colorPrim,nodes near coords=$\mathcal{T}_A$,nodes near coords style={anchor={north west}}] {x+1};
        \spy [black]  on (4.5,4.8)   in node at (6,2.3);
        % \draw[colorTer,very thick,densely dashed] (-1,-2.7) -- (-1,2.4);
      \end{axis}
    \end{tikzpicture}
  \end{center}
\end{multicols}

\begin{enumerate}
  \setcounter{enumi}{2}
  \item En utilisant le fait que la pente de la droite $(AB)$ est \textit{approximativement} égale
        à celle de la tangente $\mathcal{T}_A$, déterminez une valeur approchée de $f(0,1)$ et donc les coordonnées du point $B$.

          {\doublespacing
            \pointilles

            \pointilles

            \pointilles

          }
\end{enumerate}

\medskip
\subsection{Obtention d'un deuxième point}
On considère à présent le point $C$ d'abscisse $0,2$.

Comme précédemment, la droite $(BC)$ est approximativement tangente $\mathcal{T}_B$ à la courbe $\mathcal{C}_f$ au point $B$.

En égalant les coefficient directeur de ces deux droites, déterminez une valeur approchée de $f(0,2)$ et donc les coordonnées du point $C$.


  {\doublespacing
    \pointilles

    \pointilles

    \pointilles

  }
\subsection{Tableau de valeurs}

Compléter le tableau suivant. Si besoin, effectuer au brouillon les calculs pour les trois dernières colonnes
ou mettre en évidence un moyen rapide de calculer les images demandées.

\begin{center}
  \begin{tblr}{width=\linewidth,colspec={*{7}{X[c,1]}},hlines,vlines,stretch=2}
    $x$      & $0$ & $0,1$ & $0,2$ & $0,3$ & $0,4$ & $0,5$ \\
    $y=f(x)$ & $1$ &       &       &       &       &       \\
  \end{tblr}
\end{center}

\subsection{Cas général}\label{cas-gen}
On considère ici le point $P\nuplet{a f(a)}$ de la courbe représentative de $f$.

\begin{itemize}
  \item Quelle approximation va-t-on alors prendre pour $f(a+0,1)$ ?

        \pointilles

        \pointilles

  \item En déduire une formule générale pour $f(a+h)$ en fonction de $f(a)$ et de $h$.

        \pointilles

        \pointilles

\end{itemize}

\section{Utilisation d'une feuille de calcul}

Dans cette partie, on va automatiser dans une feuille de calcul les calculs précédents afin de construire point par point
la courbe représentative de $f$ sur l'intervalle \interff{0 1}.
\medskip

On nomme $M\nuplet{x_M y_M}$ un point de la courbe $\mathcal{C}_f$ représentative de $f$.

\begin{enumerate}
  \item Créer la feuille de calcul ci-dessous.
  \begin{center}
    \begin{tikzpicture}
      \tableur[6]{A,B,C,D}
      \celtxt*[align=center]{A}{1}{$a$}
      \celtxt*[align=center]{B}{2}{0}
      \celtxt*[align=center]{D}{2}{1}
      \celtxt*[align=center]{A}{3}{0}
      \celtxt*[align=center]{B}{3}{0,1}
      \celtxt*[align=center]{C}{3}{1}
      \celtxt*[align=center]{D}{3}{1,1}
      \celtxt*[align=center,font=\bfseries]{B}{1}{$x_M$}
      \celtxt*[align=center,font=\bfseries]{C}{1}{$f(a)$}
      \celtxt*[align=center,font=\bfseries]{D}{1}{$y_M$}
      \selecCell{B}{4}
    \end{tikzpicture}
  \end{center}
  \item Dans la cellule \texttt{A4}, entrer la formule \texttt{=A3+0,1}. et utiliser la poignée de copie
        pour étirer la formule jusqu'à la cellule \textit{A12}.
  \item Quelle formule faut-il entrer dans la cellule \texttt{B4} afin d'obtenir toutes les valeurs de $0,2$ à $1$
        dans la colonne \texttt{B} ? \pointilles
  \item En utilisant les résultats de la partie \ref{cas-gen}, donner les formules à saisir dans les cellules 
  \texttt{C4} et \texttt{D4} qui seront étenbdues jusqu'à la ligne 12 :
  \begin{itemize}
    \item Formule en cellule \texttt{C4} : \pointilles
    \item Formule en cellule \texttt{D4} : \pointilles
  \end{itemize}
  \item Sélectionner \textbf{seulement} les colonnes \texttt{B} et \texttt{D} (utiliser la touche \keys{\ctrl})
  et faire un diagramme représentant les points obtenus.

  \item Compléter $f(1)\approx\pointilles[2cm]$
\end{enumerate}


\paragraph{Prolongement} Dans une nouvelle feuille de calcul, utiliser un pas de $0,01$  au lieu de $0,1$
pour obtenir une approximation graphique plus précise.