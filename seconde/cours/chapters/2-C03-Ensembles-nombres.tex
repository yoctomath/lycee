\chapter{Ensembles de nombres}

\section{Différent types de nombres}

\begin{definition}\index{Nombre!Entier naturel}
  Un nombre \textbf{entier naturel} est un nombre entier positif ou nul.
  \medskip

  L'ensemble des nombres entiers naturels est noté \N.

  \[\N = \anuplet{0 1 2 3 \ldots{} \num{10000} \num{10001} \ldots{}}\]
\end{definition}

\begin{exemple}
  On utilise les symboles $\in$ et $\notin$ pour désigner l'appartenance ou non à un ensemble.
  \begin{itemize}
    \item $4\in \N$. On lit \og{} \textit{$4$ appartient à l'ensemble des entiers naturels} \fg{}.
    \item $1,8\notin \N$. On lit \og{} \textit{$1,8$ n'appartient pas à l'ensemble des entiers naturels} \fg{}.
  \end{itemize}
\end{exemple}

\begin{definition}\index{Nombre!Entier relatif}
  Un nombre \textbf{entier relatif} est un nombre entier positif ou négatif.
  \medskip

  L'ensemble des nombres entiers relatif est noté \Z.

  \[\Z = \anuplet{\ldots{} -4 -3 -2 -1 0 1 2 3  \ldots{}}\]
\end{definition}

\begin{exemple}
  Des nombres relatifs :
  \begin{multicols}{2}
    \begin{itemize}
      \item $-9 \in \Z$ ;
      \item $4 \in \Z$ ;
      \item $0,25 \notin \Z$.
    \end{itemize}
  \end{multicols}
\end{exemple}

\begin{definition}\index{Nombre!Décimal}
  Un nombre \textbf{décimal}est un nombre qui \textit{peut} s'écrire
  sous la forme d'une \textbf{fraction décimale}, c'est-à-dire sous la forme
  $\dfrac{a}{10^n}$ où $a$ est un nombre entier relatif et $n$ un entier naturel.
  \medskip

  L'ensemble des nombres décimaux est noté $\mathbb{D}$.
\end{definition}

\begin{exemple}
  Pour décider si un nombre est décimal ou pas , il faut impérativement en changer l'écriture afin qu'elle
  soit sous la forme $\frac{a}{10^n}$ où $a$ est un entier relatif et $n$ un entier naturel.
  \begin{itemize}
    \item $7,239 \in \mathbb{D}$ car $7,239$ peut s'écrire $\frac{\num{7239}}{\num{1000}}$ soit $\frac{\num{7239}}{10}$.
    \item $\frac{3}{4} \in \mathbb{D}$ car $\frac{3}{4}=0,75$ donc $\frac{3}{4}=\frac{75}{100}$.
  \end{itemize}
\end{exemple}

\begin{definition}\index{Nombre!Rationnel}
  Un nombre \textbf{rationnel} est un nombre qui \textit{peut} s'écrire sous la forme d'un quotient
  $\dfrac{a}{b}$ où $a$ et $b$ sont des nombres entiers relatifs, $b$ étant différent de zéros.
  \medskip

  L'ensemble des nombres relatifs est noté \Q.
\end{definition}

\begin{definition}\index{Nombre!Réel}
  Un nombre \textbf{réel} est l'abscisse d'un point sur une droite \textit{numérique} (c'est-à-dire sur une droite
  munie d'un repère $(O\,;\,\mathtt{I})$).

  \begin{center}
    \begin{tikzpicture}
      \coordinate[label=above:$O$] (O) at (0, 0);
      \coordinate[label=above:$\mathtt{I}$] (I) at (1, 0);
      \coordinate (A) at (2.35, 0);
      \draw[thick,-latex'] (-5.2,0) -- (5.2,0);
      \foreach  \x in {-5,-4,...,5} \draw[thick] ([yshift=-2pt]\x,0) -- ([yshift=2pt]\x,0);
      \fill[colorSec] (A) circle (1.5pt) node[above] {$A$} node[below,pin={[black,inner sep=1pt,font=\scriptsize]-15:nombre réel}] (X) {$x$};
      \node[below=1mm of O] {$0$};
      \node[below=1mm of I] {$1$};
    \end{tikzpicture}
  \end{center}
  \medskip

  L'ensemble des nombres réels est noté \R.
\end{definition}

\begin{exemple}
  On peut visualiser tous les nombres réels sur un axe
  \begin{center}
    \begin{tikzpicture}[x=1.5cm]
      \draw[thick,-latex'] (-5.2,0) -- (5.2,0);
      \foreach  \x in {-4,...,4} {
          \draw[thick] ([yshift=-2pt]\x,0) node[below=1mm,font=\scriptsize] {$\x$} -- ([yshift=2pt]\x,0);
        }
      \foreach \nb/\name in {
      {{-8/3}}/$-\frac{8}{3}$,
      {{-sqrt(2)}}/$-\sqrt{2}$,
      -0.25/{$0,25$},
      {5/7}/$\frac{5}{7}$,
      2.5/${2,5}$,
      pi/$\pi$}
      \fill[colorTer] (\nb,0) circle (1.5pt) node[above] {\name};
    \end{tikzpicture}
  \end{center}
\end{exemple}


\begin{remarque}
  \begin{itemize}
    \item Un nombre décimal s'écrit avec un nombre \textbf{fini} de chiffres après la virgule.
    \item Un nombre rationnel peut voir un nombre \textbf{infini} de chiffres après la virgule mais ces chiffres se répètent
          avec une certaines \textbf{période} comme dans $1,343434\underline{34}\ldots$.
    \item \textbf{Tous les nombres} connus au collège sont des nombres réels. Mais il en existent d'autres\dots
  \end{itemize}
\end{remarque}

\begin{definition}\index{Nombre!Irrationnel}
  Les nombres qui ne peuvent pas s'écrire sous la forme d'une fraction sont des nombres \textbf{irrationnels}.
\end{definition}

\begin{exemple}
  Les racines carrés de nombres entiers qui ne sont pas des carrés sont des nombres irrationnels. Ils ne sont pas les seuls\dots
  \[\sqrt{5} \notin \Q \kern2cm \pi \notin \Q\]
\end{exemple}

\begin{remarque}
  On utilise aussi d'autres notations :
  \begin{itemize}
    \item $\R[*]$ pour désigner l'ensemble des nombres réels différents de $0$ (on dit aussi \og{}\textit{non nuls}\fg{}) ;
    \item $\R[+]$ est l'ensemble des nombres réels positifs ;
    \item $\R[-]$ est l'ensemble des nombres réels négatifs.
  \end{itemize}
\end{remarque}

\begin{propriete}\index{Inclusion}
  Tout entier naturel est aussi un entier relatif, un décimal, un rationnel et un réel.
  \[\N \subset \Z \subset \mathbb{D} \subset \Q \subset \R\]
\end{propriete}

\begin{remarque}
  Cette propriété est admise.
\end{remarque}

\begin{center}
  \begin{tikzpicture}
    \coordinate (a0) at (0.5, 1.5);
    \foreach \x/\y/\etiquette [count=\i] in {
    0/0/$0$,
    0.5/-1/$\sqrt{4}$,
    0.5/1/$7$,
    1/0.6/$1$,
    1.5/-0.3/$35$,
    3/1.1/$-1$,
    3.3/-0.5/$-14$,
    3.5/0.4/$-\sqrt{144}$,
    5/0/$\frac{3}{4}$,
    5.7/1.5/$\frac{9}{100}$,
    5.4/-1/{{$0,7$}},
    6/0.5/${{-109,423}}$,
    6.1/-1.5/$-\sqrt{0,64}$,
    7.8/-1/$-\frac{5}{13}$,
    8/0.7/$\frac{2}{7}$,
    7.6/1.7/$\frac{1}{3}$,
    8.5/1.5/$\sqrt{\frac{25}{49}}$,
    9.7/1.2/$\pi$,
    10/-1.6/$\sqrt{5}$,%
    10.1/0.3/$-1\sqrt{2}$,%
    } {
    \node (a\i) at (\x,\y) {\etiquette};
    }
    \node[fit= (a0) (a1) (a2) (a2) (a4) (a5),inner sep=10pt,rounded corners,draw,very thick] (N) {};
    \node[fit= (N) (a6) (a7) (a8),inner sep=10pt,rounded corners,draw,very thick] (Z) {};
    \node[fit= (Z) (a9) (a10) (a11) (a12) (a13),inner sep=10pt,rounded corners,draw,very thick] (D) {};
    \node[fit= (D) (a14) (a15) (a16) (a17),inner sep=10pt,rounded corners,draw,very thick] (Q) {};
    \node[fit= (Q) (a18) (a19) (a20),inner sep=10pt,rounded corners,draw,very thick] (R) {};
    \foreach \pt/\ensemble in {N/\N,Z/\Z,D/$\mathbb{D}$,Q/\Q,R/\R} \node[above=3mm,fill=white,inner sep=2pt] at (\pt.south east) {\large\ensemble};
  \end{tikzpicture}
\end{center}

\begin{methode}[Déterminer la nature d'un nombre]\index{Nature!d'un nombre}
  \textbf{Énoncé}\quad Déterminer la nature des nombres suivants :
  \[\frac{-84}{7}\kern2cm\frac{17}{4}\kern2cm\sqrt{\num{40401}}\]
  \bigskip

  \textbf{Solution}\quad Pour déterminer la nature d'un nombre, on cherche le plus \textit{petit} ensemble
  (au sens de l'inclusion comme sur le schéma précédent) auquel appartient ce nombre appartient.
  \begin{itemize}
    \item On calcule $\frac{-84}{7}$ et on a $-84\div7=-12$.

          On en déduit que $\frac{-84}{7}$ est un nombre entier relatif.
          \medskip

          Réponse : $\frac{-84}{7}\in\Z$.
    \item On calcule $\frac{17}{4}$ et on a $17\div4=4,25$. En écrivant $4,25=\frac{425}{100}$,
          on peut dire que $\frac{17}{4}$ est un nombre décimal.
          \medskip

          Réponse : $\frac{17}{4}\in\mathbb{D}$.
    \item On calcule $\sqrt{\num{40401}}=201$.
          On peut dire que $\sqrt{\num{40401}}$ est un nombre entier.
          \medskip

          Réponse : $\sqrt{\num{40401}}\in\N$.
  \end{itemize}
\end{methode}

\begin{methode}[Utiliser le raisonnement par l'absurde]\index{Raisonnement par l'absurde}
  \textbf{Énoncé}\quad Prouver que la fraction $\frac{1}{3}$ n'est pas un nombre décimal.

  \textbf{Solution}\quad Nous allons raisonner par l'\textbf{absurde}\footnotemark.

  \begin{itemize}
    \item Supposons que $\frac{1}{3}$ est un nombre décimal.
    \item Il existe alors $a\in\Z$ et $n\in\N$ tel que $\frac{1}{3}=\frac{a}{10^n}$.

          On peut donc dire que $10^n=3a$. En conséquence, $a$ étant un entier relatif, on peut affirmer
          que $10^n$ est un multiple de $3$.
    \item Or, la somme de tous les chiffres de $10^n$ vaut $1$ qui n'est pas divisible par $3$.

          En conséquence, on en déduit que $10^n$ \textit{n'est pas} un multiple de $3$. \textbf{Contradiction ~!}
    \item En conclusion, la donnée supposée au départ est \textit{fausse} : $\frac{1}{3}$ \textit{n'est pas} un nombre décimal.
  \end{itemize}
\end{methode}
\footnotetext{Le \textbf{raisonnement par l'absurde} est souvent utilisé pour prouver qu'une affirmation est \textit{fausse}.

  Son principe est le suivant  :

  -- on commence par poser une donnée au départ (correspondant souvent à l'affirmation proposée) ;

  -- on montre par un enchaînement logique qu'on aboutit à une contradiction (c'est l'\textit{absurdité}) ;

  -- on en déduit que la donnée choisie au départ est \textit{fausse}.
}

\section{Encadrer et arrondir un nombre réel}

\begin{definition}\index{Encadrement}\index{Arrondi}
  Un \textbf{encadrement décimal} d'un nombre réel $x$ est une écriture de la forme
  \[d_1 \ppq x \ppq d_2\]
  où $d_1$ et $d_2$ sont des nombres décimaux.
  \medskip

  La différence $s_2-d_1$ est l'\textbf{amplitude} de l'encadrement.
  \medskip

  \textbf{Arrondir} un nombre réel, c'est déterminer la valeur la plus proche de ce nombre à une \textit{précision donnée}.

  \begin{center}
    \begin{tblr}{width=0.9\linewidth,colspec={X[l,2]*{5}{X[c,1]}},hlines,vlines,
      cell{2,3}{2-6}={mode=math},
          cell{1}{2-6}={font=\footnotesize},
          rows={valign={m}}}
      Précision\dots                      & à l'unité & au dixième & au centième & au millième & au dix-millième \\
      Puissances de dix                   & 10^0      & 10^{-1}    & 10^{-2}     & 10^{-3}     & 10^{-4}         \\
      Nombre de chiffres après la virgule & 0         & 1          & 2           & 3           & 4               \\
    \end{tblr}
  \end{center}

  \begin{itemize}\index{Valeur approchée!par défaut}\index{Valeur approchée!par excès}
    \item $d_1$ est la \textbf{valeur approchée par défaut} de $x$ à la précision donnée ;
    \item $d_2$ est la \textbf{valeur approchée par excès} de $x$ à la précision donnée.
  \end{itemize}
\end{definition}

\begin{exemple}
  On a $\sqrt{2}\approx1,141\ldots$ :
  \begin{itemize}
    \item un arrondi au dixième de $\sqrt{2}$ est $1,4$ ;
    \item un encadrement à l'unité de $\sqrt{2}$ est $1\ppq\sqrt{3}\ppq2$.
  \end{itemize}
\end{exemple}

\begin{methode}[Déterminer un encadrement et un arrondi d'un nombre réel]
  \textbf{Énoncé}\quad Déterminer les encadrements et les arrondis au dixième et au millième de $\sqrt{7}$.
  \bigskip

  \textbf{Solution}\quad À l'aide de la calculatrice, j'obtiens $\sqrt{7}=\num{2,645751311}\ldots$
  \begin{itemize}
    \item Au dixième :
          \begin{itemize}
            \item pour la valeur par défaut de l'encadrement, je prends la \textbf{troncature}\footnotemark au dixième du nombre $2,6\hcancel{45751311}$ ;
            \item pour la valeur par excès, je prend la valeur par défaut \textit{augmentée} de un dixième (l'amplitude de l'encadrement).
          \end{itemize}

          ainsi, on obtient l'encadrement au dixième de $\sqrt{7}$ suivant : \[2,6\ppq\sqrt{7}\ppq2,7\]
          La valeur par défaut est plus proche de $\sqrt{7}$ que la valeur par excès

          \begin{center}
            \begin{tikzpicture}[x=30cm]
              \draw[thick,-latex'] (2.45,0) -- (2.85,0);
              \foreach  \x in {2.5,2.6,2.7,2.8} \draw[thick] ([yshift=-2pt]\x,0) -- ([yshift=2pt]\x,0) node[below=2mm] {$\num{\x}$};
              \fill[colorSec] (2.64575131,0) coordinate (R) circle (1.5pt);
              \node[above=6mm of R,inner sep=2pt,colorSec] (R') {$\num{2,645751}\ldots$};
              \coordinate (M) at (2.65, 0);
              \node[below=6mm of M,inner sep=2pt,font=\scriptsize] (N) {$2,65$};
              \draw[thin,-latex'] (N) -- (M);
              \draw[thin,-latex',colorSec] (R') -- (R);
            \end{tikzpicture}
          \end{center}

          donc on peut dire que l'arrondi au dixième de $\sqrt{7}$ est $2,6$.

    \item Au millième :
          \begin{itemize}
            \item pour la valeur par défaut de l'encadrement, je prends la troncature au millième du nombre $2,645\hcancel{751311}$ ;
            \item pour la valeur par excès, je prend la valeur par défaut augmentée de un millième.
          \end{itemize}

          ainsi, on obtient l'encadrement au millième de $\sqrt{7}$ suivant : \[2,645\ppq\sqrt{7}\ppq2,646\]
          La valeur par excès est plus proche de $\sqrt{7}$ que la valeur par défaut

          \begin{center}
            \begin{tikzpicture}[x=3cm]
              \draw[thick,-latex'] (-0.5,0) -- (4.5,0);
              \foreach  \x/\nb in {0/2.643,1/2.644,2/2.645,3/2.646,4/2.647} \draw[thick] ([yshift=-2pt]\x,0) -- ([yshift=2pt]\x,0) node[below=2mm] {$\num{\nb}$};
              \fill[colorSec] (2.75131,0) coordinate (R) circle (1.5pt);
              \node[above=6mm of R,inner sep=2pt,colorSec] (R') {$\num{2,645751}\ldots$};
              \draw[thin,-latex',colorSec] (R') -- (R);
              \coordinate (M) at (2.5, 0);
              \node[below=6mm of M,inner sep=2pt,font=\scriptsize] (N) {$2,6455$};
              \draw[thin,-latex'] (N) -- (M);
            \end{tikzpicture}
          \end{center}

          donc on peut dire que l'arrondi au millième de $\sqrt{7}$ est $2,646$.
  \end{itemize}


\end{methode}
\footnotetext{La \textbf{troncature d'un nombre à $n$ décimales} est ce même nombre où on a gardé seulement les $n$ les chiffres après
  la virgule.}

\section{Intervalles et inégalités}

\subsection{Symboles}
On utilise les symboles suivants ($a$ et $b$ désignent deux nombres réels) :
\begin{multicols}{2}
  \begin{itemize}
    \item $a < b$ siginifie que $a$ est strictement inférieur à $b$ ;
    \item $a > b$ siginifie que $a$ est strictement supérieur à $b$ ;
    \item $a \ppq b$ siginifie que $a$ est inférieur ou égal à $b$ ;
    \item $a \pgq b$ siginifie que $a$ est supérieur ou égal à $b$ ;
    \item $\infty$ désigne l'\textit{infini}.
  \end{itemize}
\end{multicols}

\subsection{Intervalles}

\begin{definition}\index{Intervalle}
  Soient $a$ et $b$ deux réels tels que $a<b$.
  \begin{itemize}
    \item On appelle \textbf{intervalle fermé} l'ensemble noté $\interff{a b}$ des réels $x$ tels que $a \ppq x \ppq b$.
    \item On appelle \textbf{intervalle ouvert} l'ensemble noté $\interoo{a b}$ des réels $x$ tels que $a < x < b$.
    \item L'intervalle $\interfo{a b}$ désigne l'ensemble des réels $x$ tels que $a \ppq x < b$.
    \item L'intervalle $\interof{a b}$ désigne l'ensemble des réels $x$ tels que $a < x \ppq b$.
    \item On désigne par $\interoo{-\infty{} a}$ l'ensemble des réels $x$ tels que $x<a$.
    \item On désigne par $\interof{-\infty{} a}$ l'ensemble des réels $x$ tels que $x \ppq a$.
    \item On définirait de la même manière les ensemble $\interoo{a +\infty}$ et $\interfo{a +\infty}$.
  \end{itemize}
\end{definition}

\begin{exemple}
  Dans les exemples ci-dessous, on a surligné en vert sur la droite numérique les parties qui correspondent aux inégalités ou aux encadrements.
  \medskip


  \begin{longtblr}{width=\linewidth,colspec={X[c,1]X[c,3]X[c,1]},hlines,vlines,stretch=2,cell{2-8}{1}={mode={math}},rows={valign={m},ht=10mm},
    row{1}={bg=colorPrim!10,font=\bfseries},rowhead=1}
    Inégalités ou encadrements & Représentation sur la droite numérique & Intervalle \\
    1<x
                               &
    \begin{RepIntervalles}[xmin=-5,xmax=5,Unite=0.7,Graduations={-5,-4,...,5},Valeurs={0,1},Largeur=10,Police=\scriptsize]
      \tkzIntervalle[Type=OO,Couleur=colorTer,Decor=fond]{1}{*}
    \end{RepIntervalles}
                               & \interoo{1 +\infty}                                 \\
    x\ppq -2
                               &
    \begin{RepIntervalles}[xmin=-5,xmax=5,Unite=0.7,Graduations={-5,-4,...,5},Valeurs={0,1},Largeur=10,Police=\scriptsize]
      \tkzIntervalle[Type=OF,Couleur=colorTer,Decor=fond,AffValeurs]{*}{-2}
    \end{RepIntervalles}
                               & \interof{-\infty{} -2}                              \\
    -1 \ppq x
                               &
    \begin{RepIntervalles}[xmin=-5,xmax=5,Unite=0.7,Graduations={-5,-4,...,5},Valeurs={0,1},Largeur=10,Police=\scriptsize]
      \tkzIntervalle[Type=FO,Couleur=colorTer,Decor=fond,AffValeurs]{-1}{*}
    \end{RepIntervalles}
                               & \interfo{-1 +\infty}                                \\
    x<-3
                               &
    \begin{RepIntervalles}[xmin=-5,xmax=5,Unite=0.7,Graduations={-5,-4,...,5},Valeurs={0,1},Largeur=10,Police=\scriptsize]
      \tkzIntervalle[Type=OO,Couleur=colorTer,Decor=fond,AffValeurs]{*}{-3}
    \end{RepIntervalles}
                               & \interoo{-\infty -3}                                \\
    -4 \ppq x \ppq 1
                               &
    \begin{RepIntervalles}[xmin=-5,xmax=5,Unite=0.7,Graduations={-5,-4,...,5},Valeurs={0,1},Largeur=10,Police=\scriptsize]
      \tkzIntervalle[Type=FF,Couleur=colorTer,Decor=fond,AffValeurs]{-4}{1}
    \end{RepIntervalles}
                               & \interff{-4 1}                                      \\
    -2 \ppq x < 3
                               &
    \begin{RepIntervalles}[xmin=-5,xmax=5,Unite=0.7,Graduations={-5,-4,...,5},Valeurs={0,1},Largeur=10,Police=\scriptsize]
      \tkzIntervalle[Type=FO,Couleur=colorTer,Decor=fond,AffValeurs]{-2}{3}
    \end{RepIntervalles}
                               & \interfo{-2 3}                                      \\
    3 <  x \ppq 5
                               &
    \begin{RepIntervalles}[xmin=-5,xmax=5,Unite=0.7,Graduations={-5,-4,...,5},Valeurs={0,1},Largeur=10,Police=\scriptsize]
      \tkzIntervalle[Type=OF,Couleur=colorTer,Decor=fond,AffValeurs]{3}{5}
    \end{RepIntervalles}
                               &
    \interof {3 5}                                                                   \\
    -2 <  x < 1
                               &
    \begin{RepIntervalles}[xmin=-5,xmax=5,Unite=0.7,Graduations={-5,-4,...,5},Valeurs={0,1},Largeur=10,Police=\scriptsize]
      \tkzIntervalle[Type=OO,Couleur=colorTer,Decor=fond,AffValeurs]{-2}{1}
    \end{RepIntervalles}
                               & \interoo{-2 1}                                      \\
  \end{longtblr}
\end{exemple}

\begin{definition}
  Soit $\mathtt{I}=\interff{a b}$ un intervalle.
  \begin{itemize}
    \item $a$ et $b$ sont appelés \textbf{bornes} de l'intervalle de l'intervalle $\mathtt{I}$.\index{Intervalle!Bornes}\index{Bornes}


          Plus spécifiquement, $a$ est la \textbf{borne inférieure} et $b$ est la \textbf{borne supérieure}.
    \item L'\textbf{amplitude} de l'intervalle $\mathtt{I}$ est le nombre $b-a$.\index{Intervalle!Amplitude}\index{Amplitude}
    \item Le \textbf{centre} de l'intervalle $\mathtt{I}$ est le nombre $c=\dfrac{a+b}{2}$.\index{Intervalle!Centre}\index{Centre}
  \end{itemize}
  \begin{center}
    \begin{tikzpicture}[label distance=3mm,%color
      legende/.style={pin distance=1.3cm,pin edge={latex'-,shorten <=1mm},font=\lightfont\scriptsize}]
      \begin{axis}[
        clip mode=individual,
        axis y line=none, axis lines=left, axis line style={-},
        xmin=-5.5, xmax=5.5, ymin=-1, ymax=1,xticklabels={},
        restrict y to domain=-1:1, xtick={-5,...,5}, %extra x ticks = {0,1},
        point meta=explicit symbolic, axis lines = middle,
        scatter/classes={%
        open={mark=*,draw=magenta,fill=white},
        closed={mark=*,red},
        closedleft={mark=text,text mark={\Large[},color=black},
        closedright={mark=text,text mark={\Large]},color=black},
        openleft={mark=text,text mark={(},color=black},
        openright={mark=text,text mark={)},color=black},
        point={mark=*,red}},
        %axis line style={latex-latex}
      ]
        \addplot[scatter,colorSec,line width=2pt] table [y expr=0,meta index=1, header=false] {
            -4 closedleft
            4 closedright
          };
        \coordinate (C) at (axis cs:0,0);
        \coordinate[label=above:{$a$},pin={[legende]above left:borne inférieure}] (A) at (axis cs:-4,0);
        \coordinate[label=above:{$b$},pin={[legende]above right:borne supérieure}] (B) at (axis cs:4,0);
        \draw[thick] ([yshift=1mm]A) -- ([yshift=-1mm]A);
        \draw [decorate,decoration={brace,amplitude=5pt,mirror,raise=1.8ex}] (A) -- (B) node[midway,below,yshift=-2.7ex] (amplitude) {$b-a$};
        \node[above=5mm of C] (centre) {$\dfrac{a+b}{2}$};
        \draw[thin,-latex'] (centre) -- (C);
        \node[below=1mm of amplitude,font=\lightfont\scriptsize] {amplitude};
        \node[above=1mm of centre,font=\lightfont\scriptsize] {centre};
      \end{axis}
    \end{tikzpicture}
  \end{center}
\end{definition}

\begin{methode}[Déterminer le centre et l'amplitude d'un intervalle]
  \textbf{Énoncé}\quad Soit l'intervalle $\mathtt{I}=\interff{-7 3}$. Déterminer l'amplitude et le centre de l'intervalle $\mathtt{I}$.
  \bigskip


  \textbf{Solution}\quad Les bornes de l'intervalle $\mathtt{I}$ sont $a=-7$ et $b=3$.
  \begin{multicols}{2}
    L'amplitude est donc
    \begin{align*}
      A & = b-a    \\
        & = 3-(-7) \\
        & = 10
    \end{align*}

    Le centre est donc
    \begin{align*}
      C & = \frac{a+b}{2} \\
        & = \frac{-7+3}{2} \\
        & =-2
    \end{align*}
  \end{multicols}
  L'intervalle $\mathtt{I}$ a une amplitude de $10$ et un centre de $-2$.
\end{methode}

\subsection{Réunions, Intersections}

\begin{definition}\index{Réunion}\index{Intersection}
  Soient $\mathtt{I}$ et $J$ deux intervalles.
  \begin{itemize}
    \item l'\textbf{intersection} de $\mathtt{I}$ et de $J$ est l'ensemble des nombres réels qui appartiennent
          à la fois à $\mathtt{I}$ et à $J$ ;
    \item la \textbf{réunion} de $\mathtt{I}$ et de $J$ est l'ensemble des nombres réels qui appartiennent à $\mathtt{I}$
          ou à $J$.
  \end{itemize}
  \begin{multicols}{2}
    \begin{center}
      Notation de l'intersection
      {\Large \[\mathtt{I} \cap J\]}
      {\lightfont \small se lit \og{}\textit{\texttt{I} inter \texttt{J}}\fg{}}
    \end{center}

    \begin{center}
      Notation de la réunion
      {\Large \[\mathtt{I} \cup J\]}
      {\lightfont \small se lit \og{}\textit{\textit{I} union \texttt{J}}\fg{}}
    \end{center}
  \end{multicols}
\end{definition}

\begin{exemple}
  On donne $\mathtt{I}=\interff{-3 7}$ et $J=\interff{4 10}$.
  \begin{center}
    \begin{RepIntervalles}[xmin=-5,xmax=12,Unite=0.5,Graduations={-5,-4,...,12},Valeurs={0,1},Largeur=15,Police=\scriptsize]
      \tkzIntervalle[Type=FF,Couleur=colorTer,Decor=fond,AffValeurs]{-3}{7}
      \tkzIntervalle[Type=FF,Couleur=colorSec,Decor=fond,AffValeurs,Offset=1pt]{4}{10}
    \end{RepIntervalles}
  \end{center}
  \begin{itemize}
    \item On a $\mathtt{I} \cap J = \interff{4 7}$ : c'est l'ensemble des nombres communs aux deux intervalles (coloriés deux fois sur l'axe).
    \item On a $\mathtt{I} \cup J = \interff{-3 10}$ : ce sont tous les réels présents dans les deux intervalles réunis (coloriés au moin sune fois sur l'axe).
  \end{itemize}
\end{exemple}

\section{Valeur absolue}

\subsection{Distance entre deux nombres}
\begin{definition}\index{Valeur absolue}\index{Distance entre deux nombres}
  La \textbf{distance} entre deux nombres réels est la \textit{différence} entre le plus grand et le plus petit.
  \medskip

  La distance entre deux réels $a$ et $b$ est un réel positif ou nul noté $\varabs{a-b}$ et appelée \textbf{valeur absolue} de $a-b$.
\end{definition}

\begin{remarque}
  \begin{itemize}
    \item Si $a>b$ alors $\varabs{a-b}=a-b$ et si $b>a$ alors $\varabs{a-b}=b-a$.
    \item La distance entre un réel $a$ et $0$ est donc la valeur absolue du nombre $a$ et sera notée $\varabs{a}$.
  \end{itemize}
\end{remarque}

\begin{exemple}
  On se donne une droite graduée pour mieux visualiser les distances entre les nombres.
  \begin{center}
    \begin{tikzpicture}[label distance=2mm]
      \draw[thick,-latex'] (-4.3,0) -- (8.2,0);
      \foreach \x in {-4,-3,...,8} \draw[thick] ([yshift=-1mm]\x,0)--([yshift=1mm]\x,0);
      \node[below=2mm] at (0,0) {$0$};
      \foreach\nb/\name in {-3/A,1/I,2.5/B,7/C} {
          \coordinate[label=below:$\num{\nb}$] (\name) at (\nb,0);
          \fill[colorTer] (\name) circle (2pt);
        }
      \draw[very thick,colorTer,latex'-latex'] ([yshift=4mm]A) -- ([yshift=4mm]I) node[midway,above] {$4$};
      \draw[very thick,colorTer,latex'-latex'] ([yshift=4mm]B) -- ([yshift=4mm]C) node[midway,above] {$4,5$};
    \end{tikzpicture}
  \end{center}
  \begin{itemize}
    \item La distance entre les nombres $2,5$ et $4$ est $\varabs{2,5-7}$.
          On a $7>2,5$ donc on calcule $7-2,5=4,5$ et donc $\varabs{2,5-7}=4,5$.
    \item La distance entre les nombres $-3$ et $1$ est $\varabs{-3-1}$.
          On a $1>-3$ donc on calcule $1-(-3)=4$ et donc $\varabs{-3-1}=4$.
  \end{itemize}
\end{exemple}


\begin{propriete}[(admise)]
  Soit $x\in\R$.
  \begin{itemize}
    \item Si $x\pgq0$ alors $\varabs{x}=x$.
    \item Si $x<0$ alors $\varabs{x}=-x$.
  \end{itemize}
\end{propriete}

\begin{remarque}
  Si $x$ est un nombre négatif alors $-x$ est positif.
\end{remarque}

\begin{exemple}
  \[\varabs{5}=5\kern2cm\varabs{-2}=2\kern2cm\varabs{48}=48\kern2cm\varabs{-0,46}=0,46\]
\end{exemple}

\subsection{Valeurs absolues et intervalles}
\begin{propriete}[(admise)]\index{Intervalle}
  Pour tous les nombres réels $a$, $b$ et $r$ avec $r$ positif ou nul,
  \[\varabs{x-a}\ppq r \Longleftrightarrow x \in \interff{a-r a+r}\]
  On dira que $a$ est le \textbf{centre} de le centre de l'intervalle $\interff{a-r a+r}$.


  \begin{center}
    \begin{tikzpicture}[label distance=2mm]
      \begin{axis}[
        axis y line=none, axis lines=left, axis line style={-},
        xmin=-5.5, xmax=5.5, ymin=-1, ymax=1,xticklabels={},
        restrict y to domain=-1:1, xtick={-5,...,5}, %extra x ticks = {0,1},
        point meta=explicit symbolic, axis lines = middle,
        scatter/classes={%
        open={mark=*,draw=magenta,fill=white},
        closed={mark=*,red},
        closedleft={mark=text,text mark={\Large[},color=black},
        closedright={mark=text,text mark={\Large]},color=black},
        openleft={mark=text,text mark={(},color=black},
        openright={mark=text,text mark={)},color=black},
        point={mark=*,red}},
        %axis line style={latex-latex}
      ]
        \addplot[scatter,colorSec,line width=2pt] table [y expr=0,meta index=1, header=false] {
            -4 closedleft
            4 closedright
          };
        \coordinate[label=above:{$a$}] (A) at (axis cs:0,0);
        \coordinate[label=above:{$a-r$}] (AmoinsR) at (axis cs:-4,0);
        \coordinate[label=above:{$a+r$}] (AplusR) at (axis cs:4,0);
        \draw[thick] ([yshift=1mm]A) -- ([yshift=-1mm]A);
        \draw [decorate,decoration={brace,amplitude=5pt,mirror,raise=1.5ex}] (AmoinsR) -- (A) node[midway,below,yshift=-2.5ex] {$r$};
        \draw [decorate,decoration={brace,amplitude=5pt,mirror,raise=1.5ex}] (A) -- (AplusR) node[midway,below,yshift=-2.5ex] {$r$};
      \end{axis}
    \end{tikzpicture}
  \end{center}
\end{propriete}

\begin{exemple}
  Déterminons à quel intervalle correspond $\varabs{x-5}\ppq 2$.

  La distance entre $x$ et $5$ doit être inférieure à $2$.
  \begin{center}
    \begin{tikzpicture}
      \draw[thick,-latex'] (0.5,0) -- (8.7,0);
      \foreach \x in {1,...,8} {
          \draw[thick] ([yshift=1mm]\x,0)--([yshift=-1mm]\x,0) node[below=2mm] {$\x$};
        }
      \foreach\nb/\name in {3/A,5/B,7/C} {
          \coordinate (\name) at (\nb,0);
        }
      \draw[very thick,colorTer,-latex'] (B) to[out=140,in=40] node[midway,above] {$2$} (A);
      \draw[very thick,colorTer,-latex'] (B) to[out=40,in=140] node[midway,above] {$2$} (C);
    \end{tikzpicture}
  \end{center}

  On en déduit que $x\in\interff{3 7}$.
\end{exemple}

\begin{methode}[Traduire une inégalité comportant une valeur absolue en intervalle]
  \textbf{Énoncé}\quad Déterminer à quel intervalle appartiennent les réels $x$ tels que $\varabs{x+2}\ppq 3$\bigskip

  \textbf{Solution}\quad $\varabs{x+2}\ppq 3$ signifie que la distance entre le réel $x$ et le nombre $-2$ est
  inférieure ou égale à $3$ car $\varabs{x+2}=\varabs{x - (-2)}$ (n'oublions pas qu'on calcule
  toujours la distance entre deux nombres en effectuant la \textit{différence} entre ces deux nombres).
  \medskip

  Donc $\varabs{x+2}\ppq 3$ équivaut à $x\in\interff{-2-3 -2+3}$ soit $x\in\interff{-5 1}$.
\end{methode}

