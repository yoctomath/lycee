\chapter{Fonctions - Généralités}

\section{Définitions. Vocabulaire}

\subsection{Notion de fonction}
\begin{definition}\index{Fonctions!Définition}
  On appelle \textbf{fonction} tout processus qui associe un nombre \textit{unique} à un autre nombre.
  \medskip

  Si la fonction s'appelle $f$, on note :
  \begin{itemize}
    \item $f(x)$ le nombre associé à $x$.
    \item $f:x\longmapsto f(x)$ pour dire \og{}\textit{la fonction $f$ est la fonction
            qui à $x$ associe le nombre $f(x)$}\fg{}
  \end{itemize}
  \begin{center}
    \begin{tikzpicture}
      \node (fonction) at (0,0) {Fonction};
      \node[below=2mm of fonction,colorPrim,inner sep=0pt] (gears) {\Large\faCogs};
      \node[draw, thick,inner sep=3pt,fit=(fonction) (gears)] (machine) {};
      \node[left=1cm of machine] (x) {$x$};
      \node[right=1cm of machine] (y) {$y=f(x)$};
      \draw[thick] (x) --  (machine);
      \draw[->,thick] (machine) --  (y);
    \end{tikzpicture}
  \end{center}
\end{definition}

\begin{exemple}\label{seconde:cours:fctgen:exemples:1}
  La fonction $g:t\longmapsto t^2+1$ est une fonction qui associe à tout nombre $t$ son carré augmenté de $1$.
\end{exemple}

\subsection{Image}
\begin{definition}\index{Fonctions!Image}\index{Image}
  Le nombre $f(x)$ qui est associé à $x$ par la fonction $f$ est appelé l'\textbf{image} de $x$ par $f$.
\end{definition}

\begin{exemple}\label{seconde:cours:fctgen:exemples:2}
  Utilisons la fonction $g$ de l'exemple \ref{seconde:cours:fctgen:exemples:1} ci-dessus.

  L'image de $-2$ est $5$ car
  \begin{align*}
    g(-2) & =(-2)^2+1 \\
          & =4+1      \\
          & =5
  \end{align*}
\end{exemple}

\begin{remarque}
  Il est important de noter que, pour un nombre donné, la fonction $f$
  définit  un nombre \textbf{unique}.

  Dans l'exemple \ref{seconde:cours:fctgen:exemples:2}, on comprend bien que le calcul qui détermine l'image de $-2$ par $g$ ne peut pas donner deux résultats différents.
\end{remarque}

\subsection{Antécédent}

\begin{definition}\index{Fonctions!Antécédents}\index{Antécédents}
  Soit un nombre $a$ et une fonction $f$.
  \medskip

  Toutes les valeurs de $x$ qui vérifient $f(x)=a$
  sont appelées les \textbf{antécédents} de $a$ par $f$.
\end{definition}

\begin{exemple}\label{seconde:cours:fctgen:exemples:3}
  Reprenons la fonction $g$ de l'exemple \ref{seconde:cours:fctgen:exemples:1} ci-dessus et cherchons les antécédents de $5$.

  On sait déjà depuis l'exemple \ref{seconde:cours:fctgen:exemples:2} que $-2$ est un antécédent de $5$ par $g$ puisque $g(-2)=5$.

  Cependant, on a aussi $g(2)=2^2+1=4=5$ donc $5$ admet aussi $2$ comme antécédent.
  \medskip

  $5$ admet donc au moins deux antécédents par $g$ qui sont $-2$ et $2$.
\end{exemple}

\begin{remarque}
  L'exemple \ref{seconde:cours:fctgen:exemples:3} ci-dessus nous permet de constater qu'à la différence de l'image,
  un antécédent n'est pas unique~!

  On dira d'ailleurs \og{} \textit{\textbf{l}'image d'un nombre $x$} \fg{}
  mais \og{} \textit{\textbf{un} antécédent du nombre $y$} \fg{}.
\end{remarque}


\subsection{Ensemble de définition}

\begin{definition}
  Soit une fonction $f$. On appelle \textbf{ensemble de définition} de la fonction $f$ l'ensemble de
  tous les nombres pour lesquels $x$ admet une image par $f$, c'est-à-dire pour lesquels $f(x)$ existe.
\end{definition}

\begin{exemple}
  si $f:x\longmapsto \sqrt{x}$, on ne peut pas calculer $f(x)$ lorsque $x$ est négatif
  (la racine carrée n'existe pas dans ce cas).

  L'ensemble de définition de $f$ dans ce cas sera donc \textit{l'ensemble des nombres positifs ou nul}
  soit l'intervalle \interfo{0 +\infty} (ou \R[-]).
\end{exemple}

\begin{remarque}
  L'ensemble de définition peut-être tributaire de différents contextes :
  \begin{itemize}
    \item des contingences purement calculatoires : par exemple,
          on ne peut pas calculer $\sqrt{x}$ lorsque $x$ est négatif ;
    \item le contexte d'utilisation de la fonction : par exemple, si on travaille avec des longueurs en géométrie,
          la fonction peut n'accepter que des nombres positifs.
  \end{itemize}
\end{remarque}

\subsection{Tableau de valeurs}
Si on connaît l'expression d'une fonction $f$, on peut alors dresser un \textit{tableau de valeurs}
\index{Fonctions!Tableau de valeurs} c'est-à-dire un tableau qui donne la valeur de $f(x)$
pour certaines valeurs de $x$ données.

\begin{remarque}
  Les tableaux de valeurs servent notamment à se donner des points pour tracer \og{}à la main\fg{}
  une représentation graphique d'une fonction (voir section \ref{seconde:cours:fctgen:repgraphique}).

  Généralement, les valeurs de $x$ se mettent sur la première ligne du tableau et les valeurs de $f(x)$ correspondantes sur la deuxième ligne. Ainsi, les images se lisent sur la deuxième ligne et les antécédents sur la première ligne.
\end{remarque}

\begin{exemple}
  Soit la fonction $f$ définie par son expression $f(x)=3x+2$.

  Voici ci-dessous un tableau de valeurs pour $x$ entier variant de $0$ à $7$~:

  \begin{center}
    \begin{tblr}{width=0.95\linewidth,colspec={X[l,2]*{8}{X[c,1]}},hlines,vlines,stretch=2}
      $x$    & $0$ & $1$ & $2$ & $3$  & $4$  & $5$  & $6$  & $7$  \\
      $f(x)$ & $2$ & $5$ & $8$ & $11$ & $14$ & $17$ & $20$ & $23$ \\
    \end{tblr}
  \end{center}
  Grâce à ce tableau, on peut voir par exemple que :
  \begin{itemize}
    \item l'image de $1$ par $f$ est $5$ ;
    \item $6$ est un antécédent de $20$.
  \end{itemize}
\end{exemple}
\newpage
\begin{methode}[Déterminer les antécédents d'un nombre par une fonction]
  \textbf{Énoncé}\quad Soit $g$ la fonction définie sur \R par
  \[g(x)=x^2-4x+9\]
  Déterminer les antécédents du nombre $9$ par la fonction $g$\bigskip

  \textbf{Solution}\quad Pour déterminer les antécédents, on doit \textbf{résoudre une équation}.

  On cherche tous les réels $x$ tels que $g(x)=9$. On a donc :
  \begin{align*}
    x^2-4x+9 & = 9                                                      \\
    x^2-4x   & = 0 &     &     &  & \longleftarrow \text{factorisation} \\
    x(x-4)   & = 0 &     &     &  & \longleftarrow \text{produit nul}   \\
    x        & = 0 & x-4 & = 0                                          \\
    x        & = 0 & x   & = 4
  \end{align*}
  L'équation admet donc deux solutions $0$ et $4$.

  On en déduit que $9$ admet deux antécédents par $g$ qui sont $0$ et $4$.
\end{methode}

\section{Représentation graphique}\label{seconde:cours:fctgen:repgraphique}

\subsection{Définition}
\begin{multicols}{2}
  \begin{definition}
    Dans un repère du plan, on appelle \textbf{courbe représentative} ou \textbf{représentation graphique}
    d'une fonction $f$ définie sur son domaine $\mathcal{D}$, l'ensemble des points $M\nuplet{x y}$
    dont les coordonnées vérifient :
    \[x\in\mathcal{D}\quad \text{ et }\quad y=f(x)\]
    On note souvent $\mathcal{C}_f$ pour désigner la courbe représentative de $f$.
    \medskip

    La relation $y=f(x)$ est appelée \textbf{équation} de la courbe représentative de $f$.
  \end{definition}

  \begin{center}
    \begin{tikzpicture}[declare function={f1(\x) = 2*ln(\x)+1;}]
      \begin{axis}[axis y line=center,
          axis x line=middle,
          x=1cm,y=1cm,
          xmin=-2.2,xmax=4.2,
          ymin=-1.2,ymax=4.2,
          minor x tick num=1,minor y tick num=1,grid=both,
          tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
          ]
        \addplot[domain=0.1:4.2,mark=none,colorSec,thick,samples=150] {f1(x)};
        \addplot[mark=x,only marks,samples at={2.5},colorSec,thick] {f1(x)} coordinate[label=below right:$M\nuplet{x y}$] (M);
        \addplot[mark=none,samples at={3.5},colorSec] {f1(x)} coordinate[label=below:$\mathcal{C}_f$] (c);
        \coordinate[label=below left:$O$] (O) at (0,0);
        \coordinate (Mx) at ($(O)!(M)!(4,0)$);
        \coordinate (My) at ($(O)!(M)!(0,4)$);
        \draw[color=colorTer,very thick, densely dashed] (Mx) node[below=2mm,fill=white,inner sep=1pt]{$x$} |- (My) node[left=2mm,fill=white,inner sep=1pt]{$y=f(x)$};
      \end{axis}
    \end{tikzpicture}
  \end{center}
\end{multicols}

\begin{remarque}
  La courbe $\mathcal{C}_f$ est l'ensemble de tous les points dont l'ordonnée est l'image par $f$ de l'abscisse.

  On a l'équivalence fondamentale suivante :
  \[M\in\mathcal{C}_f \Longleftrightarrow M\nuplet{x f(x)}\quad\]
\end{remarque}



\begin{methode}[Déterminer si un point appartient à la courbe représentative d'une fonction]
  \textbf{Énoncé}\quad On donne la fonction $f$ définie sur \R par $f(x)=2(x+1)(x-7)$,
  de courbe représentative $\mathcal{C}_f$.
  
  Déterminer si le point $A\nuplet{-5 96}$ est un point de $\mathcal{C}_f$.\bigskip
  
  \textbf{Solution}\quad Si $A\nuplet{-5 96}$ appartient à la courbe représentative de $f$, 
  alors son ordonnée est l'image de son abscisse par la fonction $f$.

  Calculons donc $f(-5)$ :
  \begin{align*}
    f(-5) &= 2(-5+1)(-5-7) \\
          &= 2\times(-4)\times(-12) \\
          &= 96
  \end{align*}
  Comme $f(-5)=96$, le point $A\nuplet{-5 96}$ appartient bien à la courbe représentative de $f$.
\end{methode}


\subsection{Lectures graphiques}
\subsubsection{Lecture graphique d'une image}\index{Image!Lecture graphique}
\begin{multicols}{2}
  Soit $\mathcal{C}_f$ la représentation graphique d'une fonction $f$.
  L'image du nombre $x_0$ par la fonction $f$ est l'\textit{ordonnée} $y_0$ du point $M$ de $\mathcal{C}_f$ qui a pour abscisse $x_0$.
  On trace alors le chemin de lecture en pointillés (voir la figure ci-contre).

  \vfill

  % \columnbreak

  \begin{center}
    \begin{tikzpicture}[scale=0.8]
      \begin{axis}[axis y line=center,%
          axis x line=middle,x=1cm,y=1cm,%
          xmin=-2.2,xmax=3.2,ymin=-0.7,ymax=2.6,%
          minor x tick num=1,minor y tick num=1,grid=both,%
          xticklabels=\empy, yticklabels=\empty]
        \addplot[domain=-2.2:3.2,mark=none,colorSec,thick,samples=150] {sqrt(x+2)};
        \node[colorSec] at (-1.5,1.3) {$\mathcal{C}_f$};
        \coordinate[label=below left:$O$] (O) at (0,0);
        \coordinate[label=below right:$M$] (M) at(1.5,3.742/2);
        \draw[-triangle 45,color=colorPrim,dashed,thick] (1.5,0)  node[below]{$x_0$} -- (1.5,1.871/2);
        \draw[-triangle 45,color=colorPrim,dashed,thick] (1.5,1.871/2) |- (0.75,3.742/2);
        \draw[color=colorPrim,dashed] (0.75,3.742/2) -- (0,3.742/2) node[left]{$y_0$};
      \end{axis}
    \end{tikzpicture}
  \end{center}
\end{multicols}

\subsubsection{Lecture graphique d'antécédent(s)}\index{Antécédents!Lecture graphique}
\begin{multicols}{2}
  \begin{center}
    \begin{tikzpicture}[scale=0.8]
      \begin{axis}[axis y line=center,%
          axis x line=middle,x=1cm,y=1cm,%
          xmin=-1.8,xmax=3.8,ymin=-1.3,ymax=2.4,%
          minor x tick num=1,minor y tick num=1,grid=both,%
          xticklabels=\empy, yticklabels=\empty]
        \addplot[domain=-1.3:3.3,mark=none,colorSec,thick,samples=150] {0.5*((x-1)^2-1)};
        \node[colorSec] at (1,-0.85) {$\mathcal{C}_f$};
        \coordinate[label=below left:$O$] (O) at (0,0);
        \coordinate[label=left:$M_1$] (M1) at (-1,1.5);
        \coordinate[label=right:$M_2$] (M2) at (3,1.5);
        \draw[-triangle 45 reversed,color=colorPrim,thick,dashed] (3,0)  node[below]{$x_2$} -- (3,0.75);
        \draw[-triangle 45 reversed,color=colorPrim,thick,dashed] (3,1.5) |- (1.5,1.5);
        \draw[color=colorPrim,thick,dashed] (1.5,1.5) -- (0,1.5) node[above right]{$y_0$};
        \draw[-triangle 45 reversed,color=colorPrim,thick,dashed] (-1,0)  node[below]{$x_1$} -- (-1,0.75);
        \draw[-triangle 45 reversed,color=colorPrim,thick,dashed] (-1,1.5) |- (-0.5,1.5);
        \draw[color=colorPrim,thick,dashed] (-0.5,1.5) -- (0,1.5);
      \end{axis}
    \end{tikzpicture}
  \end{center}

  Soit $\mathcal{C}_f$ la représentation graphique d'une fonction $f$.
  Les antécédents du nombre $y_0$ par la fonction $f$ sont les \textit{abscisses} des points $M$ de $\mathcal{C}_f$ qui ont pour ordonnée $y_0$.\linebreak On trace alors le(s) chemin(s) de lecture en pointillés (voir la figure ci-contre où $y_0$ admet deux antécédents $x_1$ et $x_2$ par la fonction $f$).

\end{multicols}


\subsection{Résolution graphique d'équation}

\begin{propriete}\index{Equation@\'{E}quation!Résolution graphique}
  Soient  $f$ et $g$ deux fonctions définies sur un ensemble $\mathcal{D}$. On appelle $\mathcal{C}_f$ et $\mathcal{C}_g$
  leurs courbes représentatives sur $\mathcal{D}$. Soit $k$ un nombre réel.
  \medskip

  \begin{itemize}
    \item Les solutions de l'équation \[f(x)=k\] sont les abscisses des points de la courbe $\mathcal{C}_f$
    représentative de $f$ dont l'ordonnée est égale à $k$.
    \item Les solutions de l'équation \[f(x)=g(x)\] sont les abscisses des points communs (points d'intersection) aux courbes
    $\mathcal{C}_f$ et $\mathcal{C}_g$.
  \end{itemize}
\end{propriete}
% \begin{exemple}
%   \begin{tblr}{width=\linewidth,colspec={X[c,1]X[c,1]}}
%     Équation $f(x)=k$ & Équation $f(x)=g(x)$\\
%     \begin{tikzpicture}[declare function={f1(\x) = 5*e^(-\x^2/10);},scale=0.7]%
%       \begin{axis}[%
%           axis y line=center,axis x line=middle,        % centrer les axes
%           x=0.6cm,y=0.6cm,                                  % unités sur les axes
%           xmin=-5.4,xmax=5.4,ymin=-0.7,ymax=5.5,        % bornes du repère
%           xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
%           % minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
%           grid=both,                                    % indiquer quelle geilles afficher
%           tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
%         ]
%         \addplot[mark=none,colorPrim,very thick,domain=-5.4:5.4,samples=150] {f1(x)};
%         \addplot[mark=x,only marks,samples at={-2.5},colorSec,thick] {f1(x)} coordinate[label=left:$M$] (A);
%         \addplot[mark=x,only marks,samples at={2.5},colorSec,thick] {f1(x)} coordinate[label=right:$M'$] (B);
%         \coordinate (O) at ($(0,0)!(A)!(0,3)$);
%         \draw[colorSec,thick,densely dashed] (A) -- (O) node[sloped,midway,font=\scriptsize,colorSec] {/\!/}
%         -- (B) node[sloped,midway,font=\scriptsize,colorSec] {/\!/};
%       \end{axis}
%     \end{tikzpicture}
%     &
%   \end{tblr}
% \end{exemple}

\newpage
\begin{methode}[Déterminer graphiquement les solutions d'une équation {$f\left(x\right) = k$}]

  \begin{tblr}{width=\linewidth,colspec={X[l,1]X[c,1]},rows={valign={t}}}
    {\textbf{Énoncé}\quad On donne la fonction $f$ définie sur \R par \medskip\\
    $f(x)=\frac{1}{5}x^3-\frac{6}{5}x^2+\frac{3}{5}x+4$\medskip\\
    On donne sa représentation graphique $\mathcal{C}_f$ ci-contre.}
  &
          \begin{tikzpicture}[scale=0.8,baseline={(current bounding box.center)},declare function={f1(\x) = 0.2*((\x+1)*(\x-2)*(\x-5)+1/0.1);}]%
          \begin{axis}[%
              axis y line=center,axis x line=middle,        % centrer les axes
              x=1cm,y=1cm,                                  % unités sur les axes
              xmin=-1.6,xmax=5.4,ymin=-1.2,ymax=5.1,        % bornes du repère
              xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
              % minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
              grid=both,                                    % indiquer quelle geilles afficher
              tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
            ]
            \addplot[name path global=courbe,mark=none,colorPrim,very thick,domain=-1.6:5.4,samples=150] {f1(x)};
            \addplot[mark=none,samples at={1},colorPrim,thick] {f1(x)} coordinate[label=above right:$\mathcal{C}_f$] (courbe);
          \end{axis}
        \end{tikzpicture}
\end{tblr}
  
  \begin{multicols}{2}
    \begin{tikzpicture}[scale=0.8,baseline={(current bounding box.center)},declare function={f1(\x) = 0.2*((\x+1)*(\x-2)*(\x-5)+1/0.1);}]%
      \begin{axis}[%
          axis y line=center,axis x line=middle,        % centrer les axes
          x=1cm,y=1cm,                                  % unités sur les axes
          xmin=-1.6,xmax=5.4,ymin=-1.2,ymax=5.1,        % bornes du repère
          xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
          % minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
          grid=both,                                    % indiquer quelle geilles afficher
          tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
        ]
        \addplot[name path global=courbe,mark=none,colorPrim,very thick,domain=-1.6:5.4,samples=150] {f1(x)};
        \addplot[name path global=droite,mark=none,colorSec,very thick,domain=-1.6:5.4,samples=150] {2};
        \coordinate[label={[colorSec,font=\scriptsize]above:$y=2$}] (droite) at (4, 2);
        \addplot[mark=none,samples at={1},colorPrim,thick] {f1(x)} coordinate[label=above right:$\mathcal{C}_f$] (courbe);
        \fill[name intersections={of=courbe and droite,name=M,total=\t}]
        [colorTer,opacity=0.8,every node/.style={above=2mm,fill=colorTer!5,circle,inner sep=0pt,opacity=1,font=\scriptsize}]
        \foreach \s in {1,...,\t} {(M-\s) circle (2pt) node {$M_{\s}$}};
        \draw[name intersections={of=courbe and droite,name=M,total=\t}]
        [colorTer,very thick,densely dashed]
        \foreach \s in {1,...,\t} {(M-\s) -- ($(0,0)!(M-\s)!(6,0)$)};
      \end{axis}
    \end{tikzpicture}

    \textbf{Solution}\quad On doit résoudre $f(x)=2$.
    
    On commence donc par tracer la droite d'équation $y=2$.

    On regarde les \textit{abscisses} des points d'intersection entre la courbe $\mathcal{C}_f$ et la droite. 
    \medskip

    On constate qu'il existe 3 points d'intersection $M_1$, $M_2$ et $M_3$ 
    d'abscisses respectives $x=-1$, $x=2$ et $x=5$. 
    \medskip

    On en déduit donc les solutions de l'équation sont $-1$, $2$ et $5$.
  \end{multicols}
\end{methode}

\subsection{Résolution graphique d'inéquations}

\begin{propriete}\index{Inéquation!Résolution graphique}
  Soient  $f$ et $g$ deux fonctions définies sur un ensemble $\mathcal{D}$. On appelle $\mathcal{C}_f$ et $\mathcal{C}_g$
  leurs courbes représentatives sur $\mathcal{D}$. Soit $k$ un nombre réel.
  \medskip

  \begin{itemize}
    \item Les solutions de l'équation \[f(x)<k\] sont les abscisses des points de la courbe $\mathcal{C}_f$
    représentative de $f$ situés strictement au dessous de la droite d'équation $y=k$.
    \item Les solutions de l'équation \[f(x)<g(x)\] sont les abscisses des points de la courbe $\mathcal{C}_f$ 
    situés strictement au dessous de la courbe $\mathcal{C}_g$.
  \end{itemize}
\end{propriete}

\begin{remarque}
  On résoudra graphiquement de la même manière des inéquations du type $f(x)\ppq k$, $f(x)>k$, $f(x)\pgq g(x)$, etc.
\end{remarque}

\begin{methode}[Résoudre graphiquement une inéquation]
  \phantom{ligne}
  \begin{multicols}{2}
    \textbf{Énoncé}\quad Soit la fonction $g$ définie sur $\R$ dont on donne la courbe représentative $\mathcal{C}_g$ ci-dessous.
  
    \begin{center}
      \begin{tikzpicture}[scale=0.8]
        \begin{axis}[axis y line=center,%
            axis x line=middle,x=1cm,y=1cm,%
            xmin=-4.5,xmax=3.5,ymin=-8,ymax=1.5,%
            minor x tick num=1,minor y tick num=1,grid=both,%
            xticklabels=\empy, yticklabels=\empty]
          \addplot[domain=-4.5:3.3,mark=none,colorSec,ultra thick,samples=150] {((x-3)*(x+2)*(x+3)-4)/4};
          \node[colorSec] at (1.2,-7.5) {$\mathcal{C}_g$};
          \coordinate[label=below left:$O$] (O) at (0,0);
          \coordinate[label=below:$1$] (I) at (1, 0);
          \coordinate[label=left:$1$] (J) at (0, 1);
        \end{axis}
      \end{tikzpicture}
    \end{center}
  
    Résoudre graphiquement l'équation $g(x)< -1$ .
    \vspace*{\fill}
    \columnbreak
  
    \textbf{Solution} \quad On considère la représentation graphique $\mathcal{C}_g$ de $g$. 
    
    On trace dans le même repère la droite $(\Delta)$ d'équation $y=-1$.
    
    On constate que $\mathcal{C}_g$ admet trois points d'intersection avec la droite $(\Delta)$ :

    $A\left(-3\,;\,-1\right)$ et $B\left(-2\,;\,-1\right)$ et $C\left(2\,;\,-1\right)$.
    \begin{center}
      \begin{tikzpicture}[scale=0.8]
        \begin{axis}[axis y line=center,%
            axis x line=middle,x=1cm,y=1cm,%
            xmin=-4.5,xmax=3.5,ymin=-8,ymax=1.5,%
            minor x tick num=1,minor y tick num=1,grid=both,%
            xticklabels=\empy, yticklabels=\empty]
          \addplot[domain=-4.5:3.3,mark=none,colorSec,ultra thick,samples=150] {((x-3)*(x+2)*(x+3)-4)/4};
          \addplot[domain=-4.5:-3,mark=none,colorSec,line width=8pt,opacity=0.2,samples=150] {((x-3)*(x+2)*(x+3)-4)/4};
          \addplot[domain=-2:3,mark=none,colorSec,line width=8pt,opacity=0.2,samples=150] {((x-3)*(x+2)*(x+3)-4)/4};
          \node[colorSec] at (1.2,-7.5) {$\mathcal{C}_g$};
          \addplot[domain=-4.5:3.3,mark=none,colorTer,ultra thick,samples=150] {-1};
          \node[colorTer,fill=colorTer!5] at (1.5,-1.5) {$(\Delta) : y=-1$};
          \coordinate[label=below left:$O$] (O) at (0,0);
          \coordinate[label=below:$A$] (A) at (-3,-1);
          \coordinate[label=below:$B$] (B) at (-2,-1);
          \coordinate[label=below:$C$] (C) at (3, -1);
          \coordinate[label=below:$1$] (I) at (1, 0);
          \coordinate[label=left:$1$] (J) at (0, 1);
          \draw[-triangle 45,color=colorPrim,thick,dashed] (A) -- (-3,0) coordinate (Ap) node[above] {$-3$};
          \draw[-triangle 45,color=colorPrim,thick,dashed] (B) -- (-2,0) coordinate (Bp) node[above] {$-2$};
          \draw[-triangle 45,color=colorPrim,thick,dashed] (C) -- (3,0) coordinate (Cp) node[above] {$3$};
          \fill[pattern={Lines[angle=45,yshift=-.5pt]},pattern color=colorPrim] ([yshift=1.25mm]Bp) rectangle ([yshift=-1.25mm]Cp);
          \fill[pattern={Lines[angle=45,yshift=-.5pt]},pattern color=colorPrim] ([yshift=1.25mm]-4.5,0) rectangle ([yshift=-1.25mm]Ap);
        \end{axis}
      \end{tikzpicture}
    \end{center}
    Toutes les abscisses des points de $\mathcal{C}_g$ situés \textit{au dessous} de la droite $(\Delta)$ (c'est-à-dire les points de la courbe $\mathcal{C}_g$ 
    situés avant la point $A$ et ceux situés entre $B$ et $C$) correspondent aux solutions de l'équation $g(x)< -1$.\medskip
  
    En notant $\mathcal{S}$ l'ensemble des solutions de l'inéquation $g(x)< -1$, on a \[\mathcal{S}=\interoo{\moinsinf{} -3}\cup\interoo{-2 3}\]
  \end{multicols}
\end{methode}


\section{Parité}\label{seconde:cours:fctgen:parite}

\begin{definition}\index{Fonctions!Paire}\index{Fonctions!Impaire}
  Soit $f$ une fonction définie sur un domaine $\mathcal{D}$.

  \begin{itemize}
    \item On dit que $f$ est \textbf{paire} si, pour tout élément $x\in\mathcal{D}$, on a $f(-x)=f(x)$.
    \item On dit que $f$ est \textbf{impaire} si, pour tout élément $x\in\mathcal{D}$, on a $f(-x)=-f(x)$.
  \end{itemize}
\end{definition}

\begin{propriete}
  Soit $f$ une fonction et $\mathcal{C}_f$ sa courbe représentative dans un repère orthogonal. Alors :
  \begin{itemize}
    \item Si $f$ est paire, alors $\mathcal{C}_f$ est symétrique par rapport à l'axe des ordonnées.
    \item Si $f$ est impaire, alors $\mathcal{C}_f$ est symétrique par rapport à l'origine.
  \end{itemize}
\end{propriete}

\begin{exemple}
  \begin{tblr}{width=\linewidth,colspec={X[c,1]X[c,1]},row{1}={halign={l}}}
    La fonction ci-dessous est \textbf{paire} : elle admet l'axe des ordonnées comme axe de symétrie.
     &
    La fonction ci-dessous est \textbf{impaire} : elle admet l'origine du repère comme centre de symétrie. \\
    \begin{tikzpicture}[declare function={f1(\x) = 5*e^(-\x^2/10);},scale=0.7]%
      \begin{axis}[%
          axis y line=center,axis x line=middle,        % centrer les axes
          x=0.6cm,y=0.6cm,                                  % unités sur les axes
          xmin=-5.4,xmax=5.4,ymin=-0.7,ymax=5.5,        % bornes du repère
          xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
          % minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
          grid=both,                                    % indiquer quelle geilles afficher
          tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
        ]
        \addplot[mark=none,colorPrim,very thick,domain=-5.4:5.4,samples=150] {f1(x)};
        \addplot[mark=x,only marks,samples at={-2.5},colorSec,thick] {f1(x)} coordinate[label=left:$M$] (A);
        \addplot[mark=x,only marks,samples at={2.5},colorSec,thick] {f1(x)} coordinate[label=right:$M'$] (B);
        \coordinate (O) at ($(0,0)!(A)!(0,3)$);
        \draw[colorSec,thick,densely dashed] (A) -- (O) node[sloped,midway,font=\scriptsize,colorSec] {/\!/}
        -- (B) node[sloped,midway,font=\scriptsize,colorSec] {/\!/};
      \end{axis}
    \end{tikzpicture}
     &
    \begin{tikzpicture}[declare function={f1(\x) = -\x^3/2;},scale=0.7]%
      \begin{axis}[%
          axis y line=center,axis x line=middle,        % centrer les axes
          x=0.7cm,y=0.7cm,                                  % unités sur les axes
          xmin=-2.4,xmax=2.4,ymin=-2.5,ymax=2.5,        % bornes du repère
          xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
          % minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
          grid=both,                                    % indiquer quelle geilles afficher
          tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
        ]
        \addplot[mark=none,colorPrim,very thick,domain=-2.4:2.4,samples=150] {f1(x)};
        \addplot[mark=x,only marks,samples at={-1.5},colorSec,thick] {f1(x)} coordinate[label=left:$M$] (A);
        \addplot[mark=x,only marks,samples at={1.5},colorSec,thick] {f1(x)} coordinate[label=right:$M'$] (B);
        \coordinate[label=below left:$O$] (O) at (0, 0);
        \draw[colorSec,thick,densely dashed] (A) -- (O) node[sloped,midway,font=\scriptsize,colorSec] {/\!/}
        -- (B) node[sloped,midway,font=\scriptsize,colorSec] {/\!/};
      \end{axis}
    \end{tikzpicture}
  \end{tblr}
\end{exemple}

\begin{methode}[Déterminer si une fonction est paire ou impaire]
  \textbf{Énoncé}\quad On donne la fonction $f$ définie sur \interff{-10 10} par
  \[f(x)=(x-1)(x+1)\]
  La fonction $f$ est-elle paire ? impaire ? ou ni l'un, ni l'autre ?\bigskip

  \textbf{Solution}\quad On procède en deux étapes :
  \begin{enumerate}
    \item On commence par contrôler que le domaine de définition est bien centré sur $0$.

          Ici, on a $\mathcal{D}_f=\interff{-10 10}$ et on a $\frac{-10+10}{2}=0$,
          donc le domaine est bien centré sur $0$.
    \item On compare ensuite $f(x)$ et $f(-x)$. On a ici :
          \begin{align*}
            f(x) & = (x-1)(x+1) & f(-x) & = (-x-1)(-x+1)                                                 \\
                 & = x^2 - 1^1  &       & = (-x)^2 - 1^2 &  & \longleftarrow \text{identité remarquable} \\
                 & = x^2 - 1    &       & = x^2 - 1
          \end{align*}

          On a donc $f(x)=f(-x)$ pour tout $x$ dans $\mathcal{D}_f$. Donc, la fonction est paire.
  \end{enumerate}
\end{methode}