\chapter{Vecteurs (2)}

\section{Repérage}

\subsection{Notion de base}
\begin{definition}\index{Repères!Base}\index{Base}
  Une \textbf{base} du plan est un couple de vecteurs \nuplet{\vecti{} \vectj}
  non nuls et non colinéaires (c'est-à-dire \vecti et \vectj n'ont pas la même direction).
  \medskip

  On peut distinguer quatre types bases différentes :

  \begin{center}
    \begin{tblr}{width=\linewidth,colspec={*{4}{X[c,1]}},%
        stretch=2,row{1}={font=\bfseries},
        row{3}={font=\footnotesize}}
      Base quelconque & Base normée & Base orthogonale & Base orthonormée \\
      \begin{tikzpicture}[%
          scale=0.9,%
          vecteur/.style={very thick,-latex',colorPrim}
        ]
        \clip[draw] (-1,-1) rectangle (2,2);

        \begin{scope}[
            x={(1.5cm,0)},%
            y={(50:1cm)},%
          ]
          \foreach \i in {-3,...,4} {%
              \draw[thin,black!50] (\i,-3) -- (\i,4);
              \draw[thin,black!50] (-3,\i) -- (4,\i);
            }
          \draw[vecteur,black] (0,0) -- (1,0) node [midway,below] {\vecti};
          \draw[vecteur,black] (0,0) -- (0,1) node [midway,left] {\vectj};
        \end{scope}
      \end{tikzpicture}
                      &
      \begin{tikzpicture}[%
          scale=0.9,%
          vecteur/.style={very thick,-latex',colorPrim}
        ]
        \clip[draw] (-1,-1) rectangle (2,2);

        \begin{scope}[
            x={(1cm,0)},%
            y={(70:1cm)},%
          ]
          \foreach \i in {-3,...,4} {%
              \draw[thin,black!50] (\i,-3) -- (\i,4);
              \draw[thin,black!50] (-3,\i) -- (4,\i);
            }
          \draw[vecteur,black] (0,0) -- (1,0) node [midway,below] {\vecti};
          \draw[vecteur,black] (0,0) -- (0,1) node [midway,left] {\vectj};
        \end{scope}
      \end{tikzpicture}
                      &
      \begin{tikzpicture}[%
          scale=0.9,%
          vecteur/.style={very thick,-latex',colorPrim}
        ]
        \clip[draw] (-1,-1) rectangle (2,2);

        \begin{scope}[
            x={(1.6cm,0)},%
            y={(90:0.8cm)},%
          ]
          \foreach \i in {-3,...,4} {%
              \draw[thin,black!50] (\i,-3) -- (\i,4);
              \draw[thin,black!50] (-3,\i) -- (4,\i);
            }
          \draw[vecteur,black] (0,0) -- (1,0) node [midway,below] {\vecti};
          \draw[vecteur,black] (0,0) -- (0,1) node [midway,left] {\vectj};
        \end{scope}
      \end{tikzpicture}
                      &
      \begin{tikzpicture}[%
          scale=0.9,%
          vecteur/.style={very thick,-latex',colorPrim}
        ]
        \clip[draw] (-1,-1) rectangle (2,2);
        \foreach \i in {-3,...,4} {%
            \draw[thin,black!50] (\i,-3) -- (\i,4);
            \draw[thin,black!50] (-3,\i) -- (4,\i);
          }
        \draw[vecteur,black] (0,0) -- (1,0) node [midway,below] {\vecti};
        \draw[vecteur,black] (0,0) -- (0,1) node [midway,left] {\vectj};
      \end{tikzpicture}    \\
      Les vecteurs sont quelconques.
                      &
      Les vecteurs ont la même norme.
                      &
      Les vecteurs sont orthogonaux.
                      &
      Les vecteurs sont orthogonaux et ont la même norme.
    \end{tblr}
  \end{center}
\end{definition}

\subsection{Coordonnées d'un vecteur}
\begin{propriete}\index{Coordonnées!Vecteur}\index{Vecteur!Coordonnées}
  Soit \base une base quelconque.
  \medskip

  Pour tout vecteur \vectu, il existe un unique couple de nombres
  réel \nuplet{x y} tel que \[\vectu=x\,\vecti + y\vectj\]

  On dit que \vectu a pour \textbf{coordonnées} \nuplet{ x y} dans la base \base.

  \begin{itemize}
    \item on note $\vectu \nuplet{x y}$ ou $\vectu\vcoord{x y}$ (notation \textit{préférée}) ;
    \item $x$ est l'\textbf{abscisse} du vecteur \vectu ;
    \item $y$ est l'\textbf{ordonnée} du vecteur \vectu.
  \end{itemize}
\end{propriete}

\begin{remarque}
  Dans la base \base, on a $\vecteur{0}\vcoord{0 0}$, $\vecti\vcoord{1 0}$ et $\vectj\vcoord{0 1}$.
\end{remarque}

\begin{exemple}
  \begin{multicols}{2}
    On se donne une base \base.
    \medskip

    Sur la figure-ci-contre, on a $\vectu\vcoord{3 2}$ et $\vectv\vcoord{-3 -1}$

    \begin{center}
      \begin{tikzpicture}[%
          scale=0.9,%
          vecteur/.style={very thick,-latex',colorPrim},
          vlabel/.style={midway,font=\scriptsize}
        ]
        \clip[draw] (-1,-2) rectangle (8.5,3);

        \begin{scope}[
            x={(1.5cm,0)},%
            y={(70:1cm)},%
          ]
          \foreach \i in {-3,...,6} {%
              \draw[thin,black!30] (\i,-3) -- (\i,6);
              \draw[thin,black!30] (-3,\i) -- (6,\i);
            }
          \draw[vecteur,colorSec] (0,0) -- (1,0) node [midway,below] {\vecti};
          \draw[vecteur,colorTer] (0,0) -- (0,1) node [midway,left] {\vectj};
          \draw[vecteur] (2,-1) -- ++(3,2)node[vlabel,above left] {\vectu};
          \draw[vecteur,dashed,colorSec] (2,-1) -- ++(3,0) coordinate (UI) node[vlabel,below] {3\,\vecti};
          \draw[vecteur,dashed,colorTer] (UI) -- ++(0,2) node[vlabel,right] {2\vectj};
          \draw[vecteur] (1,3) -- ++(-2,-1)node[vlabel,above left] {\vectv};
          \draw[vecteur,dashed,colorTer] (1,3) -- ++(0,-1) coordinate (VJ) node[vlabel,right] {-1\vectj};
          \draw[vecteur,dashed,colorSec] (VJ) -- ++(-2,0) node[vlabel,below] {-2\,\vecti};
        \end{scope}
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{exemple}

\subsection{Propriétés algébriques}

\begin{propriete}
  Soit une base \base du plan et $k$ un réel. Soient deux vecteurs $\vectu\vcoord{x y}$
  et $\vectv\vcoord{x' y'}$.

  On a alors :
  \[\vectu+\vectv\vcoord{x+x' y+y'}%
    \qquad\vectu-\vectv\vcoord{x-x' y-y'}%
    \qquad k\,\vectu\vcoord{kx ky}\]
\end{propriete}

\begin{exemple}
  On donne $\vectu\vcoord{-3 2}$ et $\vectv\vcoord{5 3}$.
  Soit les vecteurs $\vecteur{w}=\vectu+\vectv$ et $\vecteur{z}=-4\,\vectu$. On a alors :
  \begin{align*}
    \vecteur{w}\vcoord{-3+5 2+3}               & = \vecteur{w}\vcoord{2 5}
                                               &
    \vecteur{z}\vcoord{-4\times(-3) -4\times2} & = \vecteur{z}\vcoord{12 -8}
  \end{align*}
\end{exemple}

\begin{propriete}\label{seconde:cours:vecteurs2:prop:vectegalite}
  Soient deux vecteurs $\vectu\vcoord{x y}$  et $\vectv\vcoord{x' y'}$
  dans une base \base du plan.

  \[\vectu = \vectv \text{ si et seulement si }
    \begin{cases}
      x = x' \\
      y = y'
    \end{cases}\]
\end{propriete}

\section{Repérage dans le plan}

\subsection{Notion de repère}

\begin{definition}\index[Repère]
  % \begin{multicols}{2}
  Un \textbf{repère du plan} peut-être défini à partir d'un point $O$ et d'une base \base.
  \medskip

  \begin{minipage}{0.6\textwidth}
    \begin{itemize}
      \item on note ce repère \repere ;
      \item $O$ est l'\textbf{origine} du repère ;
      \item la droite $(O\mathtt{I})$ telle que $\vecteur{O\mathtt{I}}=\vecti$ est l'\textbf{axe des abscisses}
      \item la droite $(OJ)$ telle que $\vecteur{OJ}=\vectj$ est l'\textbf{axe des ordonnées}
    \end{itemize}
  \end{minipage}
  % \vspace*{\fill}
  \begin{minipage}{0.4\textwidth}
    \begin{center}
      \begin{tikzpicture}[vecteur/.style={very thick,-latex',colorPrim},
          every node/.append style={font=\scriptsize}]
        \begin{axis}[%
            axis y line=center,axis x line=middle,        % centrer les axes
            x=0.6cm,y=0.6cm,                                  % unités sur les axes
            xmin=-3.4,xmax=3.4,ymin=-3.4,ymax=3.4,        % bornes du repère
            xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
            grid=major,                                    % indiquer quelle geilles afficher
            xticklabels=\empty,yticklabels=\empty
          ]
          \node[below left] at (0,0) {$O$};
          \draw[vecteur,black] (0,0) -- (1,0) node [midway,below] {\vecti} node[below] {$\mathtt{I}$};
          \draw[vecteur,black] (0,0) -- (0,1) node [midway,left] {\vectj} node[left] {$J$};
          \node[anchor=south west,font=\tiny] at (-3.5,0) {axe des \textbf{abscisses}};
          \node[anchor=south west,rotate=90,font=\tiny] at (0,-3.5) {axe des \textbf{ordonnées}};
        \end{axis}
      \end{tikzpicture}
    \end{center}
  \end{minipage}
  % \end{multicols}
\end{definition}

\subsection{Coordonnées d'un point}
\begin{definition}\index{Coordonnées!Points}\label{seconde:cours:vecteurs2:def:coordpoint}

  \begin{multicols}{2}
    Soit un repère du plan \repere et un point $M$ dans le plan.
    \medskip

    On appelle \textbf{coordonnées du point $M$} l'unique couple $\nuplet{x_M y_M}$
    coordonnées du vecteur $\vecteur{OM}$ dans la base \base.

    \begin{itemize}
      \item on note $M\nuplet{x_M y_M}$ ;
      \item $x_M$ est l'\textbf{abscisse} du point $M$ ;
      \item $y_M$ est l'\textbf{ordonnée} du point $M$.
    \end{itemize}
    \begin{center}
      \begin{tikzpicture}[vecteur/.style={very thick,-latex',colorPrim},
          every node/.append style={font=\scriptsize}]
        \begin{axis}[%
            axis y line=center,axis x line=middle,        % centrer les axes
            x=0.6cm,y=0.6cm,                                  % unités sur les axes
            xmin=-2.4,xmax=6.4,ymin=-2.4,ymax=4.4,        % bornes du repère
            xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
            grid=major,                                    % indiquer quelle geilles afficher
            xticklabels=\empty,yticklabels=\empty
          ]
          \node[below left] at (0,0) {$O$};
          \coordinate[label=below left:$O$] (O) at (0, 0);
          \coordinate (I) at (1, 0);
          \coordinate (J) at (0, 1);
          \draw[vecteur,black] (0,0) -- (1,0) node [midway,below] {\vecti};
          \draw[vecteur,black] (0,0) -- (0,1) node [midway,left] {\vectj};
          \addplot[mark=+,mark size=3pt,only marks,colorPrim,very thick] coordinates {(4,3)};
          \coordinate[label=above right:$M\nuplet{x_M y_M}$] (M) at (4,3);
          \coordinate[label=below:$x_M$] (xM) at ($(O)!(M)!(I)$);
          \coordinate[label=left:$y_M$] (yM) at ($(O)!(M)!(J)$);
          \draw[densely dashed,colorPrim,thick] (M) -- (xM);
          \draw[densely dashed,colorPrim,thick] (M) -- (yM);
          \draw[vecteur,colorPrim] (O) -- (M) node [midway,above,sloped] {\vecteur{OM}};
        \end{axis}
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{definition}

\begin{exemple}
  Dans la figure de la définition \ref{seconde:cours:vecteurs2:def:coordpoint} ci-dessus,
  on a $\V{OM}=4\,\vecti+3\vectj$ donc $M\nuplet{4 3}$.
\end{exemple}


\subsection{Coordonnées d'un vecteur défini par deux points}
\begin{propriete}
  Soient deux points $A\nuplet{x_A y_A}$ et $B\nuplet{x_B y_B}$ dans un repère \repere.
  \medskip

  Le vecteur $\V{AB}$ a pour coordonnées $\V{AB}\vcoord{x_B-x_A y_B-y_A}$
\end{propriete}

\begin{remarque}
  \textnormal{Attention à l'ordre !} Par exemple, l'abscisse du vecteur $\V{AB}$ est bien la différence entre l'abscisse
  de l'extrémité $B$ et l'abscisse de l'origine $A$ (l'ordre des termes est inversé par rapport
  à l'ordre des points $A$ et $B$).
\end{remarque}

\begin{exemple}
  On donne $A\nuplet{4 -3}$ et $B\nuplet{2,5}$. On a
  \begin{align*}
    \V{AB}\vcoord{x_B-x_A y_B-y_A} & = \V{AB}\vcoord{2-4 5-(-3)} \\
                                   & = \V{AB}\vcoord{-2 8}
  \end{align*}
\end{exemple}



\begin{methode}[Prouver qu'un quadrilatère est un parallélogramme]\label{cours:seconde:vecteurs2:meth:prlg1}
  \textbf{Énoncé}\quad Dans un repère \repere, on donne quatre points $A\nuplet{-2 1}$ ;
  $B\nuplet{-3 -2}$ ; $C\nuplet{0 -6}$ et $D\nuplet{1 -3}$.

  Démontrer que $ABCD$ est un parallélogramme.
  \bigskip

  \textbf{Solution}\quad Commençons par calculer les coordonnées de deux vecteurs définis par deux côtés
  opposés du quadrilatère $ABCD$, par exemple  les vecteurs $\V{AB}$ et $\V{DC}$.
  \begin{align*}
    \V{AB}\vcoord{x_B-x_A y_B-y_A} & = \V{AB}\vcoord{-3-(-2) -2-1}
                                   &
    \V{DC}\vcoord{x_C-x_D y_C-y_D} & = \V{DC}\vcoord{0-1 -6-(-3)}
    \\
                                   & = \V{AB}\vcoord{-1 -3}
                                   &
                                   & = \V{DC}\vcoord{-1 -3}
  \end{align*}
  On constate que les coordonnées des vecteurs $\V{AB}$ et $\V{DC}$ sont égales donc $\V{AB}=\V{DC}$.

  On en déduit que $ABCD$ est un parallélogramme.
\end{methode}

\begin{methode}[Déterminer les coordonnées du quatrième sommet d'un parallélogramme]
  \textbf{Énoncé}\quad Dans un repère \repere, on donne trois points $A\nuplet{-1 0}$ ;
  $B\nuplet{1 2}$ et $C\nuplet{3 1}$.
  Déterminer les coordonnées du point $D$ tel que $ABCD$ soit un parallélogramme.\bigskip

  \textbf{Solution}\quad Si $\V{AB}=\V{DC}$ alors $ABCD$ est un parallélogramme.

  Déterminons les coordonnées des vecteurs $\V{AB}$ et $\V{DC}$ :
  \begin{align*}
    \V{AB}\vcoord{x_B-x_A y_B-y_A} & = \V{AB}\vcoord{1-(-1) 2-0}
                                   &
    \V{DC}\vcoord{x_C-x_D y_C-y_D} & = \V{DC}\vcoord{3-x_D 1-y_D}
    \\
                                   & = \V{AB}\vcoord{2 2}
  \end{align*}
  On cherche donc à déterminer $x_D$ et $y_D$.\medskip

  On veut $\V{AB}=\V{DC}$ donc les coordonnées des deux vecteurs doivent être égales. Il vient :
  \begin{align*}
    3-x_D & = 2   & 1-y_D & = 2   \\
    3-2   & = x_D & 1-2   & = y_D \\
    x_D   & = 1   & y_D   & = -1
  \end{align*}
  Donc le point $D$ tel que $ABCD$ soit un parallélogramme est $D\nuplet{1 -1}$.
\end{methode}

\begin{remarque}
  Dans l'exemple donné ci-dessus, on pouvait aussi travailler avec l'égalité $\V{AC}=\V{AB}+\V{AD}$
  qui caractérise aussi le parallélogramme $ABCD$.
\end{remarque}

\subsection{Milieu}

\begin{propriete}\label{seconde:cours:vecteurs2:prop:milieu}\index{Milieu!Coordonnées}\index{Coordonnées!Milieu}
  \begin{multicols}{2}
    Soit $M$ le milieu d'un segment $[AB]$ dans un repere \repere.
    On a
    \[\begin{cases}
        x_M=\frac{x_A+x_B}{2} \\
        y_M=\frac{y_A+y_B}{2} \\
      \end{cases}\]
    soit $M\left(\frac{x_A+x_B}{2}\,;\,\frac{y_A+y_B}{2}\right)$

    \begin{center}
      \begin{tikzpicture}[vecteur/.style={very thick,-latex',colorPrim},
          every node/.append style={font=\scriptsize}]
        \begin{axis}[%
            axis y line=center,axis x line=middle,        % centrer les axes
            x=0.6cm,y=0.6cm,                                  % unités sur les axes
            xmin=-2.4,xmax=6.4,ymin=-2.4,ymax=4.4,        % bornes du repère
            xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
            grid=major,                                    % indiquer quelle geilles afficher
            xticklabels=\empty,yticklabels=\empty
          ]
          \node[below left] at (0,0) {$O$};
          \coordinate[label=below left:$O$] (O) at (0, 0);
          \coordinate (I) at (1, 0);
          \coordinate (J) at (0, 1);
          \draw[vecteur,black] (0,0) -- (1,0) node [midway,below] {\vecti};
          \draw[vecteur,black] (0,0) -- (0,1) node [midway,left] {\vectj};
          \coordinate[label=above:$A$] (A) at (-1, 3);
          \coordinate[label=below right:$B$] (B) at (5, 1);
          \addplot[mark=+,mark size=3pt,only marks,colorPrim,very thick] coordinates {(-1, 3)(2,2)(5, 1)};
          \coordinate[label=above right:$M$] (M) at (2,2);
          \draw[very thick,colorPrim] (A) --(M) node[midway,sloped,font=\scriptsize] {/\!/}
          -- (B) node[midway,sloped,font=\scriptsize] {/\!/};
        \end{axis}
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{propriete}

\begin{remarque}
  Les coordonnées du milieu sont égales à la \textit{moyenne} des coordonnées des extrémités.
\end{remarque}

\begin{exemple}
  Sur la figure de la propriété \ref{seconde:cours:vecteurs2:prop:milieu},
  on lit $A\nuplet{-1 3}$ et $B\nuplet{5 1}$. On en déduit les coordonnées de
  $M$ le milieu du segment $[AB]$
  \begin{align*}
    x_M & =\frac{x_A+x_B}{2} & y_M & =\frac{y_A+y_B}{2} \\
        & = \frac{-1+5}{2}   &     & = \frac{3+1}{2}    \\
        & = \frac{4}{2}      &     & = \frac{4}{2}      \\
        & = 2                &     & = 2
  \end{align*}
  Donc $M\nuplet{2 2}$ est le milieu de $[AB]$.
\end{exemple}

\begin{methode}[Prouver qu'un quadrilatère est un parallélogramme (avec les milieux)]
  \textbf{Énoncé}\quad On reprend l'énoncé de l'exemple de la méthode
  \ref{cours:seconde:vecteurs2:meth:prlg1} ci-dessus.\bigskip

  \textbf{Solution}\quad On a $A\nuplet{-2 1}$ ;
  $B\nuplet{-3 -2}$ ; $C\nuplet{0 -6}$ et $D\nuplet{1 -3}$.

  Calculons les coordonnées des milieux $M$ et $L$  des segments
  $[AC]$ et $[BD]$ respectivement.
  \begin{align*}
    x_M & =\frac{x_A+x_C}{2} & y_M & =\frac{y_A+y_C}{2} \\
        & = \frac{-2+0}{2}   &     & = \frac{1-6}{2}    \\
        & = \frac{-2}{2}     &     & = \frac{-5}{2}     \\
        & = -1               &     & = -\frac{5}{2}
  \end{align*}
  et
  \begin{align*}
    x_L & =\frac{x_B+x_D}{2} & y_L & =\frac{y_B+y_D}{2} \\
        & = \frac{-3+1}{2}   &     & = \frac{-2-3}{2}   \\
        & = \frac{-2}{2}     &     & = \frac{-5}{2}     \\
        & = -1               &     & = -\frac{5}{2}
  \end{align*}
  On constate que les points $M$ et $L$ ont les mêmes coordonnées : ils sont donc confondus.

  Or si les diagonales d'un quaadrilatère ont même milieu alors c'est un parallélogramme.

  On en conclut que $ABCD$ est un parallélogramme.
\end{methode}

\section{Norme d'un vecteur}


\begin{definition}\index{Repère!Orthonormé}
  Le repère \repere est un \textbf{repère orthonormé} du plan si et seulement si
  la base \base est \textit{orthonormée}.
\end{definition}

\begin{remarque}
  Si un repère est orthonormé alors les axes sont perpendiculaires et l'unité sur chaque axe
  est la même.
\end{remarque}

\begin{propriete}\index{Vecteur!Norme}
  On se place dans un repère du plan \repere \textbf{orthonormé}.
  \begin{itemize}
    \item si $A\nuplet{x_A y_A}$ et $B\nuplet{x_B y_B}$ sont deux points alors
          \[\norme{\vecteur{AB}}=\sqrt{(x_B-x_A)^2+(y_B-y_A)^2}\]
    \item si $\vectu\vcoord{x y}$ est un vecteur alors
          \[\norme{\vectu}=\sqrt{x^2+y^2}\]
  \end{itemize}
\end{propriete}

\begin{methode}[Déterminer la nature d'un triangle.]
  \textbf{Énoncé}\quad Dans le repère orthonormé \repere, on donne trois points
  $A\nuplet{3 -1}$ ; $B\nuplet{1 2}$ et $C\nuplet{-2 0}$.

  Déterminer la nature du triangle $ABC$.\bigskip

  \textbf{Solution}\quad Calculons les normes des vecteurs définis par les côtés du triangle $ABC$.
  \begin{align*}
    \norme{\V{AB}} & = \sqrt{(x_B-x_A)^2+(y_B-y_A)^2} \\
                   & = \sqrt{(1-3)^2+(2+1)^2}         \\
                   & = \sqrt{(-2)^2+3^2}              \\
                   & = \sqrt{4+9}                     \\
                   & = \sqrt{13}
  \end{align*}

  \begin{align*}
    \norme{\V{AC}} & = \sqrt{(x_C-x_A)^2+(y_C-y_A)^2} \\
                   & = \sqrt{(-2-3)^2+(0+1)^2}        \\
                   & = \sqrt{(-5)^2+1^2}              \\
                   & = \sqrt{25+1}                    \\
                   & = \sqrt{26}
  \end{align*}

  \begin{align*}
    \norme{\V{BC}} & = \sqrt{(x_C-x_B)^2+(y_C-y_B)^2} \\
                   & = \sqrt{(-2-1)^2+(0-2)^2}        \\
                   & = \sqrt{(-3)^2+(-2)^2}           \\
                   & = \sqrt{9+4}                     \\
                   & = \sqrt{13}
  \end{align*}

  On constate d'une part que $\norme{\V{AB}}=\norme{\V{BC}}$ donc le triangle $ABC$ est isocèle en $B$.

  D'autre part, on a :
  \begin{align*}
    \norme{\V{AB}}^2 + \norme{\V{BC}}^2 & = (\sqrt{13})^2 + (\sqrt{13})^2 & \norme{\V{AC}}^2 = (\sqrt{26})^2       \\
                                        & = 13 + 13                       &                                  & =26 \\
                                        & = 26
  \end{align*}
On constate que \[\norme{\V{AB}}^2 + \norme{\V{BC}}^2=\norme{\V{AC}}^2\]
d'après le théorème de Pythagore, on en déduit que le triangle $ABC$ est rectangle en $B$.
\medskip

  \textbf{Conclusion} : le triangle $ABC$ est isocèle et rectangle en $B$.
\end{methode}


% \section{Colinéarité}
% \begin{propriete}\label{seconde:cours:vecteurs2:propriete:colcoord}
%   Soient deux vecteurs $\vectu\vcoord{x y}$ et $\vectv\vcoord{x' y'}$  dans le repère \repere.

%   $\vectu$ et $\vectv$ sont \textbf{colinéaires} si et seulement si \[xy'-x'y=0\]
% \end{propriete}

% \begin{methode}[Démontrer que des vecteurs sont colinéaires.]
%   \textbf{Énoncé}\quad--\quad Dans les deux cas suivants, déterminer si les vecteurs \vectu et \vectv sont colinéaires.
%   \begin{enumerate}
%     \item $\vectu\vcoord{-2 3}$ et $\vectv\vcoord{5 1}$ ;
%     \item $\vectu\vcoord{\frac{3}{2} -\frac{1}{4}}$ et $\vectv\vcoord{-2 \frac{1}{3}}$.
%   \end{enumerate}
%   \textbf{Énoncé}\quad--\quad 
%   \begin{enumerate}
%     \item On a $\vectu\vcoord{-2 3}$ et $\vectv\vcoord{5 1}$ donc :
%           \begin{align*}
%             x_{\vectu}\times y_{\vectv} - x_{\vectv}\times y_{\vectu}
%              & = -2\times1-3\times5 \\
%              & =-2-15               \\
%              & \neq 0
%           \end{align*}
%           D'après la propriété \ref{seconde:cours:vecteurs2:propriete:colcoord},
%           on en déduit que les vecteurs $\vectu$ et $\vectv$ ne sont pas colinéaires.
%     \item On a $\vectu\vcoord{\frac{3}{2} -\frac{1}{4}}$ et $\vectv\vcoord{-2 \frac{1}{3}}$ donc :
%           \begin{align*}
%             x_{\vectu}\times y_{\vectv} - x_{\vectv}\times y_{\vectu}
%              & = \frac{3}{2}\times \frac{1}{3}-2\times \left(-\frac{1}{4}\right) \\
%              & =\frac{1}{2} - \frac{1}{2}                                        \\
%              & = 0
%           \end{align*}
%           D'après la propriété \ref{seconde:cours:vecteurs2:propriete:colcoord},
%           on en déduit que les vecteurs $\vectu$ et $\vectv$ sont colinéaires.
%   \end{enumerate}
% \end{methode}
