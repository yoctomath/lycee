% \usepackage{tdsfrmath}
% \usepackage{setspace}
% \usetikzlibrary {decorations.pathreplacing}
\chapter{Vecteurs (1)}

\section{La translation comme contexte}

\begin{multicols}{2}
  Lors d'une \textbf{translation}, les objets sont déplacés selon un \textit{glissement sans rotation}.
  Si on regarde les déplacements de chaque point de l'objet et qu'on les matérialise par une flèche,
  on remarque que toutes ces flèches ont en commun trois caractéristiques~: leur \textit{direction},
  leur \textit{sens} et leur \textit{longueur}.

  Ces trois caractéristiques vont nous servir à définir un nouvel objet mathématique~: le \textbf{vecteur}.
  \vspace*{\fill}
  \begin{center}
    \begin{tikzpicture}[scale=0.6]
      \coordinate (A) at (0, 0);
      \coordinate (B) at (3, -1);
      \coordinate (C) at (4, 1);
      \coordinate (D) at (2, 1);
      \coordinate (E) at (2, 4);
      \foreach \pt [remember=\pt as \lastpt (initially A) ]in {A,...,E} {%
      \coordinate (\pt0) at ($(\pt)+(6,2)$);
      }
      \fill[colorPrim!10,draw=colorPrim!50,dashed,thick] (A) -- (B) -- (C) -- (D) -- (E) -- cycle;
      \fill[colorPrim!30,draw=colorPrim,thick] (A0) -- (B0) -- (C0) -- (D0) -- (E0) -- cycle;
      \foreach \pt [remember=\pt as \lastpt (initially A) ]in {B,...,E,A} {%
      \draw[-latex',colorSec,very thick] (\pt) -- (\pt0);
      }
      \coordinate (commentDest) at ($(B)!0.5!(B0)$);
      \node[anchor=west,colorTer,fill=colorTer!10,text width=3cm,align=left,draw,rounded corners,font=\scriptsize] at([xshift=2cm,yshift=-0.5cm]commentDest) (comment) {Le même \textbf{vecteur} sert pour déplacer tous les points de la figure.};
      \draw[colorTer,thin] (comment) to[out=180,in=-30] (commentDest);
    \end{tikzpicture}
  \end{center}
\end{multicols}

\subsection{Définition}

\begin{definition}
  On appelle \textbf{vecteur} un objet mathématique noté $\vectu$, défini par trois caractéristiques~:
  \begin{multicols}{2}
    \begin{itemize}
      \item une \textbf{direction} ;
      \item un \textbf{sens} ;
      \item une \textbf{norme} (notée $\norme{\vectu}$).
    \end{itemize}
    \vspace*{\fill}
    \begin{center}
      \begin{tikzpicture}[decoration={brace,amplitude=10pt},scale=0.7]
        \coordinate (A) at (0, 0);
        \coordinate (B) at (4, -1);
        \coordinate (A') at ($(B)!1.3!(A)$);
        \coordinate (B') at ($(A)!1.3!(B)$);
        \draw[colorSec,dashed,thin,shorten >=-6mm,shorten <=-6mm] (A') -- (B');
        \draw[colorSec,very thick,-latex'] (A) -- (B) node[midway,above] {$\vectu$};
        \draw[thin,decorate,black!40] ([xshift=-0.5mm,yshift=-2mm]B) -- ([xshift=-0.5mm,yshift=-2mm]A) node[anchor=north west,black,midway,below=4mm,font={\lightfont\scriptsize}] (norme) {norme};
        \node[above=of A',font={\lightfont\scriptsize}] (direction) {direction};
        \coordinate (Bfleche) at ($(A)!0.95!(B)$);
        \node[above right=8mm of Bfleche,font={\lightfont\scriptsize}] (sens) {sens};
        \draw[thin,black!40] (direction) -- (A');
        \draw[thin,black!40] (sens) -- (Bfleche);
      \end{tikzpicture}
    \end{center}
  \end{multicols}
  En utilisant deux points $A$ et $B$, on définit le vecteur $\V{AB}$ par le déplacement correspondant
  à la translation qui transforme $A$ en $B$~:

  \begin{multicols}{2}
    \begin{itemize}
      \item la direction de $\V{AB}$ est celle de la droite $(AB)$~;
      \item le sens de $\V{AB}$ est celui de $A$ vers $B$.
      \item la norme $\norme{\V{AB}}$ est la distance de $A$ à $B$.
    \end{itemize}
    \textit{Lorsque le vecteur $\V{AB}$ est tracé}, on dira que $A$ est l'\textbf{origine} du représentant du vecteur $\V{AB}$ et le point $B$ son \textbf{extrémité}.
    \columnbreak
    \begin{center}
      \begin{tikzpicture}
        \draw[colorSec,very thick,-latex'] (0,0) coordinate[label=below left:$A$] (A) -- (4,1.5) coordinate[label=above right:$B$] (B) node[midway,above left] {$\V{AB}$};
        \node[above=of A,font={\lightfont\scriptsize}] (origine) {origine};
        \node[below=of B,font={\lightfont\scriptsize}] (extremite) {extrémité};
        \foreach \ptA/\ptB in {A/origine,B/extremite} \draw[thin,black!40] (\ptA) -- (\ptB);
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{definition}

\begin{remarque}
  Un vecteur est un objet \textit{abstrait} qui est associé à un déplacement.
  \begin{multicols}{2}
    \begin{center}
      \begin{tikzpicture}
        \coordinate[label=left:$A$] (A) at (0, 0.3);
        \coordinate[label=left:$B$] (B) at (1.5, -0.7);
        \coordinate[label=left:$C$] (C) at (0.5, 1.2);
        \coordinate[label=left:$D$] (D) at (-0.3, -0.5);
        \node[anchor=west,colorTer,fill=colorTer!10,text width=2.5cm,align=left,draw,rounded corners,font=\scriptsize] at (3.5,0.5) (comment) {Un seul vecteur $\vectu$, plusieurs représentants.};
        \foreach \pt in {A,...,D} {
            \node[cross=2.5pt] at (\pt) {};
            \draw[colorSec,very thick,-latex'] (\pt) -- ++(1.5,0.5) coordinate (\pt0) node[midway,above] {$\vectu$};
            \draw[thin,colorTer] (\pt0) to[out=0,in=180] (comment.west);
          }
      \end{tikzpicture}
    \end{center}
    De même que l'on peut reproduire le même déplacement depuis plusieurs points de départ, lorsqu'on trace un vecteur $\vectu$, on se donne un \textbf{représentant}.
    IL existe une infinité de représentant d'un même vecteur $\vectu$.
  \end{multicols}
\end{remarque}

\begin{methode}[Construire l'image d'une figure par une translation de vecteur $\vectu$]
  \begin{multicols}{2}
    \textbf{Énoncé}\quad Construire l'image du triangle $ABC$ par la translation de vecteur $\vectu$.
    \begin{center}
      \begin{tikzpicture}[scale=0.6,every label/.style={black,font=\scriptsize}]
        \draw[thin,LightGray] (-0.3,-0.3) grid (12.3,7.3);
        \coordinate[label=left:$A$] (A) at (1, 3);
        \coordinate[label=above:$B$] (B) at (3, 6);
        \coordinate[label=below right:$C$] (C) at (5, 2);
        \fill[colorPrim!50,opacity=0.5,draw=colorPrim] (A) -- (B) -- (C) -- cycle;
        \coordinate (u) at (5, 1);
        \coordinate (O) at (6, 0);
        \draw[colorSec,very thick,-latex'] (O) -- ++(u) node[midway,above,font=\scriptsize] {$\vectu$};
      \end{tikzpicture}
    \end{center}
    \columnbreak
    \textbf{Solution}\quad On construit trois représentants du vecteurs $\vectu$ d'origines $A$, $B$ et $C$.

    Les extrémités respectives $A'$, $B'$ et $C'$ de ces représentants définissent l'image de $ABC$ par la translation de vecteur $\vectu$.
    \begin{center}
      \begin{tikzpicture}[scale=0.6,every label/.style={black,font=\scriptsize,opacity=1}]
        \draw[thin,LightGray] (-0.3,-0.3) grid (12.3,7.8);
        \coordinate[label=left:$A$] (A) at (1, 3);
        \coordinate[label=above:$B$] (B) at (3, 6);
        \coordinate[label=below right:$C$] (C) at (5, 2);
        \fill[colorPrim!50,opacity=0.5,draw=colorPrim] (A) -- (B) -- (C) -- cycle;
        \coordinate (u) at (5, 1);
        \coordinate (O) at (6, 0);
        \draw[colorSec,very thick,-latex'] (O) -- ++(u) node[midway,above,font=\scriptsize] {$\vectu$};
        \foreach \pt in {A,B,C} \draw[colorSec,very thick,-latex',dashed,opacity=0.5] (\pt) -- ++(u) coordinate[label=above:$\pt'$] (\pt0);
        \fill[colorPrim!50,opacity=0.5,draw=colorPrim] (A0) -- (B0) -- (C0) -- cycle;
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{methode}

\subsection{Vecteurs particuliers}

\subsubsection{Vecteur nul}

\begin{definition}[Vecteur nul]\index{Vecteur!Nul}
  Le vecteur dont la norme est égale à zéro est appelé \textbf{vecteur nul} et est noté $\V{0}$.
\end{definition}

\begin{propriete}
  Soient $A$ et $B$ deux points. Le vecteur $\V{AB}$ est nul si et seulement si $A=B$
  (c'est-à-dire $A$ et $B$ sont confondus).
\end{propriete}

\begin{remarque}
  Le vecteur $\V{AA}$ est un \textit{vecteur nul} et on a $\V{AA}=\vecteur{0}$\footnotemark{}.
\end{remarque}
\footnotetext{Une translation de vecteur nul $\vecteur{0}$ est appelée une \textbf{identité}
  et l'image de toute figure est confondue avec la figure de départ.}


\subsubsection{Vecteurs opposés}

\begin{definition}[Vecteurs opposés]\index{Vecteur!Opposé}
  Deux vecteurs qui ont même direction, même norme mais des sens \textit{contraires} sont dits \textbf{opposés}.

  On notera alors $-\vectu$ pour l'opposé du vecteur $\vectu$.
\end{definition}

\begin{remarque}
  Le vecteur $\V[5]{BA}$ est le vecteur \textbf{opposé} au vecteur $\V{AB}$.
  \begin{multicols}{2}
    Le vecteur $\V[5]{BA}$ a même direction et même norme que le vecteur $\V{AB}$ mais a un sens \textit{opposé}.
    \begin{center}
      \begin{tikzpicture}[scale=0.6,every label/.style={black,font=\scriptsize,opacity=1}]
        \coordinate[label=above left:$A$] (A) at (0, 0);
        \coordinate[label=above right:$B$] (B) at (4, -1);
        \coordinate (A') at ($(B)!1.3!(A)$);
        \coordinate (B') at ($(A)!1.3!(B)$);
        \foreach \pt in {A,B}\node[cross=2.5pt] at (\pt) {};
        \draw[black!50,dashed,thin,shorten >=-6mm,shorten <=-6mm] (A') -- (B');
        \draw[colorPrim,very thick,-latex'] ([xshift=0.25pt,yshift=1pt]A) -- ([xshift=0.25pt,yshift=1pt]B) node[midway,above,font={\scriptsize}] {$\V{AB}$};
        \draw[colorSec,very thick,-latex'] ([xshift=-0.25pt,yshift=-1pt]B) -- ([xshift=-0.25pt,yshift=-1pt]A) node[midway,below,font={\scriptsize}] {$\V[5]{BA}$};
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{remarque}

\begin{exemple}
  Dans la figure ci-dessous, les vecteurs $\vectu$ et $\vectv$ sont opposés
  \begin{multicols}{2}
    En effet, $\vectu$ est représenté par $\V{AB}$ et  $\vectv$ est représenté par $\V{CD}$
    avec~:
    \begin{itemize}
      \item les droites $(AB)$ et $(CD)$ parallèles , c'est-à-dire qu'ils ont la même direction~;
      \item les longueurs $AB=CD$, c'est-à-dire qu'ils ont même norme~;
      \item aller de $A$ vers $B$, c'est aller dans le sens contraire de $C$ vers $D$ donc les sens sont contraires.
    \end{itemize}
    \vfill
    \columnbreak

    \begin{center}\footnotesize
      \begin{tikzpicture}[scale=0.7]
        \coordinate[label=left:$C$] (C) at (0,0);
        \coordinate[label=above left:$D$] (D) at (70:4cm);
        \coordinate (t1) at (2,0.5);
        \coordinate[label=below right:$A$] (A) at ($(C)+(t1)$);
        \coordinate[label=above right:$B$] (B) at ($(D)+(t1)$);
        \draw[-latex',very thick,colorPrim] (A) -- (B) node[colorSec, midway, right] {$\vectu$} node[midway, sloped] {/\!/};
        \draw[-latex',very thick,colorSec] (D) -- (C) node[colorSec,midway, left] {$\vectv$} node[midway, sloped] {/\!/};
        \draw[dashed,thin,opacity=0.5, shorten >=-5mm, shorten <=-15mm] (A) -- (B);
        \draw[dashed,thin,opacity=0.5, shorten >=-5mm, shorten <=-15mm] (C) -- (D);
        \coordinate (X) at ($(C)!-0.3!(D)$);
        \coordinate (Y) at ($(A)!-0.3!(B)$);
        \draw[colorTer, latex'-latex'] (X) -- (Y) node[midway, sloped, fill=white] {\tiny parallèle};
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{exemple}

\subsection{Égalité de vecteurs}

\begin{propriete}
  Deux vecteurs sont dits \textbf{égaux} si et seulement ils ont les mêmes caractéristiques~:
  \begin{itemize}
    \item la même direction~;
    \item le même sens~;
    \item des normes égales.
  \end{itemize}
\end{propriete}

\begin{exemple}
  On considère $\vectu$ et $\vectv$ deux vecteurs~:
  \begin{multicols}{4}\scriptsize
    \begin{center}
      \begin{tikzpicture}[scale=0.6,every node/.append style={font=\scriptsize},vect/.style={very thick,-latex'}]
        \coordinate (A) at (0,0);
        \coordinate (B) at (30:3cm);
        \coordinate (C) at (1.5,0);
        \coordinate (D) at ($(C)+(B)$);
        \draw[vect,colorPrim] (A) -- (B) node[midway,above left] {$\vectu$};
        \draw[vect,colorSec] (D) -- (C) node[midway,below right] {$\vectv$};
      \end{tikzpicture}

      Même norme et même direction mais pas le même sens~: $\vectu\neq\vectv$.
    \end{center}

    \begin{center}
      \begin{tikzpicture}[scale=0.6,every node/.append style={font=\scriptsize},vect/.style={very thick,-latex'}]
        \coordinate (A) at (0,0);
        \coordinate (B) at (30:3cm);
        \coordinate (C) at (1.5,0);
        \coordinate (D) at ($(C)+(30:4cm)$);
        \draw[vect,colorPrim] (A) -- (B) node[midway,above left] {$\vectu$};
        \draw[vect,colorSec] (C) -- (D) node[midway,below right] {$\vectv$};
      \end{tikzpicture}

      Même direction et même sens mais pas la même norme~: $\vectu\neq\vectv$.
    \end{center}
    \columnbreak

    \begin{center}
      \begin{tikzpicture}[scale=0.6,every node/.append style={font=\scriptsize},vect/.style={very thick,-latex'}]
        \coordinate (A) at (0,0);
        \coordinate (B) at (30:3cm);
        \coordinate (C) at (1.5,0);
        \coordinate (D) at ($(C)+(20:3cm)$);
        \draw[vect,colorPrim] (A) -- (B) node[midway,above left] {$\vectu$};
        \draw[vect,colorSec] (C) -- (D) node[midway,below right] {$\vectv$};
      \end{tikzpicture}

      Même norme mais pas la même direction~: $\vectu\neq\vectv$.
    \end{center}

    \begin{center}
      \begin{tikzpicture}[scale=0.6,every node/.append style={font=\scriptsize},vect/.style={very thick,-latex'}]
        \coordinate (A) at (0,0);
        \coordinate (B) at (30:3cm);
        \coordinate (C) at (1.5,0);
        \coordinate (D) at ($(C)+(B)$);
        \draw[vect,colorPrim] (A) -- (B) node[midway,above left] {$\vectu$};
        \draw[vect,colorSec] (C) -- (D) node[midway,below right] {$\vectv$};
      \end{tikzpicture}

      Même norme, même direction et même sens~: $\vectu=\vectv$.
    \end{center}
  \end{multicols}
\end{exemple}

% \begin{center}
%   \begin{tikzpicture}[scale=0.6,every label/.style={black,font=\scriptsize,opacity=1}]
%     \draw[thin,LightGray] (0,0) grid (6,4);
%     \coordinate (u) at (-3, -2);
%     \coordinate (Ou) at (5, 2);
%     \coordinate (Ov) at (4, 4);
%     \draw[colorPrim,very thick,-latex'] (Ou) -- ++(u) node[midway,above,font=\scriptsize] {$\vectu$};
%     \draw[colorSec,very thick,-latex'] (Ov) -- ++(u) node[midway,above,font=\scriptsize] {$\vectv$};
%   \end{tikzpicture}
% \end{center}

\begin{propriete}
  Soit $A$, $B$, $C$ et $D$ quatre points distincts.

  \begin{multicols}{2}
    \vspace*{\fill}
    \begin{center}\doublespacing
      $\V{AB}=\V{CD}$

      si et seulement si

      $AB\Mhl{DC}$ est un parallélogramme\footnotemark{}.
    \end{center}
    \vspace*{\fill}
    \begin{center}
      \begin{tikzpicture}[scale=0.4,every label/.style={black,font=\scriptsize,opacity=1}]
        \draw[thin,LightGray] (0,0) grid (7,6);
        \coordinate (u) at (4, 2);
        \coordinate[label=above left:$A$] (A) at (1, 3);
        \coordinate[label=below left:$C$] (C) at (2, 1);
        \coordinate[label=above right:$B$] (B) at ($(A)+(u)$);
        \coordinate[label=below right:$D$] (D) at ($(C)+(u)$);
        \draw[colorPrim,very thick,-latex'] (A) -- (B) node[midway,above,sloped,font=\scriptsize] {$\V{AB}$};
        \draw[colorSec,very thick,-latex'] (C) -- (D) node[midway,above,sloped,font=\scriptsize] {$\V{CD}$};
        \foreach \ptA/\ptB in {A/C,B/D} \draw[dashed,black!50,opacity=0.7] (\ptA) -- (\ptB);
      \end{tikzpicture}\quad
      \begin{tikzpicture}[scale=0.4,every label/.style={black,font=\scriptsize,opacity=1}]
        \draw[thin,LightGray] (0,0) grid (8,6);
        \coordinate (u) at (2,1);
        \coordinate[label=below left:$A$] (A) at (1, 1);
        \coordinate[label=below:$C$] (C) at ($(A)+2*(u)$);
        \coordinate[label=above:$B$] (B) at ($(A)+(u)$);
        \coordinate[label=above right:$D$] (D) at ($(C)+(u)$);
        \draw[dashed,black!50,opacity=0.7] (A) -- (D);
        \draw[colorPrim,very thick,-latex'] (A) -- (B) node[midway,above,sloped,font=\scriptsize] {$\V{AB}$};
        \draw[colorSec,very thick,-latex'] (C) -- (D) node[midway,above,sloped,font=\scriptsize] {$\V{CD}$};
        \foreach \pt in {A,B,C,D} \node[cross=2pt,thick] at (\pt) {};
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{propriete}
\footnotetext{Ce parallélogramme peut-être éventuellement aplati dans le cas où les points $A$, $B$, $C$ et $D$
  sont alignés (figure de droite).}



\begin{remarque}
  On rappelle que $AB\textcolor{colorSec}{DC}$ est un parallélogramme~:
  \begin{description}
    \item[si et seulement si] les segments $[AD]$ et $[BC]$ ont même milieu.
    \item[si et seulement si] les longueurs des côtés opposés sont égales, soit $AB=CD$ et $AC=BD$
    \item[si et seulement si] les côtés opposés sont parallèles deux à deux, soit $[AB] \mathbin{\!/\mkern-3mu/\!} [CD]$  et $[AC] \mathbin{\!/\mkern-3mu/\!} [BD]$.
    \item[si et seulement si] un seul couple de côtés opposés sont parallèles et de même longueur,
          soit  $[AB] \mathbin{\!/\mkern-3mu/\!} [CD]$ et $AB=CD$ (ou $[AC] \mathbin{\!/\mkern-3mu/\!} [BD]$ et $AC=BD$).
  \end{description}
\end{remarque}

\begin{propriete}[Caractérisation du milieu d'un segment]
  Soit $A$, $B$ et $M$ trois points distincts.


  \begin{multicols}{2}
    \begin{center}
      \begin{tikzpicture}[scale=0.6,every label/.style={black,font=\scriptsize,opacity=1}]
        \coordinate[label=above left:$A$] (A) at (0, 0);
        \coordinate[label=above:$M$] (M) at (4, 1);
        \coordinate[label=above right:$B$] (B) at ($(M)+(M)$);
        \coordinate (A') at ($(B)!1.2!(A)$);
        \coordinate (B') at ($(A)!1.2!(B)$);
        \foreach \pt in {A,B,M}\node[cross=2.5pt] at (\pt) {};
        \draw[black!50,dashed,thin,shorten >=-6mm,shorten <=-6mm] (A') -- (B');
        \draw[colorPrim,very thick,-latex'] (A) -- (M) node[midway,above,font={\scriptsize}] {$\V{AM}$};
        \draw[colorSec,very thick,-latex'] (M) -- (B) node[midway,below,font={\scriptsize}] {$\V[5]{MB}$};
      \end{tikzpicture}
    \end{center}
    \begin{center}\doublespacing
      $M$ est le milieu de $[AB]$

      si et seulement si

      $\V{AM}=\V{MB}$.
    \end{center}
  \end{multicols}
\end{propriete}

\subsection{Relations fondamentales}
Des relations fondamentales se déduisent des différentes définitions ci-dessus.
\begin{propriete}\label{seconde:cours:vecteurs1:prop:relationfondamentale}
  \index{Vecteur!Relation fondamentale}
  Les trois assertions suivantes sont équivalentes (c'est-à-dire interchangeables)~:
  \begin{multicols}{2}
    \vspace*{\fill}
    \begin{itemize}
      \item la translation de vecteur $\V{AB}$ transforme $C$ en $D$~;
      \item le quadrilatère $ABDC$ est un parallélogramme~;
      \item les vecteurs $\V{AB}$ et $\V{CD}$ sont égaux.
    \end{itemize}
    \vspace*{\fill}
    \begin{center}
      \newsavebox\mybox
      \begin{lrbox}{\mybox}
        \begin{tikzpicture}[scale=0.5]
          \node (C) at (0,0) {$C$};
          \node (D) at (3,0) {$D$};
          \draw[-latex'] (C) -- (D) node[midway, above] {$t_{\V{AB}}$};
        \end{tikzpicture}
      \end{lrbox}

      \begin{tikzpicture}[statement/.style={draw,rounded corners,fill=colorSec!20}, scale=0.7]
        \coordinate (t) at (90:2cm);
        \coordinate (e) at (-10:3cm);
        \coordinate (p) at (190:3cm);
        \node[statement] (translation) at (t) {\usebox\mybox};
        \node[statement] (egalite) at (e) {$\V{AB}=\V{CD}$};
        \node[statement] (parallelogramme) at (p) {$ABDC$ parallélogramme};
        \draw[to-to,shorten >=2mm, shorten <=2mm, ultra thick] (parallelogramme) -- (translation);
        \draw[<->,shorten >=2mm, shorten <=2mm, ultra thick] (parallelogramme) -- (egalite);
        \draw[<->,shorten >=2mm, shorten <=2mm, ultra thick] (egalite) -- (translation);
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{propriete}


\section{Addition}

\subsection{Somme de deux vecteurs}

\begin{definition}\index{Vecteur!Somme}
  La \textbf{somme} de deux vecteurs $\vectu$ et $\vectv$, notée  $\vectu+\vectv$, est définie
  comme le vecteur associé à la translation résultant de l'enchaînement de la translation de vecteur $\vectu$ suivie de
  la translation de vecteur $\vectv$.

  \begin{multicols}{2}
    \begin{center}\footnotesize
      Les représentants de $\vectu$ et $\vectv$ sont tracés "bout à bout".\medskip

      \begin{tikzpicture}[scale=0.5]
        \coordinate (A) at (0,0);
        \coordinate (B) at (4,3);
        \coordinate (C) at (6,1);
        \draw[very thick, -latex',colorPrim] (A) -- (B) node[midway, above left] {$\vectu$};
        \draw[very thick, -latex',colorSec] (B) -- (C) node[midway, above right] {$\vectv$};
        \draw[very thick, -latex',colorTer] (A) -- (C) node[midway, below] {$\vectu + \vectv$};
      \end{tikzpicture}

    \end{center}
    \vfill
    \columnbreak

    \begin{center}\footnotesize
      Les représentants de $\vectu$ et $\vectv$ sont tracés avec la même origine.\medskip

      \begin{tikzpicture}[scale=0.5]
        \coordinate (A) at (0,0);
        \coordinate (B) at (4,3);
        \coordinate (C) at (6,1);
        \coordinate (D) at ($(C)-(B)$);
        \draw[very thick, -latex',colorPrim] (A) -- (B) node[midway, above left] {$\vectu$};
        \draw[very thick, -latex',colorSec] (A) -- (D) node[midway, below left] {$\vectv$};
        \draw[very thick, -latex',colorTer] (A) -- (C) node[midway, below] {$\vectu + \vectv$};
        \draw[dashed,black!40] (B) -- (C) -- (D);
        \node[right=2cm of B, black!40,font=\lightfont] (comment) {parallélogramme};
        \draw[black!40,thin] (comment.west) -- ($(B)!0.5!(C)$);
      \end{tikzpicture}

    \end{center}
  \end{multicols}
\end{definition}

\begin{remarque}
  La somme de deux vecteurs est commutative~: $\vectu+\vectv = \vectv+\vectu$.
\end{remarque}
\begin{methodetwocols}[Construire la somme de 2 vecteurs]\label{seconde:cours:vecteurs1:methode:addition}
  % \begin{multicols}{2}
  \textbf{Énoncé}\quad Placer sur la figure suivante le point $A$\\ tel que $\vecteur{AB} = \vectu + \vectv$.
  \begin{center}
    \begin{tikzpicture}[every node/.style={font=\scriptsize},every label/.style={font=\scriptsize}]
      \def\Vu{(3,1)}
      \def\Vv{(-0.5,-1.5)}
      \draw[LightGray,step=5mm] (0,0) grid (6,5);
      \draw[thick,colorPrim,-latex'] (1,3) -- +\Vu node[midway,above] {$\vectu$};
      \draw[thick,colorSec,-latex'] (5.5,3.5) -- +\Vv node[midway,below right] {$\vectv$};
      \coordinate[label=above left:$A$] (A) at (1, 2);
      \node[cross=2.5pt] at (A) {};
    \end{tikzpicture}
  \end{center}
  \vspace*{\fill}%
  \columnbreak
  \textbf{Solution}
  \begin{enumerate}
    \item Je commence par construire un représentant du vecteur $\vectu$ d'origine $A$.\\
          J'obtiens un point intermédiaire à l'extrémité de ce représentant~: je le nomme $A'$.
    \item Je construis un représentant du vecteur $\vectv$ d'origine $A'$.\\
          À son extrémité se trouve le point $B$.
  \end{enumerate}
  \begin{center}
    \begin{tikzpicture}[every node/.style={font=\scriptsize},every label/.style={font=\scriptsize}]
      \def\Vu{(3,1)}
      \def\Vv{(-0.5,-1.5)}
      \draw[LightGray,step=5mm] (0,0) grid (6,5);
      \draw[very thick,colorPrim,-latex'] (1,3) -- +\Vu node[midway,above] {$\vectu$};
      \draw[very thick,colorSec,-latex'] (5.5,3.5) -- +\Vv node[midway,below right] {$\vectv$};
      \coordinate[label=above left:$A$] (A) at (1, 2);
      \node[cross=2.5pt] at (A) {};
      \draw[ultra thick,colorPrim,-latex',dashed] (A) -- +\Vu coordinate[label=above right:$A'$,text=gray] (A');
      \draw[ultra thick,colorSec,-latex',dashed] (A') -- +\Vv node[below,text=black] {$B$};
    \end{tikzpicture}
  \end{center}
  % \end{multicols}
\end{methodetwocols}


\begin{propriete}[Relation de Chasles]\index{Vecteur!Chasles}\label{seconde:cours:vecteurs1:prop:chasles}
  Soient $A$, $B$ et $C$ trois points \textit{quelconque}. On a la relation suivantes~:
  \[\V{AB}+\V{BC}=\V{AC}\]
\end{propriete}

\begin{remarque}
  Soient $A$ et $B$ deux points du plan. D'après la relation de Chasles ci-dessus, on a
  \[\V{AB}+\V{BA}=\V{0}\]
\end{remarque}

\begin{methode}[Utiliser la relation de Chasles pour transformer une expression vectorielle]
  \textbf{Énoncé}\quad Prouver que si $\V{AC}=\V{AB}+\V{AD}$ alors $ABCD$ est un parallélogramme.
  \medskip

  \textbf{Solution}\quad On a :
  \begin{align*}
    \V{AC}                    & =  \V{AB} + \V{AD}                                                                  \\
    \V{A\Mhl{D}}+\V{\Mhl{D}C} & = \V{AB} + \V{AD}          &  & \longleftarrow\text{\lightfont Relation de Chasles} \\
    \cancel{\V{AD}}+\V{DC}    & = \V{AB} + \cancel{\V{AD}}                                                          \\
    \V{DC}                    & = \V{AB}                                                                            \\
  \end{align*}
  D'après la propriété \ref{seconde:cours:vecteurs1:prop:relationfondamentale}, on a donc bien $ABCD$ un parallélogramme.
\end{methode}

\begin{remarque}
  \textit{Une autre visualisation de la somme de deux vecteurs.}
  \begin{multicols}{2}
    Dans la méthode  précédente, on expose le fait que la somme de deux vecteurs de même origine
    est matérialisé par la diagonale du  parallélogramme construit sur ces deux représentants.
    \medskip

    Cette représentation est prisée en mécanique par exemple où les forces sont représentées par des vecteurs.
    On peut alors visualiser la résultante de deux forces comme la somme des deux vecteurs.

    Avec la méthode \ref{seconde:cours:vecteurs1:methode:addition}, nous avons donc deux façons d'aborder la construction
    de la somme de deux vecteurs.
    \vspace*{\fill}

    \begin{center}
      \begin{tikzpicture}[every node/.style={font=\scriptsize},every label/.style={font=\scriptsize}]
        \def\Vu{(2,3)}
        \def\Vv{(2.5,0.5)}
        \draw[LightGray,step=5mm] (0,0) grid (5.5,4.5);
        \coordinate[label=above left:$A$] (A) at (0.5, 0.5);
        \draw[very thick,colorPrim,-latex'] (A) -- +\Vu coordinate[label=above:$D$](D) node[midway,above left] {$\vectu$};
        \draw[very thick,colorSec,-latex'] (A) -- +\Vv coordinate[label=above:$B$](B) node[midway,below right] {$\vectv$};
        \draw[black!50,-latex',dashed] (B) -- +\Vu coordinate[label=above right:$C$,text=gray] (C) -- (D);
        \draw[very thick,colorTer,-latex'] (A) -- (C) node[midway,sloped,above] {$\vectu + \vectv$};
        \foreach \pt in {A,B,C,D}\node[cross=2.5pt] at (\pt) {};
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{remarque}

\begin{propriete}[Somme nulle de deux vecteurs et milieu]
  $\V{AB}+\V{AC}=\V{0}$ si et seulement si $A$ est le milieu du segment $[BC]$.
\end{propriete}

\paragraph*{\faCogs\quad Preuve}
\begin{itemize}
  \item Si $\V{AB}+\V{AC}=\V{0}$, alors $\V{AB}=-\V{AC}$ c'est-à-dire $\V{CA}=\V{AB}$.

        On en déduit que les points $C$, $A$ et $B$ sont alignés dans cet ordre avec $CA=AB$

        Conclusion~: $A$ est
        le milieu du segment $[BC]$.
  \item \textit{Récirpoquement}, si $A$ est le milieu du segment $[BC]$, alors $BA=AC$ et $B$, $A$ et $C$ sont alignés dans cet ordre.

        On en déduit que les vecteurs $\V{BA}$ et $\V{AC}$ sont égaux, c'est-à-dire
        que $\V{AB}$ et $\V{AC}$ sont opposés.

        Conclusion~: $\V{AB}+\V{AC}=\V{0}$.
\end{itemize}

\subsection{Différence de deux vecteurs}

\begin{definition}\index{Vecteur!Différence}
  La \textbf{différence} de deux vecteurs $\vectu$ et $\vectv$, notée  $\vectu-\vectv$, est définie
  comme la somme de $\vectu$ et l'opposé de $\vectv$.
  \[\vectu-\vectv=\vectu+\left(-\vectv\right)\]
\end{definition}

\begin{methodetwocols}[Simplification d'écritures]
  \textbf{Énoncé}\quad Simplifier les expressions vectorielles.
  \begin{center}
    \begin{tblr}{width=\linewidth,colspec={l},stretch=2}
      $\V{AB}+\V{FA}$ \\ $\V{CD}-\V{BD}$ \\ $\V{ST} + \V{KS} + \V{TK}$
    \end{tblr}
  \end{center}
  \bigskip

  \textbf{Solution}\quad On utilise la relation de Chasles.
  \newcommand\Ancre[2]{\tikz[remember picture,baseline,anchor=base] \node[font=\footnotesize,inner ysep=1pt,inner xsep=0pt] (#2) {#1};}
  {\scriptsize\begin{align*}
     &\quad\; \V{AB}+\V{FA}   &  &\quad\; \V{CD} - \V{BD}   &  &\quad\; \V{ST} + \V{RS} + \V{TR}  \\
     & = \V{F\Ancre{A}{A1}}+\V{\Ancre{A}{A2}B} &  & = \V{C\Ancre{D}{D1}} + \V{\Ancre{D}{D2}B} &  & =\V{R\Ancre{S}{S1}} + \V{\Ancre{S}{S2}T} + \V{TR} \\
     \\
     & = \V{FB} &  & = \V{CB} &  & =\V{R\Ancre{T}{T1}} + \V{\Ancre{T}{T2}R} \\
     \\
     &  &  &  &  & =\V{RR} \\
     &  &  &  &  & =\V{0} \\
  \end{align*}}
  \begin{tikzpicture}[remember picture, overlay]
    \foreach \pt in {A,D,S,T}
    \draw[colorSec,thin,{Circle[length=2.5pt]}-{Circle[length=2.5pt]}] (\pt 1.south) to[out=-80,in=-100] (\pt 2.south);
  \end{tikzpicture}
\end{methodetwocols}

\section{Multiplication par un réel}

\subsection{Produit d'un vecteur par un réel}
Dans tout le paragraphe, $\vectu$ désigne un vecteur et $k$ un réel non nul.

\begin{definition}\index{Vecteur!Multiplication par un réel}
  Le produit du vecteur $\vectu$ par le réel $k$ est un vecteur qui se note $k.\vectu$ et qui est caractérisé par~:
  \begin{description}
    \item[une direction~:] celle de $\vectu$.\medskip
    \item[un sens~:] dépend du signe de $k$~:
          \begin{itemize}
            \item[\textbullet] si $k>0$ alors le sens de $\vectu$~;
            \item[\textbullet] si $k<0$ alors le sens contraire à celui de $\vectu$.
          \end{itemize}\medskip
    \item[une norme~:] $\norme{k\vectu}=|k|\times\norme{\vectu}$.
  \end{description}
\end{definition}

\begin{remarque} Quelques cas particuliers~:
  \begin{enumerate}
    \item Si $\vectu=\V{0}$ alors $k\vectu=\V{0}$ pour tout $k$.
    \item Si $k=0$ alors $k\vectu=\V{0}$ pour tout $\vectu$.
    \item Si $k=-1$ alors $-1\vectu=-\vectu$
          (il s'agit de l'opposé du vecteur $\vectu$).
  \end{enumerate}
\end{remarque}

\begin{exemple} Soit un vecteur $\vectu$.
  \begin{multicols}{2}
    \vspace*{\fill}
    Dans l'exemple ci-contre, on a tracé~:
    \begin{itemize}
      \item en vert un représentant du vecteur $3\vectu$~;
      \item en rouge un représentant du vecteur $-2\vectu$.
      \item en fuchsia un représentant du vecteur $\frac{1}{2}\vectu$~;
    \end{itemize}
    \vspace*{\fill}

    \begin{center}
      \begin{tikzpicture}[x=0.5cm,y=0.5cm,scale=0.8,every node/.append style={font=\scriptsize}]
        \draw[thin, LightGray] (0,-3) grid[step=0.5cm] (15, 6);
        \coordinate (Vu) at (4,1);
        \draw[very thick, -latex', colorPrim] (1,3) -- +(Vu) node[midway, above, sloped] {$\vectu$};
        \draw[very thick, -latex', colorSec] (14,1) -- +($-2*(Vu)$) node[midway, below, sloped] {$-2\vectu$};
        \draw[very thick, -latex', colorTer] (2,1) -- +($3*(Vu)$) node[midway, below, sloped] {$3\vectu$};
        \draw[very thick, -latex', Fuchsia] (1,-1) -- +($0.5*(Vu)$) node[midway, below, sloped] {$\frac{1}{2}\vectu$};
        \begin{scope}[every path/.append style={draw,very thick, -latex', colorSec,dashed,transform canvas={yshift=2mm},opacity=0.4}]
          \path (14,1) -- ++($-1*(Vu)$) coordinate (int1) node[midway, above, sloped] {$-\vectu$};
          \path (int1) -- ++($-1*(Vu)$) node[midway, above, sloped] {$-\vectu$};
          \path[colorTer] (2,1) -- ++(Vu) coordinate (int2) node[midway, above, sloped] {$\vectu$};
          \path[colorTer] (int2) -- ++(Vu) coordinate (int3) node[midway, above, sloped] {$\vectu$};
          \path[colorTer] (int3) -- ++(Vu) node[midway, above, sloped] {$\vectu$};
        \end{scope}
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{exemple}

\subsection{Règles de calcul}

\begin{propriete}[Distributivité entre vecteurs et réels]\label{seconde:vecteurs1:prop:reglescalcul}
  Pour tous vecteurs $\vectu$ et $\vectv$ et tous réels $k$ et $k'$, on a~:
  \begin{itemize}
    \item $k.(\vectu+\vectv)= k\vectu+k\vectv$
    \item $k.\vectu+k'.\vectu=(k+k')\vectu$
    \item $k.(k'.\vectu)=(kk')\vectu$
  \end{itemize}
\end{propriete}

\begin{exemple}
  Quelques exemples d'utilisation des règles \ref{seconde:vecteurs1:prop:reglescalcul}
  ci-dessus~:
  \begin{itemize}
    \item $2\vectu-3\vectu=(2-3)\vectu=-\vectu$.
    \item $4\V{AB}+4\V{CD}=4\left(\V{AB}+\V{CD}\right)$.
    \item $-3\left(2\vectu\right)=-6\vectu$.
  \end{itemize}
\end{exemple}

\subsection{Colinéarité}

\begin{definition}\index{Vecteur!Colinéarité}
  Soient deux vecteurs $\vectu$ et $\vectv$.

  On dit que $\vectu$ et $\vectv$ sont \textbf{colinéaires} si et seulement si
  il existe un réel $k$ tel que $\vectu=k\vectv$.

  Le nombre $k$ lorsqu'il existe est appelé \textbf{coefficient de colinéarité}.
\end{definition}

\begin{remarque}
  On peut remarquer que si deux vecteurs $\vectu$ et $\vectv$ sont tels que
  $\vectu=3\vectv$, alors on a aussi $\vectv=\dfrac{1}{3}\vectu$.
\end{remarque}

\begin{propriete}
  Deux vecteurs $\vectu$ et $\vectv$ non nuls sont colinéaires si et seulement si ils ont la même direction.
\end{propriete}


\begin{propriete}[Parallélisme et alignement]\label{seconde:cours:vecteurs1:prop:alignement}
  Soit $A$, $B$, $C$ et $D$ quatre points.
  \begin{itemize}
    \item Les droites $(AB)$ et $(CD)$ sont \textbf{parallèles} si et seulement si les vecteurs $\V{AB}$ et $\V{CD}$ sont colinéaires ;
    \item Les points $A$, $B$ et $C$ sont alignés si et seulement si les vecteurs $\V{{\textcolor{colorSec}{A}}B}$
          et $\V{{\textcolor{colorSec}{A}}C}$ (par exemple) sont colinéaires.
  \end{itemize}
\end{propriete}



\begin{remarque}{}
  L'égalité $\V{AM}=\dfrac{1}{2}\V{AB}$ montre que le point $M$ est le milieu de $[AB]$.
\end{remarque}



\begin{methode}[Démontrer avec la colinéarité]
  \textbf{Énoncé}\quad Soit trois points $A$, $B$ et $C$ tels que~:
  \[2\V{AB}-3\V{AC} = \V{0}\]
  Prouver que les points $A$, $B$ et $C$ sont alignés.
  \bigskip
  
  \textbf{Solution}\quad Transformons la relation~:
  \begin{align*}
    2\V{AB}-3\V{AC} &= \V{0}\\
    2\V{AB} &= 3\V{AC}\\
    \V{AB} &= \frac{3}{2}\V{AC}
  \end{align*}
  Il existe donc $k=\frac{3}{2}$ tel que~: $\V{AB}=k\V{AC}$.

  On en déduit que les vecteurs $\V{{\textcolor{colorSec}{A}}B}$ et $\V{{\textcolor{colorSec}{A}}C}$ sont colinéaires.

  D'après la propriété \ref{seconde:cours:vecteurs1:prop:alignement}, on en déduit que les points $A$, $B$ et $C$ sont alignés.
  \bigskip
\end{methode}
