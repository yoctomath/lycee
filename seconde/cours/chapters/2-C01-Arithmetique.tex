% @Packages profcollege,fontawesome5,tdsfrmath
\chapter{Arithmétique}
L'\textbf{arithmétique} est l'étude des nombres entiers naturels, c'est-à-dire des nombres qui servent à rendre compte
d'une collection d'objets considérés dans leur entièreté (un troupeau de chèvres, un bouquet de fleurs, etc.).

\section{Ensemble des entiers naturels}

\begin{definition}
  L'ensemble des \textbf{nombres entiers naturels} est l’ensemble des nombres $0$, $1$, $2$, $3$, etc.

  On peut le noter $\{0\,;\,1\,;\,2\,;\,3\,;\,\ldots\}$ ou plus brièvement $\N$.
\end{definition}
\paragraph*{Exemples} On a $13\in\N$ mais $1,1\notin\N$ et $-45\notin\N$


\section{Multiples et diviseurs}
\subsection{Généralités}
\begin{definition}\label{seconde:arithm:def:multiple}
  Soient $a$ et $b$ deux nombres entiers naturels.

  S'il existe un entier naturel $k$ tel que $\colorbox{colorPrim!30}{a=k \times b}$ alors on dit indifféremment que~:
  \begin{multicols}{2}
    \begin{itemize}
      \item $a$ est un \textbf{multiple} de $b$ ;\index{Nombre!Multiple}
      \item $a$ est \textbf{divisible} par $b$ ;
      \item $b$ est un \textbf{diviseur} de $a$ ;\index{Nombre!Diviseur}
      \item $b$ \textbf{divise} $a$.
    \end{itemize}
  \end{multicols}
\end{definition}

\begin{remarque}
  On peut lier aussi la définition de divisibilité à la division euclidienne et dire \textbf{$a$ est divisible par $b$ si et seulement si le reste de la division euclidienne de $a$ par $b$ est nul}.
\end{remarque}

\paragraph*{Exemple}
Considérons le nombre $\colorbox{colorSec!10}{5}$.
\begin{itemize}
  \item On peut dire que $45$ est un multiple de $\colorbox{colorSec!10}{5}$ car on peut écrire $45=9 \times \colorbox{colorSec!10}{5}$.
        Ainsi, la définition \ref{seconde:arithm:def:multiple} est bien respectée~: il existe $k$ tel que $45=k \times \colorbox{colorSec!10}{5}$ en prenant $k=9$.
  \item Par contre, on ne peut pas dire que $17$ est un multiple de $\colorbox{colorSec!10}{5}$ car $17$ n'étant pas dans la table de multiplication
        de $\colorbox{colorSec!10}{5}$, il n'existe pas de nombre entier naturel $k$ tel que $17=k \times \colorbox{colorSec!10}{5}$.
\end{itemize}

\begin{propriete}\label{seconde:arithm:prop:multiple}
  Soit $a$ un nombre entier naturel.\medskip

  La somme de deux multiples de $a$ est aussi un multiple de $a$.
\end{propriete}

\paragraph*{\faCogs\quad Preuve} Considérons $a$ un nombre entier naturel et prenons $b$ et $c$ deux nombres entiers naturels \textit{multiples} du nombre~$a$.

D'après la définition \ref{seconde:arithm:def:multiple}, il existe deux nombres entiers naturels $n_1$ et $n_2$ tels que :
\begin{align*}
  b & = n_1 \times a & c & = n_2 \times a
\end{align*}
Examinons la somme $b+c$~:
\begin{align*}
  b+c & =  n_1 \times a + n_2 \times a &  &                                             \\
  b+c & =  (n_1  + n_2) \times a       &  & \longleftarrow\text{ on factorise par } a   \\
  b+c & =  k \times a                  &  & \longleftarrow\text{ en prenant } k=n_1+n_2 \\
\end{align*}
La définition \ref{seconde:arithm:def:multiple} est bien respectée donc la somme $b+c$ est bien un multiple de $a$.

\paragraph*{Exemple} Les nombres $33$ et $21$ sont des multiples de $3$ (en effet $33=11\times3$ et $21=7\times3$).

D'après la propriété \ref{seconde:arithm:prop:multiple}, le nombre $54$, égal à la somme $33+21$ est aussi un multiple de $3$.


\subsection{Nombres pairs et impair}

\begin{definition}
  Un nombre \textbf{pair} est un nombre qui est multiple de $2$.\index{Nombre!Pair \& Impair}

  Un nombre \textbf{impair} est un nombre qui n'est pas pair.
\end{definition}

\begin{propriete}\label{seconde:arithm:prop:pair}
  Soit $a$ un nombre entier naturel.
  \begin{itemize}
    \item Un nombre $a$ pair admet comme écriture $a=2k$ où  $k$ est un entier naturel.
    \item Un nombre $a$ impair admet comme écriture $a=2k+1$ où  $k$ est un entier naturel.
  \end{itemize}
\end{propriete}

\begin{propriete}
  Soit $a$ un nombre entier naturel.
  \begin{enumerate}%[label={\textcolor{Grey}{\textbf{\arabic*. }}}]
    \item Si $a$ est \textit{pair} alors son carré est \textit{pair}.
    \item Si $a$ est \textit{impair} alors son carré est \textit{impair}.
  \end{enumerate}
\end{propriete}

\paragraph*{\faCogs\quad Preuve} Considérons le nombre $a$ entier naturel.
\begin{enumerate}
  \item Supposons que $a$ est pair. Alors, d'après la propriété \ref{seconde:arithm:prop:pair},
        il existe un nombre $k$ entier naturel tel que $a=2k$.

        Calculons $a^2$~:
        \begin{align*}
          a^2 & = (2k)^2                &  &                                           \\
          a^2 & = 2^2 \times k^2        &  &                                           \\
          a^2 & = 2 \times 2 \times k^2 &  &                                           \\
          a^2 & = 2 \times k'           &  & \longleftarrow \text{ où }k'=2 \times k^2 \\
        \end{align*}
        Toujours d'après la propriété \ref{seconde:arithm:prop:pair}, on en déduit que $a^2$ est pair.
  \item Supposons maintenant que $a$ est impair. Alors, d'après la propriété \ref{seconde:arithm:prop:pair},
        il existe un nombre $k$ entier naturel tel que $a=2k + 1$.

        Calculons $a^2$~:
        \begin{align*}
          a^2 & = (2k+1)^2                               &  &                                                         \\
          a^2 & = (2k+1)\times(2k+1)                     &  &                                                         \\
          a^2 & = (2k)^2 + 2 \times 2k \times 1 + 1^2    &  &                                                         \\
          a^2 & = 2 \times 2 \times k^2+ 2 \times 2k + 1 &  &                                                         \\
          a^2 & = 2 (2k^2+ 2k) + 1                       &  & \longleftarrow \text{ on factorise partiellement par }2 \\
          a^2 & = 2 \times k' +1                         &  & \longleftarrow \text{ où }k'=2k^2+ 2k                   \\
        \end{align*}
        D'après la propriété \ref{seconde:arithm:prop:pair}, on en déduit que $a^2$ est impair.
\end{enumerate}

\begin{methode}[Utiliser la calcul littéral pour prouver un résultat d'arithmétique]
  \textbf{Énoncé}\quad Prouver que la somme de deux nombres impairs est paire.\bigskip

  \textbf{Solution}\quad On considère $a$ et $b$ deux nombres impairs.

  D'après la propriété \ref{seconde:arithm:prop:pair}, il existe deux nombres entiers naturels $k_1$ et $k_2$ tels que
  $a=2k_1 + 1$ et $b=2k_2 + 1$. Regardons la somme de $a$ et $b$~:
  \begin{align*}
    a+b & = 2k_1 + 1 + 2k_2 + 1                                                \\
    a+b & = 2k_1 + 2k_2 + 2                                                    \\
    a+b & = 2(k_1 + k_2 + 1)    &  & \longleftarrow \text{ on factorise par }2 \\
    a+b & = 2k                  &  & \longleftarrow \text{ où }k=k_1 + k_2 + 1 \\
  \end{align*}
  D'après la propriété \ref{seconde:arithm:prop:pair}, la somme de $a+b$ est paire.
\end{methode}


\section{Nombres premiers}

\begin{definition}\label{seconde:arithm:def:premier}\index{Nombre!Premier}
  Un nombre entier naturel est \textbf{premier} si et seulement si il possède exactement deux diviseurs
  (qui sont $1$ et lui-même).
\end{definition}

\paragraph*{Exemple} Voici la liste des dix premiers nombres premiers~: $2$ ; $3$ ; $5$ ; $7$ ; $11$ ; $13$ : $17$ ; $19$ ; $23$ ; $29$.

\begin{methode}[Vérifier si un nombre est premier]
  \textbf{Énoncé}\quad Vérifier si les nombres $57$ et $97$ sont premiers ou pas.\bigskip

  \textbf{Solution}\quad On utilise ici les critères de divisibilité.
  \begin{itemize}
    \item Le nombre $57$ n'est pas premier car $5+7=12$ et $12$ est un multiple de $3$ donc $57$ est aussi un multiple de $3$.

          On en déduit que $57$ admet au moins trois diviseurs ($1$ ; $3$ et $57$). D'après la définition \ref{seconde:arithm:def:premier}
          on peut affirmer que $57$ n'est pas premier.
    \item Examinons le nombre $97$.

          \colorbox{colorTer!30}{On va contrôler si $97$ est divisible par des nombres premiers strictement inférieurs
            à la racine carrée de $97$}, c'est-à-dire jusqu'à $7$ (car $\sqrt{97}\approx9,8$ et le nombre premier qui suit $7$ est $11$).
          \begin{itemize}
            \item le nombre $97$ n'est pas divisible par $2$~: son dernier chiffre n'est pas pair~;
            \item le nombre $97$ n'est pas divisible par $3$~: $9+7=16$ et $16=3\times5+1$ n'est pas un multiple de $3$~;
            \item le nombre $97$ n'est pas divisible par $5$~: son dernier chiffre n'est ni $0$, ni $5$~;
            \item le nombre $97$ n'est pas divisible par $7$~: $97 = 7 \times 13 + 6$ ($6<7$).
          \end{itemize}
          On en déduit que $97$ est un nombre premier.
  \end{itemize}
\end{methode}


\begin{remarque}
  \begin{itemize}
    \item Il n'existe pas de plus grand nombre premier.
    \item Le nombre $1$ n'est pas premier~: en effet, il n'admet qu'un seul diviseur.
  \end{itemize}
\end{remarque}

\begin{propriete}[Décomposition en produit de facteurs premiers]\index{Nombre!Décomposition}
  Tout nombre entier naturel non premier strictement supérieur à $1$ peut se décomposer de manière unique
  sous la forme d'un produit de nombres premiers.
\end{propriete}

\begin{methode}[Décomposer un nombre en produit de facteurs premiers]
  \textbf{Énoncé}\quad Décomposer $300$ en produit de facteurs premiers.\bigskip

  \textbf{Solution}\quad
  \begin{multicols}{2}
    \textit{Au brouillon}\medskip

    \Decomposition[Arbre,Entoure]{300}
    \vspace*{\fill}

    \columnbreak
    \textit{Sur la copie}\medskip

    On décompose $300$~:
    \Decomposition[Tableau]{300}

    En conséquence, on peut dire que \[300=\Decomposition[Exposant]{300}\]
  \end{multicols}
\end{methode}

\begin{remarque}
    Dans l'exemple ci-dessus, on a utilisé un arbre dans lequel on décompose un nombre en un produit jusqu'à n'obtenir
    que des nombres premiers en bout de branche mais ce n'est pas la seule technique.\medskip
  
    La calculatrice\footnote{Dans tout le cours, le modèle présenté est la calculatrice Numworks.} a une fonction permettant de donner directement le résultat. 
    
    Utiliser la séquence \menu[,]{Boîte à outils,Arithmétique,\texttt{factor(n)}}
\end{remarque}

\begin{multicols}{2}
  \includegraphics[width=0.9\columnwidth]{2-C01-Arithmetique-img01.png}

  \includegraphics[width=0.9\columnwidth]{2-C01-Arithmetique-img02.png}
\end{multicols}

\subsubsection*{Quelques utilisation de la décomposition en produit de facteurs premiers}
\begin{definition}
  Une fraction est dite \textbf{irréductible} lorsqu'on ne peut plus la simplifier.\index{Fraction!Irréductible}
\end{definition}

\begin{methode}[Rendre une fraction irréductible]
  \textbf{Énoncé}\quad Rendre irréductible la fraction $\frac{52}{156}$\bigskip

  \textbf{Solution}\quad Décomposons le numérateur et le dénominateur de la fraction en produit de facteurs premiers. On a $52=\Decomposition[Exposant]{52}$ et $156=\Decomposition[Exposant]{156}$.
  On a donc~:
  \begin{align*}
    \frac{52}{156} & = \frac{\Mhl{2^2}\times13\times 1}{\Mhl{2^2}\times3\times\Mhl{13}} &  &                                                        \\
    \frac{52}{156} & = \frac{1}{3}                                                      &  & \longleftarrow\text{ on a simplifié par }2^2 \times 13 \\
  \end{align*}
\end{methode}

\begin{methode}[Trouver le Plus Grand Commun Diviseur de deux nombres entiers (PGCD)]\index{Nombre!PGCD}
  \textbf{Énoncé}\quad Déterminer le plus grand commun diviseur de $252$ et $450$.\bigskip

  \textbf{Solution}\quad On a $252=\Decomposition[Longue]{252}$ et $450=\Decomposition[Longue]{450}$.

  En prenant tous les facteurs qui sont dans la décomposition de $252$ \textbf{ET} dans celle de $450$
  (c'est-à-dire tous les facteurs communs aux deux décompositions),
  on établit un produit de facteurs premiers qui forment le PGCD de $252$ et $450$.
  \begin{quote}
    $\Mhl{2}\times2\times\Mhl{3}\times\Mhl{3}\times7$

    $\Mhl{2}\times\Mhl{3}\times\Mhl{3}\times5\times5$
  \end{quote}
  \begin{align*}
    PGCD(252\,;\,450) & = 2 \times 3 \times 3 \\
                      & = 18                  \\
  \end{align*}
  Le plus grand commun diviseur de $252$ et $450$ est donc $18$.
\end{methode}
\begin{methode}[Déterminer le Plus Petit Commun Multiple de deux nombres entiers (PPCM)]\index{Nombre!PPCM}
  \textbf{Énoncé}\quad Déterminer le plus petit multiple commun à $12$ et $30$\bigskip

  \textbf{Solution}\quad On a $12=\Decomposition[Exposant]{12}$ et $30=\Decomposition[Exposant]{30}$.

  En prenant tous les facteurs qui sont dans la décomposition de $12$ \textbf{OU} dans celle de $30$
  (c'est-à-dire tous les facteurs différents de plus haut degré dans les deux décompositions),
  on établit un produit de facteurs premiers qui forment le PPCM de $12$ et $30$.
  \begin{align*}
    PPCM(12\,;\,30) & = 2^2 \times 3 \times 5 \\
                    & = 60                    \\
  \end{align*}
  Le plus petit commun multiple de $12$ et $30$ est donc $60$.
\end{methode}

\begin{remarque}
  Pour les calculs de PGCD et de PPCM à l'aide de la calculatrice, on trouvera respectivement les fonctions \texttt{gcd(p\,,\,q)} et \texttt{lcm(p\,,\,q)} dans le menu \menu[,]{Boîte à outils,Arithmétique}
\end{remarque}