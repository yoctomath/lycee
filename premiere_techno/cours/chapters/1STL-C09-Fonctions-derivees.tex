\chapter{Fonctions dérivées}

\section{Notion de fonction dérivée}

\subsection{Définition}

\begin{definition}\index{Dérivées!Définition}\index{Fonctions!Dérivées}
  Soit $f$ une fonction définie sur un intervalle $\mathtt{I}$.
  \begin{itemize}
    \item On dit que la fonction $f$ est \textbf{dérivable} sur $\mathtt{I}$ si et seulement si, la fonction $f$ admet un nombre dérivé, noté $f'(a)$,  pour tout $a \in \mathtt{I}$.
    \item On appelle \textbf{fonction dérivée} de $f$ sur $\mathtt{I}$ la fonction $f'$ qui à tout $x$ de $\mathtt{I}$ associe le nombre $f'(x)$
          \[f' : x \longmapsto f'(x)\]
  \end{itemize}
\end{definition}

\subsection{Dérivées de quelques fonctions}\index{Dérivées!Fonctions polynômes}
Les fonctions ci-dessous sont définies et dérivable sur \R.

\begin{center}
  \begin{tblr}{width=0.8\linewidth,colspec={X[l,1.5]X[c,1]X[c,1]},stretch=2,%
    hline{1}={2-4}{},%
    hline{2-6},%
    vline{1}={2-5}{},%
    vline{2-4},%
    row{1}={font=\bfseries, bg=colorPrim!20},%
    rows={valign={m}},%
    cell{1}{1}={bg=white}%
    }
                       & Fonctions     & Fonctions dérivées \\
    Fonction constante & $f(x) = k$    & $f'(x)=0$          \\
    Fonction affine    & $f(x) = ax+b$ & $f'(x)=a$          \\
    Fonction carrée    & $f(x) = x^2$  & $f'(x)=2x$         \\
    Fonction cube      & $f(x) = x^3$  & $f'(x)=3x^2$       \\
  \end{tblr}
\end{center}

\subsection{Opération sur les fonctions dérivables}

\begin{propriete}\label{premiere-techno:cours:derivees:prop:operations}
  Soient $f$ et $g$ deux fonctions dérivables sur un intervalle $\mathtt{I}$ et $k$ un réel.
  \begin{itemize}
    \item La fonction $kf$ est dérivable sur $\mathtt{I}$ et \[(kf)'=kf'\]
    \item La fonction $f+g$ est dérivable sur $\mathtt{I}$ et \[(f+g)'=f'+g'\]\index{Dérivées!Somme}
  \end{itemize}
\end{propriete}

\begin{exemple}
  Utilisons les propriétés \ref{premiere-techno:cours:derivees:prop:operations} précédentes pour dériver les fonctions $u(x)=5x^2$ et $v(x)=x^3 -3x+4$.
  \begin{align*}
    u'(x) & = (5x^2)'       & v'(x) & = (x^3 -3x+4)'       \\
          & = 5\times(x^2)' &       & = (x^3)' - (3x + 4)' \\
          & = 5\times2x     &       & = 3x^2 - 3           \\
          & = 10x
  \end{align*}
\end{exemple}

\section{Signe de la dérivée et variations}

\begin{propriete}\label{premiere-techno:cours:derivees:prop:variations}\index{Fonctions!Variations}
  Soit $f$ une fonction dérivable sur un intervalle~$\mathtt{I}$.
  \begin{itemize}
    \item $f$ est croissante sur $\mathtt{I}$ si, et seulement si, $f'(x) \pgq 0$ pour tout $x \in \mathtt{I}$.
    \item $f$ est décroissante sur $\mathtt{I}$ si, et seulement si, $f'(x) \ppq 0$ pour tout $x \in \mathtt{I}$.
    \item $f$ est constante sur $\mathtt{I}$ si, et seulement si, $f'(x) = 0$ pour tout $x \in \mathtt{I}$.
  \end{itemize}
\end{propriete}

\begin{exemple}
  Prenons la fonction $f(x)=3x^2 - 6x + 5$ définie sur l'intervalle $\mathtt{I}=\interfo{1 +\infty}$.

  Calculons sa dérivée :
  \begin{align*}
    f'(x) & = 3\times2x - 6 \\
          & = 6x - 6        \\
          & = 6(x-1)
  \end{align*}
  On a $x\in\interfo{1 +\infty}$ donc :
  \begin{align*}
    x      & > 1 \\
    x - 1  & > 0 \\
    6(x-1) & > 0
  \end{align*}
  On en déduit que $f'(x)>0$ pour tout $x \in \mathtt{I}$, donc, d'après la propriété
  \ref{premiere-techno:cours:derivees:prop:variations} précédente,
  la fonction est croissante sur $\mathtt{I}$.
\end{exemple}

\begin{methode}[Étude d'une fonction]\index{Fonctions!Étude}
  \textbf{Énoncé}\quad On donne la fonction $f$ définie sur \R par \[f(x)=\frac{1}{3}x^3 - 4x + 1\]
  Étudier les variations de $f$ sur \R.\bigskip

  \textbf{Solution}\quad La fonction $f$ est un polynôme donc elle est dérivable sur \R.
  Calculons sa dérivée :
  \begin{align*}
    f'(x) & = \frac{1}{3}\times3x^2 - 4 \\
          & = x^2 - 4
  \end{align*}
  Étudions le signe de la dérivée. Pour cela, nous allons la factoriser :
  \begin{align*}
    f'(x) & = x^2 - 2^2  \\
          & = (x-2)(x+2)
  \end{align*}
  Étudions le signe de chaque facteur :
  \begin{align*}
    x-2 &> 0 & x+2 &> 0 \\
    x   &> 2 & x   &> -2
  \end{align*}
  Consignons les résultats dans un tableau résumant le signe de $f'(x)$ et les variations de $f$.
  \begin{center}
    \tikzset{node style/.append style={fill=colorTer!5}}
    \begin{tikzpicture}
      \tkzTabInit[lgt=4,espcl=2.5,deltacl=0.75]{$x$ / 1 ,$x-2$/1, $x+2$ / 1, Signe de $f'(x)$ / 1,Variations de $f$/ 2}
      {$-\infty$, $-2$,$2$, $+\infty$}
      \tkzTabLine{ ,-,z,+,t,+, }
      \tkzTabLine{ ,-,t,-,z,+, }
      \tkzTabLine{ ,+,z,-,z,+, }
      \tkzTabVar{-/, +/$\frac{19}{3}$,  -/$-\frac{13}{3}$, +/}
    \end{tikzpicture}
  \end{center}
  Les valeurs de $f$ dans le tableau ci-dessus sont calculées à part :
  \begin{align*}
    f(-2) & = \frac{1}{3}\times(-2)^3 -4\times(-2) + 1 & f(2) & = \frac{1}{3}\times2^3 -4\times2 + 1\\
    &= -\frac{8}{3} + 8 +1 & &= \frac{8}{3} -8 +1\\
    &= -\frac{8}{3} + \frac{24}{3} + \frac{3}{3} & &= \frac{8}{3} - \frac{24}{3} + \frac{3}{3}\\
    &= \frac{19}{3} & &= -\frac{13}{3}
  \end{align*}
\end{methode}

\section{Extremums}

\subsection{Maximum et minimum locaux}
\begin{definition}\index{Extremum}\index{Maximum}\index{Minimum}\index{Fonctions!Extremum}\index{Fonctions!Maximum}\index{Fonctions!Minimum}
  Soit $f$ une fonction définie sur une intervalle $\mathtt{I}$ et un intervalle  $J$ inclus dans $\mathtt{I}$.
  \begin{itemize}
    \item S'il existe $a\in J$ tel que $f(x) \ppq f(a)$ pour tout $x \in J$
    alors la fonction $f$ admet un \textbf{maximum local} sur $J$.

    On dit que $f(a)$ est un maximum local de $f$ sur $J$ et qu'il est atteint pour $x=a$.
    \item S'il existe $a\in J$ tel que $f(x) \pgq f(a)$ pour tout $x \in J$ 
    alors la fonction $f$ admet un \textbf{minimum local} sur $J$.

    On dit que $f(a)$ est un minimum local de $f$ sur $J$ et qu'il est atteint pour $x=a$.
  \end{itemize}
  \medskip

  Les maximums et minimums locaux lorsqu'ils existent s'appellent des \textbf{extremums locaux} de $f$ sur $J$.
\end{definition}

\subsection{Extremums et dérivée}
\begin{propriete}\label{premiere-techno:cours:derivees:prop:extremums}
  Soit $f$ une fonction définie sur un intervalle $\mathtt{I}$, 
  dérivable sur un intervalle \textbf{ouvert}\footnotemark $J$ inclus dans $\mathtt{I}$.
  \begin{itemize}
  \item Si la fonction $f$ admet un extremum local en $a\in J$, alors $f'(a) = 0$.
  \item Réciproquement, si $f'(a)=0$ pour $a\in J$ et \textbf{change de signe} en $a$,\\
  alors $f$ admet un extremum local en $a$. 
  \end{itemize}
\end{propriete}
\footnotetext{L'intervalle est choisi ouvert afin d'exclure les cas aux bornes d'un intervalle ce qui rendrait la propriété fausse.}

\begin{methode}[Étude des extremums locaux d'une fonction]
  \textbf{Énoncé}\quad On considère une fonction $f$ définie sur \interff{0 10} dont on donne le tableau de variations
  ci-dessous :
  
  \begin{center}
    \tikzset{node style/.append style={fill=colorTer!5}}
    \begin{tikzpicture}
      \tkzTabInit[lgt=4,espcl=2.5,deltacl=0.75]{$x$ / 1 , Signe de $f'(x)$ / 1,Variations de $f$/ 2}
      {$0$, $3$, $7$, $10$}
      \tkzTabLine{ ,+,z,-,z,+, }
      \tkzTabVar{-/$-6$, +/$1$,  -/$-4$, +/$3$}
    \end{tikzpicture}
  \end{center}
  Déterminer les extremums locaux de la fonction $f$ sur l'intervalle \interff{0 10} s'ils existent.
  \bigskip
  
  \textbf{Solution}\quad 
  Le tableau des variations de $f$ indique que la dérivée s'annule deux fois pour $x=3$ et pour $x=7$
  et change de signe dans ces deux cas.
  \medskip

  On en déduit, d'après la propriété \ref{premiere-techno:cours:derivees:prop:extremums} précédente, 
  que sur l'intervalle \interff{0 10}, $f$ admet deux extremums locaux :
  \begin{itemize}
    \item un maximum local en $x=3$ et d'après le tableau de variation de $f$, ce maximum vaut $f(3)=1$ ;
    \item un minimum local en $x=7$ et d'après le tableau de variation de $f$, ce minimum vaut $f(7)=-4$.
  \end{itemize}
  \bigskip
  
  \textbf{Remarque}\quad Ces deux extremums locaux \textit{ne sont pas} les extremums de la fonction $f$ sur \interff{0 10} :
  le tableau de variation de $f$ montre que ces extremums sont $-6$ (minimum) et $3$ (maximum), 
  atteints en $x=0$ et $x=10$ respectivement. 
\end{methode}



