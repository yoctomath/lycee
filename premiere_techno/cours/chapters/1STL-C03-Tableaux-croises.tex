\chapter{Tableaux croisés}

\section{Généralités}

\begin{definition}
  Soit deux caractères $A$ et $B$ étudiés sur une même population $E$.

  Soient les valeurs prises par les caractères $A$ et $B$ :
  \begin{itemize}
    \item valeurs de $A$ :  $A_i$ avec $i\in\N$ tel que $0 \ppq i \ppq n$ ;
    \item valeurs de $B$ :  $B_j$ avec $j\in\N$ tel que $0 \ppq j \ppq m$.
  \end{itemize}

  On appelle \textbf{tableau croisé}\index{Tableau croisé} (ou \textbf{tableau de contingences}) le tableau à double entrées dans lequel on va \textit{croiser}
  les effectifs relatifs aux valeurs de chaque caractère. Les valeurs du caractère $A$ seront présentées
  en ligne et celles du caractère $B$ en colonnes.
  \begin{center}
    \begin{tblr}{width=\linewidth,
      colspec={X[c,1]X[c,1]X[c,0.5]X[c,1]X[c,0.5]X[c,1]X[c,1]},
      hline{1}={2-9}{},
      hline{2-9},
      vline{1}={2-8}{},
      vline{2-8},
      stretch=1.6,
      row{1}={
          bg=colorPrim!20,
          font=\bfseries
        },
      column{1}={
          bg=colorPrim!20,
          font=\bfseries
        },
      cell{1}{1}={bg=colorPrim!5},
      rows={mode=math}}
                             & B_1 & \ldots & B_j                       & \ldots & B_m & \text{\bfseries Total} \\
      A_1                    &     &        &                           &        &     &                        \\
      \vdots                 &     &        &                           &        &     &                        \\
      A_i                    &     &        & \text{Card}(A_i \cap B_j) &        &     & \text{Card}(A_i)       \\
      \vdots                 &     &        &                           &        &     &                        \\
      A_n                    &     &        &                           &        &     &                        \\
      \text{\bfseries Total} &     &        & \text{Card}(B_j)          &        &     & \text{Card}(E)         \\
    \end{tblr}
  \end{center}
  \begin{itemize}
    \item $\text{Card}(E)$ est l'\textbf{effectif total}\index{Effectif!Total}

          taille de la population ;
    \item $\text{Card}(A_i)$ est l'\textbf{effectif marginal}\index{Effectif!Marginal} de $A_i$

          nombre d'individus pour lesquels le caractère $A$ prend la valeur $A_i$ ;
    \item $\text{Card}(B_j)$ est l'\textbf{effectif marginal} de $B_j$

          nombre d'individus pour lesquels le caractère $B$ prend la valeur $B_j$ ;
    \item $\text{Card}(A_i \cap B_j)$ est l'\textbf{effectif conjoint} de $A_i$ et $B_j$

          nombre d'individus présentant {\sethlcolor{colorSec!30}\hl{simultanément}} la valeur $A_i$ pour le caractère $A$ et la valeur $B_j$ pour le caractère $B$.
  \end{itemize}
\end{definition}

\begin{remarque}
  Dans chaque cellule du tableau d'une ligne ou d'une colonne avec le titre \textit{Total}, on a des effectifs marginaux 
  (hormis la cellule en bas à droite) où c'est l'\textit{effectif total}.

  On parle quelque fois d'effectif marginaux \textit{en lignes} et \textit{en colonnes}.
\end{remarque}

\begin{exemple}\label{pstl:tableaux:ex:tableau}
  Au sein des élèves de première d'un lycée, on analyse deux caractères :
  \begin{itemize}
    \item le sexe (valeurs prises : \og{}Fille\fg{} et \og{}Garçons\fg{}) ;
    \item la filière (valeurs prises : \og{}Général\fg{} et \og{}Technologique\fg{}) ;
  \end{itemize}


  \begin{center}
    \begin{tblr}{%
        width=0.8\linewidth,
        colspec={*{4}{X[c,1]}},
        hline{1}={2-5}{},
        hline{2-5},
        vline{1}={2-5}{},
        vline{2-5},
        stretch=1.6,
        row{1}={
            bg=colorPrim!20,
            font=\bfseries
          },
        column{1}={
            bg=colorPrim!20,
            font=\bfseries
          },
        cell{1}{1}={bg=white}
      }
              & Général & Technologique & Total \\
      Filles  & 176     & 44            & 220   \\
      Garçons & 134     & 146           & 280   \\
      Total   & 310     & 190           & 500   \\
    \end{tblr}
  \end{center}

  D'après le tableau croisé ci-dessus, on peut affirmer que :
  \begin{itemize}
    \item il y a $500$ élèves en première (\textit{effectif total} de la population) ;
    \item il y a $190$ élèves en filière technologique (\textit{effectif marginal} de la valeur \og{}Technologique\fg{} du caractère \textit{filière}) ;
    \item il y a $176$ élèves fille en filière générale (\textit{effectif contingent} des valeurs \og{}Fille\fg{} et \og{}Général\fg{}) ;
    \item l'\textit{effectif marginal} de la valeur \og{}Fille\fg{} est $220$.
  \end{itemize}
\end{exemple}

\section{Utilisation}

\begin{definition}
  On peut distinguer deux types de fréquence :
  \begin{itemize}
    \item la \textbf{fréquence marginale}\index{Fréquence!Marginale} d'une valeur $A_i$ d'un caractère $A$ est la proportion des individus présentant cette valeur
    au sein de la population de référence ;
    \[f(A_i)=\frac{\text{Card}(A_i)}{\text{Card}(E)}\]
    \item la \textbf{fréquence conditionnelle}\index{Fréquence!Conditionnelle} d'une valeur $A_i$ d'un caractère 
    par rapport à la valeur $B_j$ du second caractère $B$ est la proportion d'individus présentant simultanément les deux valeurs $A_i$ et $B_j$ au sein
    de la partie de la population présentant la valeur $B_j$.

    On note cette dernière fréquence $f_{B_j}(A_i)$ et on a :
    \[f_{B_j}(A_i)=\frac{\text{Card}(A_i \cap B_j)}{\text{Card}(B_j)}\]
  \end{itemize}
\end{definition}

\begin{remarque}
  On peut remarquer que
  \begin{itemize}
    \item On parle de fréquences conditionnelles \textit{en lignes} et \textit{en colonnes}.
    \item On peut retenir 
    \[\text{fréquence marginale}=\frac{\text{effectif}}{\text{effectif total}}\quad \text{ et } \quad\text{fréquence conditionnelle}=\frac{\text{effectif}}{\text{effectif marginal}}\]
    \item La somme des fréquences conditionnelles en ligne (ou en colonnes) vaut $1$.
    \end{itemize}
\end{remarque}

\begin{exemple}
  En reprenant l'exemple \ref{pstl:tableaux:ex:tableau} ci-dessus, on peut dire que :
  \begin{itemize}
    \item La proportion de garçons en filière générale est de $\frac{134}{500}=26,8\,\%$.
    
    C'est une \textit{fréquence marginale}.
    \item La proportion de filles \textit{parmi} les élèves de premières technologiques est de $\frac{44}{190}\approx23,2\,\%$.
    
    C'est une \textit{fréquence conditionnelle}.
  \end{itemize}
\end{exemple}
