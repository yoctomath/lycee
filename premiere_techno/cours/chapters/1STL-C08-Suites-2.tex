\chapter[Suites arithmétiques et géométriques]{Suites arithmétiques \\ et suites géométriques}

\section{Suites arithmétiques}

\subsection{Principe}
Une suite arithmétique est une suite infinie de nombres avec un écart constant entre chacun d'eux.
\begin{center}
  \begin{tikzpicture}[fleche/.style={-latex',black!40},comment/.style={midway,above,colorSec,font=\scriptsize}]
    \pgfmathsetlengthmacro{\TermSep}{25}
    \node (u0) at (0,0) {$u_0$};
    \foreach \i [remember=\i as \lasti (initially 0)] in {1,...,4}{%
        \node[right=\TermSep of u\lasti] (u\i) {$u_\i$};
        \draw [fleche] (u\lasti) to[out=30,in=150] node[comment] {$+r$} (u\i);
      }
    \node[right=6*\TermSep of u4] (un) {$u_n$};
    \node[right=\TermSep of un] (unp1) {$u_{n+1}$};
    \node[right=\TermSep of u4] (u5) {\phantom{$u_5$}};
    \node[left=\TermSep of un] (unm1) {\phantom{$u_5$}};
    \node[right=\TermSep of unp1] (unp2) {$\ldots$};
    \draw [fleche] (u4) to[out=30,in=150] node[comment] {$+r$} (u5);
    \draw [fleche] (unm1) to[out=30,in=150] node[comment] {$+r$} (un);
    \draw [fleche] (un) to[out=30,in=150] node[comment] {$+r$} (unp1);
    \draw [fleche] (unp1) to[out=30,in=150] node[comment] {$+r$} (unp2);
    \draw [fleche,looseness=0.2] (u0) to[out=-30,in=210] node[comment,below] {$+ n \times r$} (un);
    \draw [dotted,very thick] (u5) -- (unm1);
  \end{tikzpicture}
\end{center}

\subsection{Définition par récurrence}
Des considérations précédentes, il vient la définition suivante où les termes de la suite arithmétique sont définis en fonction du terme précédent.
\begin{definition}[Définition par récurrence d'une suite arithmétique]\index{Suites!Arithmétiques!Définition par récurrence}
  Une suite \suite est dite \textbf{arithmétique} s'il existe un réel $r$ tel que, pour tout entier naturel $n$, on a \[u_{n+1}=u_n+r\]
  Le nombre $r$ est appelé la \textbf{raison} de la suite et $u_0$ le \textbf{premier terme}.
\end{definition}

\begin{exemple}
  La suite définie ci-dessous est une suite arithmétique de premier terme $5$ et de raison $3$.
  \[\begin{cases}
      u_0 = 5 \\
      u_{n+1} = u_n + 3
    \end{cases}\]
\end{exemple}

\subsection{Définition explicite}

\begin{propriete}[Définition explicite d'une suite arithmétique]\label{TSTL:cours:suitesarithmetiques:prop:explicite}\index{Suites!Arithmétiques!Définition explicite}
  Soit \suite une suite arithmétique de raison $r$ et de premier terme $u_0$. Pour tout entier naturel $n$, on a \[u_n=u_0+rn\]
  De même, si $u_1$ est le premier terme de la suite, alors on a \[u_n=u_1+r(n-1)\]
\end{propriete}

\begin{exemple}
  Soit \suitar[w]{3}{5}. Pour tout $n \in \N$, on a $w_n=5+3n$.
\end{exemple}

\begin{remarque}
  L'avantage de la définition explicite d'une suite arithmétique réside dans le fait qu'on a pas besoin de connaître les termes précédent $u_n$ pour le calculer.
\end{remarque}

\begin{methodetwocols}[Calculer un terme d'une suite arithmétique]

  \textbf{Énoncé}\quad Soit \suitar{9}{-7}. Calculer $u_{33}$.\bigskip

  \textbf{Solution}\quad D'après la définition explicite \ref{TSTL:cours:suitesarithmetiques:prop:explicite} d'une suite arithmétique, on peut dire que
  \columnbreak
  \begin{align*}
    u_{33} & = u_0 + r \times 33 \\
    u_{33} & = -7 + 9 \times 33  \\
    u_{33} & = -7 + 297          \\
    u_{33} & = 290               \\
  \end{align*}
\end{methodetwocols}

\subsection{Variations}

\begin{propriete}\label{TSTL:cours:suitesarithmetiques:prop:variations}\index{Suites!Arithmétiques!Variations}
  Soit \suite une suite arithmétique de raison $r$.
  \begin{itemize}
    \item Si $r>0$ alors \suite est \textbf{croissante}.
    \item Si $r<0$ alors \suite est \textbf{décroissante}.
    \item Si $r=0$ alors \suite est \textbf{constante}.
  \end{itemize}
  On parle alors de croissance (ou décroissance) \textbf{linéaire}.
\end{propriete}

\paragraph*{\faCogs\quad Preuve} Pour connaître le sens de variation, étudions le signe de $u_{n+1} - u_n$ :
\begin{align*}
  u_{n+1} - u_n & =  u_n + r - u_n \\
                & = r
\end{align*}

Ainsi : $r>0 \Longleftrightarrow  u_{n+1} - u_n > 0 \Longleftrightarrow u_{n+1} > u_n$, c'est-à-dire que $(u_n)$ est croissante.

De même pour les deux autres cas de figure.

\begin{exemple}
  Soit \suitar[v]{-2}{12}.

  On a une raison négative égale à $-2$ donc, d'après la propriété \ref{TSTL:cours:suitesarithmetiques:prop:variations} \suite[v] est décroissante.
\end{exemple}

\begin{remarque}
  Lorsqu'une système peut être modélisé en utilisant une suite arithmétique, on dira qu'il connaît une \textbf{croissance linéaire}.
\end{remarque}

\subsection {Représentations graphiques}
\begin{multicols}{3}\small
  Représentation graphique de \suitar{3}{-2} (croissante).
  \begin{center}
    \begin{tikzpicture}[scale=0.5]
      \begin{axis}[axis y line=center,%
          axis x line=middle,x=1cm,y=0.25cm,%
          xmin=-0.8,xmax=8.8,ymin=-3.7,ymax=20,%
          xtick distance=1,ytick distance =1,grid=both,%
          xticklabels=\empy, yticklabels=\empty]
        \addplot+[domain=0:7,only marks,colorPrim,mark=+,mark size=3pt,ultra thick,samples=8] {-2+3*x} node [pin=0:{$\suite$}]{};
        \coordinate[label=below:$1$] (I) at (1, 0);
        \coordinate[label=left:$1$] (J) at (0, 1);
      \end{axis}
    \end{tikzpicture}
  \end{center}

  Représentation graphique de \suitar[v]{-2}{12} (décroissante).
  \begin{center}
    \begin{tikzpicture}[scale=0.5]
      \begin{axis}[axis y line=center,%
          axis x line=middle,x=1cm,y=0.3cm,%
          xmin=-0.8,xmax=8.8,ymin=-3.7,ymax=13,%
          xtick distance=1,ytick distance =1,grid=both,%
          xticklabels=\empy, yticklabels=\empty]
        \addplot+[domain=0:7,only marks,colorSec,mark=+,mark size=3pt,ultra thick,samples=8] {12-2*x} node [pin=0:{$\suite[v]$}]{};
        \coordinate[label=below:$1$] (I) at (1, 0);
        \coordinate[label=left:$1$] (J) at (0, 1);
      \end{axis}
    \end{tikzpicture}
  \end{center}

  Représentation graphique de \suitar[v]{0}{7} (constate).
  \begin{center}
    \begin{tikzpicture}[every node/.append style={font=\scriptsize},scale=0.5]
      \begin{axis}[axis y line=center,%
          axis x line=middle,x=1cm,y=0.3cm,%
          xmin=-0.8,xmax=8.8,ymin=-3.7,ymax=13,%
          xtick distance=1,ytick distance =1,grid=both,%
          xticklabels=\empy, yticklabels=\empty]
        \addplot+[domain=0:7,only marks,colorTer,mark=+,mark size=3pt,ultra thick,samples=8] {7} node [pin=0:{$\suite[w]$}]{};
        \coordinate[label=below:$1$] (I) at (1, 0);
        \coordinate[label=left:$1$] (J) at (0, 1);
      \end{axis}
    \end{tikzpicture}
  \end{center}
\end{multicols}

\section{Suite géométriques}

\subsection{Principe}
Une suite géométrique est une suite infinie de nombres avec un \textbf{facteur} constant entre chacun d'eux.
\begin{center}
  \begin{tikzpicture}[fleche/.style={-latex',black!40},comment/.style={midway,above,colorSec,font=\scriptsize}]
    \pgfmathsetlengthmacro{\TermSep}{25}
    \node (u0) at (0,0) {$u_0$};
    \foreach \i [remember=\i as \lasti (initially 0)] in {1,...,4}{%
        \node[right=\TermSep of u\lasti] (u\i) {$u_\i$};
        \draw [fleche] (u\lasti) to[out=30,in=150] node[comment] {$\times q$} (u\i);
      }
    \node[right=6*\TermSep of u4] (un) {$u_n$};
    \node[right=\TermSep of un] (unp1) {$u_{n+1}$};
    \node[right=\TermSep of u4] (u5) {\phantom{$u_5$}};
    \node[left=\TermSep of un] (unm1) {\phantom{$u_5$}};
    \node[right=\TermSep of unp1] (unp2) {$\ldots$};
    \draw [fleche] (u4) to[out=30,in=150] node[comment] {$\times q$} (u5);
    \draw [fleche] (unm1) to[out=30,in=150] node[comment] {$\times q$} (un);
    \draw [fleche] (un) to[out=30,in=150] node[comment] {$\times q$} (unp1);
    \draw [fleche] (unp1) to[out=30,in=150] node[comment] {$\times q$} (unp2);
    \draw [fleche,looseness=0.2] (u0) to[out=-30,in=210] node[comment,below] {$\times q^n$} (un);
    \draw [dotted,very thick] (u5) -- (unm1);
  \end{tikzpicture}
\end{center}


\begin{exemple}
  Dans un système de compte bancaire d'épargne rémunéré à $3\,\%$, les intérêts sont cumulés
  (voir détails partie \ref{premieretechno:cours:suites2:part:interets}) d'une année sur l'autre.
  Le capital déposé augmente donc de $3\,\%$ chaque année.

  Si le montant du dépôt initial est de $M_0$ et qu'il n'y ait plus d'apport alors,
  l'année suivante, le capital sera $M_0\times 1,03$, l'année d'après $M_0\times1,03\times1,03$, etc jusqu'à $n$ années après le dépôt initial
  où le capital sera $M_0\times 1,03^n$.
  \medskip

  Le capital sur ce compte bancaire suit donc une suite géométrique car il est multiplié chaque année par $1,03$.
\end{exemple}

\subsection{Définition par récurrence}
En utilisant le principe précédent, on peut définir les termes de la suite géométrique en fonction du terme précédent.
\begin{definition}[Définition par récurrence d'une suite géométrique]\index{Suites!Géométriques!Définition par récurrence}
  Une suite \suite est dite \textbf{géométrique} s'il existe un réel $q$ tel que, pour tout entier naturel $n$, on a \[u_{n+1}=u_n \times q\]
  Le nombre $q$ est appelé la \textbf{raison} de la suite et $u_0$ le \textbf{premier terme} (ou terme \textbf{initial}).
\end{definition}

\begin{exemple}
  La suite définie ci-dessous est une suite géométrique de premier terme $2$ et de raison $7$.
  \[\begin{cases}
      u_0 = 2 \\
      u_{n+1} = u_n \times 7
    \end{cases}\]
\end{exemple}

\subsection{Définition explicite}

\begin{propriete}[Définition explicite d'une suite géométrique]\label{TSTL:cours:suitesgeometriques:prop:explicite}\index{Suites!Géométriques!Définition explicite}
  Soit \suite une suite géométrique de raison $q$ et de premier terme $u_0$. Pour tout entier naturel $n$, on a \[u_n=u_0 \times q^n\]
  De même, si $u_1$ est le premier terme de la suite, alors on a \[u_n=u_1 \times q^{n-1}\]
\end{propriete}

\begin{exemple}
  Soit \suitar[w]{3}{5}. Pour tout $n \in \N$, on a $w_n=5+3n$.
\end{exemple}

\begin{remarque}
  Grâce à la définition explicite d'une suite géométrique, on a pas besoin de connaître les termes précédent $u_n$ pour le calculer.
\end{remarque}

\begin{methodetwocols}[Calculer un terme d'une suite géométrique]

  \textbf{Énoncé}\quad Soit \suitgeo{2}{5}. Calculer $u_{8}$.\bigskip

  \textbf{Solution}\quad D'après la définition explicite \ref{TSTL:cours:suitesgeometriques:prop:explicite} d'une suite géométrique, on peut dire que
  \columnbreak
  \begin{align*}
    u_{8} & = u_0 \times q^8 \\
    u_{8} & = 5 \times 2^8   \\
    u_{8} & = 5 \times 256   \\
    u_{8} & = \num{1280}     \\
  \end{align*}
\end{methodetwocols}


\begin{methode}[Déterminer si les termes d'une suite sont ceux d'une suite géométrique]\label{TSTL::cours:suitesgeometriques:meth:sommepremierstermes}
  \textbf{Énoncé}\quad Les nombres  des séries suivantes peuvent-ils être des termes consécutifs d'une suite géométrique~?

  \begin{quote}
    \begin{description}
      \item[\color{colorTer}Serie A]\qquad
            \begin{tblr}{width=0.5\linewidth,colspec={*{4}{X[l,1]}},rows={mode=math}}
              5 & 8 & 12,8 & 20,48 \\
            \end{tblr}
      \item[\color{colorTer}Serie B]\qquad
            \begin{tblr}{width=0.5\linewidth,colspec={*{4}{X[l,1]}},rows={mode=math}}
              3 & 6 & 7,2 & 10,8
            \end{tblr}
    \end{description}
  \end{quote}
  \bigskip

  \textbf{Solution}
  \begin{description}
    \item[\color{colorTer}Série A] Posons $u_0=5$, $u_1=8$, $u_2=12,8$ et $u_3=20,48$.
          \begin{align*}
            \frac{u_1}{u_0} & = \frac{8}{5} & \frac{u_2}{u_1} & = \frac{12,8}{8} & \frac{u_3}{u_2} & = \frac{20,48}{12,8} \\
                            & = 1,6         &                 & = 1,6            &                 & = 1,6                \\
          \end{align*}
          On remarque que nous passons d'un terme à l'autre en multipliant par le même facteur $1,6$ donc ces nombres
          peuvent être les termes consécutifs d'une suite géométrique de raison $q=1,6$ et de terme initial $5$.
    \item[\color{colorTer}Série B] Posons $v_0=5$, $v_1=6$, $v_2=7,2$ et $v_3=10,8$.
          \begin{align*}
            \frac{v_1}{v_0} & = \frac{6}{5} & \frac{v_2}{v_1} & = \frac{7,2}{6} & \frac{v_3}{v_2} & = \frac{10,8}{7,2} \\
                            & = 1,2         &                 & = 1,2           &                 & = 1,5              \\
          \end{align*}
          On remarque que nous passons d'un terme à l'autre en multipliant par le même facteur $1,2$
          sauf pour passer de $v_2$ à $v_3$ où il faut multiplier par $1,5$.

          Donc ces nombres ne peuvent pas être les termes d'une suite géométrique.
  \end{description}

\end{methode}

\subsection{Variations}

\begin{propriete}
  Soit $(u_n)_{n\in\N}$ une suite géométrique de raison $q$ et de terme initial $u_0$.


  \begin{multicols}{2}
    Cas où $u_0>0$.
    \begin{itemize}
      \item Si $q>1$, alors la suite est \textbf{croissante}.
      \item Si $0<q<1$, alors la suite est \textbf{décroissante}.
      \item Si $q=1$, alors la suite est \textbf{constante}.
    \end{itemize}

    Cas où $u_0<0$.
    \begin{itemize}
      \item Si $q>1$, alors la suite est \textbf{décroissante}.
      \item Si $0<q<1$, alors la suite est \textbf{croissante}.
      \item Si $q=1$, alors la suite est \textbf{constante}.
    \end{itemize}
  \end{multicols}
  On parle alors de croissante (ou décroissance) \textbf{exponentielle}.
\end{propriete}

\begin{exemple}
  Soit les suites \suitgeo[v]{0,95}{\num{1200}} et \suitgeo[w]{1,13}{\num{900}}.
  \begin{itemize}
    \item Pour la suite \suite[v], la raison est $0,95<1$ et le premier terme est $\num{1200}>0$.

          \suite[v] est donc \textit{décroissante}.
    \item Pour la suite \suite[w], la raison est $1,13>1$ et le premier terme est $\num{900}>0$.

          \suite[w] est \textit{croissante}.
  \end{itemize}
\end{exemple}

\section{Application : intérêts simples et composés}\label{premieretechno:cours:suites2:part:interets}

Dans le cadre d'un placement d'argent, on considère que le compte sur lequel le \textit{capital}
va être déposé est \textit{rémunéré} si ce capital va rapporter un certain montant durant une période
donné (chaque année ou chaque mois par exemple). Ce montant qui s'ajoute au capital s'appelle les \textit{intérêts}.
On les exprime souvent sous la forme d'un pourcentage du capital : \textit{le taux d'intérêt}.
\medskip

Il existe plusieurs moyens de calculer ces intérêts et ici on s'intéresse à deux méthodes :
intérêts \textit{simples} et intérêts \textit{composés}.

\subsection{Intérêts simples}

\subsubsection{Définition et propriété}

\begin{definition}
  Les intérêts \textbf{simples} représentent une partie \textit{fixe} correspondant à un pourcentage
  du capital de départ.
\end{definition}

\begin{propriete}
  Les valeurs successives d'un capital placé avec intérêts \textbf{simples} correspondent aux termes d'une suite \textbf{arithmétique}.
\end{propriete}
\newpage
\begin{methode}[Utiliser la suite associée à des intérêts simples]
  \textbf{Énoncé}\quad--\quad On dépose \Eu{2500} sur compte avec intérêts simples à $3\,\%$ par an.

  Déterminer le montant du capital au bout de $10$ ans.
  \bigskip

  \textbf{Solution}\quad--\quad Désignons par \suite la suite des montants annuels.

  Les intérêts étant \textit{simples}, la suite \suite est arithmétique. Déterminons le premier terme et la raison $r$.

  Le capital initial \Eu{2500} correspond au premier terme de ma suite \suite : $u_0=\num{2500}$.

  Les intérêts annuels s'élèvent à $3\,\%$ du capital initial donc
  \begin{align*}
    r & = u_0\times\frac{3}{100} \\
      & = \num{2500}\times0,03   \\
      & = 75
  \end{align*}

  On en déduit donc la forme explicite de la suite \suite : pour tout $n\in\N$, on a $u_n=\num{2500}+75n$.

  On souhaite connaître le capital au bout de $10$ ans, c'est-à-dire $u_{10}$ :
  \begin{align*}
    u_{10} & = \num{2500}+75\times10 \\
           & = \num{2500}+750        \\
           & =\num{3250}
  \end{align*}
  Au bout de dix ans, le capital sera de \Eu{3250}.
\end{methode}



\subsection{Intérêts composés}

\subsubsection{Définition et propriété}

\begin{definition}
  Les intérêts \textbf{composés} représentent une partie \textit{variable} correspondant à un pourcentage
  du capital précédent la période pour laquelle ils sont calculés.
\end{definition}

\begin{propriete}
  Les valeurs successives d'un capital placé avec intérêts \textbf{composés} correspondent aux termes d'une suite \textbf{géométrique}.
\end{propriete}

\begin{methode}[Utiliser la suite associée à des intérêts composés]
  \textbf{Énoncé}\quad--\quad On dépose \Eu{2500} sur compte avec intérêts composés à $3\,\%$ par an.

  Déterminer le montant du capital au bout de $10$ ans.
  \bigskip

  \textbf{Solution}\quad--\quad Désignons par \suite[v] la suite des montants annuels.

  Les intérêts étant \textit{composés}, la suite \suite[v] est géométrique. Déterminons le premier terme et la raison $q$.

  Le capital initial \Eu{2500} correspond au premier terme de ma suite \suite[v] : $v_0=\num{2500}$.

  Le capital au bout d'un an a augmenté de $3\,\%$ donc la raison $q$ de la suite géométrique \suite[v]
  correspond au \textit{coefficient multiplicateur}
  d'une augmentation de $3\,\%$, soit $1+\frac{3}{100}=1,03$.

  On en déduit donc la forme explicite de la suite \suite[v] : pour tout $n\in\N$, on a $v_n=\num{2500}\times1,03^n$.

  On souhaite connaître le capital au bout de $10$ ans, c'est-à-dire $v_{10}$ :
  \begin{align*}
    v_{10} & = \num{2500}\times1,03^10 \\
           & \approx \num{3359,79}
  \end{align*}
  Au bout de dix ans, le capital sera de \Eu{3359,79}.
\end{methode}