\chapter{Trigonométrie}

\section{Vers une définition des angles}

\subsection{Cercle trigonométrique}

\begin{definition}[Cercle trigonométrique]\index{Trigonométrie!Cercle trigonométrique}
  On se place dans un repère orthonormé \repere.
  \begin{multicols}{2}
    On définit :
    \begin{itemize}
      \item le \textbf{cercle trigonométrique} est le cercle $\mathcal{C}$ de centre $O$ et de rayon $1$.
      \item le \textbf{sens direct} (ou \textbf{sens trigonométrique}, ou encore \textbf{sens positif}) est le sens contraire des aiguilles d'une montre ;
      \item le \textbf{sens indirect} ou anti-trigonométrique est le sens horaire.
    \end{itemize}
    \begin{center}
      \begin{tikzpicture}[every label/.append style={font=\scriptsize}]
        \pgfmathsetmacro{\rayon}{1.6}
        \coordinate[label=below left:$O$] (O) at (0, 0);
        \coordinate[label= below right:$\mathtt{I}$] (I) at (\rayon, 0);
        \coordinate[label=above left:$J$] (J) at (0, \rayon);
        \coordinate (J') at ($(O)!-1!(J)$);
        \coordinate (I') at ($(O)!-1!(I)$);
        \draw[very thick,-{Latex[round]}] (O) -- (I) node[midway,below,font=\scriptsize] {$\vecti$};
        \draw[very thick,-{Latex[round]}] (O) -- (J) node[midway,left,font=\scriptsize] {$\vectj$};
        \draw[thick] (O) circle (\rayon cm);
        \coordinate[label=above left:$\mathcal{C}$] (C) at (135:\rayon);
        \draw[thick,-latex',black] ([xshift=-6mm]I') -- ([xshift=6mm]I);
        \draw[thick,-latex',black] ([yshift=-6mm]J') -- ([yshift=6mm]J);
        \coordinate (A) at (15:1.3*\rayon cm);
        \draw[very thick,-{Latex[round]},colorSec] (A) arc[start angle=15, delta angle=45, radius=1.3*\rayon cm] node[midway,above right,font=\scriptsize] {\faPlus};
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{definition}

\subsection{Enroulement de la droite des réels}

\begin{multicols}{2}


  \begin{center}
    \begin{tikzpicture}[every label/.append style={font=\scriptsize}]
      \pgfmathsetmacro{\rayon}{2}
      \pgfmathsetmacro{\angleIOM}{127}
      \coordinate[label=below left:$O$] (O) at (0, 0);
      \coordinate[label= below right:$\mathtt{I}$] (I) at (\rayon, 0);
      \coordinate[label=above left:$J$] (J) at (0, \rayon);
      \coordinate (J') at ($(O)!-1!(J)$);
      \coordinate (I') at ($(O)!-1!(I)$);
      \draw[very thick,-{Latex[round]}] (O) -- (I) node[midway,below,font=\scriptsize] {$\vecti$};
      \draw[very thick,-{Latex[round]}] (O) -- (J) node[midway,left,font=\scriptsize] {$\vectj$};
      \draw[thick] (O) circle (\rayon cm);
      \coordinate[label=below left:$\mathcal{C}$] (C) at (225:\rayon);
      \draw[thick,-latex',black] ([xshift=-6mm]I') -- ([xshift=6mm]I);
      \draw[thick,-latex',black] ([yshift=-6mm]J') -- ([yshift=6mm]J);
      \coordinate (A) at (195:1.7*\rayon cm);
      \draw[very thick,-{Latex[round]},colorTer] (A) arc[start angle=195, delta angle=45, radius=1.7*\rayon cm] node[midway,below left,font=\scriptsize] {\faPlus};
      \coordinate[label=above left:$M(x)$] (M) at (\angleIOM:\rayon);
      \coordinate[label=right:$x$] (Ax) at (\rayon, {(\angleIOM*pi*\rayon)/180});
      \coordinate[label=right:$1$] (A1) at (\rayon, \rayon);
      \coordinate[label=right:$-1$] (Am1) at (\rayon, -\rayon);
      \node[above left=1mm,font=\scriptsize] at (Ax) {$M'$};

      \draw[thick,-latex'] (\rayon,-\rayon*2) coordinate[label=above left:$(d)$] (d) -- (\rayon,\rayon*2.5);
      \draw[thick] (O) circle (\rayon cm);
      \draw[line width=1.5pt, colorSec] (I) arc[start angle=0, end angle=\angleIOM, radius=\rayon cm];
      \draw[line width=1.5pt, colorSec] (I) -- (Ax);
      \draw[ultra thick, colorSec] ([xshift=-1mm]Ax) -- ([xshift=1mm]Ax);
      \draw[thick] ([xshift=-1mm]A1) -- ([xshift=1mm]A1);
      \draw[thick] ([xshift=-1mm]Am1) -- ([xshift=1mm]Am1);
      \node[cross=3.5pt, very thick,colorSec] at (M) {};

      \draw[dashed,-{Latex[]}] (Ax) to[out=190,in=80] node[midway,above left,font=\scriptsize] {$\mathtt{I}M'=\arc{\mathtt{I}M}$} (M);
      \node[below,anchor=north west,font={\lightfont\scriptsize},rotate=90] at (d) {axe des réels};
    \end{tikzpicture}
  \end{center}
  Sur la figure ci-contre, on enroule la droite $(d)$ des réels autour du cercle trigonométrique $\mathcal{C}$.
  \medskip

  On imagine que le segment $[\mathtt{I}M']$ est une \og{}ficelle\fg{} qu'on enroule
  sur le cercle trigonométrique $\mathcal{C}$, c'est-à-dire  qu'au point $M'$ de $(d)$ correspond
  le point $M$ du cercle tel que $\mathtt{I}M = \arc{\mathtt{I}M}$.
  \medskip

  À tout point de la droite~$(d)$ (c'est-à-dire à tout nombre réel $x$), on peut donc associer un unique point $M$ du cercle $\mathcal{C}$.
  \medskip

  Réciproquement, à tout point du cercle $\mathcal{C}$, on peut associer une \textit{infinité} de point de la droite $(d)$
  à un tour près (soit à $2\pi$ près).

  Tous ces points auront pour abscisses des nombres de la forme $x+k\times2\pi$ (en prenant $k\in\Z$ et $x\in\R$) :
  {\footnotesize\[
    \begin{matrix*}[l]
      \ldots        \\
      x-4\pi        \\
      x-2\pi        \\
      x             \\
      x+2\pi        \\
      x+4\pi        \\
      \ldots        \\
    \end{matrix*}\]}
\end{multicols}

\begin{exemple}
  Le point $M$ associé à $x=\pi$ est aussi associé à $x'=7\pi$. En effet $7\pi = \pi + 3\times 2\pi$.
\end{exemple}

\subsection{Le radian}
\begin{definition}[Le radian]\index{Trigonométrie!Radian}
  On se place dans un repère orthonormé $(O\,;\,I\,, J)$.
  \begin{multicols}{2}
    \begin{center}
      \begin{tikzpicture}[every label/.append style={font=\scriptsize}]
        \pgfmathsetmacro{\rayon}{1.6}
        \pgfmathsetmacro{\angleIOM}{180/pi}
        \coordinate[label=below left:$O$] (O) at (0, 0);
        \coordinate[label= below right:$\mathtt{I}$] (I) at (\rayon, 0);
        \coordinate[label=above left:$J$] (J) at (0, \rayon);
        \coordinate[label=above:$M$] (M) at (\angleIOM:\rayon);
        % \node[cross=3.5pt, very thick,colorSec] at (M) {};
        \draw[colorPrim,{Latex[round]}-{Latex[round]}] ([yshift=-1mm]O) -- ([yshift=-1mm]I) node[midway,below,font=\scriptsize] {$1$};
        % \draw[thick] (O) circle (\rayon cm);
        \draw[thick] (-15:\rayon) arc[start angle=-15, end angle = 100,radius=\rayon];
        \pic [draw,-latex',colorPrim,thick,angle radius=5mm,fill=colorPrim!10,"\scriptsize$1$ rad",angle eccentricity=1.7] {angle = I--O--M};
        \coordinate (A) at (0:1.1*\rayon cm);
        \draw[{Latex[round]}-{Latex[round]},colorPrim] (A) arc[start angle=0, delta angle=\angleIOM, radius=1.1*\rayon cm] node[midway,above right,font=\scriptsize] {$1$};
        \draw[ultra thick,colorPrim] (I) arc[start angle=0, delta angle=\angleIOM,radius=\rayon];
        \draw[thick,colorPrim] (O) -- (M);
        \draw[thick,-latex',black] ([xshift=-6mm]O) -- ([xshift=6mm]I);
        \draw[thick,-latex',black] ([yshift=-6mm]O) -- ([yshift=6mm]J);
      \end{tikzpicture}
    \end{center}
    On définit le \textbf{radian} comme la mesure de l'angle $\widehat{\mathtt{I}OM}$ où $M$ est le point du cercle trigonométrique tel que $\arc{\mathtt{I}M}=1$.
    \bigskip

    D'une manière générale, la mesure de l'arc $\arc{\mathtt{I}M}$ sur le cercle trigonométrique correspondra à la \textit{mesure en radians} de l'angle géométrique $\widehat{\mathtt{I}OM}$.
  \end{multicols}
\end{definition}


\begin{propriete}\index{Trigonométrie!Radian (conversions)}
  La mesure en radians d'un angle est \textit{proportionnelle} à sa mesure en degrés.

  Si $d$ est la mesure de l'angle en degrés et $\alpha$ sa mesure en radians alors on a
  \begin{align*}
    \alpha & = \frac{d \times \pi}{180} & d & = \frac{\alpha \times 180}{\pi}
  \end{align*}

  On a donc le tableau de correspondance suivant pour les valeurs courantes :

  \begin{center}
    \begin{tblr}{width=\linewidth,colspec={X[l,2]*{8}{X[c,1]}},hlines,vlines,stretch=1.2,column{1}={bg=colorSec!10},rows={valign={m}}}
      Mesure en degrés  & $\ang{0}$ & $\ang{30}$      & $\ang{45}$      & $\ang{60}$      & $\ang{90}$      & $\ang{120}$      & $\ang{180}$ & $\ang{360}$ \\
      Mesure en radians & $0$       & $\frac{\pi}{6}$ & $\frac{\pi}{4}$ & $\frac{\pi}{3}$ & $\frac{\pi}{2}$ & $\frac{2\pi}{3}$ & $\pi$       & $2\pi$      \\
    \end{tblr}
    \medskip
    \begin{tikzpicture}[every label/.append style={font=\scriptsize}]
      \pgfmathsetmacro{\rayon}{2}
      \coordinate[label=below left:$O$] (O) at (0, 0);
      \coordinate (I) at (\rayon, 0);
      \coordinate (J) at (0, \rayon);
      \coordinate (J') at ($(O)!-1!(J)$);
      \coordinate (I') at ($(O)!-1!(I)$);
      \draw[thick] (O) circle (\rayon cm);

      \begin{scope}[every label/.append style={font=\scriptsize}]
        \foreach \angle/\label [count=\i] in {
            30/\frac{\pi}{6},
            45/\frac{\pi}{4},
            60/\frac{\pi}{3},
            90/\frac{\pi}{2},
            120/\frac{2\pi}{3},
            135/\frac{3\pi}{4},
            150/\frac{5\pi}{6}
          }{
            \coordinate[label=\angle:$\label$] (pp\i) at (\angle:\rayon);
            \draw[thick] (\angle:\rayon*0.95) -- (\angle:\rayon*1.05);
            \coordinate[label=-\angle:$-\label$] (pn\i) at (-\angle:\rayon);
            \draw[thick] (-\angle:\rayon*0.95) -- (-\angle:\rayon*1.05);
          }
        \foreach \angle/\label [count=\i] in {
            0/O,
            180/\pi
          }{
            \coordinate[label=\angle:$\label$] (pe\i) at (\angle:\rayon);
            \draw[thick] (\angle:\rayon*0.95) -- (\angle:\rayon*1.05);
          }
        \node[inner sep=0pt,left=3.5mm of pe2.west,font=\scriptsize,anchor=east] {$-\pi$~ou~};
      \end{scope}
      \draw (I') -- (I);
      \draw (J') -- (J);
      \coordinate (A) at (15:1.3*\rayon cm);
      % \draw[very thick,-{Latex[round]},colorSec] (A) arc[start angle=15, delta angle=45, radius=1.3*\rayon cm] node[midway,above right,font=\scriptsize] {\faPlus};
    \end{tikzpicture}
  \end{center}
\end{propriete}

\subsection{Mesure principale}

\begin{definition}\index{Trigonométrie!Mesure principale}
  Soit $M$ un point du cercle trigonométrique dans un repère orthonormé $(O\,;\,I\,, J)$.
  \medskip

  Il existe une seule mesure de l'angle orienté $\left(\vecti\,;\,\vecteur{OM}\right)$ qui appartiennent à l'intervalle $\interof{-\pi{} \pi}$\footnotemark.

  Cette mesure s'appelle la \textbf{mesure principale} de l'angle $\left(\vecti\,;\,\vecteur{OM}\right)$.
\end{definition}
\footnotetext{Cette mesure correspond au \textit{plus court} chemin de $\mathtt{I}$ à $M$ sur le cercle trigonométrique.}


\begin{methode}[Déterminer la mesure principale d'un angle]
  \textbf{Énoncé}\quad On donne l'angle $\alpha=\frac{77\pi}{4}$.
  Quelle est la mesure principale de l'angle $\alpha$~?\bigskip

  Il existe deux méthodes : une pratique, une rigoureuse.\bigskip

  \textbf{Solution 1 (pratique)}\quad On remarque que le dénominateur est $4$. On va donc chercher le multiple de $4\times2=8$ le plus proche de $77$.
  \begin{align*}
    8 \times 9 & = 72 & 8 \times 10 & = 80
  \end{align*}
  On remarque que $80$ est plus proche de $77$ que $72$. On peut donc écrire que :
  \begin{align*}
    \alpha & = \frac{77\pi}{4}                   \\
    \alpha & = \frac{(80-3)\pi}{4}               \\
    \alpha & = \frac{80\pi}{4} - \frac{3\pi}{4}  \\
    \alpha & = 20\pi - \frac{3\pi}{4}            \\
    \alpha & =  - \frac{3\pi}{4}+ 10 \times 2\pi \\
  \end{align*}
  L'angle $\alpha$ étant défini à $k \times 2\pi$ près  $(k\in\Z)$, on peut dire que $\alpha = - \frac{3\pi}{4}$.

  On a $- \frac{3\pi}{4} \in \interof{-\pi{} \pi}$ donc la mesure \textit{principale} de l'angle $\alpha$ est $ - \frac{3\pi}{4}$.
  \bigskip

  \textbf{Solution 2 (rigoureuse)}\quad On cherche $k\in\Z$ tel que \[-\pi < \alpha + k \times 2\pi \ppq \pi\]
  On peut donc écrire :
  \begin{alignat*}{3}
    -\pi                                 & < & \frac{77\pi}{4} + k \times 2\pi & \ppq & \pi                                    & \longleftarrow\text{ on soustrait }\frac{77\pi}{4}\text{ à chaque membre} \\
    -\pi-\frac{77\pi}{4}                 & < & k \times 2\pi                   & \ppq & \pi-\frac{77\pi}{4}                    &                                                                           \\
    -\frac{4\pi}{4}-\frac{77\pi}{4}      & < & k \times 2\pi                   & \ppq & \frac{4\pi}{4}-\frac{77\pi}{4}         &                                                                           \\
    -\frac{81\pi}{4}                     & < & k \times 2\pi                   & \ppq & -\frac{73\pi}{4}                       & \longleftarrow\text{ on multiplie chaque membre par l'inverse de }2\pi    \\
    -\frac{81\pi}{4}\times\frac{1}{2\pi} & < & k                               & \ppq & -\frac{73\pi}{4}  \times\frac{1}{2\pi} &                                                                           \\
    -\frac{81}{8}                        & < & k                               & \ppq & -\frac{73}{8}                          & \longleftarrow\text{ on calcule les quotients }                           \\
    -10,125                              & < & k                               & \ppq & -9,125                                 &                                                                           \\
  \end{alignat*}
  On en déduit que $k=-10$. On peut donc à présent calculer la mesure principale de $\alpha$.
  \begin{align*}
    \alpha & = \frac{77\pi}{4} + k \times 2\pi  \\
    \alpha & = \frac{77\pi}{4} -10 \times 2\pi  \\
    \alpha & = \frac{77\pi}{4} -\frac{80\pi}{4} \\
    \alpha & = -\frac{3\pi}{4}                  \\
  \end{align*}
  La mesure \textit{principale} de l'angle $\alpha$ est $ - \frac{3\pi}{4}$.
\end{methode}

\section{Cosinus et sinus d'un nombre réel}

\subsection{Cosinus et sinus définis à l'aide du cercle trigonométrique}

\begin{definition}\index{Trigonométrie!Cosinus}\index{Trigonométrie!Sinus}
  Soit $x\in\R$ et $M$ le point associé\footnotemark sur le cercle trigonométrique dans le repère \Repere.
  \begin{multicols}{2}

    On définit alors les deux quantités suivantes
    \begin{itemize}
      \item le \textbf{cosinus} de $x$, noté $\cos x$, comme l'abscisse du point $M$ dans \Repere;
      \item le \textbf{sinus} de $x$, noté $\sin x$, comme l'ordonnée du point $M$ dans \Repere.
    \end{itemize}
    Ainsi, dans \Repere, on a : \[M\left(\cos x \,; \sin x\right)\]


    \begin{center}
      \begin{tikzpicture}[every label/.append style={font=\scriptsize}]
        \pgfmathsetmacro{\rayon}{2.3}
        \pgfmathsetmacro{\angleIOM}{55}
        \coordinate[label=below left:$O$] (O) at (0, 0);
        \coordinate[label= below right:$\mathtt{I}$] (I) at (\rayon, 0);
        \coordinate[label=above left:$J$] (J) at (0, \rayon);
        \coordinate[label={[anchor=south west]above:$M\left(\cos x \,; \sin x\right)$}] (M) at (\angleIOM:\rayon);
        \coordinate[label=below:$\cos x$] (C) at ($(O)!(M)!(I)$);
        \coordinate[label=left:$\sin x$] (S) at ($(O)!(M)!(J)$);
        \draw[thick] (-15:\rayon) arc[start angle=-15, end angle = 100,radius=\rayon];
        \pic [draw,-latex',black,thick,angle radius=5mm,fill=black!10,"$x$",angle eccentricity=1.5,font=\scriptsize] {angle = I--O--M};
        \draw (O) -- (M);
        \draw[dashed] (S) -- (M) -- (C);
        \draw[-latex',black] ([xshift=-6mm]O) -- ([xshift=6mm]I);
        \draw[-latex',black] ([yshift=-6mm]O) -- ([yshift=6mm]J);
        \draw[very thick, colorPrim!70] (O) -- (C);
        \draw[very thick, colorSec] (O) -- (S);
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{definition}
\footnotetext{On a donc $x$ qui est une mesure en radians de l'angle $\widehat{\mathtt{I}OM}$.}
\subsection{Valeurs remarquables}

\begin{multicols}{2}
  \begin{center}
    \begin{tblr}{width=\linewidth,colspec={X[l,1]*{5}{X[c,1]}},hlines,vlines,stretch=2,column{1}={bg=colorPrim!10},rows={valign={m},mode={math}}}
      x      & 0 & \frac{\pi}{6}     & \frac{\pi}{4}     & \frac{\pi}{3}     & \frac{\pi}{2} \\
      \cos x & 1 & \frac{\sqrt 3}{2} & \frac{\sqrt 2}{2} & \frac{1}{2}       & 0             \\
      \sin x & 0 & \frac{1}{2}       & \frac{\sqrt 2}{2} & \frac{\sqrt 3}{2} & 1             \\
    \end{tblr}
  \end{center}

  \begin{center}
    \begin{tikzpicture}[every label/.append style={font=\scriptsize}]
      \pgfmathsetmacro{\rayon}{3}
      \coordinate[label=below left:$O$] (O) at (0, 0);
      \coordinate[label= below right:$\mathtt{I}$] (I) at (\rayon, 0);
      \coordinate[label=above left:$J$] (J) at (0, \rayon);
      \foreach \angle/\label/\cosPt/\sinPt [count=\i] in {
          30/\frac{\pi}{6}/\frac{\sqrt 3}{2}/\frac{1}{2},
          45/\frac{\pi}{4}/\frac{\sqrt 2}{2}/\frac{\sqrt 2}{2},
          60/\frac{\pi}{3}/\frac{1}{2}/\frac{\sqrt 3}{2}
        }{
          \coordinate[label=\angle:$\label$] (p\i) at (\angle:\rayon);
          \draw[thick] (\angle:\rayon*0.95) -- (\angle:\rayon*1.05);
          \draw[thin,densely dashed,black!40] (p\i) -- ($(O)!(p\i)!(I)$) node[below, black, font=\scriptsize] {$\cosPt$};
          \draw[thin,densely dashed,black!40] (p\i) -- ($(O)!(p\i)!(J)$) node[left, black, font=\scriptsize] {$\sinPt$};
        }
      \draw[thick] (-15:\rayon) arc[start angle=-15, end angle = 100,radius=\rayon];
      \draw[-latex',black] ([xshift=-6mm]O) -- ([xshift=6mm]I);
      \draw[-latex',black] ([yshift=-6mm]O) -- ([yshift=6mm]J);
      \node[inner sep=0pt,font=\scriptsize,above right=1mm of J] {$\frac{\pi}{2}$};
      \node[inner sep=0pt,font=\scriptsize,above right=1mm of I] {$0$};
    \end{tikzpicture}
  \end{center}
\end{multicols}

\subsection{Angles associés}\index{Trigonométrie!Angles associés}\index{Angles associés}

Soit $x$ un nombre réel et $M$ le point du cercle trigonométrique associé à $x$. Soit $k$ un entier relatif.

\begin{center}
  \newcounter{mycnta}
  \newcommand{\mycnta}{\stepcounter{mycnta}\Circled{\arabic{mycnta}}}
  \begin{tblr}{width=\linewidth,colspec={X[l,0.1]X[l,1]X[c,1]X[l,2]},hlines,stretch=1.5,column{2}={mode={math}},rows={valign={m}},vline{1,5}={1-6}{}}
    % x et x + 2kpi
    \setcounter{mycnta}{0}
    \mycnta &
    \SetCell{font=\small}
    \begin{aligned}
      \cos(x+ k \times 2\pi) & = \cos x \\
      \\
      \sin(x+ k \times 2\pi) & = \sin x
    \end{aligned}
            &
    \begin{tikzpicture}[
        baseline,
        every label/.append style={font=\scriptsize,inner sep=0pt},
        label distance=1mm,
        angle stuff/.style={draw,angle radius=5mm,angle eccentricity=1.4,font=\scriptsize},
        angle mark/.style={angle stuff,-latex'},
        angle mark reverse/.style={angle stuff,latex'-}]
      \pgfmathsetmacro{\rayon}{1.2}
      \pgfmathsetmacro{\angleIOM}{40}
      \coordinate[label=below left:$O$] (O) at (0, 0);
      \draw[thick] (O) circle (\rayon);
      \coordinate[label= right:$\mathtt{I}$] (I) at (\rayon, 0);
      \coordinate[label=above:$J$] (J) at (0, \rayon);
      \coordinate[label={[colorPrim]above:$M$}] (M) at (\angleIOM:\rayon);
      \coordinate (M') at ({\angleIOM-0.05}:\rayon);
      \node[right=1mm of M.east,inner sep=0pt,colorSec,font=\scriptsize] {$M'$};
      \coordinate (J') at ($(O)!-1!(J)$);
      \coordinate (I') at ($(O)!-1!(I)$);
      \draw (I') -- (I);
      \draw (J') -- (J);
      \draw[colorPrim] (O) -- (M);
      \pic [angle mark,colorPrim,"$x$"] {angle = I--O--M};
      \pic [angle mark,colorSec,"$x+2\pi$",angle radius=8mm] {angle = M--O--M'};
    \end{tikzpicture}
            &
    Les points $M$ et $M'$ (repéré par $x+ k \times 2\pi$) sont confondus.                                       \\
    % x et -x
    \mycnta &
    \begin{aligned}
      \cos(-x) & = \cos x  \\
      \\
      \sin(-x) & = -\sin x
    \end{aligned}
            &
    \begin{tikzpicture}[
        baseline,
        every label/.append style={font=\scriptsize,inner sep=0pt},
        label distance=1mm,
        angle stuff/.style={draw,angle radius=5mm,angle eccentricity=1.4,font=\scriptsize},
        angle mark/.style={angle stuff,-latex'},
        angle mark reverse/.style={angle stuff,latex'-}]
      \pgfmathsetmacro{\rayon}{1.2}
      \pgfmathsetmacro{\angleIOM}{40}
      \coordinate[label=below left:$O$] (O) at (0, 0);
      \draw[thick] (O) circle (\rayon);
      \coordinate[label= right:$\mathtt{I}$] (I) at (\rayon, 0);
      \coordinate[label=above:$J$] (J) at (0, \rayon);
      \coordinate[label={[colorPrim]\angleIOM:$M$}] (M) at (\angleIOM:\rayon);
      \coordinate[label={[colorSec]{-\angleIOM}:$M'$}] (M') at (-\angleIOM:\rayon);
      \coordinate (J') at ($(O)!-1!(J)$);
      \coordinate (I') at ($(O)!-1!(I)$);
      \coordinate (S') at ($(O)!(M')!(J')$);
      \coordinate (S) at ($(O)!(M)!(J)$);
      \draw (I') -- (I);
      \draw (J') -- (J);
      \draw[thick,densely dotted,black!60] (S) -- (M) -- (M') -- (S');
      \draw[colorPrim] (O) -- (M);
      \draw[colorSec] (O) -- (M');
      \pic [angle mark,colorPrim,"$x$"] {angle = I--O--M};
      \pic [angle mark reverse,colorSec,"$-x$"] {angle = M'--O--I};
    \end{tikzpicture}
            &
    Les points $M$ et $M'$ (repéré par $-x$) sont symétriques par rapport à l'axe des abscisses.                 \\
    % x et pi - x
    \mycnta &
    \begin{aligned}
      \cos(\pi-x) & = -\cos x \\
      \\
      \sin(\pi-x) & = \sin x
    \end{aligned}
            &
    \begin{tikzpicture}[
        baseline,
        every label/.append style={font=\scriptsize,inner sep=0pt},
        label distance=1mm,
        angle stuff/.style={draw,angle radius=5mm,angle eccentricity=1.4,font=\scriptsize},
        angle mark/.style={angle stuff,-latex'},
        angle mark reverse/.style={angle stuff,latex'-}]
      \pgfmathsetmacro{\rayon}{1.2}
      \pgfmathsetmacro{\angleIOM}{50}
      \coordinate[label=below left:$O$] (O) at (0, 0);
      \draw[thick] (O) circle (\rayon);
      \coordinate[label= right:$\mathtt{I}$] (I) at (\rayon, 0);
      \coordinate[label=above:$J$] (J) at (0, \rayon);
      \coordinate[label={[colorPrim]\angleIOM:$M$}] (M) at (\angleIOM:\rayon);
      \coordinate[label={[colorSec]{180-\angleIOM}:$M'$}] (M') at (180-\angleIOM:\rayon);
      \coordinate (J') at ($(O)!-1!(J)$);
      \coordinate (I') at ($(O)!-1!(I)$);
      \coordinate (C') at ($(O)!(M')!(I')$);
      \coordinate (C) at ($(O)!(M)!(I)$);
      \draw (I') -- (I);
      \draw (J') -- (J);
      \draw[thick,densely dotted,black!60] (C) -- (M) -- (M') -- (C');
      \draw[colorPrim] (O) -- (M);
      \draw[colorSec] (O) -- (M');
      \pic [angle mark,colorPrim,"$x$"] {angle = I--O--M};
      \pic [angle mark,colorSec,angle radius=8mm] {angle = I--O--M'};
      \node[colorSec,font=\scriptsize] at (0,\rayon*0.5) {$\pi-x$};
    \end{tikzpicture}
            &
    Les points $M$ et $M'$ (repéré par $\pi-x$) sont symétriques par rapport à l'axe des ordonnées.              \\
    % x et pi + x
    \mycnta &
    \begin{aligned}
      \cos(\pi+x) & = -\cos x \\
      \\
      \sin(\pi+x) & = -\sin x
    \end{aligned}
            &
    \begin{tikzpicture}[
        baseline,
        every label/.append style={font=\scriptsize,inner sep=0pt},
        label distance=1mm,
        angle stuff/.style={draw,angle radius=5mm,angle eccentricity=1.4,font=\scriptsize},
        angle mark/.style={angle stuff,-latex'},
        angle mark reverse/.style={angle stuff,latex'-}]
      \pgfmathsetmacro{\rayon}{1.2}
      \pgfmathsetmacro{\angleIOM}{40}
      \coordinate[label=below left:$O$] (O) at (0, 0);
      \draw[thick] (O) circle (\rayon);
      \coordinate[label= right:$\mathtt{I}$] (I) at (\rayon, 0);
      \coordinate[label=above:$J$] (J) at (0, \rayon);
      \coordinate[label={[colorPrim]\angleIOM:$M$}] (M) at (\angleIOM:\rayon);
      \coordinate[label={[colorSec]{180+\angleIOM}:$M'$}] (M') at (180+\angleIOM:\rayon);
      \coordinate (J') at ($(O)!-1!(J)$);
      \coordinate (I') at ($(O)!-1!(I)$);
      \coordinate (S') at ($(O)!(M')!(J')$);
      \coordinate (S) at ($(O)!(M)!(J)$);
      \coordinate (C') at ($(O)!(M')!(I')$);
      \coordinate (C) at ($(O)!(M)!(I)$);
      \draw (I') -- (I);
      \draw (J') -- (J);
      \draw[thick,densely dotted,black!60] (C) -- (M) -- (S);
      \draw[thick,densely dotted,black!60] (C') -- (M') -- (S');
      \draw[colorPrim] (O) -- (M);
      \draw[colorSec] (O) -- (M');
      \pic [angle mark,colorPrim,"$x$"] {angle = I--O--M};
      \pic [angle mark,colorSec,angle radius=8mm,angle eccentricity=1.2,"$\pi+x$"] {angle = I--O--M'};
      % \node[colorSec,font=\scriptsize] at (0,\rayon*0.5) {$\pi+x$};
    \end{tikzpicture}
            &
    Les points $M$ et $M'$ (repéré par $\pi+x$) sont symétriques par rapport à l'origine $O$ du repère.          \\
    % x, pi/2 -x et pi/2+x
    \mycnta &
    \SetCell{font=\footnotesize}
    \begin{aligned}
      \cos(\frac{\pi}{2}-x) & = \sin x  \\
      \sin(\frac{\pi}{2}-x) & = \cos x  \\
      \\
      \cos(\frac{\pi}{2}+x) & = -\sin x \\
      \sin(\frac{\pi}{2}+x) & = \cos x
    \end{aligned}
            &
    \begin{tikzpicture}[
        baseline,
        every label/.append style={font=\scriptsize,inner sep=0pt},
        label distance=1mm,
        angle stuff/.style={draw,angle radius=5mm,angle eccentricity=1.4,font=\scriptsize},
        angle mark/.style={angle stuff,-latex'},
        angle mark reverse/.style={angle stuff,latex'-}]
      \pgfmathsetmacro{\rayon}{2.5}
      \pgfmathsetmacro{\angleIOM}{20}
      \coordinate[label=below left:$O$] (O) at (0, 0);
      % \draw[thick] (O) circle (\rayon);
      \coordinate[label= right:$\mathtt{I}$] (I) at (\rayon, 0);
      \coordinate[label=above:$J$] (J) at (0, \rayon);
      \draw[thick] (-5:\rayon) arc[start angle=-5, end angle = 115,radius=\rayon];
      \coordinate[label={[colorPrim]\angleIOM:$M$}] (M) at (\angleIOM:\rayon);
      \coordinate[label={[colorSec]{90-\angleIOM}:$M_1$}] (M') at (90-\angleIOM:\rayon);
      \coordinate[label={[colorTer]{90+\angleIOM}:$M_2$}] (M2) at (90+\angleIOM:\rayon);
      \coordinate (S) at ($(O)!(M)!(J)$);
      \coordinate (S') at ($(O)!(M')!(J)$);
      \coordinate (S2) at ($(O)!(M2)!(J)$);
      \coordinate (C) at ($(O)!(M)!(I)$);
      \coordinate (C') at ($(O)!(M')!(I)$);
      \coordinate (C2) at ($(O)!(M2)!(I)$);
      \draw ([xshift=-10mm]O) -- (I);
      \draw ([yshift=-2mm]O) -- (J);
      % \draw[thick,densely dotted,black!60] (C) -- (M) -- (S);
      % \draw[thick,densely dotted,black!60] (C') -- (M') -- (S');
      \draw[colorPrim] (O) -- (M);
      \draw[colorSec] (O) -- (M');
      \draw[colorTer] (O) -- (M2);
      \draw[thick,densely dotted,black!60] (C) -- (M) -- (S);
      \draw[thick,densely dotted,black!60] (C') -- (M') -- (S');
      \draw[thick,densely dotted,black!60] (C2) -- (M2) -- (S2);
      \pic [angle mark,colorPrim,angle radius=8mm,"$x$"] {angle = I--O--M};
      \pic [angle mark,colorSec,angle radius=13mm,angle eccentricity=1.2,"$\frac{\pi}{2}-x$"] {angle = I--O--M'};
      \pic [angle mark,colorTer,angle radius=18mm,angle eccentricity=1.2,"$\frac{\pi}{2}+x$"] {angle = I--O--M2};
      % \node[colorSec,font=\scriptsize] at (0,\rayon*0.5) {$\pi+x$};
    \end{tikzpicture}
            &
    Les points $M$ et $M_1$ (repéré par $\frac{\pi}{2}-x$) sont symétriques par rapport à la bissectrice du premier secteur du repère.

    Les points $M_1$ et $M_2$ (repéré par $\frac{\pi}{2}+x$) sont symétriques par rapport à l'axe des ordonnées. \\
  \end{tblr}
\end{center}

\begin{methode}[Déterminer la valeur exacte d'un cosinus ou d'un sinus]
  \textbf{Énoncé}\quad Déterminer la valeur exacte des nombres suivants.
  \begin{align*}
    A & = \cos\left(-\frac{\pi}{6}\right) & B & = \cos\left(\frac{2\pi}{3}\right) & C & = \sin\left(\frac{5\pi}{4}\right)
  \end{align*}
  \bigskip

  \textbf{Solution}\quad On va travailler avec les angles associés.
  \begin{itemize}
    \item On sait que $\cos\left(\frac{\pi}{6}\right)=\frac{\sqrt{3}}{2}$.

          En appliquant les formules en \Circled{2}, on en déduit que
          \begin{align*}
            \cos\left(-\frac{\pi}{6}\right) & = \cos\left(\frac{\pi}{6}\right) \\
                                            & = \frac{\sqrt{3}}{2}
          \end{align*}
          Donc $A=\frac{\sqrt{3}}{2}$.
    \item On sait que $\cos\left(\frac{\pi}{3}\right)=\frac{1}{2}$ et on remarque que :
          \begin{align*}
            \frac{2\pi}{3} & = \frac{3\pi-\pi}{3}                             \\
                           & = \frac{\cancel{3}\pi}{\cancel{3}}-\frac{\pi}{3} \\
                           & = \pi-\frac{\pi}{3}
          \end{align*}

          En appliquant les formules en \Circled{3}, on en déduit que
          \begin{align*}
            \cos\left(\frac{2\pi}{3}\right) & = \cos\left(\pi-\frac{\pi}{3}\right) \\
                                            & = -\cos\left(\frac{\pi}{3}\right)    \\
                                            & = -\frac{1}{2}
          \end{align*}
          Donc $B=-\frac{1}{2}$.
    \item On sait que $\sin\left(\frac{\pi}{4}\right)=\frac{\sqrt{2}}{2}$ et on remarque que :
          \begin{align*}
            \frac{5\pi}{4} & = \frac{4\pi+\pi}{4}                             \\
                           & = \frac{\cancel{4}\pi}{\cancel{4}}+\frac{\pi}{4} \\
                           & =\pi + \frac{\pi}{4}
          \end{align*}

          En appliquant les formules en \Circled{4}, on en déduit que
          \begin{align*}
            \sin\left(\frac{5\pi}{5}\right) & = \sin\left(\pi+\frac{\pi}{4}\right) \\
                                            & = -\sin\left(\frac{\pi}{4}\right)    \\
                                            & = -\frac{\sqrt{2}}{2}
          \end{align*}
          Donc $C=-\frac{\sqrt{2}}{2}$.
  \end{itemize}
\end{methode}

\subsection{Équations trigonométriques}

\begin{propriete}\index{Trigonométrie!Équations}
  Soit $\alpha\in\R$ et $A$ le point associé à $\alpha$ sur le cercle trigonométrique. $k$ est un entier relatif.
  \begin{multicols}{2}
    \begin{align*}
      \cos x = \cos \alpha & \Longleftrightarrow
      \begin{cases}
        x = \alpha + k \times 2\pi  \\
        \text{ou}                    \\
        x = -\alpha + k \times 2\pi \\
      \end{cases}
    \end{align*}

    \begin{center}
      \begin{tikzpicture}[
          baseline,
          every label/.append style={font=\scriptsize,inner sep=0pt},
          label distance=1mm,
          angle stuff/.style={draw,angle radius=5mm,angle eccentricity=1.4,font=\scriptsize},
          angle mark/.style={angle stuff,-latex'},
          angle mark reverse/.style={angle stuff,latex'-}]
        \pgfmathsetmacro{\rayon}{1.5}
        \pgfmathsetmacro{\angleIOM}{40}
        \coordinate[label=below left:$O$] (O) at (0, 0);
        \draw[thick] (O) circle (\rayon);
        \coordinate[label= right:$\mathtt{I}$] (I) at (\rayon, 0);
        \coordinate[label=above:$J$] (J) at (0, \rayon);
        \coordinate[label={[colorPrim]\angleIOM:$A$}] (M) at (\angleIOM:\rayon);
        \coordinate (M') at (-\angleIOM:\rayon);
        \coordinate (J') at ($(O)!-1!(J)$);
        \coordinate (I') at ($(O)!-1!(I)$);
        \coordinate (S') at ($(O)!(M')!(J')$);
        \coordinate (S) at ($(O)!(M)!(J)$);
        \coordinate (C) at ($(O)!(M)!(I)$);
        \draw (I') -- (I);
        \draw (J') -- (J);
        \draw[thick,densely dotted,black!60] (M) -- (M');
        \draw[colorPrim] (O) -- (M);
        \draw[colorSec] (O) -- (M');
        \pic [angle mark,colorPrim,"$\alpha$"] {angle = I--O--M};
        \pic [angle mark reverse,colorSec,"$-\alpha$"] {angle = M'--O--I};
        \fill (C) circle (1.5pt) node[fill=colorSec!5,font=\scriptsize,above=1mm,inner sep=1pt] {$\cos x$};
      \end{tikzpicture}
    \end{center}

    \begin{align*}
      \sin x = \sin \alpha & \Longleftrightarrow
      \begin{cases}
        x = \alpha + k \times 2\pi     \\
        \text{ou}                       \\
        x = \pi-\alpha + k \times 2\pi \\
      \end{cases}
    \end{align*}

    \begin{center}
      \begin{tikzpicture}[
          baseline,
          every label/.append style={font=\scriptsize,inner sep=0pt},
          label distance=1mm,
          angle stuff/.style={draw,angle radius=5mm,angle eccentricity=1.4,font=\scriptsize},
          angle mark/.style={angle stuff,-latex'},
          angle mark reverse/.style={angle stuff,latex'-}]
        \pgfmathsetmacro{\rayon}{1.5}
        \pgfmathsetmacro{\angleIOM}{40}
        \coordinate[label=below left:$O$] (O) at (0, 0);
        \draw[thick] (O) circle (\rayon);
        \coordinate[label= right:$\mathtt{I}$] (I) at (\rayon, 0);
        \coordinate[label=above:$J$] (J) at (0, \rayon);
        \coordinate[label={[colorPrim]\angleIOM:$A$}] (M) at (\angleIOM:\rayon);
        \coordinate (M') at (180-\angleIOM:\rayon);
        \coordinate (J') at ($(O)!-1!(J)$);
        \coordinate (I') at ($(O)!-1!(I)$);
        \coordinate (C') at ($(O)!(M')!(I')$);
        \coordinate (C) at ($(O)!(M)!(I)$);
        \coordinate (S) at ($(O)!(M)!(J)$);
        \draw (I') -- (I);
        \draw (J') -- (J);
        \draw[thick,densely dotted,black!60] (M) -- (M');
        \draw[colorPrim] (O) -- (M);
        \draw[colorSec] (O) -- (M');
        \pic [angle mark,colorPrim,"$\alpha$"] {angle = I--O--M};
        \pic [angle mark,colorSec,angle radius=8mm] {angle = I--O--M'};
        \node[colorSec,font=\scriptsize] at (0,\rayon*0.4) {$\pi-\alpha$};
        \fill (S) circle (1.5pt) node[fill=colorSec!5,font=\scriptsize,above=1mm,inner sep=1pt] {$\sin x$};
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{propriete}

\begin{remarque}
  Voir les cas \Circled{2} et \Circled{3} des angles associés pour une idée preuve.
\end{remarque}

\begin{methode}[Résoudre une équation trigonométrique]
  \textbf{Énoncé}\quad Résoudre dans $\interof{-\pi{} \pi}$ l'équation $(E) : \cos x = \frac{1}{2}$, et représenter les solutions sur le cercle trigonométrique.\bigskip
  
  \textbf{Solution}\quad On sait que $\cos\left(\frac{\pi}{3}\right)=\frac{1}{2}$. Donc $(E) $.
  
  \begin{multicols}{2}
    \begin{align*}
      (E) &\Longleftrightarrow \cos x = \cos\left(\frac{\pi}{3}\right)\\
      &\Longleftrightarrow
      \begin{cases}
        x = \frac{\pi}{3} + k \times 2\pi  \\
        \text{ou}                    \\
        x = -\frac{\pi}{3} + k \times 2\pi \\
      \end{cases}
    \end{align*}


    \begin{center}
      \begin{tikzpicture}[
          baseline,
          every label/.append style={font=\scriptsize,inner sep=0pt},
          label distance=1mm,
          angle stuff/.style={draw,angle radius=5mm,angle eccentricity=1.4,font=\scriptsize},
          angle mark/.style={angle stuff,-latex'},
          angle mark reverse/.style={angle stuff,latex'-}]
        \pgfmathsetmacro{\rayon}{1.5}
        \pgfmathsetmacro{\angleIOM}{60}
        \coordinate[label=below left:$O$] (O) at (0, 0);
        \draw[thick] (O) circle (\rayon);
        \coordinate[label= right:$\mathtt{I}$] (I) at (\rayon, 0);
        \coordinate[label=above:$J$] (J) at (0, \rayon);
        \coordinate[label={[colorPrim]\angleIOM:$A\left(\frac{\pi}{3}\right)$}] (M) at (\angleIOM:\rayon);
        \coordinate[label={[colorSec]-\angleIOM:$A'\left(\frac{\pi}{3}\right)$}] (M') at (-\angleIOM:\rayon);
        \coordinate (J') at ($(O)!-1!(J)$);
        \coordinate (I') at ($(O)!-1!(I)$);
        \coordinate (S') at ($(O)!(M')!(J')$);
        \coordinate (S) at ($(O)!(M)!(J)$);
        \coordinate (C) at ($(O)!(M)!(I)$);
        \draw (I') -- (I);
        \draw (J') -- (J);
        \draw[thick,densely dotted,black!60] (M) -- (M');
        \draw[colorPrim] (O) -- (M);
        \draw[colorSec] (O) -- (M');
        \pic [angle mark,colorPrim,"$\frac{\pi}{3}$"] {angle = I--O--M};
        \pic [angle mark reverse,colorSec,"$-\frac{\pi}{3}$",angle eccentricity=1.9] {angle = M'--O--I};
        \fill (C) circle (1.5pt);
        \node[font=\scriptsize,inner sep=1pt,Gray] at (\rayon*2,\rayon*0.5) (cos) {$\cos x=\frac{1}{2}$};
        \draw[thin,Gray] (cos.west) -- (C);
      \end{tikzpicture}
    \end{center}
  \end{multicols}
L'équation $(E)$ admet donc deux solution (à $k \times 2\pi$ près) : $-\frac{\pi}{3}$ et $\frac{\pi}{3}$.

\end{methode}

\subsection{Propriétés des fonctions $\cos$ et $\sin$}

\begin{propriete}[La fonction $\cos$]\index{Trigonométrie!Cosinus (fonction)}
  La fonction $\cos$ est \textbf{paire} et \textbf{$2\pi$-périodique}
\end{propriete}

\paragraph*{\faCogs\quad Preuve}
\begin{itemize}
  \item  \textbf{paire} : pour tout $x\in\R$, on a $\cos(-x)= \cos(x)$.
  
 Sa courbe représentative est symétrique par rapport à l'axe des ordonnées.
 \item  \textbf{$2\pi$-périodique} : pour tout $k\in\Z$ et $x\in\R$, on a $\cos(x+2\pi\times k)=\cos(x)$.
 
  Sa courbe représentative est invariante par translation de vecteur $2\pi\,\vecti$.
\end{itemize}
\begin{propriete}[La fonction $\sin$]\index{Trigonométrie!Sinus (fonction)}
  La fonction $\sin$ est \textbf{impaire} et \textbf{$2\pi$-périodique}
\end{propriete}

\paragraph*{\faCogs\quad Preuve}
\begin{itemize}
  \item  \textbf{impaire} : pour tout $x\in\R$, on a $\sin(-x)= -\sin(x)$.
  
 Sa courbe représentative est symétrique par rapport à l'axe des ordonnées.
 \item  \textbf{$2\pi$-périodique} : pour tout $k\in\Z$ et $x\in\R$, on a $\sin(x+2\pi\times k)=\sin(x)$.
 
  Sa courbe représentative est invariante par translation de vecteur $2\pi\,\vecti$.
\end{itemize}

\subsection{Représentations graphiques}

\begin{center}
  \begin{tikzpicture}
    \begin{axis}[%
        axis y line=center,axis x line=middle,        % centrer les axes
        x=1cm,y=3cm,                                  % unités sur les axes
        xmin=-2.6*pi,xmax=2.6*pi,ymin=-1.2,ymax=1.2,        % bornes du repère
        xtick distance=pi/2, ytick distance=1,           % distance entre les tirets sur les axes
        minor y tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
        grid=both,                                    % indiquer quelle geilles afficher
        tick label style={font=\scriptsize,black!70}, % description des labels sur les axes
        xtick={-2.5*pi,-2*pi,...,2.5*pi},
        xticklabels={%
          ,$-2\pi$,$-\frac{3\pi}{2}$,$-\pi$,$-\frac{\pi}{2}$,0,$\frac{\pi}{2}$,$\pi$,$\frac{3\pi}{2}$,$2\pi$
        }
      ]
      \addplot[mark=none,colorSec,very thick,domain=-2.5*pi:2.5*pi,samples=150] {sin(deg(x))};
      \addplot[mark=none,colorPrim,very thick,domain=-2.5*pi:2.5*pi,samples=150] {cos(deg(x))};

      \node[colorSec,fill=white,font=\Large] at ({-2.5*pi},{sin(deg(-2.5*pi))}) {$\sin$};
      \node[colorPrim,fill=white,font=\Large] at ({-2.5*pi},{cos(deg(-2.5*pi))}) {$\cos$};
      \draw[black!70,latex'-latex'] ({pi/6},{cos(deg(pi/6))}) -- ({pi/6+2*pi},{cos(deg(pi/6+2*pi))}) node[midway,font=\large,fill=white] {$2\pi$};
      \draw[black!70,latex'-latex'] ({-2*pi-pi/6},{sin(deg(-2*pi-pi/6))}) -- ({-pi/6},{sin(deg(-pi/6))}) node[midway,font=\large,fill=white] {$2\pi$};
      \draw[black!70,-latex'] ({2},{cos(deg(2))}) -- ({2+pi/2},{sin(deg(2+pi/2))}) node[midway,fill=white] {$+\dfrac{\pi}{2}$};
      \coordinate (P) at ({pi/4},{sin(deg(pi/4))});
      \coordinate[label={[colorTer]below:$\frac{\pi}{4}$}] (Px) at ({pi/4},0);
      \coordinate[label={[colorTer]left:$\frac{\sqrt{2}}{2}$}] (Py) at (0,{sin(deg(pi/4))});
      \draw[densely dashed,colorTer] (Py)-| (Px);
    \end{axis}
  \end{tikzpicture}
\end{center}