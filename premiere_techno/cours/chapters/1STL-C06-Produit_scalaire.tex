% @Package tdsfrmath
\chapter{Produit scalaire}

\section{Définition}

\begin{definition}
  Soit $\vectu$ et $\vectv$ deux vecteurs du plan. Le \textbf{produit scalaire} de $\vectu$ et $\vectv$, noté $\vectu \cdot \vectv$, est défini par :
  \begin{itemize}
    \item si $\vectu$ et $\vectv$ sont non nuls, alors :
          \[
            \vectu \cdot \vectv = \norme{\vectu} \times \norme{\vectv} \times \cos\left(\vectu\,;\,\vectv\right)
          \]
    \item si $\vectu$ ou $\vectv$ est nul, alors :
          \[
            \vectu \cdot \vectv = 0
          \]
  \end{itemize}
\end{definition}

\begin{propriete}[Expression géométrique du produit scalaire]\index{Produit scalaire!Expression géométrique}
  Soient $O, A, B$ trois points du plan. Alors le produit scalaire $\overrightarrow{OA} \cdot \overrightarrow{OB}$ est égal à :
  \[
    \V{OA} \cdot \V{OB} = OA \times OB \times \cos(\widehat{AOB})
  \]
\end{propriete}

\begin{exemple}
  On donne le triangle suivant :
  \begin{multicols}{2}
    \begin{center}
      \begin{tikzpicture}[scale=0.8]
        \coordinate[label=below:$A$] (A) at (0, 0);
        \coordinate[label=below left:$D$] (D) at (-2, 0);
        \coordinate[label=below right:$B$] (B) at (6, 0);
        \coordinate[label=above:$C$] (C) at (30:{3*sqrt(3)});
        \draw[thick] (A) -- (B) node[midway,below,font=\scriptsize,black!50] {$6$}
        -- (C) node[midway,above right,font=\scriptsize,black!50] {$3$}
        -- (A) node[midway,above left,font=\scriptsize,black!50] {$3\sqrt{3}$};
        \draw[thick, -|] (A) -- (D) node[midway,below,font=\scriptsize,black!50] {$2$};
        \pic [draw,-latex',black!50,thick,angle radius=10mm,angle eccentricity=1.7,"$\frac{\pi}{6}$"] {angle = B--A--C};
        \pic [draw,-latex',black!50,thick,angle radius=4mm,angle eccentricity=2,"$\frac{5\pi}{6}$"] {angle = C--A--D};
        \pic [draw,latex'-,black!50,thick,angle radius=7mm,angle eccentricity=1.7,"$-\frac{\pi}{3}$"] {angle = C--B--A};
        \pic [draw,thick,angle radius=3mm]  {right angle = A--C--B};
      \end{tikzpicture}
    \end{center}
    On a :
    \begin{align*}
      \V{AB}\cdot\V{AC} & = AB \times AC \times \cos\left(\widehat{BAC}\right)       \\
                        & = 6 \times 3\sqrt{3} \times \cos\left(\frac{\pi}{6}\right) \\
                        & = 6 \times 3\sqrt{3} \times \frac{\sqrt{3}}{2}             \\
                        & = 27                                                       \\
    \end{align*}
  \end{multicols}
  \begin{align*}
    \V{CA}\cdot\V{CB} & = CA \times CB \times \cos\left(\widehat{BCA}\right)       & \V{BA}\cdot\V{BC} & = BA \times BC \times \cos\left(\widehat{ABC}\right) & \V{AC}\cdot\V{AD} & = AC \times AD \times \cos\left(\widehat{CAD}\right)    \\
                      & = 3\sqrt{3} \times 3 \times \cos\left(\frac{\pi}{2}\right) &                   & = 6 \times 3 \times \cos\left(-\frac{\pi}{3}\right)  &                   & = 3\sqrt{3}\times2\times\cos\left(\frac{5\pi}{6}\right) \\
                      & = 3\sqrt{3} \times 3 \times 0                              &                   & = 18 \times \frac{1}{2}                              &                   & = 6\sqrt{3} \times \left(-\frac{\sqrt{3}}{2}\right)     \\
                      & = 0                                                        &                   & = 9                                                  &                   & = -9                                                    \\
  \end{align*}
\end{exemple}

\section{Avec le projeté orthogonal}

\subsection{Projeté orthogonal}
\begin{definition}
  Soit un point $A$ et une droite $(d)$.


  \begin{multicols}{2}
    Le \textbf{projeté orthogonal} de $A$ sur la droite $(d)$ est le point $H$ tel que :
    \[
      (AH) \perp (d)
    \]
    \vspace*{\fill}

    \begin{center}
      \begin{tikzpicture}[scale=0.6,every label/.append style={font=\scriptsize}]
        \coordinate[label=above left:$A$] (A) at (2.5, 3);
        \node[cross=3pt,thick] at (A) {};
        \draw[thick,shorten >=-6mm,shorten <=-6mm] (0,0) coordinate (B) -- (6,-1.5) coordinate (C) node[above,font=\scriptsize] {$(d)$};
        \coordinate[label=below left:$H$] (H) at ($(B)!(A)!(C)$);
        \draw[thick,densely dashed,colorSec,shorten >=-6mm,shorten <=-6mm] (A) -- (H);
        \pic [draw,thick,angle radius=2mm]  {right angle = C--H--A};
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{definition}

\begin{remarque}
  On dit aussi que le point $H$ est le \textbf{pied} de la perpendiculaire à $(d)$ passant par $A$.
\end{remarque}

\subsection{Produit scalaire et projeté orthogonal}
\begin{propriete}\index{Produit scalaire!Projeté orthogonal}
  Soient $O, A, B$ trois points du plan et $H$ le projeté orthogonal de $B$ sur $(OA)$.

  Le produit scalaire $\V{OA}$ par $\V{OB}$ est égal à :
  \begin{itemize}
    \item $\V{OA} \cdot \V{OB} = OA \times OH$ si $\V{OA}$ et $\V{OH}$ ont le même sens ;
    \item $\V{OA} \cdot \V{OB} = - OA \times OH$ si $\V{OA}$ et $\V{OH}$ ont des sens opposés.
  \end{itemize}
\end{propriete}

\begin{exemple}
  Voici deux exemples illustrant les deux situations possibles :
  \begin{multicols}{2}
    \begin{center}
      \begin{tikzpicture}
        \coordinate[label=below left:$O$] (O) at (0, 0);
        \coordinate[label=below right:$A$] (A) at (4, 1);
        \coordinate[label=above:$B$] (B) at (2, 2.5);
        \coordinate[label=below:$H$] (H) at ($(O)!(B)!(A)$);
        \draw[-latex',colorPrim,very thick] (O) -- (A);
        \draw[-latex',colorSec,very thick] (O) -- (B);
        \draw[densely dashed] (B) -- (H);
        \pic [draw,thick,angle radius=2mm]  {right angle = O--H--B};
      \end{tikzpicture}

      $\V{OA}$ et $\V{OH}$ ont même sens.
      \[\V{OA} \cdot \V{OB} = OA \times OH\]
    \end{center}
    \begin{center}
      \begin{tikzpicture}
        \coordinate[label=below left:$O$] (O) at (0, 0);
        \coordinate[label=below right:$A$] (A) at (3, -0.3);
        \coordinate[label=above:$B$] (B) at (-2, 2.5);
        \coordinate[label=below:$H$] (H) at ($(O)!(B)!(A)$);
        \draw[-latex',colorPrim,very thick] (O) -- (A);
        \draw[-latex',colorSec,very thick] (O) -- (B);
        \draw[densely dashed] (B) -- (H);
        \draw[densely dashed,shorten >=-6mm] (O) -- (H);
        \pic [draw,thick,angle radius=2mm]  {right angle = O--H--B};
      \end{tikzpicture}

      $\V{OA}$ et $\V{OH}$ ont des sens contraires.
      \[\V{OA} \cdot \V{OB} = - OA \times OH\]
    \end{center}
  \end{multicols}
\end{exemple}
\newpage

\begin{methode}[Calculer un produit scalaire à l'aide du projeté orthogonal dans un quadrillage]
  \textbf{Énoncé}\quad On donne la figure ci-dessous :
  \begin{multicols}{2}

    \begin{center}
      \begin{tikzpicture}[background grid/.style={thick,draw=black!50,step=.5cm},%
          show background grid,%
          vecteur/.style={very thick,colorPrim,-latex'}]
        \draw[vecteur] (0,0) coordinate[label=above left:$A$] (A) -- (-2,0) coordinate[label=left:$B$] (B);
        \draw[vecteur] (A) -- (0.5,1.5) coordinate[label=above:$C$] (C);
        \draw[vecteur] (A) -- (-1,-1.5) coordinate[label=below:$E$] (E);
        \draw[vecteur] (A) -- (1.5,-1) coordinate[label=below right:$D$] (D);
      \end{tikzpicture}
    \end{center}
    Calculer les produits scalaires suivants :
    \begin{itemize}
      \item $\V{AB} \cdot \V{AE}$
      \item $\V{AB} \cdot \V{AC}$
      \item $\V{BA} \cdot \V{AD}$
    \end{itemize}
  \end{multicols}

  \textbf{Solution}\quad On utilise les projetés orthogonaux des points $C$, $D$ et $E$ sur $(AB)$
  et on considère que un carreau définit l'unité de longueur.

  \begin{multicols}{2}
    \begin{center}
      \begin{tikzpicture}[background grid/.style={thick,draw=black!30,step=.5cm},%
          show background grid,%
          vecteur/.style={very thick,colorPrim,-latex'},%
          every label/.append style={font=\scriptsize}]
        \draw[vecteur] (0,0) coordinate[label=above left:$A$] (A) -- (-2,0) coordinate[label=left:$B$] (B);
        \draw[vecteur] (A) -- (0.5,1.5) coordinate[label=above:$C$] (C);
        \draw[vecteur] (A) -- (-1,-1.5) coordinate[label=below:$E$] (E);
        \draw[vecteur] (A) -- (1.5,-1) coordinate[label=below right:$D$] (D);
        \coordinate[label=above:$H_E$] (HE) at ($(A)!(E)!(B)$);
        \coordinate[label=above:$H_D$] (HD) at ($(A)!(D)!(B)$);
        \coordinate[label=below right:$H_C$] (HC) at ($(A)!(C)!(B)$);
        \draw[thick,densely dashed] (A) -- (2,0);
        \foreach \pt in {C,D,E} {%
            \draw[thick,densely dashed] (\pt) -- (H\pt);
            \pic [draw,thick,angle radius=2mm]  {right angle = \pt--H\pt--A};
          }
      \end{tikzpicture}
    \end{center}
    $\V{AB}$ et $\V{AH_E}$ ont même sens.
    \begin{align*}
      \V{AB} \cdot \V{AE} & = AB \times AH_E \\
                          & = 4 \times 2     \\
                          & = 2              \\
    \end{align*}
    $\V{AB}$ et $\V{AH_C}$ ont des sens contraires.
    \begin{align*}
      \V{AB} \cdot \V{AC} & = - AB \times AH_C   \\
                          & = -4 \times 1      & \\
                          & = -4               &
    \end{align*}
    $\V{BA}$ et $\V{AH_D}$ ont même sens.
    \begin{align*}
      \V{BA} \cdot \V{AD} & = BA \times AH_D \\
                          & = 4 \times 3     \\
                          & = 12
    \end{align*}
  \end{multicols}
\end{methode}

\section{Dans un repère}

Dans cette partie, le plan est muni d'un repère orthonormé $\repere$.

\subsection{Expression analytique}


\begin{propriete}[Expression analytique du produit scalaire]\index{Produit scalaire!Expression analytique}
  Soient $\vectu\vcoord{x y}$ et $\vectv\vcoord{x' y'}$ deux vecteurs du plan. Alors le produit scalaire de ces deux vecteurs est donné par :
  \[
    \vectu \cdot \vectv = x \times x' + y \times y'
  \]
\end{propriete}

\begin{exemple}
  On donne les vecteurs $\vectu\vcoord{-2 3}$ et $\vectv\vcoord{5 4}$. On peut donc dire que :
  \begin{align*}
    \vectu \cdot \vectv & = -2 \times 5 + 3 \times 4 \\
                        & = -10 + 12                 \\
                        & = 2
  \end{align*}
\end{exemple}

\subsection{Propriétés algébriques}

Les propriétés suivantes sont des conséquences directes de l'expression analytique du produit scalaire.

\begin{propriete}\index{Produit scalaire!Propriétés algébriques}
  Soient des vecteurs $\vectu$, $\vectv$ et $\vecteur{w}$ et un réel $k$. Alors :
  \begin{enumerate}[label=\textcolor{colorSec}{\bfseries(\alph*)}]
    \item $\vectu \cdot \vectv = \vectv \cdot \vectu$
    \item $(\vectu + \vectv) \cdot \vecteur{w} = \vectu \cdot \vecteur{w} + \vectv \cdot \vecteur{w}$
    \item $k(\vectu \cdot \vectv) = (k\vectu) \cdot \vectv = \vectu \cdot (k\vectv)$
  \end{enumerate}
\end{propriete}

\section{Orthogonalité}

\begin{definition}\index{Vecteurs!Orthogonalité}
  Deux vecteurs $\vectu$ et $\vectv$ sont dits \textbf{orthogonaux}
  si leurs directions sont perpendiculaires (ou si un des deux vecteurs est le vecteur nul).
\end{definition}

\begin{propriete}
  Deux vecteurs $\vectu$ et $\vectv$ sont orthogonaux si et seulement si :
  \[
    \vectu \cdot \vectv = 0
  \]
\end{propriete}

\begin{methode}[Utiliser le produit scalaire pour prouver la perpendicularité de deux droites]
  \textbf{Énoncé}\quad On donne les points $A\nuplet{0 1}$, $B\nuplet{2,5 5}$ et $C\nuplet{3 -1}$ et $D\nuplet{-5 4}$.

  Prouver que les droites $(AB)$ et $(CD)$ sont perpendiculaires.
  \bigskip

  \textbf{Solution}\quad On a $\V{A}\vcoord{2,5-0 5-1}$ et $\V{CD}\vcoord{-5-3 4-(-1)}$.
  Donc $\V{AB}\vcoord{2,5 4}$ et $\V{CD}\vcoord{-8 5}$.

  Calculons leur produit scalaire :
  \begin{align*}
    \V{AB} \cdot \V{CD} & = x_{\V{AB}}x_{\V{CD}} + y_{\V{AB}}y_{\V{CD}} \\
                        & = 2,5\times(-8) + 4\times5                    \\
                        & = -20 + 20                                    \\
                        & = 0
  \end{align*}
  On en déduit que les vecteurs $\V{AB}$ et $\V{CD}$ sont orthogonaux, donc les droites $(AB)$ et $(CD)$ sont perpendiculaires.
\end{methode}

\section{Théorème d'Al-Kashi}

\begin{theoreme}[Al-Kashi]\index{Al-Kashi (théorème)}
  Soit $ABC$ un triangle.
  \begin{multicols}{2}
    On donne $a=BC$, $b=CA$ et $c=AB$. On a alors :
    \[
      a^2 = b^2 + c^2 - 2bc\cos(\widehat{A})
    \]

    mais aussi :
    \begin{align*}
      b^2 & = a^2 + c^2 - 2ac\cos(\widehat{B}) \\
      c^2 & = a^2 + b^2 - 2ab\cos(\widehat{C})
    \end{align*}
    \begin{center}
      \begin{tikzpicture}
        \coordinate[label=below left:$A$] (A) at (0, 0);
        \coordinate[label=below right:$B$] (B) at (6, -1);
        \coordinate[label=above:$C$] (C) at (4,2.5);
        \draw[very thick,colorPrim] (B) -- (C) node[midway,above right]{$a$};
        \draw[very thick,colorSec] (A) -- (C) node[midway,above left]{$b$};
        \draw[very thick,colorTer] (A) -- (B) node[midway,below left]{$c$};
        \pic [draw,fill=colorPrim,colorPrim,thick,angle radius=7mm,angle eccentricity=1.7,"$\widehat{A}$"] {angle = B--A--C};
        \pic [draw,fill=colorSec,colorSec,thick,angle radius=5mm,angle eccentricity=1.7,"$\widehat{B}$"] {angle = C--B--A};
        \pic [draw,fill=colorTer,colorTer,thick,angle radius=5mm,angle eccentricity=1.7,"$\widehat{C}$"] {angle = A--C--B};
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{theoreme}

\begin{methode}[Utilisation du théorème d'Al-Kashi pour déterminer une longueur]
  \textbf{Énoncé}\quad On donne le triangle $ABC$ avec $AB=5$, $AC=2$ et $\widehat{BAC}=\ang{60}$.

  Déterminer la longueur du côté $[BC]$.
  \bigskip

  \textbf{Solution}\quad On utilise le théorème d'Al-Kashi dans le triangle $ABC$
  pour déterminer la longueur du côté $[BC]$ :
  \begin{align*}
    BC^2 & = AB^2 + AC^2 - 2 \times AB \times AC \times \cos(\widehat{BAC}) \\
    BC^2 & = 5^2 + 2^2 - 2 \times 5 \times 2 \times \cos(60^\circ)          \\
    BC^2 & = 25 + 4 - 20 \times \frac{1}{2}                                 \\
    BC^2 & = 29 - 10                                                        \\
    BC^2 & = 19                                                             \\
    BC   & = \sqrt{19}
  \end{align*}
  On en déduit que $BC=\sqrt{19}$, soit $BC\approx\Lg{4,4}$.
\end{methode}

\begin{methode}[Utiliser le théorème d'Al-Kashi pour déterminer un angle]
  \textbf{Énoncé}\quad On donne le triangle $ABC$ avec $AB = 6$, $BC = 7$, et $CA = 8$
  (l'unité étant le centimètre).

  Déterminer la valeur de l'angle $\widehat{ABC}$.
  \bigskip


  \begin{multicols}{2}
    \textbf{Solution}\quad On souhaite déterminer la valeur de l'angle $\widehat{ABC}$
    dans le triangle $ABC$ donc on va utiliser le théorème d'Al-Kashi avec le côté \textbf{opposé}
    à cet angle. Dans notre cas, c'est le côté $AC$. On a alors :

    {\small\begin{align*}
      AC^2 & = AB^2 + BC^2 - 2 \times AB \times BC \times \cos(\widehat{ABC}) \\
      8^2  & = 6^2 + 7^2 - 2 \times 6 \times 7 \times \cos(\widehat{ABC})     \\
      64   & = 36 + 49 - 84 \times \cos(\widehat{ABC})                        \\
      64   & = 85 - 84 \times \cos(\widehat{ABC})                             \\
      64   & = 85 - 84 \times \cos(\widehat{ABC})                             \\
    \end{align*}}
    On peut donc dire que :
    {\small\begin{align*}
      84 \times \cos(\widehat{ABC}) & = 21                              \\
      \cos(\widehat{ABC})           & = \frac{21}{84}                   \\
      \cos(\widehat{ABC})           & = \frac{1}{4}                     \\
      \widehat{ABC}                 & = \arccos\left(\frac{1}{4}\right) \\
      \widehat{ABC}                 & \approx \ang{75,5}
    \end{align*}}
  \end{multicols}

\end{methode}