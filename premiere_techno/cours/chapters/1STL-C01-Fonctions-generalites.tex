% \usetikzlibrary{patterns,patterns.meta}, \usepackage{tdsfrmath}
\chapter{Fonctions -- Généralités}

\section{Notion de fonction}

\begin{definition}\index{Fonctions!Définition}
  On appelle \textbf{fonction} tout procédé de calcul (machine à calculer) qui, à tout nombre $x$ associe un nombre \emph{unique} noté $f(x)$. On notera \[f:x\longmapsto f(x)\]
\end{definition}


\begin{exemple}\label{pstl:fctgen:exemples:1}
  La fonction $g:t\longmapsto t^2+1$ est un procédé qui associe à tout nombre $t$ son carré augmenté de $1$.
\end{exemple}

\begin{definition}\index{Fonctions!Image}\index{Image}
  Le nombre $f(x)$ qui est associé à $x$ par la fonction $f$ est appelé l'\textbf{image} de $x$ par $f$.
\end{definition}


\begin{multicols}{2}
  \begin{exemple}\label{pstl:fctgen:exemples:2}
    Utilisons la fonction $g$ de l'exemple \ref{pstl:fctgen:exemples:1} ci-dessus.

    L'image de $-2$ par $g$ est $5$ car
    \begin{align*}
      g(-2) & = (-2)^2+1 \\
            & = 4+1      \\
            & = 5        \\
    \end{align*}
  \end{exemple}

  \begin{remarque}
    Il est important de noter que, pour un nombre donné, le procédé fonction $f$ définit  un nombre \textbf{unique}.

    Dans l'exemple \ref{pstl:fctgen:exemples:2}, on comprend bien que le calcul qui détermine l'image de $-2$ par $g$ ne peut pas donner deux résultats différents.
  \end{remarque}
\end{multicols}

\begin{definition}\index{Fonctions!Antécédents}\index{Antécédents}
  Soit un nombre $a$ et une fonction $f$.

  Toutes les valeurs de $x$ qui vérifient $f(x)=a$ sont appelées les \textbf{antécédents} de $a$ par $f$.
\end{definition}


\begin{exemple}\label{pstl:fctgen:exemples:3}
  Reprenons la fonction $g$ de l'exemple \ref{pstl:fctgen:exemples:1} ci-dessus et cherchons les antécédents de $5$.

  On sait déjà depuis l'exemple \ref{pstl:fctgen:exemples:2} que $-2$ est un antécédent de $5$ par $g$ puisque $g(-2)=5$.

  Cependant, on a aussi $g(2)=2^2+1$ donc $g(2)=5$ et $5$ admet aussi $2$ comme antécédent.\smallskip

  $5$ admet donc au moins deux antécédents par $g$ qui sont $-2$ et $2$.
\end{exemple}

\begin{remarque}
  L'exemple \ref{pstl:fctgen:exemples:3} ci-dessus nous permet de constater qu'à la différence de l'image, un antécédent n'est pas unique~! On dira d'ailleurs \og{} \textit{\textbf{l}'image d'un nombre $x$} \fg{} mais \og{} \textit{\textbf{un} antécédent du nombre $y$} \fg{}.
\end{remarque}

\section{Ensemble de définition}

\begin{definition}
  Soit une fonction $f$. On appelle \textbf{ensemble de définition} de la fonction $f$ l'ensemble de
  tous les nombres pour lesquels $x$ admet une image par $f$, c'est-à-dire pour lesquels $f(x)$ existe.
\end{definition}

L'ensemble de définition d'une fonction peut se comprendre comme l'ensemble des nombres pour lesquels la fonction $f$ "marche" (c'est-à-dire pour lesquels on peut faire le calcul).

\begin{remarque}
  L'ensemble de définition peut-être non seulement  donné par des contingences purement calculatoires (par exemple,
  on ne peut pas calculer $\frac{1}{x}$ lorsque $x$ est nul) mais aussi par le contexte d'utilisation de
  la fonction (par exemple, si on travaille avec des longueurs en géométrie, la fonction peut n'accepter que des
  nombres positifs).
\end{remarque}

\begin{exemple}   
  Si $f:x\longmapsto \frac{1}{x}$, on ne peut pas calculer $f(x)$ lorsque $x$ est nul (l'inverse n'existe pas dans ce cas).

  L'ensemble de définition de $f$ dans ce cas sera donc \textit{l'ensemble des nombres positifs ou nul, noté $\R[*]$}.
\end{exemple}

\section{Représentation graphique}
\subsection{Généralités}
\begin{definition}
  Si $f$ est une fonction définie sur son ensemble de définition $\mathcal{D}$ et si le plan est muni d'un repère, alors \textbf{la courbe représentative} de $f$ est l'ensemble des points $M\left(x\,;\,f(x)\right)$ où $x\in\mathcal{D}$.

  La relation $y=f(x)$ est appelée \textbf{équation de la courbe représentative de $f$}.
\end{definition}

\begin{remarque}
  Un point $M\left(x_M\,;\,y_M\right)$ appartient à la courbe représentative d'une fonction $f$ si et seulement si $y_M=f(x_M)$.
\end{remarque}

\begin{exemple}
  Voici la représentation graphique d'une fonction $f$.
  \begin{multicols}{2}
    \begin{center}\tikzset{every node/.append style={font=\scriptsize}}
      \begin{tikzpicture}
        \begin{axis}[axis y line=center,
            axis x line=middle,x=1cm,y=1cm,xmin=-2.2,xmax=5.2,ymin=-1.2,ymax=4.2,
            ,minor x tick num=1,minor y tick num=1,grid=both,
            xticklabels=\empy, yticklabels=\empty]
          \addplot[domain=0.5:3,mark=none,colorSec,thick,samples=150] {3*ln(x)+1};
          \node at (axis cs:3.3,4) {\color{colorSec}$\mathcal{C}_f$};
          \coordinate (M) at (2.1,3.226);
          \node[right] at (M) {$M\left(x_M\,;\,f(x_M)\right)$};
          \node[cross=2pt,rotate=90] at (M) {};
          \coordinate[label=below left:$O$] (O) at (0,0);
          \draw[color=colorPrim,dashed] (2.1,0) node[below]{$x_M$} |- (0,3.226) node[left]{$y_M=f(x_M)$};
          % \draw[thick, -latex'] (axis cs:0,0) node[below left] {$O$}
          %      -- (axis cs:1,0) node[midway,below] {\footnotesize$\vect{i}$};
          % \draw[thick, -latex'] (axis cs:0,0) -- (axis cs:0,1) node[midway,left] {\footnotesize$\vect{j}$};
        \end{axis}
      \end{tikzpicture}
    \end{center}

    Si on note $\mathcal{C}_f$ la représentation graphique de la fonction~$f$, $x_M$ et $y_M$ l'abscisse et l'ordonnée du point $M$ respectivement,
    alors, on peut alors dire~:
    \begin{itemize}
      \item si $M\in\mathcal{C}_f$ alors $y_M=f(x_M)$.
      \item réciproquement, si $y_M=f(x_M)$ alors $M\in\mathcal{C}_f$.
    \end{itemize}

    Ce qu'on peut résumer par \[M\left(x_M\,;\,y_M\right)\in\mathcal{C}_f \text{ est équivalent à } y_M=f(x_M)\]
  \end{multicols}
\end{exemple}

\subsection{Lectures graphiques}
\subsubsection{Lecture graphique d'une image}
\begin{multicols}{2}
  Soit $\mathcal{C}_f$ la représentation graphique d'une fonction $f$.
  L'image du nombre $x_0$ par la fonction $f$ est l'\textit{ordonnée} $y_0$ du point $M$ de $\mathcal{C}_f$ qui a pour abscisse $x_0$.
  On trace alors le chemin de lecture en pointillés (voir la figure ci-contre).

  \vfill

  % \columnbreak

  \begin{center}
    \begin{tikzpicture}[scale=0.8]
      \begin{axis}[axis y line=center,%
          axis x line=middle,x=1cm,y=1cm,%
          xmin=-2.2,xmax=3.2,ymin=-0.7,ymax=2.6,%
          minor x tick num=1,minor y tick num=1,grid=both,%
          xticklabels=\empy, yticklabels=\empty]
        \addplot[domain=-2.2:3.2,mark=none,colorSec,thick,samples=150] {sqrt(x+2)};
        \node[colorSec] at (-1.5,1.3) {$\mathcal{C}_f$};
        \coordinate[label=below left:$O$] (O) at (0,0);
        \coordinate[label=below right:$M$] (M) at(1.5,3.742/2);
        \draw[-triangle 45,color=colorPrim,dashed,thick] (1.5,0)  node[below]{$x_0$} -- (1.5,1.871/2);
        \draw[-triangle 45,color=colorPrim,dashed,thick] (1.5,1.871/2) |- (0.75,3.742/2);
        \draw[color=colorPrim,dashed] (0.75,3.742/2) -- (0,3.742/2) node[left]{$y_0$};
      \end{axis}
    \end{tikzpicture}
  \end{center}
\end{multicols}

\subsubsection{Lecture graphique d'antécédent(s)}
\begin{multicols}{2}
  \begin{center}
    \begin{tikzpicture}[scale=0.8]
      \begin{axis}[axis y line=center,%
          axis x line=middle,x=1cm,y=1cm,%
          xmin=-1.8,xmax=3.8,ymin=-1.3,ymax=2.4,%
          minor x tick num=1,minor y tick num=1,grid=both,%
          xticklabels=\empy, yticklabels=\empty]
        \addplot[domain=-1.3:3.3,mark=none,colorSec,thick,samples=150] {0.5*((x-1)^2-1)};
        \node[colorSec] at (1,-0.85) {$\mathcal{C}_f$};
        \coordinate[label=below left:$O$] (O) at (0,0);
        \coordinate[label=left:$M_1$] (M1) at (-1,1.5);
        \coordinate[label=right:$M_2$] (M2) at (3,1.5);
        \draw[-triangle 45 reversed,color=colorPrim,thick,dashed] (3,0)  node[below]{$x_2$} -- (3,0.75);
        \draw[-triangle 45 reversed,color=colorPrim,thick,dashed] (3,1.5) |- (1.5,1.5);
        \draw[color=colorPrim,thick,dashed] (1.5,1.5) -- (0,1.5) node[above right]{$y_0$};
        \draw[-triangle 45 reversed,color=colorPrim,thick,dashed] (-1,0)  node[below]{$x_1$} -- (-1,0.75);
        \draw[-triangle 45 reversed,color=colorPrim,thick,dashed] (-1,1.5) |- (-0.5,1.5);
        \draw[color=colorPrim,thick,dashed] (-0.5,1.5) -- (0,1.5);
      \end{axis}
    \end{tikzpicture}
  \end{center}

  Soit $\mathcal{C}_f$ la représentation graphique d'une fonction $f$.
  Les antécédents du nombre $y_0$ par la fonction $f$ sont les \textit{abscisses} des points $M$ de $\mathcal{C}_f$ qui ont pour ordonnée $y_0$.\linebreak On trace alors le(s) chemin(s) de lecture en pointillés (voir la figure ci-contre où $y_0$ admet deux antécédents $x_1$ et $x_2$ par la fonction $f$).

\end{multicols}

\subsubsection{Résolution graphique d'équations}

Résoudre graphiquement une équation du type $f(x)=k$, cela revient à lire les abscisses des points d'intersection de la courbe représentative de $f$ (sur son domaine de définition) et de la droite d'équation $y=k$.

\begin{methode}[Résoudre graphiquement une équation]
  \textbf{Énoncé}\quad Soit la fonction $f$ définie sur $\R$ par $f\,:\,x\longmapsto \frac{1}{3}\left(x^2+2x-2\right)$ et $\mathcal{C}_f$ sa représentation graphique.

  Résoudre graphiquement l'équation $f(x)=2$ .\bigskip

  \textbf{Solution} \quad On considère la représentation graphique $\mathcal{C}_f$ de $f$. On trace dans le même repère la droite $(d)$ d'équation $y=2$.
  On constate que $\mathcal{C}_f$ admet deux points d'intersection $A\left(-4\,;\,2\right)$ et $B\left(2\,;\,2\right)$ avec la droite $(d)$.
  \begin{center}
    \begin{tikzpicture}
      \begin{axis}[axis y line=center,%
          axis x line=middle,x=1cm,y=1cm,%
          xmin=-4.5,xmax=2.5,ymin=-1.5,ymax=2.5,%
          minor x tick num=1,minor y tick num=1,grid=both,%
          xticklabels=\empy, yticklabels=\empty]
        \addplot[domain=-4.5:3.3,mark=none,colorSec,ultra thick,samples=150] {((x-2)*(x+4)+6)/3};
        \node[colorSec] at (1,-0.85) {$\mathcal{C}_f$};
        \addplot[domain=-4.5:3.3,mark=none,colorTer,ultra thick,samples=150] {2};
        \node[colorTer] at (-2.5,1.5) {$(d)$};
        \coordinate[label=below left:$O$] (O) at (0,0);
        \coordinate[label=above right:$A$] (A) at (-4,2);
        \coordinate[label=above left:$B$] (B) at (2,2);
        \coordinate[label=below:$1$] (I) at (1, 0);
        \coordinate[label=left:$1$] (J) at (0, 1);
        \draw[-triangle 45,color=colorPrim,thick,dashed] (A) -- (-4,0) node[below] {$-4$};
        \draw[-triangle 45,color=colorPrim,thick,dashed] (B) -- (2,0) node[below] {$2$};
        \node[cross=3pt] at (A) {};
        \node[cross=3pt] at (B) {};
      \end{axis}
    \end{tikzpicture}
  \end{center}
  Les abscisses des points $A$ et $B$ sont les solutions de l'équation $f(x)=2$.

  En notant $\mathcal{S}$ l'ensemble des solutions de cette équation, on a \[\mathcal{S}=\{-4 \, ; \, 2\}\]
\end{methode}


\subsubsection{Résolution graphique d'inéquations}

Résoudre graphiquement une inéquation du type $f(x)<k$ (respectivement $f(x)>k$), cela revient à lire les abscisses des points situés \textbf{au dessous}
(respectivement \textbf{au dessus}) de la courbe représentative de $f$ (sur son domaine de définition).

\begin{methode}[Résoudre graphiquement une inéquation]
  
  \begin{multicols}{2}
    \textbf{Énoncé}\quad Soit la fonction $g$ définie sur $\R$ dont on donne la courbe représentative $\mathcal{C}_g$ ci-dessous.
  
    \begin{center}
      \begin{tikzpicture}[scale=0.8]
        \begin{axis}[axis y line=center,%
            axis x line=middle,x=1cm,y=1cm,%
            xmin=-4.5,xmax=3.5,ymin=-8,ymax=1.5,%
            minor x tick num=1,minor y tick num=1,grid=both,%
            xticklabels=\empy, yticklabels=\empty]
          \addplot[domain=-4.5:3.3,mark=none,colorSec,ultra thick,samples=150] {((x-3)*(x+2)*(x+3)-4)/4};
          \node[colorSec] at (1.2,-7.5) {$\mathcal{C}_f$};
          \coordinate[label=below left:$O$] (O) at (0,0);
          \coordinate[label=below:$1$] (I) at (1, 0);
          \coordinate[label=left:$1$] (J) at (0, 1);
        \end{axis}
      \end{tikzpicture}
    \end{center}
  
    Résoudre graphiquement l'équation $g(x)< -1$ .\bigskip

    \columnbreak
  
    \textbf{Solution} \quad On considère la représentation graphique $\mathcal{C}_g$ de $g$. On trace dans le même repère la droite $(d)$ d'équation $y=-1$.
    On constate que $\mathcal{C}_f$ admet trois points d'intersection $A\left(-3\,;\,-1\right)$ et $B\left(-2\,;\,-1\right)$ et $C\left(2\,;\,-1\right)$ avec la droite $(\Delta)$.
    \begin{center}
      \begin{tikzpicture}[scale=0.8]
        \begin{axis}[axis y line=center,%
            axis x line=middle,x=1cm,y=1cm,%
            xmin=-4.5,xmax=3.5,ymin=-8,ymax=1.5,%
            minor x tick num=1,minor y tick num=1,grid=both,%
            xticklabels=\empy, yticklabels=\empty]
          \addplot[domain=-4.5:3.3,mark=none,colorSec,ultra thick,samples=150] {((x-3)*(x+2)*(x+3)-4)/4};
          \addplot[domain=-4.5:-3,mark=none,colorSec,line width=8pt,opacity=0.2,samples=150] {((x-3)*(x+2)*(x+3)-4)/4};
          \addplot[domain=-2:3,mark=none,colorSec,line width=8pt,opacity=0.2,samples=150] {((x-3)*(x+2)*(x+3)-4)/4};
          \node[colorSec] at (1.2,-7.5) {$\mathcal{C}_f$};
          \addplot[domain=-4.5:3.3,mark=none,colorTer,ultra thick,samples=150] {-1};
          \node[colorTer] at (2,-1.5) {$(d)$};
          \coordinate[label=below left:$O$] (O) at (0,0);
          \coordinate[label=below:$A$] (A) at (-3,-1);
          \coordinate[label=below:$B$] (B) at (-2,-1);
          \coordinate[label=below:$C$] (C) at (3, -1);
          \coordinate[label=below:$1$] (I) at (1, 0);
          \coordinate[label=left:$1$] (J) at (0, 1);
          \draw[-triangle 45,color=colorPrim,thick,dashed] (A) -- (-3,0) coordinate (Ap) node[above] {$-3$};
          \draw[-triangle 45,color=colorPrim,thick,dashed] (B) -- (-2,0) coordinate (Bp) node[above] {$-2$};
          \draw[-triangle 45,color=colorPrim,thick,dashed] (C) -- (3,0) coordinate (Cp) node[above] {$3$};
          \fill[pattern={Lines[angle=45,yshift=-.5pt]},pattern color=colorPrim] ([yshift=1.25mm]Bp) rectangle ([yshift=-1.25mm]Cp);
          \fill[pattern={Lines[angle=45,yshift=-.5pt]},pattern color=colorPrim] ([yshift=1.25mm]-4.5,0) rectangle ([yshift=-1.25mm]Ap);
        \end{axis}
      \end{tikzpicture}
    \end{center}
    Toutes les abscisses des points de $\mathcal{C}_g$ situés \textit{au dessous} de la droite $(\Delta)$ (c'est-à-dire les points de la courbe $\mathcal{C}_g$ 
    situés avant la point $A$ et ceux situés entre $B$ et $C$) correspondent aux solutions de l'équation $g(x)< -1$.\medskip
  
    En notant $\mathcal{S}$ l'ensemble des solutions de l'inéquation $g(x)< -1$, on a \[\mathcal{S}=\interoo{\moinsinf{} -3}\cup\interoo{-2 3}\]
  \end{multicols}
\end{methode}
\clearpage
\section{Variations d'une fonction}

\subsection{Taux de variation}

\begin{definition}\index{Fonctions!Taux de variation}
  Soit $f$ une fonction définie sur $\mathcal{D}_f$ et $a, b \in \mathcal{D}_f$.


  On appelle \textbf{taux de variation} de la fonction $f$ entre $a$ et $b$ le nombre suivant~:
  \[\frac{f(a) - f(b)}{a-b}\]
\end{definition}

\begin{methode}[Calculer un taux de variation]
  \textbf{Énoncé}\quad Soit $f$ une fonction définie sur $\R$ par $f\,:\,x\longmapsto 3x²-2x+1$.

  Calculer le taux de variation de $f$ entre $1$ et $4$.\bigskip

  \textbf{Solution}\quad On commence par calculer $f(1)$ et $f(4)$, puis le taux de variation entre $1$ et $4$.
    {\small\begin{align*}
        f(1) & = 3\times1^2-2\times1+1 & f(4) & = 3\times4^2-2\times4+1 & \frac{f(4)-f(1)}{4-1} & = \frac{41-2}{4-1} \\
             & = 3-2+1                 &      & = 3\times16-2\times4+1  &                       & = \frac{39}{3}     \\
             & = 2                     &      & = 48-8+1                &                       & = 13               \\
             &                         &      & = 41                    &                       &                    \\
      \end{align*}}
\end{methode}

\subsection{Sens de variation}

\begin{definition}\index{Fonctions!Croissante}
  Soit $f$ une fonction et $\mathtt{I}$ un intervalle contenu dans son ensemble de définition.\smallskip

  La fonction est dite \textbf{croissante} sur $\mathtt{I}$, si lorsque $x$ augmente alors $f(x)$ \textbf{augmente}.
  \[\text{Pour tout } x_1, x_2 \in \mathtt{I} \text{ tel que } x_1<x_2 \text{ alors } f(x_1)<f(x_2) \]
\end{definition}

\begin{exemple}\label{pstl:fctgen:exemple:croissant}
  Voici un exemple de fonction croissante.
  \begin{multicols}{2}
    \begin{center}
      \begin{tikzpicture}[every node/.append style={font=\scriptsize}]
        \begin{axis}[axis y line=center,%
            axis x line=middle,x=1cm,y=1cm,%
            xmin=-0.8,xmax=5.8,ymin=-0.7,ymax=2.5,%
            minor x tick num=1,minor y tick num=1,grid=both,%
            xticklabels=\empy, yticklabels=\empty]
          \addplot[domain=0.9:5.3,mark=none,DarkGreen,thick,samples=350] {sqrt(x-1)};
          \node[DarkGreen] at (3.2,1.2) {$\mathcal{C}_f$};
          \coordinate[label=below left:$O$] (O) at (0,0);
          \coordinate[label=above:$M_1$] (M1) at (2,1);
          \coordinate[label=above:$M_2$] (M2) at (5,2);
          \draw[-triangle 45,color=colorPrim,thick,dashed] (2,0)  node[below]{$x_1$} -- (M1);
          \draw[-triangle 45,color=colorPrim,thick,dashed] (M1) -- (0,1) node[left] {$f(x_1)$};
          \draw[-triangle 45,color=colorSec,thick,dashed] (5,0)  node[below]{$x_2$} -- (M2);
          \draw[-triangle 45,color=colorSec,thick,dashed] (M2) -- (0,2) node[left] {$f(x_2)$};
        \end{axis}
      \end{tikzpicture}
    \end{center}

    Sur la figure ci-contre, on voit que~:
    \[x_1<x_2 \Longrightarrow f(x_1)<f(x_2)\] Les images sont rangées dans le même ordre et la fonction est donc \textit{croissante}.
  \end{multicols}
\end{exemple}

\begin{definition}\index{Fonctions!Décroissante}
  Soit $f$ une fonction et $\mathtt{I}$ un intervalle contenu dans son ensemble de définition.\smallskip

  La fonction est dite \textbf{décroissante} sur $\mathtt{I}$, si lorsque $x$ augmente alors $f(x)$ \textbf{diminue}.
  \[\text{Pour tout } x_1, x_2 \in \mathtt{I} \text{ tel que } x_1<x_2 \text{ alors } f(x_1)>f(x_2) \]
\end{definition}

\begin{exemple}
  Voici un exemple de fonction décroissante.
  \begin{multicols}{2}
    \begin{center}
      \begin{tikzpicture}[every node/.append style={font=\scriptsize}]
        \begin{axis}[axis y line=center,%
            axis x line=middle,x=1cm,y=1cm,%
            xmin=-0.8,xmax=5.8,ymin=-0.7,ymax=2.5,%
            minor x tick num=1,minor y tick num=1,grid=both,%
            xticklabels=\empy, yticklabels=\empty]
          \addplot[domain=0:5,mark=none,DarkGreen,thick,samples=350] {0.1*(x-5)^2};
          \node[DarkGreen] at (5,0.3) {$\mathcal{C}_f$};
          \coordinate[label=below left:$O$] (O) at (0,0);
          \coordinate[label=above:$M_1$] (M1) at (1,1.6);
          \coordinate[label=above:$M_2$] (M2) at (3,0.4);
          \draw[-triangle 45,color=colorPrim,thick,dashed] (1,0)  node[below]{$x_1$} -- (M1);
          \draw[-triangle 45,color=colorPrim,thick,dashed] (M1) -- (0,1.6) node[left] {$f(x_1)$};
          \draw[-triangle 45,color=colorSec,thick,dashed] (3,0)  node[below]{$x_2$} -- (M2);
          \draw[-triangle 45,color=colorSec,thick,dashed] (M2) -- (0,0.4) node[left] {$f(x_2)$};
        \end{axis}
      \end{tikzpicture}
    \end{center}

    Sur la figure ci-contre, à l'inverse de l'exemple \ref{pstl:fctgen:exemple:croissant} ci-dessus, on voit que si on a
    \[x_1<x_2 \Longrightarrow f(x_1)>f(x_2)\] Les images sont rangées dans l'ordre \textit{inverse} et la fonction est donc \textit{décroissante}.
  \end{multicols}
\end{exemple}

\begin{definition}\index{Fonctions!Constante}
  Si l'image $f(x)$ ne varie pas sur un intervalle $\mathtt{I}$, alors on dit que la fonction $f$ est \textbf{constante} sur $\mathtt{I}$.
\end{definition}

\begin{exemple}
  La fonction $h:x \longmapsto 7$ est une fonction \textit{constante} car pour tout $x \in \R$, on a toujours $h(x)=7$.
\end{exemple}

\begin{definition}\index{Fonctions!Monotone}
  Soit $f$ une fonction et $\mathtt{I}$ un intervalle contenu dans son ensemble de définition.

  La fonction $f$ est dite \textbf{monotone} sur $\mathtt{I}$ dans les trois cas suivants~:
  \begin{multicols}{3}
    $f$ est croissante sur $\mathtt{I}$

    $f$ est décroissante sur $\mathtt{I}$

    $f$ est constante sur $\mathtt{I}$
  \end{multicols}
\end{definition}

\begin{propriete}\label{pstl:fctgen:prop:monotonie}
  Soit  $f$ une fonction définie sur un intervalle $\mathtt{I}$.
  \begin{itemize}
    \item Si le taux de variation de $f$  entre deux nombres quelconques de $\mathtt{I}$ est \textit{strictement positif} alors la fonction $f$ est \textit{croissante} ;
    \item Si le taux de variation de $f$  entre deux nombres quelconques de $\mathtt{I}$ est \textit{strictement négatif} alors la fonction $f$ est \textit{décroissante} ;
    \item Si le taux de variation de $f$  entre deux nombres quelconques de $\mathtt{I}$ est \textit{nul} alors la fonction $f$ est \textit{constante}.
  \end{itemize}
\end{propriete}

\begin{methode}[Prouver qu'une fonction est monotone]
  \textbf{Énoncé}\quad Soit $f:x\longmapsto 3 - 2x$. Prouver que $f$ est monotone sur $\R$.\bigskip

  \textbf{Solution}\quad Soit deux nombres quelconques $a, b \in \R$. Calculons $T$ le taux de variation de $f$ entre $a$ et $b$.
  \begin{align*}
    T & = \frac{f(a)-f(b)}{a-b}   & T & = \frac{-2a+2b}{a-b}  \\
    T & = \frac{3-2a-(3-2b)}{a-b} & T & = \frac{-2(a-b)}{a-b} \\
    T & = \frac{3-2a-3+2b}{a-b}   & T & = -2
  \end{align*}
  Le taux de variations de la fonction $f$ est donc négatif sur $\R$. D'après la propriété \ref{pstl:fctgen:prop:monotonie}, on en déduit que $f$ est décroissante sur $\R$, donc monotone.
\end{methode}