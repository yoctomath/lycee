\chapter{Polynôme de degré 2}

\section{La fonction carrée (rappels de Seconde)}

\subsection{Définition et représentation graphique}


\begin{multicols}{2}
  \begin{definition}
    La \textbf{fonction carrée} est la fonction définie sur \R par
    \[x \longmapsto x^2\]
    Sa courbe représentative est une \textbf{parabole}.
  \end{definition}

  \begin{propriete}
    La fonction carrée est une fonction \textbf{paire}.

    Sa courbe représentative admet l'axe des ordonnées comme axe de symétrie.
  \end{propriete}

  \begin{center}
    \begin{tikzpicture}
      \begin{axis}[%
          axis y line=center,axis x line=middle,        % centrer les axes
          x=1cm,y=1cm,                                  % unités sur les axes
          xmin=-3.2,xmax=3.2,ymin=-0.4,ymax=5.9,        % bornes du repère
          xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
          minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
          grid=both,                                    % indiquer quelle geilles afficher
          tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
        ]
        \addplot[mark=none,colorSec,very thick,domain=-3:3,samples=150] {x^2};
      \end{axis}
    \end{tikzpicture}
  \end{center}
\end{multicols}

\begin{methode}[Résoudre des équations ou inéquations contenant la fonction carrée]
  \textbf{Énoncé}\quad Résoudre l'équation et l'inéquation suivante :
  \[x^2 = 5    \kern2cmx^2 \pgq 5\]
  % \medskip

  \textbf{Solution}\quad
  \begin{enumerate}[label = \textcolor{colorTer}{\textbf{(\arabic*)} }]
    \item On transforme l'équation en équation produit nul.

            {\setlength{\columnsep}{1.2cm}
              \setlength{\columnseprule}{0.25pt}
              \begin{multicols}{2}
                On factorise l'expression grâce
                à une identité remarquable :
                \begin{align*}
                  x^2                                              & = 5 \\
                  x^2    -5                                        & = 0 \\
                  x^2 - \left(\sqrt{5}\right)^2                    & = 0 \\
                  \left(x - \sqrt{5}\right)\left(x+\sqrt{5}\right) & = 0
                \end{align*}
                Chaque facteur peut s'annuler donc
                \begin{align*}
                  x - \sqrt{5} & = 0          & x+\sqrt{5} & = 0        \\
                  x            & = - \sqrt{5} & x          & = \sqrt{5} \\
                \end{align*}
                L'équation admet deux solutions : \[\mathcal{S}=\left\{ -\sqrt{5}\,;\,\sqrt{5}\right\}\]
              \end{multicols}}
          \medskip

          \faChartLine\quad \textbf{Graphiquement}, on regarde les abscisses des points d'intersection
          de la courbe $\mathcal{C}_f$ avec la droite d'équation $y=5$.
    \item On utilise la forme factorisée ci-dessus et on dresse un tableau de signes.
          \begin{align*}
            x^2                                              & \pgq 5 \\
            x^2 - \left(\sqrt{5}\right)^2                    & \pgq 0 \\
            \left(x - \sqrt{5}\right)\left(x+\sqrt{5}\right) & \pgq 0 \\
          \end{align*}

          \begin{center}\small
            \begin{tikzpicture}
              \tkzTabInit[lgt=4,espcl=3,deltacl=0.75]{%
                $x$ / 0.7,%
                Signe de $x-\sqrt{5}$ / 0.7,%
                Signe de $x+\sqrt{5}$ / 0.7,%
                Signe de $\left(x - \sqrt{5}\right)\left(x+\sqrt{5}\right)$ / 1.5}%
              {$-\infty$, $-\sqrt{5}$, $\sqrt{5}$, $+\infty$}
              \tkzTabLine{ , - , z , + , t , + ,}
              \tkzTabLine{ , - , t , - , z , + ,}
              \tkzTabLine{ , + , z , - , z , + ,}
            \end{tikzpicture}
          \end{center}
          On cherche quand est-ce que le produit est positif ou nul.

          On a donc $\mathcal{S}=\interof{-\infty{} -\sqrt{5}} \cup \interfo{\sqrt{5} +\infty}$.
          \medskip

          \faChartLine\quad \textbf{Graphiquement}, on regarde les abscisses des points
          de la courbe $\mathcal{C}_f$ situés \textit{au-dessus} la droite d'équation $y=5$.
  \end{enumerate}
\end{methode}

\subsection{Variations}

\begin{propriete}
  La fonction carrée est
  \begin{itemize}
    \item \textbf{décroissante} sur \interof{-\infty{} 0} ;
    \item \textbf{croissante} sur \interfo{0 +\infty}.
  \end{itemize}

  \begin{center}
    \begin{tikzpicture}
      \tkzTabInit[lgt=2,espcl=3,deltacl=0.75]{$x$ / 1, Variations de $f$ / 1.5}{$-\infty$, $0$, $+\infty$}
      \tkzTabVar{+/, -/$0$, +/}
    \end{tikzpicture}
  \end{center}
\end{propriete}

\section{Les fonctions $x\longmapsto ax^2$}

\begin{propriete}
  Soit $a$ un nombre réel non nul et $f$ la fonction définie sur \R telle que \[f(x)=ax^2\]
  On peut affirmer :
  \begin{itemize}
    \item La courbe représentative de cette fonction est une parabole.
    \item Si $a>0$, la parabole est orientée vers le haut.
    \item Si $a<0$, la parabole est orientée vers le bas.
    \item L'axe de symétrie de la parabole est l'axe des ordonnées.
    \item Le sommet de la parabole est l'origine du repère.
  \end{itemize}
\end{propriete}

\begin{exemple}
  Voici les représentations graphiques des fonctions $f\,:\,x\longmapsto \frac{3}{4}x^2$ et $g\,:\,x\longmapsto -2x^2$.
  \begin{multicols}{2}
    \begin{center}
      \begin{tikzpicture}[declare function={f1(\x) = (\x)^2*0.75;}]
        \begin{axis}[%
            axis y line=center,axis x line=middle,        % centrer les axes
            x=1cm,y=1cm,                                  % unités sur les axes
            xmin=-3.2,xmax=3.2,ymin=-0.4,ymax=5.9,        % bornes du repère
            xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
            minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
            grid=both,                                    % indiquer quelle geilles afficher
            tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
          ]
          \addplot[mark=none,colorSec,very thick,domain=-3:3,samples=150] {f1(x)};
          \addplot[mark=none,only marks,samples at={-2.5},colorSec,nodes near coords=$\mathcal{C}_f$,nodes near coords style={anchor={west},font={\large}}] {f1(x)};
        \end{axis}
      \end{tikzpicture}
    \end{center}

    \begin{center}
      \begin{tikzpicture}[declare function={f1(\x) = (\x)^2*(-2);}]
        \begin{axis}[%
            axis y line=center,axis x line=middle,        % centrer les axes
            x=1cm,y=1cm,                                  % unités sur les axes
            xmin=-3.2,xmax=3.2,ymin=-5.9,ymax=0.4,        % bornes du repère
            xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
            minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
            grid=both,                                    % indiquer quelle geilles afficher
            tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
          ]
          \addplot[mark=none,colorTer,very thick,domain=-3:3,samples=150] {f1(x)};
          \addplot[mark=none,only marks,samples at={1.5},colorTer,nodes near coords=$\mathcal{C}_g$,nodes near coords style={anchor={west},font={\large}}] {f1(x)};
        \end{axis}
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{exemple}

\begin{methode}[Déterminer la valeur de $a$ connaissant la courbe représentative de $x\longmapsto ax^2$]
  \textbf{Énoncé}
  \begin{multicols}{2}
  On donne ci-contre la représentation graphique d'une fonction $f$.
  \medskip

  Déterminer l'expression de $f(x)$.
  \vspace*{\fill}
  \columnbreak

  \begin{center}
    \begin{tikzpicture}[declare function={f1(\x) = (\x)^2*(-1/3);}]
      \begin{axis}[%
          axis y line=center,axis x line=middle,        % centrer les axes
          x=0.6cm,y=0.6cm,                                  % unités sur les axes
          xmin=-3.4,xmax=3.4,ymin=-3.9,ymax=0.4,        % bornes du repère
          xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
          minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
          grid=both,                                    % indiquer quelle geilles afficher
          tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
        ]
        \addplot[mark=none,colorSec,very thick,domain=-3.4:3.4,samples=150] {f1(x)};
        \addplot[mark=none,only marks,samples at={-3},colorSec,nodes near coords=$\mathcal{C}_f$,nodes near coords style={anchor={west},font={\large}}] {f1(x)};
        \draw [colorTer, very thick, densely dashed] (3, 0) |- (0, -3);
      \end{axis}
    \end{tikzpicture}
  \end{center}
  \end{multicols}

  \textbf{Solution}
  \begin{multicols}{2}
    On remarque que la courbe $\mathcal{C}_f$ est une parabole tournée vers le bas et symétrique par rapport à l'axe des ordonnées.
    De plus, son sommet est l'origine du repère. On en déduit que la fonction $f$ cherchée est de la forme $f\,:\,x\longmapsto ax^2$.
    \vspace*{\fill}
    \columnbreak
  
    On remarque par lecture graphique que $f(3)=-3$. On a donc :
    {\small\begin{align*}
      f(3) & = a \times 3^2 \\
      -3   & = a \times 9   \\
      a    & = \frac{-3}{9} \\
      a    & = -\frac{1}{3}
    \end{align*}}
  \end{multicols}
  La valeur de $a$ est donc $-\frac{1}{3}$ et finalement $f\,:\,x\longmapsto -\frac{1}{3}x^2$.
\end{methode}

\section{Les fonctions $x\longmapsto ax^2+b$}

\begin{propriete}
  Soit $a,b \in \R$ avec $a\neq0$ et la fonction $f$ définie sur \R telle que : \[f(x) = ax^2 + b.\] Alors :
  \begin{itemize}
    \item La courbe représentative de $f$ est une parabole.
    \item Si $a>0$, la parabole est tournée vers le haut et si $a<0$, elle est tournée vers le bas.
    \item L'axe de symétrie de la parabole est l'axe des ordonnées.
    \item Le sommet de la parabole est le point de coordonnées $(0\,;\,b)$.
  \end{itemize}
\end{propriete}

\begin{exemple}
  Voici les représentations graphiques des fonctions $f\,:\,x\longmapsto \frac{1}{2}x^2-4$ et $g\,:\,x\longmapsto -1,5x^2+2$.
  \begin{multicols}{2}
    \begin{center}
      \begin{tikzpicture}[declare function={f1(\x) = (\x)^2*0.5-4;}]
        \begin{axis}[%
            axis y line=center,axis x line=middle,        % centrer les axes
            x=1cm,y=1cm,                                  % unités sur les axes
            xmin=-3.2,xmax=3.2,ymin=-5.4,ymax=1.2,        % bornes du repère
            xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
            minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
            grid=both,                                    % indiquer quelle geilles afficher
            tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
          ]
          \addplot[mark=none,colorSec,very thick,domain=-3.2:3.2,samples=150] {f1(x)};
          \addplot[mark=none,only marks,samples at={-2.5},colorSec,nodes near coords=$\mathcal{C}_f$,nodes near coords style={anchor={west},font={\large}}] {f1(x)};
        \end{axis}
      \end{tikzpicture}
    \end{center}

    \begin{center}
      \begin{tikzpicture}[declare function={f1(\x) = (\x)^2*(-1.5)+2;}]
        \begin{axis}[%
            axis y line=center,axis x line=middle,        % centrer les axes
            x=1cm,y=1cm,                                  % unités sur les axes
            xmin=-3.2,xmax=3.2,ymin=-3.5,ymax=2.4,        % bornes du repère
            xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
            minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
            grid=both,                                    % indiquer quelle geilles afficher
            tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
          ]
          \addplot[mark=none,colorTer,very thick,domain=-3:3,samples=150] {f1(x)};
          \addplot[mark=none,only marks,samples at={1.5},colorTer,nodes near coords=$\mathcal{C}_g$,nodes near coords style={anchor={west},font={\large}}] {f1(x)};
        \end{axis}
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{exemple}

\begin{methode}[Déterminer les valeurs de $a$ et $b$ connaissant la courbe représentative de $x\longmapsto ax^2+b$]
  \textbf{Énoncé}
  \begin{multicols}{2}
    On donne ci-contre la représentation graphique d'une fonction $h$.
    \begin{center}
      \begin{tikzpicture}[declare function={f1(\x) = (\x)^2*0.5-2;}]
        \begin{axis}[%
            axis y line=center,axis x line=middle,        % centrer les axes
            x=0.6cm,y=0.6cm,                                  % unités sur les axes
            xmin=-3.4,xmax=3.4,ymin=-2.7,ymax=2.4,        % bornes du repère
            xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
            minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
            grid=both,                                    % indiquer quelle geilles afficher
            tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
          ]
          \addplot[mark=none,colorSec,very thick,domain=-3.4:3.4,samples=150] {f1(x)};
          \addplot[mark=none,only marks,samples at={-2.5},colorSec,nodes near coords=$\mathcal{C}_h$,nodes near coords style={anchor={west},font={\large}}] {f1(x)};
        \end{axis}
      \end{tikzpicture}
    \end{center}
  \end{multicols}

  \textbf{Solution}\quad On remarque que la courbe $\mathcal{C}_f$ est une parabole tournée vers le bas et symétrique par rapport à l'axe des ordonnées.
  \begin{multicols}{2}
    De plus, son sommet est le point de coordonnées $(0\,;\,-2)$. On en déduit que la fonction $h$ cherchée est de la forme $h\,:\,x\longmapsto ax^2 + b$ avec $b=-2$.
    \medskip
  
    On remarque par lecture graphique que la courbe $\mathcal{C}_f$ passe par le point de coordonnées $(2\,;\,0)$.
    \vspace*{\fill}
    \columnbreak

    On en déduit que $h(2)=0$ et il vient :
    \begin{align*}
      h(2) & = a\times2^2 + b \\
      0    & = 4a - 2         \\
      4a   & = 2              \\
      a    & = \frac{1}{2}
    \end{align*}
  \end{multicols}
  La valeur de $a$ est donc $-\frac{1}{2}$ et finalement $h\,:\,x\longmapsto \frac{1}{2}x^2-2$.
\end{methode}

\section{Les fonctions $x\longmapsto a(x-x_1)(x-x_2)$}

\subsection{Notion de racine d'un polynôme}

\begin{definition}\index{Polynôme!Racine}\index{Racine}
  Soit $p$ une fonction polynôme définie sur \R et $x_0$ un réel.

  On dit que $x_0$ est une \textbf{racine} de $p$ si $p(x_0)=0$.
\end{definition}

\begin{exemple}
  Soit $f$ la fonction polynôme de degré 2 définie sur \R par $f\,:\,x\longmapsto x^2-4$.


  \begin{multicols}{2}
    On a :
    \begin{align*}
      f(-2) & = (-2)^2 - 4 \\
            & = 4 - 4      \\
            & = 0
    \end{align*}

    On en déduit que $-2$ est une \textit{racine} de la fonction $f$.

    De même, on a :
    \begin{align*}
      f(2) & = (2)^2 - 4 \\
           & = 4 - 4     \\
           & = 0
    \end{align*}

    On en déduit que $2$ est une autre \textit{racine} de la fonction $f$.
  \end{multicols}
\end{exemple}

\subsection{Version factorisée d'un polynôme de degré 2}

\begin{propriete}
  Soit $a\in\R$ avec $a\neq0$. Soient $x_1$ et $x_2$ deux nombres réels. 
  
  Soit $f$ la fonction polynôme de degré 2 définie sur \R par :
  \[f(x)=a(x-x_1)(x-x_2)\]
  On peut alors affirmer :
  \begin{itemize}
    \item La courbe représentative de $f$ est une parabole.
    \item Cette parabole est tournée vers le haut si $a>0$, et vers le bas si $a<0$.
    \item Cette parabole est symétrique par rapport à la droite d'équation $x=\frac{x_1+x_2}{2}$.
    \item Le sommet de cette parabole est le point d'abscisse $\frac{x_1+x_2}{2}$.
    \item Cette parabole coupe l'axe des abscisses aux points de coordonnées $(x_1\,;\,0)$ et $(x_2\,;\,0)$.
    \item Les nombres $x_1$ et $x_2$ sont les deux seules racines de la fonction $f$.
  \end{itemize}
\end{propriete}

\begin{methode}[Déterminer l'expression factorisée d'un polynôme de degré 2 connaissant sa courbe représentative]
  \textbf{Énoncé} :
  
  \begin{multicols}{2}
    \begin{center}
      \begin{tikzpicture}[declare function={f1(\x) = 0.5*(\x-1)*(\x+3);}]
        \begin{axis}[%
          axis y line=center,axis x line=middle,        % centrer les axes
          x=0.6cm,y=0.6cm,                                  % unités sur les axes
          xmin=-5.4,xmax=2.4,ymin=-2.7,ymax=2.4,        % bornes du repère
          xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
          minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
          grid=both,                                    % indiquer quelle geilles afficher
          tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
          ]
          \addplot[mark=none,colorSec,very thick,domain=-5.4:2.4,samples=150] {f1(x)};
          \addplot[mark=none,only marks,samples at={-2.5},colorSec,nodes near coords=$\mathcal{C}_f$,nodes near coords style={anchor={east},font={\large}}] {f1(x)};
          \draw[colorTer,very thick,densely dashed] (-1,-2.7) -- (-1,2.4);
        \end{axis}
      \end{tikzpicture}
    \end{center}
    Soit $f$ la fonction représentée ci-contre.
    \medskip

    Déterminer l'expression factorisée de $f(x)$.
  \end{multicols}
  
  \textbf{Solution}
  \begin{multicols}{2}
   On remarque que la courbe est une parabole et qu'elle coupe l'axe des abscisses en deux points
  de coordonnées $(-3\,;\,0)$ et $(1\,;\,0)$.
  
    On peut donc dire que $f(x)$ va s'écrire \[f(x)=a(x+3)(x-1)\] où $a$ est un réel à déterminer.
\columnbreak

    On remarque que le sommet de la courbe est le point de coordonnées $(-1\,;\,-2)$. Il vient donc :
    {\small\begin{align*}
      f(-1)         & = -2          \\
      a(-1+3)(-1-1) & = -2          \\
      a(2)(-2)      & = -2          \\
      -4a           & = -2          \\
      a             & = \frac{1}{2}
    \end{align*}}
    On en déduit  $f\,:\,x\longmapsto\frac{1}{2}(x+3)(x-1)$.
  \end{multicols}

\end{methode}

\begin{remarque}
  Les deux racines de la fonction $f$ ci-dessus sont $-3$ et $1$. On a $\frac{-3+1}{2}=-1$.
  
  On peut remarquer que la courbe représentative de $f$ admet bien la droite d'équation $x=-1$ comme axe de symétrie.

  De plus, le point $(-1\,;\,-2)$ est bien le sommet de la parabole.
\end{remarque}

\begin{methode}[Déterminer la deuxième racine d'un polynôme de degré 2 lorsqu'on en connait déjà une]
  \textbf{Énoncé}\quad Soit le polynôme de degré 2 défini par $f(x)=2x^2+2x-24$.
  \begin{enumerate}
    \item Montrer que $\alpha=3$ est une racine de cette fonction $f$.
    \item Déterminer la deuxième racine.
  \end{enumerate}

  \textbf{Solution}
  \begin{enumerate}
    \item On a :
          \begin{align*}
            f(\alpha) & =2\alpha^2+2\alpha-24        \\
                      & = 2\times3^2 + 2\times3 - 24 \\
                      & = 2\times9 + 6 - 24          \\
                      & = 18 + 6 - 24                \\
                      & = 0
          \end{align*}
          On en déduit que $\alpha=3$ est bien une racine de la fonction $f$.
    \item Soit $\beta$ la deuxième racine. On a $f(x)=2x^2+2x-24$ donc $a=2$ et $f(x)$ peut s'écrire comme suit :
          \begin{align*}
            f(x) & = 2(x-3)(x-\beta)               &  & \longleftarrow \text{double développement} \\
                 & = 2(x^2 - 3x-\beta x + 3\beta)  &  & \longleftarrow \text{on distribue }2       \\
                 & = 2x^2 - 6x - 2\beta x + 6\beta                                                 \\
                 & = 2x^2 - (6 + 2\beta)x + 6\beta                                                 \\
          \end{align*}
          On sait aussi que $f(x)=2x^2+2x-24$. En identifiant les coefficients, on obtient :
          \begin{align*}
            6\beta & = -24           \\
            \beta  & = \frac{-24}{6} \\
            \beta  & = -4
          \end{align*}
          On en déduit que $-4$ est une racine de la fonction $f$ et que $f(x)$ peut s'écrire :
          \[f(x) = 2(x-3)(x+4)\]
  \end{enumerate}
\end{methode}

\subsection{Signe}

\begin{propriete}
  Soit $a, x_1, x_2 \in \R$ avec $a$ non nul et $x_1<x_2$. Soit $f$ la fonction définie sur $\R$ par :
  \[f\,:\,x\longmapsto a(x-x_1)(x-x_2)\]
  \begin{itemize}
    \item Pour tout $x\in\interof{-\infty{} x_1} \cup \interfo{x_2 +\infty}$, on a $f(x)$ et $a$ qui sont du même signe.
    \item Pour tout $x\in\interff{x_1 x_2}$, on a $f(x)$ et $a$ qui sont de signes opposés.
  \end{itemize}
  \begin{multicols}{2}
    \begin{center}
      {\large$a>0$}
      \medskip

      {\scriptsize
      \begin{tikzpicture}
        \tkzTabInit[lgt=2.5,espcl=1,deltacl=0.5]{%
          $x$ / 0.7,%
          Signe de $a\left(x - x_1\right)\left(x-x_2\right)$ / 1.5}%
        {$-\infty$, $x_1$, $x_2$, $+\infty$}
        \tkzTabLine{ , + , z , - , z , + ,}
      \end{tikzpicture}}
    \end{center}

    \begin{center}
      {\large$a<0$}
      \medskip
      
      {\scriptsize
      \begin{tikzpicture}
        \tkzTabInit[lgt=2.5,espcl=1,deltacl=0.5]{%
        $x$ / 0.7,%
          Signe de $a\left(x - x_1\right)\left(x-x_2\right)$ / 1.5}%
        {$-\infty$, $x_1$, $x_2$, $+\infty$}
        \tkzTabLine{ , - , z , + , z , - ,}
      \end{tikzpicture}}
    \end{center}
  \end{multicols}
\faHandPointRight[regular]\quad On dit que $f(x)$ est du signe de $a$ à l'extérieur des racines.
\end{propriete}

\begin{exemple}
  % On a représenté ci-dessous deux polynômes de degré 2 dans leur formes factorisées.
  \begin{multicols}{2}
    Dans ce premier cas, les racines sont $-3$ et $1$ et $a=0,4$.
    \begin{center}
      \begin{tikzpicture}[declare function={f1(\x) = 0.4*(\x+3)*(\x-1);},%
        signe/.style={draw,circle,inner sep=1pt,fill opacity=0.5,anchor={east},font={\large}}]
        \begin{axis}[%
          axis y line=center,axis x line=middle,        % centrer les axes
          x=0.6cm,y=0.6cm,                                  % unités sur les axes
          xmin=-5.4,xmax=3.2,ymin=-1.7,ymax=2.4,        % bornes du repère
          xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
          minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
          grid=both,                                    % indiquer quelle geilles afficher
          tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
          ]
          \addplot[mark=none,colorPrim,very thick,domain=-5.4:2.4,samples=150] {f1(x)};
          \addplot[mark=none,only marks,samples at={-3.5},colorSec,nodes near coords=\faPlus,nodes near coords style={fill=colorSec!10,signe,xshift={-1mm}}] {f1(x)};
          \addplot[mark=none,only marks,samples at={1.5},colorSec,nodes near coords=\faPlus,nodes near coords style={fill=colorSec!10,signe,anchor={west},xshift={1mm}}] {f1(x)};
          \addplot[mark=none,only marks,samples at={-1},colorTer,nodes near coords=\faMinus,nodes near coords style={fill=colorTer!10,signe,anchor={south},yshift={1mm}}] {f1(x)};
        \end{axis}
      \end{tikzpicture}
    \end{center}

    Dans ce premier cas, les racines sont $-1$ et $2$ et $a=-0,6$.
    \begin{center}
      \begin{tikzpicture}[declare function={f1(\x) = -0.6*(\x+1)*(\x-2);},%
        signe/.style={draw,circle,inner sep=1pt,fill opacity=0.5,anchor={east},font={\large}}]
        \begin{axis}[%
          axis y line=center,axis x line=middle,        % centrer les axes
          x=0.6cm,y=0.6cm,                                  % unités sur les axes
          xmin=-3.8,xmax=4.3,ymin=-2.7,ymax=1.8,        % bornes du repère
          xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
          minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
          grid=both,                                    % indiquer quelle geilles afficher
          tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
          ]
          \addplot[mark=none,colorPrim,very thick,domain=-2.4:3.4,samples=150] {f1(x)};
          \addplot[mark=none,only marks,samples at={-1.8},colorTer,nodes near coords=\faMinus,nodes near coords style={fill=colorTer!10,signe,xshift={-1mm}}] {f1(x)};
          \addplot[mark=none,only marks,samples at={2.8},colorTer,nodes near coords=\faMinus,nodes near coords style={fill=colorTer!10,signe,anchor={west},xshift={1mm}}] {f1(x)};
          \addplot[mark=none,only marks,samples at={0.5},colorSec,nodes near coords=\faPlus,nodes near coords style={fill=colorSec!10,signe,anchor={north},yshift={-1mm}}] {f1(x)};
        \end{axis}
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{exemple}

\subsection{Variations}

\begin{propriete}
  Soit $a, x_1, x_2 \in \R$ avec $a$ non nul et $x_1<x_2$. Soit $f$ la fonction définie sur $\R$ par :
  \[f(x) = a(x-x_1)(x-x_2)\]
  \begin{itemize}
    \item Si $a>0$, alors $f$ est croissante sur $\interof{-\infty{} \frac{x_1+x_2}{2}}$ et décroissante sur $\interfo{\frac{x_1+x_2}{2} +\infty}$.
    \item Si $a<0$, alors $f$ est décroissante sur $\interof{-\infty{} \frac{x_1+x_2}{2}}$ et croissante sur $\interfo{\frac{x_1+x_2}{2} +\infty}$.
  \end{itemize}
  \begin{multicols}{2}
    \begin{center}
      {\large$a<0$}
      \medskip

      {\scriptsize
      \begin{tikzpicture}
        \tkzTabInit[lgt=2.5,espcl=1,deltacl=0.5]{%
          $x$ / 0.7,%
          Variations de $a\left(x - x_1\right)\left(x-x_2\right)$ / 1.5}%
        {$-\infty$, $\frac{x_1+x_2}{2}$, $+\infty$}
      \tkzTabVar{-/, +/, -/}
    \end{tikzpicture}}
    \end{center}

    \begin{center}
      {\large$a>0$}
      \medskip
      
      {\scriptsize
      \begin{tikzpicture}
        \tkzTabInit[lgt=2.5,espcl=1,deltacl=0.5]{%
        $x$ / 0.7,%
          Variations de $a\left(x - x_1\right)\left(x-x_2\right)$ / 1.5}%
        {$-\infty$, $\frac{x_1+x_2}{2}$, $+\infty$}
      \tkzTabVar{+/, -/, +/}
    \end{tikzpicture}}
    \end{center}
  \end{multicols}
\end{propriete}

