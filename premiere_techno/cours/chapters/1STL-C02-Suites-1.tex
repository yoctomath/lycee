% \usetikzlibrary{patterns,patterns.meta}, \usepackage{tdsfrmath}
\chapter{Suites -- Généralités}

\section{Notion de suite}

\begin{definition}\index{Suites!Définition}
  On appelle \textbf{suite} tout procédé de calcul qui, à tout nombre entier naturel $n\in\N$
  associe un nombre réel \emph{unique} noté $u_n$.
  \[\begin{array}{lrcl}
      u\,: & \N & \longrightarrow & \R  \\
           & n  & \longmapsto     & u_n
    \end{array}\]
  Cette suite est notée \suite ou encore \suite* ou plus simplement $u$.
  \medskip

  Les nombres générés par la suite sont appelés des \textbf{termes}.

  On note $u_n$ le terme associé au nombre entier $n$. On dira que c'est le terme de \textbf{rang} $n$.
  \medskip
\end{definition}

\begin{remarque}
  \vspace{-5mm}
  \begin{itemize}
    \item Une suite peut se concevoir comme une liste \textit{rangée et infinie} de nombres réels.
          \begin{center}
            \begin{tikzpicture}[remember picture,scale=0.7]
              \node (u0) {$u_0$};
              \foreach \i  in {1,...,3} {
                  \pgfmathsetmacro{\last}{\i-1}
                  \node[right=4mm of u\last] (u\i) {$u_{\i}$};
                }
              \node[right=4mm of u3] (u4) {$u_{\tikz[remember picture]\node[font=\scriptsize,inner sep=0pt,fill=colorSec!30,circle] (indice4){$4$};}$};

              \foreach \i  in {5,...,9} {
                  \pgfmathsetmacro{\last}{\i-1}
                  \node[right=4mm of u\last] (u\i) {$u_{\i}$};
                }
              \node[right=2mm of u9] (points1) {$\ldots$};
              \node[right=2mm of points1] (un) {$u_n$};
              \node[right=2mm of un] (points2) {$\ldots$};
              \node[below=5mm of u4,font={\lightfont\scriptsize}] (terme1) {terme de rang $4$};
              \node[below=6mm of un,font={\lightfont\scriptsize}] (terme1n) {terme de rang $n$};
              \node[above=5mm of u4,font={\lightfont\scriptsize}] (terme2) {$5$\ieme{} terme};
              \node[above=6mm of un,font={\lightfont\scriptsize}] (terme2n) {$(n+1)$\ieme{} terme};
              \node[below right=4mm of indice4,text=colorSec,font={\lightfont\scriptsize}] (rang) {rang};

              \draw[thin,black!40] (u4) -- (terme1);
              \draw[thin,black!40] (un) -- (terme1n);
              \draw[thin,black!40] (u4) -- (terme2);
              \draw[thin,black!40] (un) -- (terme2n);
              \draw[thin,colorSec!60] (indice4) -- (rang);
            \end{tikzpicture}
          \end{center}
    \item Quelque fois, on fera commencer une suite à un rang différent de zéro.

          \begin{center}
            \begin{tikzpicture}[remember picture,scale=0.7]
              \node (u1) {$u_1$};
              \foreach \i  in {2,...,3} {
                  \pgfmathsetmacro{\last}{\i-1}
                  \node[right=4mm of u\last] (u\i) {$u_{\i}$};
                }
              \node[right=4mm of u3] (u4) {$u_{4}$};

              \foreach \i  in {5,...,9} {
                  \pgfmathsetmacro{\last}{\i-1}
                  \node[right=4mm of u\last] (u\i) {$u_{\i}$};
                }
              \node[right=2mm of u9] (points1) {$\ldots$};
              \node[right=2mm of points1] (un) {$u_n$};
              \node[right=2mm of un] (points2) {$\ldots$};
              \node[below=5mm of u4,font={\lightfont\scriptsize}] (terme1) {terme de rang $4$};
              \node[below=6mm of un,font={\lightfont\scriptsize}] (terme1n) {terme de rang $n$};
              \node[above=5mm of u4,font={\lightfont\scriptsize}] (terme2) {$4$\ieme{} terme};
              \node[above=6mm of un,font={\lightfont\scriptsize}] (terme2n) {$n$\ieme{} terme};

              \draw[thin,black!40] (u4) -- (terme1);
              \draw[thin,black!40] (un) -- (terme1n);
              \draw[thin,black!40] (u4) -- (terme2);
              \draw[thin,black!40] (un) -- (terme2n);
            \end{tikzpicture}
          \end{center}
    \item $u_{n+1}$ est le terme qui suit $u_{n}$.
  \end{itemize}
\end{remarque}

\section{Modes de génération d'une suite}

Il existe trois modes de génération d'une suite :
\begin{enumerate}
  \item en expliquant comment calculer terme~:
        \begin{enumerate}
          \item en utilisant une expression qui ne dépende que du rang de chaque terme~;
          \item en utilisant les termes précédents, de proche en proche ;
        \end{enumerate}
  \item en expliquant le processus d'obtention des termes par une phrase en français.
\end{enumerate}

\subsection{Formulation explicite}

\begin{definition}\index{Suites!Formulation explicite}
  Une suite $u$ est définie de \textbf{manière explicite} lorsque l'on peut exprimer le terme général $u_n$ en fonction de son indice $n$.
\end{definition}

\begin{exemple}\label{pstl:cours:suites1:exemple:pairs}
  Soit \suite[p] la suite des nombres entiers naturels \textit{pairs}. On peut donner l'expression de chaque terme en fonction de son rang $n$~:
  \[p_n = 2n, \text{ pour tout } n\in\N\]
  En effet, les nombres pairs sont les multiples de deux.
\end{exemple}

\begin{remarque}
  Dans le cas d'une formulation explicite du mode de génération de la suite, on peut calculer n'importe quel terme quelque soit son rang.

  Considérons la suite précédente de l'exemple \ref{pstl:cours:suites1:exemple:pairs}. On a $u_4=2\times4$ soit $u_4=8$. En effet, dans la liste des nombres pairs $0$, $2$, $4$, $6$, $8$,\dots on trouve bien le terme de rang 4
  (5\ieme{} terme car on commence avec $0$) qui est $8$.

  Nous n'avons pas eu besoin des termes précédents pour calculer $u_4$.
\end{remarque}

\subsection{Formulation par récurrence}

\begin{definition}\index{Suites!Formulation par récurrence}
  Une suite $u$ est définie \textbf{par récurrence} quand elle est définie par la donnée :
  \begin{itemize}
    \item de son \textbf{terme initial}, généralement $u_0$ ;
    \item d'une relation qui permet de calculer le terme suivant à partir du (ou des) termes précédent(s)..

          Cette relation est appelée \textbf{relation de récurrence}.
  \end{itemize}
\end{definition}

\begin{exemple}\label{pstl:cours:suites1:exemple:recurrence}
  On considère la suite \suite[v] définie par récurrence :
  \[\begin{cases}
      v_0=5                                      \\
      v_{n+1} = 2v_n+3\text{ pour tout } n\in \N \\
    \end{cases}\]
  On a $v_0=5$ qui est donné (terme initial). Grâce à la relation de récurrence, on a les autres termes de proche en proche :


  \begin{align*}
    v_1 & = 2v_0+3                                                           & v_2 & = 2\tikz[baseline, anchor=base,remember picture] \node (v12) {$v_1$};+3 & v_3 & = 2\tikz[baseline, anchor=base,remember picture] \node (v22) {$v_2$};+3 & v_4 & = 2\tikz[baseline, anchor=base,remember picture] \node (v32) {$v_3$};+3 \\
    v_1 & = 2 \times 5 +3                                                    & v_2 & =2 \times 13 +3                                                         & v_3 & =2 \times 29 +3                                                         & v_4 & =2 \times 61 +3                                                         \\
    v_1 & = 10+3                                                             & v_2 & = 26+3                                                                  & v_3 & = 58+3                                                                  & v_4 & = 122+3                                                                 \\
    v_1 & =\tikz[baseline, anchor=base,remember picture] \node (v11) {$13$}; & v_2 & = \tikz[baseline, anchor=base,remember picture] \node (v21) {$29$};     & v_3 & = \tikz[baseline, anchor=base,remember picture] \node (v31) {$61$};     & v_4 & = 125;                                                                  \\
  \end{align*}
  \begin{tikzpicture}[remember picture,overlay]
    \foreach \i in {1,2,3} {
        \coordinate (vm\i) at ($(v\i 1)!0.5!(v\i 2)$);
        \draw[thin,black!60] (v\i 1) to[out=0,in=-90] (vm\i);
        \draw[thin,black!60,-latex'] (vm\i) to[out=90,in=90,out looseness=1.5] (v\i 2);

      }
  \end{tikzpicture}
\end{exemple}

\begin{remarque}
  Pour ce type de suite, on ne peut pas calculer directement n'importe quel terme.

  En effet, comme on peut le voir dans l'exemple \ref{pstl:cours:suites1:exemple:recurrence} ci-dessus, accéder à la valeur de $u_4$ par exemple, il nous faudra connaître tous les termes jusqu'à $u_3$.
\end{remarque}


\subsection{Formulation en français}

Parfois, il n'existe pas de formule ni de processus de proche en proche pour expliquer comment générer les termes d'une suite.
Dans ce cas, on peut utiliser une formulation en français du mode de génération de la suite.

\begin{exemple}
  La suite \suite est définie en attribuant à chaque terme le nombre de lettres contenus dans le nombre correspondant à son rang.

  Ainsi, on aura $u_0=4$ car il y a quatre lettres dans le mot \textcolor{colorPrim}{\texttt{zéro}}, $u_1=2$ (le mot \textcolor{colorPrim}{\texttt{un}} compte deux lettres),
  $u_2=4$, $u_3=5$, $u_4=6$, $u_5=4$, etc.

  On comprend bien que dans ce cas, on ne peut pas donner d'expression générale pour le terme$u_n$ de rang $n$.
\end{exemple}

\section{Représentation graphique}

\begin{definition}\index{Suites!Représentation graphique}
  Dans un repère \repere, la \textbf{représentation graphique} d'une suite $u$ est l'ensemble des points $M_n$ de coordonnées $M_n\nuplet{n u_n}$ pour tout $n\in \N$.
\end{definition}

\begin{remarque}
  Contrairement à la représentation graphique d'une fonction, celle d'une suite n'est pas une courbe mais un \textbf{nuage de points}.
\end{remarque}

\begin{exemple}\label{pstl:cours:suites1:exemple:graphique}
  Soit la suite \suite définie par $u_n=\frac{1}{2}n-3$ pour tout $n\in \N$.

  \begin{multicols}{2}
    On a la tableau des valeurs suivant

    \begin{center}
      \begin{tblr}{width=\linewidth,colspec={X[l,1]*{8}{X[c,1]}},hlines,vlines,stretch=1.4,rows={mode=math,font=\scriptsize},column{1}={bg=colorPrim!20}}
        n   & 0  & 2    & 3  & 4    & 5  & 6    & 7 & 8   \\
        u_n & -3 & -2,5 & -2 & -1,5 & -1 & -0,5 & 0 & 0,5 \\
      \end{tblr}
    \end{center}

    qui nous permet de construire la représentation graphique de la suite \suite ci-contre.
    \vspace*{\fill}

    \begin{center}
      \begin{tikzpicture}[scale=0.7]
        \begin{axis}[axis y line=center,%
            axis x line=middle,x=1cm,y=1cm,%
            xmin=-0.8,xmax=8.8,ymin=-3.7,ymax=1.7,%
            xtick distance=1,ytick distance =1,grid=both,%
            xticklabels=\empy, yticklabels=\empty]
          \addplot+[domain=0:7,only marks,colorPrim,mark=+,mark size=3pt,ultra thick,samples=8] {0.5*x-3} node [pin=90:{$M_8\nuplet{8 u_8}$}]{};
          \draw[very thick,-latex'] (0,0) coordinate[label=below left:$O$] (O) -- (1, 0) node[below,midway] {$\vecti$};
          \draw[very thick,-latex'] (0,0) -- (0, 1) node[left,midway] {$\vectj$} ;
        \end{axis}
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{exemple}



\section{Variations}

\begin{definition}[Sens de variation d'une suite]\index{Suites!Sens de variation}\label{pstl:cours:suites1:def:variation}
  Pour tout entier naturel $n$,
  \begin{itemize}
    \item Une suite $u$ est \textbf{croissante} si  $n$: $u_n\ppq u_{n+1}$ ;
    \item Une suite $u$ est \textbf{décroissante} si  $n$: $u_n\pgq u_{n+1}$ ;
    \item Une suite $u$ est \textbf{constante} si  $n$: $u_{n+1}=u_n$ ;
    \item Une suite $u$ est \textbf{monotone} si elle est croissante ou décroissante.
  \end{itemize}
\end{definition}

\begin{remarque}
  Toutes les suites ne sont pas croissantes ou décroissantes.

  Par exemple, la suite \suite définie par $u_n=(-1)^n$ pour tout $n\in\N$.
\end{remarque}

\begin{methode}[Déterminer le sens de variation d'une suite]
  Pour déterminer le sens de variation d'une suite, on peut utiliser les techniques exposées ci-après. 
  
  Il faut vérifier la relation pour tout $n\in\N$.
  \medskip

  \begin{tblr}{width=\linewidth,colspec={X[c,1]*{3}{X[c,1]}},hlines,vlines,stretch=2,row{1}={bg=colorTer!20,font={\bfseries}},column{1}={bg=colorTer!20,font={\bfseries}}}
    Sens de variation & Définition    & Étude de $u_{n+1}-u_n$ & Étude\footnotemark{} de $\frac{u_{n+1}}{u_n}$ \\
    Croissante        & $u_n\ppq u_{n+1}$ & $u_{n+1}-u_n\pgq0$        & $\frac{u_{n+1}}{u_n}\pgq1$                       \\
    Décroissante      & $u_n\pgq u_{n+1}$ & $u_{n+1}-u_n\ppq0$        & $\frac{u_{n+1}}{u_n}\ppq1$                       \\
    Constante         & $u_n=u_{n+1}$ & $u_{n+1}-u_n=0$        & $\frac{u_{n+1}}{u_n}=1$                       \\
  \end{tblr}
\end{methode}
\footnotetext{Ce cas est particulier, il faut donc vérifier si $u_n\neq0$.}

\begin{exemple}
  Examinons la suite \suite de l'exemple \ref{pstl:cours:suites1:exemple:graphique}. On a $u_n=\frac{1}{2}n-3$ pour tout $n\in \N$.

  Étudions le signe de $u_{n+1}-u_n$~:
  \begin{align*}
    u_{n+1}-u_n &= \frac{1}{2}(n+1)-3-\left(\frac{1}{2}n-3\right)\\
    &= \cancel{\frac{n}{2}}+\frac{1}{2}-\cancel{3}-\cancel{\frac{n}{2}}+\cancel{3}\\
    &= \frac{1}{2}
  \end{align*}

  On a $\frac{1}{2}>0$ donc on a prouvé que $u_{n+1}-u_n\pgq0$. D'après la définition \ref{pstl:cours:suites1:def:variation}, on en conclut que la suite \suite est croissante.
\end{exemple}