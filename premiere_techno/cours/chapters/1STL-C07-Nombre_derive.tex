\chapter{Nombre dérivé}

\section{Taux de variation}

\subsection{Sécante à une courbe}

\begin{definition}\index{Courbe!Sécante}\index{Sécante}

  \begin{multicols}{2}
    Soit une fonction $f$ définie sur un intervalle $\mathtt{I}$
    et $\mathcal{C}_f$ sa courbe représentative sur $\mathtt{I}$.

    On appelle \textbf{sécante} à la courbe $\mathcal{C}_f$
    toute droite définie par deux points distincts de $\mathcal{C}_f$.
    \vspace*{\fill}

    \begin{center}
      \begin{tikzpicture}[declare function={f(\x)=0.3*e^\x+0.1;},%
          scale=0.7,%
          every node/.append style={font=\scriptsize}]
        \begin{axis}[%
            axis y line=center,axis x line=middle,        % centrer les axes
            x=1.5cm,y=1.5cm,                                  % unités sur les axes
            xmin=-0.4,xmax=2.4,ymin=-0.4,ymax=2.4,        % bornes du repère
            % xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
            xticklabels=\empty,yticklabels=\empty
          ]
          \node[below left] at (0,0) {$O$};
          \addplot[domain=-0.5:2,samples=150,very thick,colorPrim] {f(x)};
          \addplot[mark=+,mark size=3pt,very thick,only marks,samples at={0.5},colorSec,nodes near coords=$A$,nodes near coords style={anchor={south east}}] {f(x)} coordinate (A);
          \addplot[mark=+,mark size=3pt,very thick,only marks,samples at={1.8},colorSec,nodes near coords=$B$,nodes near coords style={anchor={south east}}] {f(x)} coordinate (B);
          \addplot[mark=none,very thick,only marks,samples at={-0.4},colorPrim,nodes near coords=$\mathcal{C}_f$,nodes near coords style={anchor={south}}] {f(x)};
          \draw[thick,colorSec,shorten >=-12mm,shorten <=-12mm] (A) -- (B) node[midway,above,sloped,font=\lightfont\scriptsize] {sécante};
        \end{axis}
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{definition}

\subsection{Taux de variation}

\begin{propriete}\index{Fonctions!Taux de variation}
  \begin{multicols}{2}
    Soit une fonction $f$ définie sur un intervalle $\mathtt{I}$
    et $\mathcal{C}_f$ sa courbe représentative sur $\mathtt{I}$.

    On écrit $x_M=a+h$ où $h$ est l'écart horizontal entre $A$ et $M$.

    Le coefficient directeur de la sécante $(AM)$ à la courbe $\mathcal C_f$
    est le \textbf{taux de variation} en $a$ de la fonction~$f$ :
    \[\frac{y_M-y_A}{x_M-x_A} = \frac{f(a+h) - f(a)}{h}\]

    \vspace*{\fill}

    \begin{center}
      \begin{tikzpicture}[declare function={f(\x)=0.3*e^\x+0.1;},%
          scale=0.7,%
          every node/.append style={font=\scriptsize}]
        \begin{axis}[%
            clip mode=individual,
            clip=false,
            axis y line=center,axis x line=middle,        % centrer les axes
            x=1.5cm,y=1.5cm,                                  % unités sur les axes
            xmin=-0.8,xmax=2.4,ymin=-0.4,ymax=2.4,        % bornes du repère
            % xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
            xticklabels=\empty,yticklabels=\empty
          ]
          \coordinate[label=below left:$O$] (O) at (0, 0);
          \addplot[domain=-0.5:2,samples=150,very thick,colorPrim] {f(x)};
          \addplot[mark=none,very thick,only marks,samples at={0.5},colorSec,nodes near coords=$A$,nodes near coords style={anchor={south east}}] {f(x)} coordinate (A);
          \addplot[mark=none,very thick,only marks,samples at={1.8},colorSec,nodes near coords=$M$,nodes near coords style={anchor={south east}}] {f(x)} coordinate (M);
          \addplot[mark=none,very thick,only marks,samples at={-0.5},colorPrim,nodes near coords=$\mathcal{C}_f$,nodes near coords style={anchor={east}}] {f(x)};
          \draw[thick,colorSec,shorten >=-12mm,shorten <=-12mm] (A) -- (M);
          \draw[densely dashed] (A) -- ($(O)!(A)!(0,1)$) node[left] {$f(a)$};
          \draw[densely dashed] (M) -- ($(O)!(M)!(0,1)$) node[left] {$f(a+h)$};
          \draw[densely dashed] (A) -- ($(O)!(A)!(1,0)$) node[below] {$a$};
          \draw[densely dashed] (M) -- ($(O)!(M)!(1,0)$) coordinate (Mx) node[below] {$a+h$};
          \draw[thick,colorTer,-latex'] (A) -- ($(Mx)!(A)!(M)$) coordinate (inter) node[midway,below] {$h$};
          \draw[thick,colorTer,-latex'] (inter) -- (M) node[midway,right] {$f(a+h) - f(a)$};
        \end{axis}
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{propriete}

\section{Nombre dérivé}

\begin{definition}\index{Nombre dérivé}
  Soit une fonction $f$ définie sur un intervalle $\mathtt{I}$ et $a\in\mathtt{I}$ un réel.

  La limite lorsque $h$ tend vers $0$ du taux de variation de la fonction $f$ en $a$ est appelé \textbf{nombre dérivé} de $f$ en $a$.

  Le nombre dérivé de la fonction $f$ en $a$ se note $f'(a)$.
  \[f'(a) = \lim \limits_{h\to 0} \frac{f(a+h) - f(a)}{h}\]
\end{definition}
\newpage
\begin{methode}[Déterminer par le calcul le nombre dérivé]
  \textbf{Énoncé}\quad Soit la fonction $f$ définie sur \R par \[f(x)=x^2-3x+7\]
  Déterminer le nombre dérivé de $f$ en $1$
  \bigskip

  \textbf{Solution}\quad Appelons $\tau(h)$ le taux de variation de $f$ en $1$. On a :
  \begin{align*}
    \tau(h) & =\frac{f(1+h) - f(1)}{h}                                  \\
    \tau(h) & =\frac{(1+h)^2-3(1+h)+7 - \left(1^2-3\times1+7\right)}{h} \\
    \tau(h) & =\frac{h^2+2h+1-3-3h+7 - 5}{h}                            \\
    \tau(h) & =\frac{h^2-h}{h}                                          \\
    \tau(h) & =h-1                                                      \\
  \end{align*}
  On fait tendre $h$ vers $0$ pour déterminer le nombre dérivé en $1$.
  \begin{align*}
    f'(1) & = \lim\limits_{h\to0} \tau(h) \\
    f'(1) & = \lim\limits_{h\to0} h-1     \\
    f'(1) & = -1                          \\
  \end{align*}
  Le nombre dérivé de $f$ en $1$ vaut $-1$.
\end{methode}


\section{Tangente à une courbe}

\begin{definition}\index{Courbe!Tangente}\index{Tangente}
  Soit une fonction $f$ définie sur un intervalle $\mathtt{I}$
  et $(AM)$ un sécante à sa courbe représentative $\mathcal{C}_f$.
  \medskip

  Lors que le point $M$ se rapproche du point $A$, la position \textit{limite}
  de la droite $(AM)$ définie une droite appelée \textbf{tangente} à la courbe
  $\mathcal{C}_f$  au point $A$.

  \begin{tblr}{width=\linewidth,colspec={X[c,2]X[c,1]X[c,2]X[c,1]X[c,2]},stretch=2,rows={valign={m}}}
    \begin{tikzpicture}[baseline,declare function={f(\x)=0.3*e^\x+0.1;},%
        scale=0.7,%
        every node/.append style={font=\scriptsize}]
      \begin{axis}[%
          axis y line=center,axis x line=middle,        % centrer les axes
          x=1.5cm,y=1.5cm,                                  % unités sur les axes
          xmin=-0.4,xmax=2.4,ymin=-0.4,ymax=2.4,        % bornes du repère
          % xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
          xticklabels=\empty,yticklabels=\empty
        ]
        \node[below left] at (0,0) {$O$};
        \addplot[domain=-0.5:2,samples=150,very thick,colorPrim] {f(x)};
        \addplot[mark=+,mark size=3pt,very thick,only marks,samples at={0.5},colorSec,nodes near coords=$A$,nodes near coords style={anchor={south east}}] {f(x)} coordinate (A);
        \addplot[mark=+,mark size=3pt,very thick,only marks,samples at={1.8},colorSec,nodes near coords=$M$,nodes near coords style={anchor={south east}}] {f(x)} coordinate (M);
        \addplot[mark=none,very thick,only marks,samples at={-0.4},colorPrim,nodes near coords=$\mathcal{C}_f$,nodes near coords style={anchor={south}}] {f(x)};
        \draw[thick,colorSec,shorten >=-12mm,shorten <=-12mm] (A) -- (M);
      \end{axis}
    \end{tikzpicture}
     &
    \begin{tikzpicture}[baseline=(b.base)]
      \node[single arrow,draw=none,fill=colorPrim!20] (f) {\phantom{bla}};
      \node[below=1cm of f] (b) {};
    \end{tikzpicture}
     &
    \begin{tikzpicture}[baseline,declare function={f(\x)=0.3*e^\x+0.1;},%
        scale=0.7,%
        every node/.append style={font=\scriptsize}]
      \begin{axis}[%
          axis y line=center,axis x line=middle,        % centrer les axes
          x=1.5cm,y=1.5cm,                                  % unités sur les axes
          xmin=-0.4,xmax=2.4,ymin=-0.4,ymax=2.4,        % bornes du repère
          % xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
          xticklabels=\empty,yticklabels=\empty
        ]
        \node[below left] at (0,0) {$O$};
        \addplot[domain=-0.5:2,samples=150,very thick,colorPrim] {f(x)};
        \addplot[mark=+,mark size=3pt,very thick,only marks,samples at={0.5},colorSec,nodes near coords=$A$,nodes near coords style={anchor={south east}}] {f(x)} coordinate (A);
        \addplot[mark=+,mark size=3pt,very thick,only marks,samples at={1},colorSec,nodes near coords=$M$,nodes near coords style={anchor={south east}}] {f(x)} coordinate (M);
        \addplot[mark=none,very thick,only marks,samples at={-0.4},colorPrim,nodes near coords=$\mathcal{C}_f$,nodes near coords style={anchor={south}}] {f(x)};
        \draw[thick,colorSec,shorten >=-25mm,shorten <=-12mm] (A) -- (M);
      \end{axis}
    \end{tikzpicture}
     &
    \begin{tikzpicture}[baseline=(b.base)]
      \node[single arrow,draw=none,fill=colorPrim!20] (f) {\phantom{bla}};
      \node[below=1cm of f] (b) {};
    \end{tikzpicture}
     &
    \begin{tikzpicture}[baseline,declare function={f(\x)=0.3*e^\x+0.1;},%
        scale=0.7,%
        every node/.append style={font=\scriptsize}]
      \begin{axis}[%
          axis y line=center,axis x line=middle,        % centrer les axes
          x=1.5cm,y=1.5cm,                                  % unités sur les axes
          xmin=-0.4,xmax=2.4,ymin=-0.4,ymax=2.4,        % bornes du repère
          % xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
          xticklabels=\empty,yticklabels=\empty
        ]
        \node[below left] at (0,0) {$O$};
        \addplot[domain=-0.5:2,samples=150,very thick,colorPrim] {f(x)};
        \addplot[domain=-0.5:2,samples=150,very thick,colorSec] {0.3*e^0.5*x+0.1+0.15*e^0.5};
        \addplot[mark=+,mark size=3pt,very thick,only marks,samples at={0.5},colorSec,nodes near coords=$A$,nodes near coords style={anchor={south east}}] {f(x)} coordinate (A);
        % \addplot[mark=+,mark size=3pt,very thick,only marks,samples at={1.8},colorSec,nodes near coords=$B$,nodes near coords style={anchor={south east}}] {f(x)} coordinate (B);
        \addplot[mark=none,very thick,only marks,samples at={-0.4},colorPrim,nodes near coords=$\mathcal{C}_f$,nodes near coords style={anchor={south}}] {f(x)};
        % \draw[thick,colorSec,shorten >=-12mm,shorten <=-12mm] (A) -- (B) node[midway,above,sloped,font=\lightfont\scriptsize] {sécante};
      \end{axis}
    \end{tikzpicture}
  \end{tblr}
\end{definition}

\begin{propriete}\index{Tangente!Équation réduite}
  Soit $f$ une fonction définie sur un intervalle $\mathtt{I}$ et soit $A$ le point de $\mathcal C_f$ d'abscisse $a \in \mathtt{I}$.

  La tangente à la courbe $\mathcal C_f$ au point $A$ est la droite passant par $A$ et de coefficient directeur $f'(a)$.

  Son équation réduite est : \[y = f'(a) \times (x-a) + f(a)\]
\end{propriete}

\begin{methode}[Déterminer graphiquement le nombre dérivé]
  \textbf{Énoncé}\quad On donne $\mathcal{C}_f$ la représentation d'une fonction $f$ sur son intervalle de définition.

  La droite $T$ est tangente à $\mathcal{C}_f$ au point $A$ d'abscisse $-2$
  \begin{center}
    \begin{tikzpicture}[declare function={f(\x) = 0.5*\x^2;}]
      \begin{axis}[%
          axis y line=center,axis x line=middle,        % centrer les axes
          x=1cm,y=1cm,                                  % unités sur les axes
          xmin=-3.4,xmax=3.4,ymin=-2.7,ymax=4.4,        % bornes du repère
          xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
          minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
          grid=both,                                    % indiquer quelle geilles afficher
          tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
        ]
        \addplot[mark=none,colorSec,very thick,domain=-3:3,samples=150] {f(x)};
        \addplot[mark=none,only marks,samples at={2.5},colorSec,nodes near coords=$\mathcal{C}_f$,nodes near coords style={anchor={north west},font={\large}}] {f(x)};
        \addplot[mark=none,colorPrim,very thick,domain=-3.4:3.4,samples=150] {-2*x-2};
        \addplot[mark=none,only marks,samples at={0.25},colorPrim,nodes near coords=$T$,nodes near coords style={anchor={south west},font={\large}}] {-2*x-2};
        \addplot[mark=+,mark size=3pt,only marks,samples at={-2},black,nodes near coords=$A$,nodes near coords style={anchor={south west},font={\large}}] {-2*x-2};
      \end{axis}
    \end{tikzpicture}
  \end{center}
  Déterminer le nombre dérivé en $-2$
  \bigskip


  \begin{multicols}{2}
    \textbf{Solution}\quad Le nombre dérivé de $f$ pour $x=-2$ est égal au coefficient directeur de la tangente à la courbe $\mathcal{C}_f$
    au point $A$ d'abscisse $-2$.

    Il suffit donc de lire sur le graphique la pente de la droite $T$.
    \medskip

    On en déduit que :
    \begin{align*}
      f'(2) & = \frac{\Delta y}{\Delta x} \\
      f'(2) & = \frac{-4}{2}              \\
      f'(2) & = -2
    \end{align*}
    Donc le nombre dérivé de $f$ pour $x=-2$ vaut $-2$ ($f'(2) = -2$).

    \begin{center}
      \begin{tikzpicture}[declare function={f(\x) = 0.5*\x^2;}]
        \begin{axis}[%
            axis y line=center,axis x line=middle,        % centrer les axes
            x=1cm,y=1cm,                                  % unités sur les axes
            xmin=-3.4,xmax=3.4,ymin=-2.7,ymax=4.4,        % bornes du repère
            xtick distance=1, ytick distance=1,           % distance entre les tirets sur les axes
            minor tick num = 1,                           % sous grille suppl'ementaire (indiquer le nombre de trait en plus)
            grid=both,                                    % indiquer quelle geilles afficher
            tick label style={font=\scriptsize,black!50}, % description des labels sur les axes
          ]
          \addplot[mark=none,colorSec,very thick,domain=-3:3,samples=150] {f(x)};
          \addplot[mark=none,only marks,samples at={2.5},colorSec,nodes near coords=$\mathcal{C}_f$,nodes near coords style={anchor={north west},font={\large}}] {f(x)};
          \addplot[mark=none,colorPrim,very thick,domain=-3.4:3.4,samples=150] {-2*x-2};
          \addplot[mark=none,only marks,samples at={0.25},colorPrim,nodes near coords=$T$,nodes near coords style={anchor={south west},font={\large}}] {-2*x-2};
          \addplot[mark=+,mark size=3pt,only marks,samples at={-2},black,nodes near coords=$A$,nodes near coords style={anchor={south west},font={\large}}] {-2*x-2} coordinate (A);
          \draw[ultra thick,colorTer,-latex'] (A) -- (-2,-2) node[midway,sloped,fill=colorTer!80,font=\scriptsize,text=white] {$\Delta y = -4$};
          \draw[ultra thick,colorTer,-latex'] (-2,-2) -- (0,-2) node[midway,sloped,fill=colorTer!80,font=\scriptsize,text=white] {$\Delta x = 2$};
        \end{axis}
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{methode}

\begin{methode}[Déterminer l'équation réduite d'une tangente en un point]
  \textbf{Énoncé}\quad Soit la fonction $f(x) = x^2$ définie sur \R.

  Déterminer l'équation de la tangente $\mathcal{T}$ à la courbe représentative de $f$ au point d'abscisse $2$.
  \bigskip

  \textbf{Solution}\quad On sait que l'équation réduite de $\mathcal{T}$ en $2$ est :
  \[\mathcal{T}: y = f'(2) \times (x-2) + f(2)\]

  Le taux de variation d'une fonction $f$ en $a$ est $\frac{f(a+h) - f(a)}{h}$.

  Calculons le taux de variation $\tau(h)$ pour $a=2$ :
  \begin{align*}
    \tau(h) & = \frac{f(a+h) - f(a)}{h} \\
    \tau(h) & = \frac{(2+h)^2 - 2^2}{h} \\
    \tau(h) & = \frac{h^2+4h+4 - 4}{h}  \\
    \tau(h) & = \frac{h^2+4h}{h}        \\
    \tau(h) & = h+4                     \\
  \end{align*}
  On fait tendre $h$ vers $0$ pour déterminer le nombre dérivé en $2$.
  \begin{align*}
    f'(2) & = \lim\limits_{h\to0} \tau(h) \\
    f'(2) & = \lim\limits_{h\to0} h+4     \\
    f'(2) & = 4                           \\
  \end{align*}
  On a donc \[\mathcal{T}: y = 4 \times (x-2) + f(2)\]
  Calculons $f(2)$.
  \begin{align*}
    f(2) & = 2^2 \\
    f(2) & = 4
  \end{align*}
  Finalement
  \begin{align*}
    \mathcal{T}: y & = 4 \times (x-2) + 4 \\
    \mathcal{T}: y & = 4 x - 8 + 4        \\
    \mathcal{T}: y & = 4 x - 4            \\
  \end{align*}
\end{methode}