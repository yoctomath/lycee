{\Large \lightfont \faBullseye{}\quad OBJECTIF~: Étudier des données chiffrées grâce aux tableaux croisés}
\bigskip

\section*{Avant de commencer\dots}
Récupérer sur l'ENT à la séance du jour le fichier correspondant au parcours choisi~: \verb!1STLTableaux02.ods!.

\section*{Étude}
Le fichier comporte deux feuilles~:
\begin{itemize}
  \item La feuille \verb!data! contient les données brutes de la campagne Parcoursup en France en 2023
  % \footnote{On peut retrouver ces données \href{https://data.enseignementsup-recherche.gouv.fr/api/explore/v2.1/catalog/datasets/fr-esr-parcoursup/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B}{>>>ici<<<}}.
  \item La feuille \verb!traitement! dans laquelle nous allons travailler.  
\end{itemize}
L'idée de cette activité est d'apprendre à extraire certaines de ces données et à les présenter dans un tableau croisé pour que ces données soient plus parlantes.
\bigskip

\faHandPointRight[regular]\quad {\bfseries Nous allons étudier la poursuite d'études des néo bacheliers de la sessions 2023 dans l'Académie de Toulouse.}

\subsection*{Observation}
Dans la feuille \verb!data!~:
\begin{enumerate}
  \item combien y-t-il de lignes~? \pointilles
  \item combien y-t-il de colonnes~? \pointilles
\end{enumerate}

\subsection*{Récupération des formations}
La liste des types de formations (BUT, BTS, etc.) disponible est dans la colonne \texttt{L}.
On va extraire toutes les valeurs distinctes de cette colonne dans la colonne \texttt{A} de la feuille \texttt{traitement}.

\begin{multicols}{2}
  \begin{enumerate}
    \item Dans la feuille \verb!data!, sélectionner la colonne \texttt{L}.
    \item Cliquer sur \menu[,]{Données, Plus de filtres, Filtre standard}.
    \begin{enumerate}
      \item Nom de champ : laisser \texttt{L}
      \item Condition : \texttt{=}
      \item Valeur : \texttt{Non vide}
      \item Options :
      \begin{itemize}
        \item Cocher \texttt{Sans doublons}
        \item Cocher \texttt{Copier le résultat vers} 
        et saisir la première case 
        de la feuille de traitement \verb!$traitement.$A$1!
      \end{itemize}
    \end{enumerate}
  \end{enumerate}
  \begin{center}
    \includegraphics[width=0.9\columnwidth]{1STLActTableaux02_img02.png}
  \end{center}
\end{multicols}

\subsection*{Récupération des valeurs de l'autre caractère}

\begin{multicols}{2}
  \begin{center}
    \includegraphics[width=0.7\columnwidth]{1STLActTableaux02_img01.png}
  \end{center}
  Les valeurs relatives aux néo bachelier se trouve dans les cellules \texttt{BD1} jusqu'à \texttt{BG1} inclus
  dans la feuille \verb!data!.
  \medskip
  
  Recopier simplement le contenu de ces cases dans la feuille \texttt{traitement}
  afin d'elles deviennent les entêtes de votre tableau.
  \medskip
  
  Vous devez obtenir quelque chose qui ressemble au tableau ci-contre.
\end{multicols}

\subsection*{Obtention des effectifs}
\begin{boitetitre}{Fonction \texttt{SOMME.SI.ENS()}}
  Pour faire le total des effectifs correspondants à certains critères dans une plage donnée,
on utilise la fonction 
\medskip

\begin{quote}\large
  \verb!SOMME.SI.ENS(PLAGE_DONNEES;PLAGE_1;"CRITÈRE_1";PLAGE_2;"CRITÈRE_2";...)!
\end{quote}
\medskip

Explication des paramètres de cette fonction~:
\begin{itemize}
  \item \verb!PLAGE_DONNEES! : la plage sur laquelle doivent être additionnés les effectifs
  \item \verb!PLAGE_X! : une plage à évaluer pour le test
  \item \verb!CRITÈRE_X! : description du test pour le critère (il doit être entre guillemets droits \texttt{"\dots"})
\end{itemize}

\textbf{Remarque}\quad
Pour tester si une valeur de la plage est égale au contenu de la cellule \texttt{B3} par exemple,
le critère sera \verb!"="&B3!
\end{boitetitre}

\begin{enumerate}
  \item Dans la cellule \texttt{B2} de la feuille \texttt{traitement}, nous allons calculer le total des effectifs
  des néo bacheliers admis dans la formation désignée dans la cellule \texttt{A2} des formations de l'Académie de Toulouse.

  Utiliser la fonction \verb!SOMME.NB.SI()! en prenant en compte que~:
  \begin{itemize}
    \item les données à analyser sont dans la feuille \verb!data! ~;
    \item les effectifs à additionner sont dans la plage de la colonne \texttt{BD} (combien de lignes dans la feuille déjà ?)~;
    \item les formations sont contenues dans la plage correspondante de la colonne \texttt{L}~;
    \item les académies sont contenues dans la plage correspondante de la colonne \texttt{H}.
  \end{itemize}
  \faHandPointRight[regular]\quad Penser à verrouiller avec le caractère \verb!$!
   les colonnes \texttt{L} et \texttt{H} de votre formule correspondant aux critères de la formation et de l'Académie
   afin de protéger cette formule lors de copies future.
  \item Copier les formules pour remplir toutes les cellules du tableau.
  \item Ajouter une ligne et une colonne pour les \textbf{effectif marginaux}.
\end{enumerate}

\subsection*{Tableaux des fréquences}

Sur la feuille \texttt{traitement}, ajouter trois tableaux afin de faire calculer les fréquences marginales, 
les fréquences conditionnelles en colonnes et les fréquences conditionnelles en ligne.

Pour réaliser ces trois tableaux, on se basera sur le tableau des effectifs de la partie précédente.

\subsection*{Analyse des données}
Répondre aux question posées sur une feuille à part\dots
\begin{enumerate}
  \item Le choix post-bac de la poursuite d’étude est-il impacté par le type de
  baccalauréat obtenu (général, technologique ou professionnel) ?

  \item Proposer une  \textbf{représentation graphique} permettant de visualiser le type de baccalauréat
  obtenu par ces élèves en fonction de la formation choisie.
  \item \begin{enumerate}
    \item Parmi les néo bacheliers, les filières BUT accueillent-elles majoritairement
    des étudiants titulaires d’un baccalauréat technologique ?
    \item Est-il exact que parmi les néo bacheliers généraux, moins d’un sur six
    poursuit en BUT ?
  \end{enumerate}
\end{enumerate}

\subsection*{Prolongement}

Proposer un autre traitement qui croiserait deux autres caractères (par exemple, genres et filières).